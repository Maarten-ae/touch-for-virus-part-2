//
//  VC_Modulation.swift
//  Rage
//
//  Created by M. Lierop on 26-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit


class ModViewController: UIViewController {
    
  
    
    @IBOutlet weak var ModsSubViewContainer: UIView!
    @IBOutlet weak var ModsSubViewSegmentControl: UISegmentedControl!
    
    
    lazy var ModsQuickPresetsSubView: ModsQuickPresetsSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ModsQuickPresetsSubView") as! ModsQuickPresetsSubView
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var ModsLFOSubView: ModsLFOSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ModsLFOSubView") as! ModsLFOSubView
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var ModsEnvelopesSubView: ModsEnvelopesSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ModsEnvelopesSubView") as! ModsEnvelopesSubView
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var ModsMatrixSubView: ModsMatrixSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ModsMatrixSubView") as! ModsMatrixSubView
        self.addChild(viewController)
        return viewController
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        ModsSubViewContainer.addSubview(ModsQuickPresetsSubView.view)
        setupCallbacks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    func setupCallbacks() {
        
       
    }
    
    @IBAction func ModsSubViewControlChange(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 0:
            ModsQuickPresetsSubView.updateView()
            ModsSubViewContainer.addSubview(ModsQuickPresetsSubView.view)
        case 1:
            ModsSubViewContainer.addSubview(ModsLFOSubView.view)
            ModsLFOSubView.updateView()
        case 2:
            ModsSubViewContainer.addSubview(ModsEnvelopesSubView.view)
            ModsEnvelopesSubView.updateView()
        case 3:
            ModsSubViewContainer.addSubview(ModsMatrixSubView.view)
            ModsMatrixSubView.updateView()
        default:
            ModsQuickPresetsSubView.updateView()
            ModsSubViewContainer.addSubview(ModsQuickPresetsSubView.view)
        }
    }
}
