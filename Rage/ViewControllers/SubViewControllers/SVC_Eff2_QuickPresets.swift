//
//  SVC_Eff1_QuickPresets.swift
//  Rage
//
//  Created by M. Lierop on 21-02-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//


import Foundation
import UIKit
import RealmSwift

class Eff2QuickPresetsSubView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var Effects2PresetsTable: UITableView!
    var currentSelectedRow: IndexPath!
    var currentScrollPosition: UITableView.ScrollPosition!
    
    @IBOutlet weak var ReverbSendKnob: Knob!
    @IBOutlet weak var ReverbTimeKnob: Knob!
    @IBOutlet weak var ReverbDampingKnob: Knob!
    @IBOutlet weak var ReverbColorationKnob: Knob!
    @IBOutlet weak var ReverbFeedbackKnob: Knob!
    @IBOutlet weak var ReverbModeBtn: PMSuperButton!
    @IBOutlet weak var ReverbTypeBtn: PMSuperButton!
    @IBOutlet weak var ReverbClockBtn: PMSuperButton!
    private var ReverbModeLbls: ReverbModeSrc?
    private var ReverbTypeLbls: ReverbTypeSrc?
    private var ReverbClockLbls: ReverbClockSrc?
    
    @IBOutlet weak var DelaySendKnob: Knob!
    @IBOutlet weak var DelayFeedbackKnob: Knob!
    @IBOutlet weak var DelayClockKnob: Knob!
    @IBOutlet weak var DelayColorKnob: Knob!
    @IBOutlet weak var TapeDelayLeftClockKnob: Knob!
    @IBOutlet weak var TapeDelayRightClockKnob: Knob!
    @IBOutlet weak var TapeDelayRatioKnob: Knob!
    @IBOutlet weak var TapeDelayBandwidthKnob: Knob!
    @IBOutlet weak var DelayLFODepthKnob: Knob!
    @IBOutlet weak var DelayLFORateKnob: Knob!
    @IBOutlet weak var DelayLFOShapeBtn: PMSuperButton!
    @IBOutlet weak var DelayModeBtn: PMSuperButton!
    @IBOutlet weak var DelayTypeBtn: PMSuperButton!
    private var DelayModeLbls: DelayModeSrc?
    private var DelayTypeLbls: DelayTypeSrc?
    private var DelayClockLbls: DelayClockSrc?
    private var DelayLFOWaveLbls: DelayLFOWaveSrc?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        setupCallbacks()
        
        ReverbModeLbls = ReverbModeSrc()
        ReverbTypeLbls = ReverbTypeSrc()
        ReverbClockLbls = ReverbClockSrc()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("ReverbModeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("ReverbTypeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("ReverbClockChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
        DelayModeLbls = DelayModeSrc()
        DelayTypeLbls = DelayTypeSrc()
        DelayClockLbls = DelayClockSrc()
        DelayLFOWaveLbls = DelayLFOWaveSrc()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("DelayModeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("DelayTypeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("DelayClockChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("DelayLFOShapeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        self.Effects2PresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        self.Effects2PresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ Effects2PresetsTable: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patchLibrary.objects( subPatchAmbiance.self).count
    }
    
    func tableView(_ Effects2PresetsTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (self.Effects2PresetsTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = patchLibrary.objects(subPatchAmbiance.self)[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ Effects2PresetsTable: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedRow = indexPath
        if (patchCtrl.LoadEffPatch(NewPatch: patchLibrary.objects(subPatchAmbiance.self)[indexPath.row]) )
        {
            updateView()
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.currentSelectedRow = indexPath
            let deleteObject = patchLibrary.objects(subPatchAmbiance.self)[indexPath.row]
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.delete(deleteObject)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    
    @objc func UpdatePresets(){
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.Effects2PresetsTable.reloadData()
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){

        ReverbSendKnob.value = Double(patchCtrl.CurrentPatch.reverb_Send )
        ReverbTimeKnob.value = Double(patchCtrl.CurrentPatch.reverb_Time)
        ReverbDampingKnob.value = Double(patchCtrl.CurrentPatch.reverb_Damping)
        ReverbColorationKnob.value = Double(patchCtrl.CurrentPatch.reverb_Color)
        ReverbFeedbackKnob.value = Double(patchCtrl.CurrentPatch.reverb_Feedback)
        ReverbModeBtn.setTitle(ReverbModes[safe: patchCtrl.CurrentPatch.reverb_Mode ], for: .normal)
        ReverbTypeBtn.setTitle(ReverbTypes[safe: patchCtrl.CurrentPatch.reverb_Type], for: .normal)
        ReverbClockBtn.setTitle(ReverbClock[safe: patchCtrl.CurrentPatch.reverb_Clock], for: .normal)
        
        
        DelaySendKnob.value = Double(patchCtrl.CurrentPatch.delay_Send)
        DelayFeedbackKnob.value = Double(patchCtrl.CurrentPatch.delay_Feedback)
        DelayClockKnob.value = Double(patchCtrl.CurrentPatch.delay_Clock)
        DelayClockKnob.label = "Delay Clock"
        DelayColorKnob.value = Double(patchCtrl.CurrentPatch.delay_Color)
        TapeDelayLeftClockKnob.value = Double(patchCtrl.CurrentPatch.delay_TapeDelayClockLeft)
        TapeDelayLeftClockKnob.label = "Left Clock"
        TapeDelayRightClockKnob.value = Double(patchCtrl.CurrentPatch.delay_TapeDelayClockRight)
        TapeDelayRightClockKnob.label = "Right clock"
        TapeDelayBandwidthKnob.value = Double(patchCtrl.CurrentPatch.delay_TapeDelayBandwidth)
        TapeDelayBandwidthKnob.label = "Bandwidth"
        DelayLFODepthKnob.value = Double(patchCtrl.CurrentPatch.delay_LFODepth)
        DelayLFORateKnob.value = Double(patchCtrl.CurrentPatch.delay_LFORate)
        DelayLFORateKnob.label = "LFO Rate"
        
        DelayLFOShapeBtn.setTitle(DelayLFOWave[safe: patchCtrl.CurrentPatch.delay_LFOShape ], for: .normal)
        DelayModeBtn.setTitle(DelayModes[safe: patchCtrl.CurrentPatch.delay_Mode ], for: .normal)
        DelayTypeBtn.setTitle(DelayTypes[safe: patchCtrl.CurrentPatch.delay_Type ], for: .normal)
        
        switch patchCtrl.CurrentPatch.delay_Type {
        case 0:
            //Classic
            DelayColorKnob.enabled = true
            DelayColorKnob.label = "Coloration"
            
            DelayLFODepthKnob.enabled = true
            DelayLFODepthKnob.label = "LFO Depth"
            DelayLFORateKnob.enabled = true
            
            TapeDelayBandwidthKnob.enabled = false
            TapeDelayLeftClockKnob.enabled = false
            TapeDelayRightClockKnob.enabled = false
            TapeDelayRatioKnob.value = Double(patchCtrl.CurrentPatch.delay_Time)
            TapeDelayRatioKnob.label = "Delay Time"
            
            if (patchCtrl.CurrentPatch.delay_Mode <= 5 ){
                DelayClockKnob.enabled = true
                TapeDelayRatioKnob.enabled = true
            } else {
                DelayClockKnob.enabled = false
                TapeDelayRatioKnob.enabled = false
            }
            DelayLFOShapeBtn.isEnabled = true
            DelayModeBtn.isEnabled = true
        case 1:
            //Tape Clocked
            DelayColorKnob.enabled = true
            DelayColorKnob.label = "Frequency"
            DelayClockKnob.enabled = false
            
            DelayLFODepthKnob.enabled = true
            DelayLFODepthKnob.label = "Modulation"
            DelayLFORateKnob.enabled = false
            
            TapeDelayLeftClockKnob.enabled = true
            TapeDelayRightClockKnob.enabled = true
            TapeDelayBandwidthKnob.enabled = true
            TapeDelayRatioKnob.enabled = false
            
            DelayLFOShapeBtn.isEnabled = false
            DelayModeBtn.isEnabled = false

        case 2:
            //Tape free
            
            DelayColorKnob.enabled = true
            DelayColorKnob.label = "Frequency"
            DelayClockKnob.enabled = true
            DelayClockKnob.label = "Time"
            
            DelayLFODepthKnob.enabled = true
            DelayLFODepthKnob.label = "Modulation"
            DelayLFORateKnob.enabled = true
            
            TapeDelayLeftClockKnob.enabled = false
            TapeDelayRightClockKnob.enabled = false
            TapeDelayBandwidthKnob.enabled = true
            TapeDelayRatioKnob.value = Double(patchCtrl.CurrentPatch.delay_TapeDelayRatio)
            TapeDelayRatioKnob.label = "Ratio"
            TapeDelayRatioKnob.enabled = true
            
            DelayLFOShapeBtn.isEnabled = false
            DelayModeBtn.isEnabled = false
        case 3:
            //Tape Doppler
            DelayColorKnob.enabled = true
            DelayColorKnob.label = "Frequency"
            DelayClockKnob.enabled = true
            DelayClockKnob.label = "Time"
            
            DelayLFODepthKnob.enabled = true
            DelayLFODepthKnob.label = "Modulation"
            DelayLFORateKnob.enabled = false
            
            TapeDelayBandwidthKnob.enabled = true
            TapeDelayLeftClockKnob.enabled = false
            TapeDelayRightClockKnob.enabled = false
            TapeDelayRatioKnob.value = Double(patchCtrl.CurrentPatch.delay_TapeDelayRatio)
            TapeDelayRatioKnob.label = "Ratio"
            TapeDelayRatioKnob.enabled = true
            
            DelayLFOShapeBtn.isEnabled = false
            DelayModeBtn.isEnabled = false
        default:
            TapeDelayLeftClockKnob.enabled = false
            TapeDelayRightClockKnob.enabled = false
            DelayColorKnob.enabled = false
            //DelayColorKnob.label = ""
            TapeDelayBandwidthKnob.enabled = false
            
            DelayLFODepthKnob.enabled = false
           // DelayLFODepthKnob.label = ""
            
            DelayLFORateKnob.enabled = false
           // DelayLFORateKnob.label = ""
            
            DelayModeBtn.isEnabled = false
            //TapeDelayRatioKnob.label = ""
            TapeDelayRatioKnob.enabled = false
        }
        
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        
        ReverbSendKnob.callback = { value in patchCtrl.setReverbSend(value: Int(value)) }
        ReverbTimeKnob.callback = { value in patchCtrl.setReverbTime(value: Int(value)) }
        ReverbDampingKnob.callback = { value in patchCtrl.setReverbDamping(value: Int(value)) }
        ReverbColorationKnob.callback = { value in patchCtrl.setReverbColor(value: Int(value)) }
        ReverbFeedbackKnob.callback = { value in patchCtrl.setReverbFeedback(value: Int(value)) }
        
        DelaySendKnob.callback = { value in patchCtrl.setDelaySend(value: Int(value)) }
        DelayFeedbackKnob.callback = { value in patchCtrl.setDelayFeedback(value: Int(value)) }
        DelayColorKnob.callback = { value in patchCtrl.setDelayColor(value: Int(value)) }
        TapeDelayLeftClockKnob.callback = { value in patchCtrl.setDelayTapeDelayClockLeft(value: Int(value)) }
        TapeDelayRightClockKnob.callback = { value in patchCtrl.setDelayTapeDelayClockRight(value: Int(value)) }
        TapeDelayBandwidthKnob.callback = { value in patchCtrl.setDelayTapeDelayBandwidth(value: Int(value)) }
        DelayLFODepthKnob.callback = { value in patchCtrl.setDelayLFODepth(value: Int(value)) }
        DelayLFORateKnob.callback = { value in patchCtrl.setDelayLFORate(value: Int(value)) }
        
        DelayClockKnob.callback = { value in
            
            if ( patchCtrl.CurrentPatch.delay_Type == 0 ){
                patchCtrl.setDelayClock(value: Int(value))
            } else {
                patchCtrl.setDelayTime(value: Int(value))
            }
        }
        
        
        TapeDelayRatioKnob.callback = { value in
            if ( patchCtrl.CurrentPatch.delay_Type == 0 ){
                patchCtrl.setDelayTime(value: Int(value))
            } else {
                patchCtrl.setDelayTapeDelayRatio(value: Int(value))
            }
        }
    }
    
    @IBAction func ReverbModePopupBtn(_ sender: PMSuperButton) {
        let ReverbModesPopover = ReverbModePopover()
        ReverbModesPopover.tableView.dataSource = ReverbModeLbls!
        ReverbModesPopover.tableView.delegate = ReverbModesPopover
        ReverbModesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ReverbModesPopover, animated: true, completion: nil)
        let popoverPresentationController = ReverbModesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func ReverbTypeePopupBtn(_ sender: PMSuperButton) {
        let ReverbTypesPopover = ReverbTypePopover()
        ReverbTypesPopover.tableView.dataSource = ReverbTypeLbls!
        ReverbTypesPopover.tableView.delegate = ReverbTypesPopover
        ReverbTypesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ReverbTypesPopover, animated: true, completion: nil)
        let popoverPresentationController = ReverbTypesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func ReverbClockPopupBtn(_ sender: PMSuperButton) {
       let ReverbClocksPopover = ReverbClockPopover()
        ReverbClocksPopover.tableView.dataSource = ReverbClockLbls!
        ReverbClocksPopover.tableView.delegate = ReverbClocksPopover
        ReverbClocksPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ReverbClocksPopover, animated: true, completion: nil)
        let popoverPresentationController = ReverbClocksPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func DelayModePopupBtn(_ sender: PMSuperButton) {
        let DelayModesPopover = DelayModePopover()
        DelayModesPopover.tableView.dataSource = DelayModeLbls!
        DelayModesPopover.tableView.delegate = DelayModesPopover
        DelayModesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(DelayModesPopover, animated: true, completion: nil)
        let popoverPresentationController = DelayModesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    @IBAction func DelayTypePopupBtn(_ sender: PMSuperButton) {
        let DelayTypesPopover = DelayTypePopover()
        DelayTypesPopover.tableView.dataSource = DelayTypeLbls!
        DelayTypesPopover.tableView.delegate = DelayTypesPopover
        DelayTypesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(DelayTypesPopover, animated: true, completion: nil)
        let popoverPresentationController = DelayTypesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    @IBAction func DelayLFOShapePopupBtn(_ sender: PMSuperButton) {
        let DelayLFOShapePopover = DelayLFOWavePopover()
        DelayLFOShapePopover.tableView.dataSource = DelayLFOWaveLbls!
        DelayLFOShapePopover.tableView.delegate = DelayLFOShapePopover
        DelayLFOShapePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(DelayLFOShapePopover, animated: true, completion: nil)
        let popoverPresentationController = DelayLFOShapePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func SaveNewSubPatch(_ sender: UIButton) {
        self.Effects2PresetsTable.reloadData()
        showInputDialog(title: "Save new effects preset",
                        subtitle: "Save current efects settings as preset",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New preset name",
                        inputKeyboardType: .alphabet,
                        actionHandler:{ (input:String?) in
                            print("The name is \(input ?? "")")
                            if (input != ""){
                                patchCtrl.EffectsPatch.name = input!
                                
                                let realm = try! Realm(configuration: config)
                                
                                try! realm.write {
                                    realm.create(subPatchAmbiance.self, value: patchCtrl.EffectsPatch)
                                    realm.refresh()
                                }
                            }
                            self.Effects2PresetsTable.reloadData()
                            let realm = try! Realm(configuration: config)
                            realm.refresh()
                            
                            self.UpdatePresets()
                        })
    }
}

