//
//  SVC_Mods_QuickPresets.swift
//  Rage
//
//  Created by M. Lierop on 09-01-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ModsQuickPresetsSubView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var Matrix = MatrixSummary()
    
    @IBOutlet weak var LFO1RateKnob: Knob!
    @IBOutlet weak var LFO1RateLabel: UILabel!
    @IBOutlet weak var LFO1Contour: Knob!
    @IBOutlet weak var LFO1ClockSyncBtn: ToggleButton!
    @IBOutlet weak var LFO1PolyModeBtn: ToggleButton!
    @IBOutlet weak var LFO1EnvModeBtn: ToggleButton!
    
    @IBOutlet weak var LFO2RateKnob: Knob!
    @IBOutlet weak var LFO2RateLabel: UILabel!
    @IBOutlet weak var LFO2Contour: Knob!
    @IBOutlet weak var LFO2ClockSyncBtn: ToggleButton!
    @IBOutlet weak var LFO2PolyModeBtn: ToggleButton!
    @IBOutlet weak var LFO2EnvModeBtn: ToggleButton!
    
    
    @IBOutlet weak var LFO3RateKnob: Knob!
    @IBOutlet weak var LFO3RateLabel: UILabel!
    @IBOutlet weak var LFO3Contour: Knob!
    @IBOutlet weak var LFO3FadeTimeKnob: Knob!
    @IBOutlet weak var LFO3ClockSyncBtn: ToggleButton!
    @IBOutlet weak var LFO3PolyModeBtn: ToggleButton!
    
    @IBOutlet weak var MAtrixSlot1Indicator: UILabel!
    @IBOutlet weak var MatrixSlot2Indicator: UILabel!
    @IBOutlet weak var MatrixSlot3Indicator: UILabel!
    @IBOutlet weak var MatrixSlot4Indicator: UILabel!
    @IBOutlet weak var MatrixSlot5Indicator: UILabel!
    @IBOutlet weak var MatrixSlot6Indicator: UILabel!
    
    @IBOutlet weak var ModPresetsTable: UITableView!
    var currentSelectedRow: IndexPath!
    var currentScrollPosition: UITableView.ScrollPosition!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ModPresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        updateView()
        setupCallbacks()
        Matrix = MatrixSummary()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        updateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ OscPresetsTable: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patchLibrary.objects(subPatchMod.self).count
    }
    
    func tableView(_ OscPresetsTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell! = self.ModPresetsTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = patchLibrary.objects(subPatchMod.self)[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ OscPresetsTable: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedRow = indexPath
        print(patchLibrary.objects(subPatchMod.self)[indexPath.row])
        if (patchCtrl.LoadModPatch(NewPatch: patchLibrary.objects(subPatchMod.self)[indexPath.row]) )
        {
            updateView()
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.currentSelectedRow = indexPath
            let deleteObject = patchLibrary.objects(subPatchMod.self)[indexPath.row]
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.delete(deleteObject)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    @objc func UpdatePresets(){
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.ModPresetsTable.reloadData()
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){

        LFO1ClockSyncBtn.setOn(val: (patchCtrl.CurrentPatch.lfo1_Clock >= 1 ))
        LFO2ClockSyncBtn.setOn(val: (patchCtrl.CurrentPatch.lfo2_Clock >= 1 ))
        LFO3ClockSyncBtn.setOn(val: (patchCtrl.CurrentPatch.lfo3_Clock >= 1 ))
        
        if ( patchCtrl.CurrentPatch.lfo1_Clock >= 1 ) {

            LFO1RateKnob.maximum = 21
            LFO1RateKnob.minimum = 1
            LFO1RateKnob.value = Double(patchCtrl.CurrentPatch.lfo1_Clock)
        } else {

            LFO1RateKnob.maximum = 127
            LFO1RateKnob.minimum = 0
            LFO1RateKnob.value = Double(patchCtrl.CurrentPatch.lfo1_Rate)
        }
        self.setLFO1RateLabel()
        
        if ( patchCtrl.CurrentPatch.lfo2_Clock >= 1 ) {
            
            LFO2RateKnob.maximum = 21
            LFO2RateKnob.minimum = 1
            LFO2RateKnob.value = Double(patchCtrl.CurrentPatch.lfo2_Clock)
        } else {

            LFO2RateKnob.maximum = 127
            LFO2RateKnob.minimum = 0
            LFO2RateKnob.value = Double(patchCtrl.CurrentPatch.lfo2_Rate)
        }
        self.setLFO2RateLabel()
        
        if ( patchCtrl.CurrentPatch.lfo3_Clock >= 1 ) {
            
            LFO3RateKnob.maximum = 21
            LFO3RateKnob.minimum = 1
            LFO3RateKnob.value = Double(patchCtrl.CurrentPatch.lfo3_Clock)
        } else {

            LFO3RateKnob.maximum = 127
            LFO3RateKnob.minimum = 0
            LFO3RateKnob.value = Double(patchCtrl.CurrentPatch.lfo3_Rate)
        }
        self.setLFO3RateLabel()
        
        LFO1Contour.value = Double(patchCtrl.CurrentPatch.lfo1_WaveformContour)
        LFO2Contour.value = Double(patchCtrl.CurrentPatch.lfo2_WaveformContour)
        LFO3FadeTimeKnob.value = Double(patchCtrl.CurrentPatch.lfo3_FadeInTime)
        
        LFO1PolyModeBtn.setOn(val: (patchCtrl.CurrentPatch.lfo1_Mode == 1 ))
        LFO2PolyModeBtn.setOn(val: (patchCtrl.CurrentPatch.lfo2_Mode == 1 ))
        LFO3PolyModeBtn.setOn(val: (patchCtrl.CurrentPatch.lfo3_Mode == 1 ))
        
        LFO1EnvModeBtn.setOn(val: (patchCtrl.CurrentPatch.lfo1_EnvelopeMode == 1 ))
        LFO2EnvModeBtn.setOn(val: (patchCtrl.CurrentPatch.lfo2_EnvelopeMode == 1 ))
        
        MAtrixSlot1Indicator.text = Matrix.slot1Summary()
        MatrixSlot2Indicator.text = Matrix.slot2Summary()
        MatrixSlot3Indicator.text = Matrix.slot3Summary()
        MatrixSlot4Indicator.text = Matrix.slot4Summary()
        MatrixSlot5Indicator.text = Matrix.slot5Summary()
        MatrixSlot6Indicator.text = Matrix.slot6Summary()
        
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setLFO1RateLabel() {
        if ( self.LFO1ClockSyncBtn.isOn ) {
            self.LFO1RateLabel.text = LFOClockValues[safe: Int(patchCtrl.CurrentPatch.lfo1_Clock)]
        } else {
            self.LFO1RateLabel.text = String(patchCtrl.CurrentPatch.lfo1_Rate)
        }
    }
    
    func setLFO2RateLabel() {
        if ( self.LFO2ClockSyncBtn.isOn ) {
            self.LFO2RateLabel.text = LFOClockValues[safe: Int(patchCtrl.CurrentPatch.lfo2_Clock)]
        } else {
            self.LFO2RateLabel.text = String(patchCtrl.CurrentPatch.lfo2_Rate)
        }
    }
    
    func setLFO3RateLabel() {
        if ( self.LFO3ClockSyncBtn.isOn ) {
            self.LFO3RateLabel.text = LFOClockValues[safe: Int(patchCtrl.CurrentPatch.lfo3_Clock)]
        } else {
            self.LFO3RateLabel.text = String(patchCtrl.CurrentPatch.lfo3_Rate)
        }
    }
    
    func setupCallbacks() {
        
        LFO1RateKnob.callback = { value in
            if ( self.LFO1ClockSyncBtn.isOn ) {
                patchCtrl.setLFO1Clock(value: Int(value))
            } else {
                patchCtrl.setLFO1Rate(value: Int(value))
            }
            self.setLFO1RateLabel()
        }
        
        LFO1Contour.callback = { value in
            patchCtrl.setLFO1WaveformContour(value: Int(value))
        }
        
        
        LFO2RateKnob.callback = { value in
            if ( self.LFO2ClockSyncBtn.isOn ) {
                patchCtrl.setLFO2Clock(value: Int(value))
            } else {
                patchCtrl.setLFO2Rate(value: Int(value))
            }
            self.setLFO2RateLabel()
        }
        
        LFO2Contour.callback = { value in
            patchCtrl.setLFO2WaveformContour(value: Int(value))
        }
        
        
        LFO3RateKnob.callback = { value in
            if ( self.LFO3ClockSyncBtn.isOn ) {
                patchCtrl.setLFO3Clock(value: Int(value))
            } else {
                patchCtrl.setLFO3Rate(value: Int(value))
            }
            self.setLFO3RateLabel()
        }
        
        //clocks
        LFO1ClockSyncBtn.callback = { value in
            if ( value == 1.0 ) {
                patchCtrl.setLFO1Clock(value: 1)
            } else {
                patchCtrl.setLFO1Clock(value: 0)
            }
            self.updateView()
        }
        LFO2ClockSyncBtn.callback = { value in
            if ( value == 1.0 ) {
                patchCtrl.setLFO2Clock(value: 1)
            } else {
                patchCtrl.setLFO2Clock(value: 0)
            }
            self.updateView()
        }
        LFO3ClockSyncBtn.callback = { value in
            if ( value == 1.0 ) {
                patchCtrl.setLFO3Clock(value: 1)
            } else {
                patchCtrl.setLFO3Clock(value: 0)
            }
            self.updateView()
        }
        
        //poly
        LFO1PolyModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO1Mode(value: 1)
            } else {
                patchCtrl.setLFO1Mode(value: 0)
            }
        }
        LFO2PolyModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO2Mode(value: 1)
            } else {
                patchCtrl.setLFO2Mode(value: 0)
            }
        }
        LFO3PolyModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO3Mode(value: 1)
            } else {
                patchCtrl.setLFO3Mode(value: 0)
            }
        }
        
        
        // env
        LFO1EnvModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO1EnvelopeMode(value: 1)
            } else {
                patchCtrl.setLFO1EnvelopeMode(value: 0)
            }
        }
        LFO2EnvModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO2EnvelopeMode(value: 1)
            } else {
                patchCtrl.setLFO2EnvelopeMode(value: 0)
            }
        }
        
    }
    

    
    @IBAction func SaveNewSubPatch(_ sender: UIButton) {
        self.ModPresetsTable.reloadData()
        showInputDialog(title: "Save new Modulation preset",
                        subtitle: "Save current modulation settings as preset",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New preset name",
                        inputKeyboardType: .alphabet,
                        actionHandler: { (input:String?) in
                            print("The name is \(input ?? "")")
                            if (input != ""){
                                patchCtrl.ModulatorPatch.name = input!
                                
                                let realm = try! Realm(configuration: config)
                                
                                try! realm.write {
                                    realm.create(subPatchMod.self, value: patchCtrl.ModulatorPatch)
                                    realm.refresh()
                                }
                            }
                            self.ModPresetsTable.reloadData()
                            let realm = try! Realm(configuration: config)
                            realm.refresh()
                            
                            self.UpdatePresets()
                        })
    }
}

