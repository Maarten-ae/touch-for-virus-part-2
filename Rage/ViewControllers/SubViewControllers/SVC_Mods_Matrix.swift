//
//  SVC_Mods_Matrix.swift
//  Rage
//
//  Created by M. Lierop on 09-01-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//


import Foundation
import UIKit

class ModsMatrixSubView: UIViewController, UITableViewDelegate {

    private var matrixSrc:MatrixSources?
    private var matrixDest:MatrixDestinations?
    
    @IBOutlet weak var MatrixSlot1SrcBtn: PMSuperButton!
    @IBOutlet weak var MatrixSlot1Dest1Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot1Dest2Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot1Dest3Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot1Dest1Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot1Dest2Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot1Dest3Slider: HorizontalSlider!
    
    @IBOutlet weak var MatrixSlot2SrcBtn: PMSuperButton!
    @IBOutlet weak var MatrixSlot2Dest1Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot2Dest2Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot2Dest3Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot2Dest1Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot2Dest2Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot2Dest3Slider: HorizontalSlider!
    
    @IBOutlet weak var MatrixSlot3SrcBtn: PMSuperButton!
    @IBOutlet weak var MatrixSlot3Dest1Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot3Dest2Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot3Dest3Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot3Dest1Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot3Dest2Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot3Dest3Slider: HorizontalSlider!

    @IBOutlet weak var MatrixSlot4SrcBtn: PMSuperButton!
    @IBOutlet weak var MatrixSlot4Dest1Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot4Dest2Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot4Dest3Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot4Dest1Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot4Dest2Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot4Dest3Slider: HorizontalSlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("MatrixSlotSourceChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("MatrixSlotDestinationChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
        MatrixSlot1Dest1Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot1Dest1Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot1Dest1Slider.EnableResetGesture()
        
        MatrixSlot1Dest2Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot1Dest2Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot1Dest2Slider.EnableResetGesture()
        
        MatrixSlot1Dest3Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot1Dest3Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot1Dest3Slider.EnableResetGesture()
        
        MatrixSlot2Dest1Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot2Dest1Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot2Dest1Slider.EnableResetGesture()
        
        MatrixSlot2Dest2Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot2Dest2Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot2Dest2Slider.EnableResetGesture()
        
        MatrixSlot2Dest3Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot2Dest3Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot2Dest3Slider.EnableResetGesture()
        
        MatrixSlot3Dest1Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot3Dest1Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot3Dest1Slider.EnableResetGesture()
        
        MatrixSlot3Dest2Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot3Dest2Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot3Dest2Slider.EnableResetGesture()
        
        MatrixSlot3Dest3Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot3Dest3Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot3Dest3Slider.EnableResetGesture()
        
        MatrixSlot4Dest1Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot4Dest1Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot4Dest1Slider.EnableResetGesture()
        
        MatrixSlot4Dest2Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot4Dest2Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot4Dest2Slider.EnableResetGesture()
        
        MatrixSlot4Dest3Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot4Dest3Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot4Dest3Slider.EnableResetGesture()
        
        matrixSrc = MatrixSources()
        matrixDest = MatrixDestinations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        MatrixSlot1SrcBtn.setTitle( MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot1_Source], for: .normal)
        MatrixSlot1Dest1Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot1_Destination1 ], for: .normal)
        MatrixSlot1Dest2Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot1_Destination2 ], for: .normal)
        MatrixSlot1Dest3Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot1_Destination3 ], for: .normal)
        MatrixSlot1Dest1Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot1_Amount1)
        MatrixSlot1Dest1Slider.showValue()
        MatrixSlot1Dest2Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot1_Amount2)
        MatrixSlot1Dest2Slider.showValue()
        MatrixSlot1Dest3Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot1_Amount3)
        MatrixSlot1Dest3Slider.showValue()
        
        MatrixSlot2SrcBtn.setTitle( MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot2_Source], for: .normal)
        MatrixSlot2Dest1Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot2_Destination1 ], for: .normal)
        MatrixSlot2Dest2Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot2_Destination2 ], for: .normal)
        MatrixSlot2Dest3Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot2_Destination3 ], for: .normal)
        MatrixSlot2Dest1Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot2_Amount1)
        MatrixSlot2Dest1Slider.showValue()
        MatrixSlot2Dest2Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot2_Amount2)
        MatrixSlot2Dest2Slider.showValue()
        MatrixSlot2Dest3Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot2_Amount3)
        MatrixSlot2Dest3Slider.showValue()
        
        MatrixSlot3SrcBtn.setTitle( MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot3_Source], for: .normal)
        MatrixSlot3Dest1Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot3_Destination1 ], for: .normal)
        MatrixSlot3Dest2Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot3_Destination2 ], for: .normal)
        MatrixSlot3Dest3Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot3_Destination3 ], for: .normal)
        MatrixSlot3Dest1Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot3_Amount1)
        MatrixSlot3Dest1Slider.showValue()
        MatrixSlot3Dest2Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot3_Amount2)
        MatrixSlot3Dest2Slider.showValue()
        MatrixSlot3Dest3Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot3_Amount3)
        MatrixSlot3Dest3Slider.showValue()
        
        MatrixSlot4SrcBtn.setTitle( MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot4_Source], for: .normal)
        MatrixSlot4Dest1Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot4_Destination1 ], for: .normal)
        MatrixSlot4Dest2Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot4_Destination2 ], for: .normal)
        MatrixSlot4Dest3Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot4_Destination3 ], for: .normal)
        MatrixSlot4Dest1Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot4_Amount1)
        MatrixSlot4Dest1Slider.showValue()
        MatrixSlot4Dest2Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot4_Amount2)
        MatrixSlot4Dest2Slider.showValue()
        MatrixSlot4Dest3Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot4_Amount3)
        MatrixSlot4Dest3Slider.showValue()
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        
    }
    
    @IBOutlet weak var MatrixSlot3SourceBtn: PMSuperButton!
    
    //*****************************************************************
    // MARK: - MAtrix Slot 1
    //*****************************************************************

    @IBAction func Slot1SourceClick(_ sender: UIButton) {
        let matrixslot1SrcPopover = MatrixSourcePopover()
        matrixslot1SrcPopover.matrixSlotNr = 1
        matrixslot1SrcPopover.tableView.dataSource = matrixSrc!
        matrixslot1SrcPopover.tableView.delegate = matrixslot1SrcPopover
        
        //matrixslot1SrcPopover.tableView.reloadData()
        matrixslot1SrcPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1SrcPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1SrcPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot1Dest1Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 1
        matrixslot1DestPopover.matrixSlotDestNr = 1
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot1Dest2Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 1
        matrixslot1DestPopover.matrixSlotDestNr = 2
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot1Dest3Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 1
        matrixslot1DestPopover.matrixSlotDestNr = 3
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    @IBAction func MatrixSlotDestinationsliderChange(_ sender: UISlider) {
        switch (sender.restorationIdentifier!){
        case "1a":
            patchCtrl.setMatrixSlot1Amount1(value: Int(sender.value))
            MatrixSlot1Dest1Slider.showValue()
        case "1b":
            patchCtrl.setMatrixSlot1Amount2(value: Int(sender.value))
            MatrixSlot1Dest2Slider.showValue()
        case "1c":
            patchCtrl.setMatrixSlot1Amount3(value: Int(sender.value))
            MatrixSlot1Dest3Slider.showValue()
        
        case "2a":
            patchCtrl.setMatrixSlot2Amount1(value: Int(sender.value))
            MatrixSlot2Dest1Slider.showValue()
        case "2b":
            patchCtrl.setMatrixSlot2Amount2(value: Int(sender.value))
            MatrixSlot2Dest2Slider.showValue()
        case "2c":
            patchCtrl.setMatrixSlot2Amount3(value: Int(sender.value))
            MatrixSlot2Dest3Slider.showValue()

        case "3a":
            patchCtrl.setMatrixSlot3Amount1(value: Int(sender.value))
            MatrixSlot3Dest1Slider.showValue()
        case "3b":
            patchCtrl.setMatrixSlot3Amount2(value: Int(sender.value))
            MatrixSlot3Dest2Slider.showValue()
        case "3c":
            patchCtrl.setMatrixSlot3Amount3(value: Int(sender.value))
            MatrixSlot3Dest3Slider.showValue()

        case "4a":
            patchCtrl.setMatrixSlot4Amount1(value: Int(sender.value))
            MatrixSlot4Dest1Slider.showValue()
        case "4b":
            patchCtrl.setMatrixSlot4Amount2(value: Int(sender.value))
            MatrixSlot4Dest2Slider.showValue()
        case "4c":
            patchCtrl.setMatrixSlot4Amount3(value: Int(sender.value))
            MatrixSlot4Dest3Slider.showValue()

        default:
            print("onbekend")
        }
        print("slider nr ",sender.restorationIdentifier!)
    }
    
    //*****************************************************************
    // MARK: - MAtrix Slot 2
    //*****************************************************************
    
    @IBAction func Slot2SourceClick(_ sender: UIButton) {
        let matrixslot2SrcPopover = MatrixSourcePopover()
        matrixslot2SrcPopover.matrixSlotNr = 2
        matrixslot2SrcPopover.tableView.dataSource = matrixSrc!
        matrixslot2SrcPopover.tableView.delegate = matrixslot2SrcPopover
        //matrixslot2SrcPopover.tableView.reloadData()
        matrixslot2SrcPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot2SrcPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot2SrcPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot2Dest1Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 2
        matrixslot1DestPopover.matrixSlotDestNr = 1
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot2Dest2Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 2
        matrixslot1DestPopover.matrixSlotDestNr = 2
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot2Dest3Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 2
        matrixslot1DestPopover.matrixSlotDestNr = 3
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    //*****************************************************************
    // MARK: - MAtrix Slot 3
    //*****************************************************************
    
    @IBAction func Slot3SourceClick(_ sender: UIButton) {
        let matrixslot3SrcPopover = MatrixSourcePopover()
        matrixslot3SrcPopover.matrixSlotNr = 3
        matrixslot3SrcPopover.tableView.dataSource = matrixSrc!
        matrixslot3SrcPopover.tableView.delegate = matrixslot3SrcPopover
        //matrixslot3SrcPopover.tableView.reloadData()
        matrixslot3SrcPopover.modalPresentationStyle = UIModalPresentationStyle.popover

        present(matrixslot3SrcPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot3SrcPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot3Dest1Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 3
        matrixslot1DestPopover.matrixSlotDestNr = 1
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot3Dest2Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 3
        matrixslot1DestPopover.matrixSlotDestNr = 2
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot3Dest3Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 3
        matrixslot1DestPopover.matrixSlotDestNr = 3
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    //*****************************************************************
    // MARK: - MAtrix Slot 4
    //*****************************************************************
    
    @IBAction func Slot4SourceClick(_ sender: UIButton) {
        let matrixslot4SrcPopover = MatrixSourcePopover()
        matrixslot4SrcPopover.matrixSlotNr = 4
        matrixslot4SrcPopover.tableView.dataSource = matrixSrc!
        matrixslot4SrcPopover.tableView.delegate = matrixslot4SrcPopover
        //matrixslot4SrcPopover.tableView.reloadData()
        matrixslot4SrcPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot4SrcPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot4SrcPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot4Dest1Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 4
        matrixslot1DestPopover.matrixSlotDestNr = 1
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot4Dest2Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 4
        matrixslot1DestPopover.matrixSlotDestNr = 2
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot4Dest3Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 4
        matrixslot1DestPopover.matrixSlotDestNr = 3
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
}




