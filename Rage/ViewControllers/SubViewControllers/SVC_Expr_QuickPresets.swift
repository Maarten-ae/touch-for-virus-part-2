//
//  SVC_Expr_QuickPresets.swift
//  Rage
//
//  Created by M. Lierop on 11-02-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ExprQuickPresetsSubView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var SoftKnobDestinationsLbls:SoftKnobDestinationsSrc?
    private var SoftKnobNamesLbls:SoftKnobNamesSrc?
    private var KeyboardModeLbls:KeyboardModesSrc?
    
    @IBOutlet weak var KeyboardModeBtn: PMSuperButton!
    @IBOutlet weak var VolumeVelocityKnob: Knob!
    @IBOutlet weak var PanoramaVelocityKnob: Knob!
    
    @IBOutlet weak var OscPulseWidthVelocityKnob: Knob!
    @IBOutlet weak var OscFMAmountVelocityKnob: Knob!
    
    @IBOutlet weak var Filter2EnvAmountKnob: Knob!
    @IBOutlet weak var Filter2ResonanceKnob: Knob!
    @IBOutlet weak var Filter1ResonanceKnob: Knob!
    @IBOutlet weak var Filter1EnvAmountKnob: Knob!
    
    @IBOutlet weak var Osc1WaveFormShapeVelocityKnob: Knob!
    @IBOutlet weak var Osc2WaveFormShapeVelocityKnob: Knob!
    
    @IBOutlet weak var BenderUpKnob: Knob!
    @IBOutlet weak var BenderDownKnob: Knob!
    @IBOutlet weak var BenderScaleCntrl: UISegmentedControl!

    @IBOutlet weak var Knob1DestinationBtn: PMSuperButton!
    @IBOutlet weak var Knob1NameBtn: PMSuperButton!
    
    @IBOutlet weak var Knob2DestinationBtn: PMSuperButton!
    @IBOutlet weak var Knob2NameBtn: PMSuperButton!
    
    @IBOutlet weak var Knob3DestinationBtn: PMSuperButton!
    @IBOutlet weak var Knob3NameBtn: PMSuperButton!
    
    @IBOutlet weak var SmoothModeBtn: ToggleButton!
    
    @IBOutlet weak var ExprPresetsTable: UITableView!
    var currentSelectedRow: IndexPath!
    var currentScrollPosition: UITableView.ScrollPosition!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        setupCallbacks()
        
        SoftKnobDestinationsLbls = SoftKnobDestinationsSrc()
        SoftKnobNamesLbls = SoftKnobNamesSrc()
        KeyboardModeLbls = KeyboardModesSrc()
        
        self.ExprPresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("SoftKnobChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("KeyboardModeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        self.ExprPresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ ExprPresetsTable: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patchLibrary.objects(subPatchControl.self).count
    }
    
    func tableView(_ ExprPresetsTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (self.ExprPresetsTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = patchLibrary.objects(subPatchControl.self)[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ ExprPresetsTable: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedRow = indexPath
        if (patchCtrl.LoadControlPatch(NewPatch: patchLibrary.objects(subPatchControl.self)[indexPath.row]) )
        {
            updateView()
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        return true
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.currentSelectedRow = indexPath
            let deleteObject = patchLibrary.objects(subPatchControl.self)[indexPath.row]
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.delete(deleteObject)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    @objc func UpdatePresets(){
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.ExprPresetsTable.reloadData()
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        VolumeVelocityKnob.value = Double (patchCtrl.CurrentPatch.velocity_Volume)
        PanoramaVelocityKnob.value = Double (patchCtrl.CurrentPatch.velocity_Panorama)
        OscPulseWidthVelocityKnob.value = Double (patchCtrl.CurrentPatch.velocity_OscPulsewidth)
        OscFMAmountVelocityKnob.value = Double (patchCtrl.CurrentPatch.velocity_OscFMAmount)
        Filter2EnvAmountKnob.value = Double (patchCtrl.CurrentPatch.velocity_Filter2EnvAmount)
        Filter2ResonanceKnob.value = Double (patchCtrl.CurrentPatch.velocity_Filter2Resonance)
        Filter1ResonanceKnob.value = Double (patchCtrl.CurrentPatch.velocity_Filter1Resonance)
        Filter1EnvAmountKnob.value = Double (patchCtrl.CurrentPatch.velocity_Filter1EnvAmount)
        Osc1WaveFormShapeVelocityKnob.value = Double (patchCtrl.CurrentPatch.velocity_Osc1WaveformShape)
        Osc2WaveFormShapeVelocityKnob.value = Double (patchCtrl.CurrentPatch.velocity_Osc2WaveformShape)
        BenderUpKnob.value = Double (patchCtrl.CurrentPatch.control_BenderUp)
        BenderDownKnob.value = Double (patchCtrl.CurrentPatch.control_BenderDown)
        BenderScaleCntrl.selectedSegmentIndex = patchCtrl.CurrentPatch.control_BenderScale
        KeyboardModeBtn.setTitle( keyboardModeNames[safe:  patchCtrl.CurrentPatch.control_KeyboardMode ], for: .normal )
        
        Knob1DestinationBtn.setTitle( SoftKnobDestinations[safe: patchCtrl.CurrentPatch.control_Softknob1Destination], for: .normal)
        Knob1NameBtn.setTitle( SoftKnobNames[safe: patchCtrl.CurrentPatch.control_Softknob1Name], for: .normal)
        
        Knob2DestinationBtn.setTitle( SoftKnobDestinations[safe: patchCtrl.CurrentPatch.control_Softknob2Destination], for: .normal)
        Knob2NameBtn.setTitle( SoftKnobNames[safe: patchCtrl.CurrentPatch.control_Softknob2Name], for: .normal)
        
        Knob3DestinationBtn.setTitle( SoftKnobDestinations[safe: patchCtrl.CurrentPatch.control_Softknob3Destination], for: .normal)
        Knob3NameBtn.setTitle( SoftKnobNames[safe: patchCtrl.CurrentPatch.control_Softknob3Name], for: .normal)
        
        SmoothModeBtn.setOn(val: (patchCtrl.CurrentPatch.control_parameterSmoothMode == 1))
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        
        VolumeVelocityKnob.callback = { value in
            patchCtrl.setVelocityVolume(value: Int(value))
        }
        
        PanoramaVelocityKnob.callback = { value in
            patchCtrl.setVelocityPanorama(value: Int(value))
        }
        
        OscPulseWidthVelocityKnob.callback = { value in
            patchCtrl.setVelocityOscPulsewidth(value: Int(value))
        }
        OscFMAmountVelocityKnob.callback = { value in
            patchCtrl.setVelocityOscFMAmount(value: Int(value))
        }
        
        Filter2EnvAmountKnob.callback = { value in
            patchCtrl.setVelocityFilter2EnvAmount(value: Int(value))
        }
        Filter2ResonanceKnob.callback = { value in
            patchCtrl.setVelocityFilter2Resonance(value: Int(value))
        }
        Filter1ResonanceKnob.callback = { value in
            patchCtrl.setVelocityFilter1Resonance(value: Int(value))
        }
        Filter1EnvAmountKnob.callback = { value in
            patchCtrl.setVelocityFilter1EnvAmount(value: Int(value))
        }
        
        Osc1WaveFormShapeVelocityKnob.callback = { value in
            patchCtrl.setVelocityOsc1WaveformShape(value: Int(value))
        }
        Osc2WaveFormShapeVelocityKnob.callback = { value in
            patchCtrl.setVelocityOsc2WaveformShape(value: Int(value))
        }
        
        BenderUpKnob.callback = { value in
            patchCtrl.setBenderUp(value: Int(value))
        }
        BenderDownKnob.callback = { value in
            patchCtrl.setBenderDown(value: Int(value))
        }
        
        SmoothModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setParameterSmoothMode(value: 1)
            } else {
                patchCtrl.setParameterSmoothMode(value: 0)
            }
        }
    }
    
    @IBAction func SoftKnob1DestBtn(_ sender: UIButton) {
        let SoftKnob1DestPopover = SoftKnobDestinationsPopover()
        SoftKnob1DestPopover.SoftKnobNr = 1
        SoftKnob1DestPopover.tableView.dataSource = SoftKnobDestinationsLbls!
        SoftKnob1DestPopover.tableView.delegate = SoftKnob1DestPopover
        SoftKnob1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(SoftKnob1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = SoftKnob1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func SoftKnob1NameBtn(_ sender: UIButton) {
        let SoftKnob1NamePopover = SoftKnobNamesPopover()
        SoftKnob1NamePopover.SoftKnobNr = 1
        SoftKnob1NamePopover.tableView.dataSource = SoftKnobNamesLbls!
        SoftKnob1NamePopover.tableView.delegate = SoftKnob1NamePopover
        SoftKnob1NamePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(SoftKnob1NamePopover, animated: true, completion: nil)
        let popoverPresentationController = SoftKnob1NamePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    @IBAction func SoftKnob2DestBtn(_ sender: UIButton) {
        let SoftKnob2DestPopover = SoftKnobDestinationsPopover()
        SoftKnob2DestPopover.SoftKnobNr = 2
        SoftKnob2DestPopover.tableView.dataSource = SoftKnobDestinationsLbls!
        SoftKnob2DestPopover.tableView.delegate = SoftKnob2DestPopover
        SoftKnob2DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(SoftKnob2DestPopover, animated: true, completion: nil)
        let popoverPresentationController = SoftKnob2DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func SoftKnob2NameBtn(_ sender: UIButton) {
        let SoftKnob2NamePopover = SoftKnobNamesPopover()
        SoftKnob2NamePopover.SoftKnobNr = 2
        SoftKnob2NamePopover.tableView.dataSource = SoftKnobNamesLbls!
        SoftKnob2NamePopover.tableView.delegate = SoftKnob2NamePopover
        SoftKnob2NamePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(SoftKnob2NamePopover, animated: true, completion: nil)
        let popoverPresentationController = SoftKnob2NamePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    @IBAction func SoftKnob3DestBtn(_ sender: UIButton) {
        let SoftKnob3DestPopover = SoftKnobDestinationsPopover()
        SoftKnob3DestPopover.SoftKnobNr = 3
        SoftKnob3DestPopover.tableView.dataSource = SoftKnobDestinationsLbls!
        SoftKnob3DestPopover.tableView.delegate = SoftKnob3DestPopover
        SoftKnob3DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(SoftKnob3DestPopover, animated: true, completion: nil)
        let popoverPresentationController = SoftKnob3DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func SoftKnob3NameBtn(_ sender: UIButton) {
        let SoftKnob3NamePopover = SoftKnobNamesPopover()
        SoftKnob3NamePopover.SoftKnobNr = 3
        SoftKnob3NamePopover.tableView.dataSource = SoftKnobNamesLbls!
        SoftKnob3NamePopover.tableView.delegate = SoftKnob3NamePopover
        SoftKnob3NamePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(SoftKnob3NamePopover, animated: true, completion: nil)
        let popoverPresentationController = SoftKnob3NamePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    @IBAction func onKeyboardModeChanged(_ sender: PMSuperButton) {
        let KeyboardModePopover = KeyboardModesPopover()
        KeyboardModePopover.tableView.dataSource = KeyboardModeLbls!
        KeyboardModePopover.tableView.delegate = KeyboardModePopover
        KeyboardModePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(KeyboardModePopover, animated: true, completion: nil)
        let popoverPresentationController = KeyboardModePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    
    @IBAction func onBenderScaleChanged(_ sender: UISegmentedControl) {
        patchCtrl.setBenderScale(value: Int( sender.selectedSegmentIndex ))
    }
    
    @IBAction func SaveNewSubPatch(_ sender: UIButton) {
        self.ExprPresetsTable.reloadData()
        showInputDialog(title: "Save new Modulation preset",
                        subtitle: "Save current modulation settings as preset",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New preset name",
                        inputKeyboardType: .alphabet,
                        actionHandler: { (input:String?) in
                            print("The name is \(input ?? "")")
                            if (input != ""){
                                patchCtrl.ControlPatch.name = input!
                                
                                let realm = try! Realm(configuration: config)
                                
                                try! realm.write {
                                    realm.create(subPatchControl.self, value: patchCtrl.ControlPatch)
                                    realm.refresh()
                                }
                            }
                            self.ExprPresetsTable.reloadData()
                            let realm = try! Realm(configuration: config)
                            realm.refresh()
                            
                            self.UpdatePresets()
                        })
    }
    
}
