//
//  SVC_Eff1_QuickPresets.swift
//  Rage
//
//  Created by M. Lierop on 21-02-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

//
import Foundation
import UIKit
import RealmSwift

class Eff1QuickPresetsSubView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var Effects1PresetsTable: UITableView!
    var currentSelectedRow: IndexPath!
    var currentScrollPosition: UITableView.ScrollPosition!
    
    @IBOutlet weak var LowEqGainKnob: Knob!
    @IBOutlet weak var LowEqFreqKnob: Knob!
    
    @IBOutlet weak var MidEqGainKnob: Knob!
    @IBOutlet weak var MidEqFreqKnob: Knob!
    @IBOutlet weak var MidEqQKnob: Knob!
    
    @IBOutlet weak var HiEqGainKnob: Knob!
    @IBOutlet weak var HiEqFreqKnob: Knob!
    
    
    @IBOutlet weak var ChorusTypeLbl: UILabel!
    @IBOutlet weak var ChorusMixKnob: Knob!
    @IBOutlet weak var ChorusDelayKnob: Knob!
    
    @IBOutlet weak var DistortionTypeLbl: UILabel!
    @IBOutlet weak var DistortionMixKnob: Knob!
    @IBOutlet weak var DistortionIntensityKnob: Knob!
    
    @IBOutlet weak var FilterBankTypeLbl: UILabel!
    @IBOutlet weak var FilterBankMixKnob: Knob!
    @IBOutlet weak var FilterBankFreqKnob: Knob!
    
    @IBOutlet weak var PhaserMixKnob: Knob!
    @IBOutlet weak var PhaserFreqKnob: Knob!
    
    @IBOutlet weak var CharacterTypeLbl: UILabel!
    @IBOutlet weak var CharacterIntensityKnob: Knob!
    @IBOutlet weak var CharacterTuneKnob: Knob!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        setupCallbacks()
        
        self.Effects1PresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpdatePresets), name: Notification.Name("UpdateView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        self.Effects1PresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ Effects1PresetsTable: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patchLibrary.objects( subPatchEffects.self).count
    }
    
    func tableView(_ Effects1PresetsTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (self.Effects1PresetsTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = patchLibrary.objects(subPatchEffects.self)[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ Effects1PresetsTable: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedRow = indexPath
        if (patchCtrl.LoadCharacterPatch(NewPatch: patchLibrary.objects(subPatchEffects.self)[indexPath.row]) )
        {
            updateView()
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.currentSelectedRow = indexPath
            let deleteObject = patchLibrary.objects(subPatchEffects.self)[indexPath.row]
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.delete(deleteObject)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    
    @objc func UpdatePresets(){
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.Effects1PresetsTable.reloadData()
    }
    
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        
        ChorusTypeLbl.text = ChorusTypes[safe: patchCtrl.CurrentPatch.chorus_Type]
        ChorusMixKnob.value = Double(patchCtrl.CurrentPatch.chorus_Mix)
        ChorusDelayKnob.value = Double(patchCtrl.CurrentPatch.chorus_Delay)
        
        switch patchCtrl.CurrentPatch.chorus_Type {
        case 0:
            ChorusMixKnob.enabled = false
            ChorusDelayKnob.enabled = false
        case 1:
            ChorusMixKnob.enabled = true
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "Delay"
        case 4:
            ChorusMixKnob.enabled = false
        case 5:
            ChorusMixKnob.enabled = false
        case 6:
            ChorusMixKnob.enabled = true
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "Mic Angle"
        default:
            //2,3,4,5
            ChorusMixKnob.enabled = true
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "X-over"
        }

        DistortionMixKnob.value = Double(patchCtrl.CurrentPatch.distortion_Mix)
        DistortionIntensityKnob.value = Double(patchCtrl.CurrentPatch.distortion_Intensity)
        switch patchCtrl.CurrentPatch.distortion_Type {
        case 0:
            DistortionMixKnob.enabled = false
            DistortionIntensityKnob.enabled = false
        case 20,21,22,23,24,25:
            DistortionMixKnob.enabled = true
            DistortionIntensityKnob.enabled = false
        default:
            DistortionMixKnob.enabled = true
            DistortionIntensityKnob.enabled = true
        }
        
        switch patchCtrl.CurrentPatch.frequencyShifter_Type {
        case 0:
            FilterBankMixKnob.enabled = false
            FilterBankFreqKnob.enabled = false
        default:
            FilterBankMixKnob.enabled = true
            FilterBankFreqKnob.enabled = true
        }
        FilterBankMixKnob.value = Double(patchCtrl.CurrentPatch.frequencyShifter_Mix)
        FilterBankFreqKnob.value = Double(patchCtrl.CurrentPatch.frequencyShifter_Frequency)
        
        
        PhaserMixKnob.value = Double(patchCtrl.CurrentPatch.phaser_Mix)
        PhaserFreqKnob.value = Double(patchCtrl.CurrentPatch.phaser_Frequency)
        
        switch patchCtrl.CurrentPatch.character_Type {
        case 0,7,8:
            CharacterIntensityKnob.enabled = true
            CharacterTuneKnob.enabled = true
        default:
            CharacterIntensityKnob.enabled = false
            CharacterTuneKnob.enabled = false
        }
        
        CharacterIntensityKnob.value = Double(patchCtrl.CurrentPatch.character_Intensity)
        CharacterTuneKnob.value = Double(patchCtrl.CurrentPatch.character_Tune)
        
        
        DistortionTypeLbl.text = DistortionTypes[safe: patchCtrl.CurrentPatch.distortion_Type]
        FilterBankTypeLbl.text = FilterBankTypes[safe: patchCtrl.CurrentPatch.frequencyShifter_Type]
        CharacterTypeLbl.text = CharacterTypes[safe: patchCtrl.CurrentPatch.character_Type]
        
        LowEqGainKnob.value = Double(patchCtrl.CurrentPatch.eq_LowGain)
        LowEqFreqKnob.value = Double(patchCtrl.CurrentPatch.eq_LowFrequency)
        MidEqGainKnob.value = Double(patchCtrl.CurrentPatch.eq_MidGain)
        MidEqFreqKnob.value = Double(patchCtrl.CurrentPatch.eq_MidFrequency)
        MidEqQKnob.value = Double(patchCtrl.CurrentPatch.eq_MidQFactor)
        HiEqGainKnob.value = Double(patchCtrl.CurrentPatch.eq_HighGain)
        HiEqFreqKnob.value = Double(patchCtrl.CurrentPatch.eq_HighFrequency)
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        LowEqGainKnob.callback = { value in patchCtrl.setEQLowGain(value: Int(value)) }
        LowEqFreqKnob.callback = { value in patchCtrl.setEQLowFrequency(value: Int(value)) }
        MidEqGainKnob.callback = { value in patchCtrl.setEQMidGain(value: Int(value)) }
        MidEqFreqKnob.callback = { value in patchCtrl.setEQMidFrequency(value: Int(value)) }
        MidEqQKnob.callback = { value in patchCtrl.setEQMidQFactor(value: Int(value)) }
        HiEqGainKnob.callback = { value in patchCtrl.setEQHighGain(value: Int(value)) }
        HiEqFreqKnob.callback = { value in patchCtrl.setEQHighFrequency(value: Int(value)) }
        
        ChorusMixKnob.callback = { value in
            if ( patchCtrl.CurrentPatch.chorus_Type == 1 ){
                patchCtrl.setChorusMixClassic(value: Int(value))
            } else {
                patchCtrl.setChorusMix(value: Int(value))
            }
        }
        ChorusDelayKnob.callback = { value in
            switch patchCtrl.CurrentPatch.chorus_Type {
            case 1:
                patchCtrl.setChorusDelay(value: Int(value))
            case 6:
                patchCtrl.setChorusDelay(value: Int(value))
            default:
                patchCtrl.setChorusXOver(value: Int(value))
            }
        }
        
        
        DistortionMixKnob.callback = { value in patchCtrl.setDistortionMix(value: Int(value))}
        DistortionIntensityKnob.callback = { value in patchCtrl.setDistortionIntensity(value: Int(value))}
        
        FilterBankMixKnob.callback = { value in patchCtrl.setFrequencyShifterMix(value: Int(value))}
        FilterBankFreqKnob.callback = { value in patchCtrl.setFrequencyShifterFrequency(value: Int(value))}
        
        PhaserMixKnob.callback = { value in patchCtrl.setPhaserMix(value: Int(value))}
        PhaserFreqKnob.callback = { value in patchCtrl.setPhaserFrequency(value: Int(value))}

        CharacterTuneKnob.callback = { value in patchCtrl.setCharacterTune(value: Int(value))}
        CharacterIntensityKnob.callback = { value in patchCtrl.setCharacterIntensity(value: Int(value))}
    }
    
    @IBAction func SaveNewSubPatch(_ sender: UIButton) {
        self.Effects1PresetsTable.reloadData()
        showInputDialog(title: "Save new arpeggiator preset",
                        subtitle: "Save current arpeggiator settings as preset",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New preset name",
                        inputKeyboardType: .alphabet,
                        actionHandler: { (input:String?) in
                                print("The name is \(input ?? "")")
                                if (input != ""){
                                    patchCtrl.CharacterPatch.name = input!
                                    
                                    let realm = try! Realm(configuration: config)
                                    
                                    try! realm.write {
                                        realm.create(subPatchEffects.self, value: patchCtrl.CharacterPatch)
                                        realm.refresh()
                                    }
                                }
                                self.Effects1PresetsTable.reloadData()
                                let realm = try! Realm(configuration: config)
                                realm.refresh()
                                
                                self.UpdatePresets()
                            })
    }
}
