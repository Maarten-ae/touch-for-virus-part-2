//
//  SVC_Arp_QuickPresets.swift
//  Rage
//
//  Created by M. Lierop on 11-02-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ArpQuickPresetsSubView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var ArpModeLbls: ArpModesSrc?
    private var ArpPatternLbls: ArpPatternsSrc?
    private var ArpOct: ArpOctSrc?
    private var ArpResolutionLbls: ArpResolutionsSrc?
    
    @IBOutlet weak var ArpPresetsTable: UITableView!
    var currentSelectedRow: IndexPath!
    var currentScrollPosition: UITableView.ScrollPosition!
    
    @IBOutlet weak var ArpModeBtn: PMSuperButton!
    @IBOutlet weak var ArpPatternBtn: PMSuperButton!
    @IBOutlet weak var ArpOctRangeBtn: PMSuperButton!
    @IBOutlet weak var ArpResolutionBtn: PMSuperButton!
    
    @IBOutlet weak var NoteLengthKnob: Knob!
    @IBOutlet weak var SwingKnob: Knob!
    
    @IBOutlet weak var TempoKnob: Knob!
    @IBOutlet weak var TempoLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        setupCallbacks()
        
        ArpModeLbls = ArpModesSrc()
        ArpPatternLbls = ArpPatternsSrc()
        ArpOct = ArpOctSrc()
        ArpResolutionLbls = ArpResolutionsSrc()
        
        self.ArpPresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("ArpSettingsChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        self.ArpPresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ ArpPresetsTable: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patchLibrary.objects(subPatchArp.self).count
    }
    
    func tableView(_ ArpPresetsTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (self.ArpPresetsTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = patchLibrary.objects(subPatchArp.self)[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ ArpPresetsTable: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedRow = indexPath
        if (patchCtrl.LoadArpPatch(NewPatch: patchLibrary.objects(subPatchArp.self)[indexPath.row]) )
        {
            updateView()
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.currentSelectedRow = indexPath
            let deleteObject = patchLibrary.objects(subPatchArp.self)[indexPath.row]
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.delete(deleteObject)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    
    @objc func UpdatePresets(){
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.ArpPresetsTable.reloadData()
    }
    
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        NoteLengthKnob.value = Double( patchCtrl.CurrentPatch.arp_noteLength)
        SwingKnob.value = Double( patchCtrl.CurrentPatch.arp_swingFactor )
        ArpModeBtn.setTitle(ArpModeLabels[safe: patchCtrl.CurrentPatch.arp_mode], for: .normal)
        ArpPatternBtn.setTitle(ArpPatternLabels[safe: patchCtrl.CurrentPatch.arp_pattern], for: .normal)
        ArpOctRangeBtn.setTitle(ArpOctaveRangeLabels[safe: patchCtrl.CurrentPatch.arp_range], for: .normal)
        ArpResolutionBtn.setTitle(ArpClockLabels[safe: patchCtrl.CurrentPatch.arp_clock], for: .normal)
        
        TempoKnob.value = Double( patchCtrl.CurrentPatch.arp_tempo )
        self.TempoLbl.text =  String(patchCtrl.CurrentPatch.arp_tempo + 63) + " bpm"
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        NoteLengthKnob.callback = { value in
            patchCtrl.setArpNoteLength(value: Int(value))
        }
        
        SwingKnob.callback = { value in
            patchCtrl.setArpSwingFactor(value: Int(value))
        }
        
        TempoKnob.callback = { value in
            patchCtrl.setArpTempo(value: Int(value))
            self.TempoLbl.text =  String(patchCtrl.CurrentPatch.arp_tempo + 63) + " bpm"
        }
    }
    
    
    @IBAction func ArpModechanged(_ sender: PMSuperButton) {
        let ArpModesPopover = ArpModePopover()
        ArpModesPopover.tableView.dataSource = ArpModeLbls!
        ArpModesPopover.tableView.delegate = ArpModesPopover
        ArpModesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ArpModesPopover, animated: true, completion: nil)
        let popoverPresentationController = ArpModesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func ArpPatternchanged(_ sender: PMSuperButton) {
        let ArpPattersnPopover = ArpPatternPopover()
        ArpPattersnPopover.tableView.dataSource = ArpPatternLbls!
        ArpPattersnPopover.tableView.delegate = ArpPattersnPopover
        ArpPattersnPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ArpPattersnPopover, animated: true, completion: nil)
        let popoverPresentationController = ArpPattersnPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func OctRangechanged(_ sender: PMSuperButton) {
        let ArpOctRangePopover = ArpOctPopover()
        ArpOctRangePopover.tableView.dataSource = ArpOct!
        ArpOctRangePopover.tableView.delegate = ArpOctRangePopover
        ArpOctRangePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ArpOctRangePopover, animated: true, completion: nil)
        let popoverPresentationController = ArpOctRangePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func ResolutionKnob(_ sender: PMSuperButton) {
        let ArpResoPopover = ArpResolutionPopover()
        ArpResoPopover.tableView.dataSource = ArpResolutionLbls!
        ArpResoPopover.tableView.delegate = ArpResoPopover
        ArpResoPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ArpResoPopover, animated: true, completion: nil)
        let popoverPresentationController = ArpResoPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func SaveNewSubPatch(_ sender: UIButton) {
        self.ArpPresetsTable.reloadData()
        showInputDialog(title: "Save new arpeggiator preset",
                        subtitle: "Save current arpeggiator settings as preset",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New preset name",
                        inputKeyboardType: .alphabet,
                        actionHandler: { (input:String?) in
                            print("The name is \(input ?? "")")
                            if (input != ""){
                                patchCtrl.ArpPatch.name = input!
                                
                                let realm = try! Realm(configuration: config)
                                
                                try! realm.write {
                                    realm.create(subPatchArp.self, value: patchCtrl.ArpPatch)
                                    realm.refresh()
                                }
                            }
                            self.ArpPresetsTable.reloadData()
                            let realm = try! Realm(configuration: config)
                            realm.refresh()
                            
                            self.UpdatePresets()
                        })
    }
    
}
