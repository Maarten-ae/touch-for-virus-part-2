//
//  SVC_Filters_QuickPresets.swift
//  Rage
//
//  Created by M. Lierop on 06-12-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class FilterQuickPresetsSubViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var FilterEnvControl: EnvelopeChart!
    
    @IBOutlet weak var AmplifierEnvControl: EnvelopeChart!
    
    @IBOutlet weak var Filter1ModeSelect: UISegmentedControl!
    @IBOutlet weak var Filter1cutoffKnob: Knob!
    @IBOutlet weak var Filter1ResoKnob: Knob!
    var Filter1AnalogMode = true;
    
    @IBOutlet weak var Filter2ModeSelect: UISegmentedControl!
    @IBOutlet weak var Filter2ResoKnob: Knob!
    @IBOutlet weak var Filter2CutoffKnob: Knob!
    
    @IBOutlet weak var FilterBalanceKnob: Knob!
    
    @IBOutlet weak var FilterPresetsTable: UITableView!
    var currentSelectedRow: IndexPath!
    var currentScrollPosition: UITableView.ScrollPosition!
    
    @IBOutlet weak var vertSlider: VerticalSlider!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallbacks()
        
        self.FilterPresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.FilterEnvControl.envelopePrefix = "Filter"
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvAttack(notification:)), name: Notification.Name("FilterEnvelope_Attack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvDecay(notification:)), name: Notification.Name("FilterEnvelope_Decay"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvSustain(notification:)), name: Notification.Name("FilterEnvelope_Sustain"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvSustainSlope(notification:)), name: Notification.Name("FilterEnvelope_SustainSlope"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvRelease(notification:)), name: Notification.Name("FilterEnvelope_Release"), object: nil)
        
        self.AmplifierEnvControl.envelopePrefix = "Amp"
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAmpEnvAttack(notification:)), name: Notification.Name("AmpEnvelope_Attack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAmpEnvDecay(notification:)), name: Notification.Name("AmpEnvelope_Decay"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAmpEnvSustain(notification:)), name: Notification.Name("AmpEnvelope_Sustain"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAmpEnvSustainSlope(notification:)), name: Notification.Name("AmpEnvelope_SustainSlope"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateAmpEnvRelease(notification:)), name: Notification.Name("AmpEnvelope_Release"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
        vertSlider.slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        vertSlider.slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        
        updateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten

        updateView()
        super.viewWillAppear(animated)
        self.FilterPresetsTable.reloadData()
        self.FilterPresetsTable.selectRow(at: currentSelectedRow, animated: true, scrollPosition: UITableView.ScrollPosition(rawValue: 0)!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ FilterPresetsTable: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patchLibrary.objects(subPatchFilters.self).count
    }
    
    func tableView(_ FilterPresetsTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell! = self.FilterPresetsTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = patchLibrary.objects(subPatchFilters.self)[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ FilterPresetsTable: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedRow = indexPath
        if (patchCtrl.LoadFilterPatch(NewPatch: patchLibrary.objects(subPatchFilters.self)[indexPath.row]) )
        {
           updateView()
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        return true
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.currentSelectedRow = indexPath
            let deleteObject = patchLibrary.objects(subPatchFilters.self)[indexPath.row]
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.delete(deleteObject)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }

    
    @objc func UpdatePresets(){
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.FilterPresetsTable.reloadData()
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        if ( patchCtrl.CurrentPatch.filter1_Mode >= 4 ){
            Filter1ModeSelect.setTitle("LP 1", forSegmentAt: 0)
            Filter1ModeSelect.setTitle("LP 2", forSegmentAt: 1)
            Filter1ModeSelect.setTitle("LP 3", forSegmentAt: 2)
            Filter1ModeSelect.setTitle("LP 4", forSegmentAt: 3)
            Filter1ModeSelect.selectedSegmentIndex = patchCtrl.CurrentPatch.filter1_Mode - 4
            Filter1AnalogMode = true
        } else {
            Filter1ModeSelect.setTitle("LP", forSegmentAt: 0)
            Filter1ModeSelect.setTitle("HP", forSegmentAt: 1)
            Filter1ModeSelect.setTitle("BP", forSegmentAt: 2)
            Filter1ModeSelect.setTitle("BS", forSegmentAt: 3)
            Filter1ModeSelect.selectedSegmentIndex = patchCtrl.CurrentPatch.filter1_Mode
            Filter1AnalogMode = false
        }
        
        Filter1cutoffKnob.value = Double(patchCtrl.CurrentPatch.filter1_Cutoff)
        Filter1ResoKnob.value = Double(patchCtrl.CurrentPatch.filter1_Resonance)
        Filter2ModeSelect.selectedSegmentIndex = patchCtrl.CurrentPatch.filter2_Mode
        Filter2ResoKnob.value = Double(patchCtrl.CurrentPatch.filter2_Resonance)
        Filter2CutoffKnob.value = Double(patchCtrl.CurrentPatch.filter2_Cutoff)
        FilterBalanceKnob.value = Double(patchCtrl.CurrentPatch.filter_Balance)
        
        vertSlider.value = Float(patchCtrl.CurrentPatch.amp_PatchVolume)
        
        self.FilterEnvControl.setEnvelopeValues(
            attack: patchCtrl.CurrentPatch.filterEnvelope_Attack,
            decay: patchCtrl.CurrentPatch.filterEnvelope_Decay,
            sustain: patchCtrl.CurrentPatch.filterEnvelope_Sustain,
            sustainSlope: patchCtrl.CurrentPatch.filterEnvelope_SustainSlope,
            release: patchCtrl.CurrentPatch.filterEnvelope_Release)
        self.AmplifierEnvControl.setEnvelopeValues(
            attack: patchCtrl.CurrentPatch.amplifierEnvelope_Attack,
            decay: patchCtrl.CurrentPatch.amplifierEnvelope_Decay,
            sustain: patchCtrl.CurrentPatch.amplifierEnvelope_Sustain,
            sustainSlope: patchCtrl.CurrentPatch.amplifierEnvelope_SustainSlope,
            release: patchCtrl.CurrentPatch.amplifierEnvelope_Release)
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {

        Filter1cutoffKnob.callback = { value in
            patchCtrl.setFilter1Cutoff(value: Int(value))
        }
        Filter1ResoKnob.callback = { value in
            patchCtrl.setFilter1Resonance(value: Int(value))
        }
        Filter2ResoKnob.callback = { value in
            patchCtrl.setFilter2Resonance(value: Int(value))
        }
        Filter2CutoffKnob.callback = { value in
            patchCtrl.setFilter2Cutoff(value: Int(value))
        }
        FilterBalanceKnob.callback = { value in
            patchCtrl.setFilterBalance(value: Int(value))
        }
    }
    
    @objc func updateFilterEnvAttack(notification: Notification){
        patchCtrl.setFilterEnvelopeAttack(value: FilterEnvControl.attackTime!)
    }
    @objc func updateFilterEnvDecay(notification: Notification){
        patchCtrl.setFilterEnvelopeDecay(value: FilterEnvControl.decayTime!)
    }
    @objc func updateFilterEnvSustain(notification: Notification){
        patchCtrl.setFilterEnvelopeSustain(value: FilterEnvControl.sustainLvl!)
    }
    @objc func updateFilterEnvSustainSlope(notification: Notification){
        patchCtrl.setFilterEnvelopeSustainSlope(value: FilterEnvControl.sustainSlopeLvl!)
    }
    @objc func updateFilterEnvRelease(notification: Notification){
        patchCtrl.setFilterEnvelopeRelease(value: Int(FilterEnvControl.releaseTime!))
    }
    
    @objc func updateAmpEnvAttack(notification: Notification){
        patchCtrl.setAmplifierEnvelopeAttack(value: AmplifierEnvControl.attackTime!)
    }
    @objc func updateAmpEnvDecay(notification: Notification){
        patchCtrl.setAmplifierEnvelopeDecay(value: AmplifierEnvControl.decayTime!)
    }
    @objc func updateAmpEnvSustain(notification: Notification){
        patchCtrl.setAmplifierEnvelopeSustain(value: AmplifierEnvControl.sustainLvl!)
    }
    @objc func updateAmpEnvSustainSlope(notification: Notification){
        patchCtrl.setAmplifierEnvelopeSustainSlope(value: AmplifierEnvControl.sustainSlopeLvl!)
    }
    @objc func updateAmpEnvRelease(notification: Notification){
        patchCtrl.setAmplifierEnvelopeRelease(value: AmplifierEnvControl.releaseTime!)
    }
    
    @IBAction func filter1ModeChange(_ sender: UISegmentedControl) {
        if (Filter1AnalogMode){
            patchCtrl.setFilter1Mode(value: Int(sender.selectedSegmentIndex + 4))
        } else {
            patchCtrl.setFilter1Mode(value: Int(sender.selectedSegmentIndex))
        }
    }
    
    @IBAction func filter2ModeChange(_ sender: UISegmentedControl) {
        patchCtrl.setFilter2Mode(value: Int(sender.selectedSegmentIndex))
    }
    
    @IBAction func onchangePatchVolume(_ sender: VerticalSlider) {
        patchCtrl.setPatchVolume(value: Int( vertSlider.value ))
    }
    
    @IBAction func SaveNewSubPatch(_ sender: UIButton) {
        self.FilterPresetsTable.reloadData()
        showInputDialog(title: "Save new filter preset",
                        subtitle: "Save current filter settings as preset",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New preset name",
                        inputKeyboardType: .alphabet,
                        actionHandler: { (input:String?) in
                            print("The name is \(input ?? "")")
                            if (input != ""){
                                patchCtrl.FilterPatch.name = input!
                                
                                let realm = try! Realm(configuration: config)
                                
                                try! realm.write {
                                    realm.create(subPatchFilters.self, value: patchCtrl.FilterPatch)
                                    realm.refresh()
                                }
                            }
                            self.FilterPresetsTable.reloadData()
                            let realm = try! Realm(configuration: config)
                            realm.refresh()
                            
                            self.UpdatePresets()
                        })
    }
    
}
