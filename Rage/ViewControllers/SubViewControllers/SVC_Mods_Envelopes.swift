//
//  SVC_Mods_Envelopes.swift
//  Rage
//
//  Created by M. Lierop on 09-01-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//


import Foundation
import UIKit

class ModsEnvelopesSubView: UIViewController {
    
    private var matrixSrc:MatrixSources?
    private var matrixDest:MatrixDestinations?
    
    @IBOutlet weak var MatrixSlot5SrcBtn: PMSuperButton!
    @IBOutlet weak var MatrixSlot5Dest1Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot5Dest2Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot5Dest3Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot5Dest1Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot5Dest2Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot5Dest3Slider: HorizontalSlider!
    
    @IBOutlet weak var MatrixSlot6SrcBtn: PMSuperButton!
    @IBOutlet weak var MatrixSlot6Dest1Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot6Dest2Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot6Dest3Btn: PMSuperButton!
    @IBOutlet weak var MatrixSlot6Dest1Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot6Dest2Slider: HorizontalSlider!
    @IBOutlet weak var MatrixSlot6Dest3Slider: HorizontalSlider!
    
    @IBOutlet weak var Env3Control: EnvelopeChart!
    @IBOutlet weak var Env4Control: EnvelopeChart!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("MatrixSlotSourceChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("MatrixSlotDestinationChanged"), object: nil)
        
        self.Env3Control.envelopePrefix = "Env3"
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv3Attack(notification:)), name: Notification.Name("Env3Envelope_Attack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv3Decay(notification:)), name: Notification.Name("Env3Envelope_Decay"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv3Sustain(notification:)), name: Notification.Name("Env3Envelope_Sustain"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv3SustainSlope(notification:)), name: Notification.Name("Env3Envelope_SustainSlope"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv3Release(notification:)), name: Notification.Name("Env3Envelope_Release"), object: nil)
        
        self.Env4Control.envelopePrefix = "Env4"
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv4Attack(notification:)), name: Notification.Name("Env4Envelope_Attack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv4Decay(notification:)), name: Notification.Name("Env4Envelope_Decay"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv4Sustain(notification:)), name: Notification.Name("Env4Envelope_Sustain"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv4SustainSlope(notification:)), name: Notification.Name("Env4Envelope_SustainSlope"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateEnv4Release(notification:)), name: Notification.Name("Env4Envelope_Release"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
        MatrixSlot5Dest1Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot5Dest1Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot5Dest1Slider.EnableResetGesture()
        
        MatrixSlot5Dest2Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot5Dest2Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot5Dest2Slider.EnableResetGesture()
        
        MatrixSlot5Dest3Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot5Dest3Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot5Dest3Slider.EnableResetGesture()
        
        MatrixSlot6Dest1Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot6Dest1Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot6Dest1Slider.EnableResetGesture()
        
        MatrixSlot6Dest2Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot6Dest2Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot6Dest2Slider.EnableResetGesture()
        
        MatrixSlot6Dest3Slider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        MatrixSlot6Dest3Slider.setThumbImage( UIImage(named: "SliderHor-Tch"),for: .highlighted)
        MatrixSlot6Dest3Slider.EnableResetGesture()
        
        matrixSrc = MatrixSources()
        matrixDest = MatrixDestinations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        
        MatrixSlot5SrcBtn.setTitle( MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot5_Source], for: .normal)
        MatrixSlot5Dest1Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot5_Destination1 ], for: .normal)
        MatrixSlot5Dest2Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot5_Destination2 ], for: .normal)
        MatrixSlot5Dest3Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot5_Destination3 ], for: .normal)
        MatrixSlot5Dest1Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot5_Amount1)
        MatrixSlot5Dest1Slider.showValue()
        MatrixSlot5Dest2Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot5_Amount2)
        MatrixSlot5Dest2Slider.showValue()
        MatrixSlot5Dest3Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot5_Amount3)
        MatrixSlot5Dest3Slider.showValue()
        
        MatrixSlot6SrcBtn.setTitle( MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot6_Source], for: .normal)
        MatrixSlot6Dest1Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot6_Destination1 ], for: .normal)
        MatrixSlot6Dest2Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot6_Destination2 ], for: .normal)
        MatrixSlot6Dest3Btn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.matrixSlot6_Destination3 ], for: .normal)
        MatrixSlot6Dest1Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot6_Amount1)
        MatrixSlot6Dest1Slider.showValue()
        MatrixSlot6Dest2Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot6_Amount2)
        MatrixSlot6Dest2Slider.showValue()
        MatrixSlot6Dest3Slider.value = Float(patchCtrl.CurrentPatch.matrixSlot6_Amount3)
        MatrixSlot6Dest3Slider.showValue()
        
        self.Env3Control.setEnvelopeValues(
            attack: patchCtrl.CurrentPatch.envelope3_Attack,
            decay: patchCtrl.CurrentPatch.envelope3_Decay,
            sustain: patchCtrl.CurrentPatch.envelope3_Sustain,
            sustainSlope: patchCtrl.CurrentPatch.envelope3_SustainSlope,
            release: patchCtrl.CurrentPatch.envelope3_Release)
        self.Env4Control.setEnvelopeValues(
            attack: patchCtrl.CurrentPatch.envelope4_Attack,
            decay: patchCtrl.CurrentPatch.envelope4_Decay,
            sustain: patchCtrl.CurrentPatch.envelope4_Sustain,
            sustainSlope: patchCtrl.CurrentPatch.envelope4_SustainSlope,
            release: patchCtrl.CurrentPatch.envelope4_Release)
        
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        
    }
    
    @IBAction func MatrixSlotDestinationsliderChange(_ sender: UISlider) {
        switch (sender.restorationIdentifier!){
        case "5a":
            patchCtrl.setMatrixSlot5Amount1(value: Int(sender.value))
            MatrixSlot5Dest1Slider.showValue()
        case "5b":
            patchCtrl.setMatrixSlot5Amount2(value: Int(sender.value))
            MatrixSlot5Dest2Slider.showValue()
        case "5c":
            patchCtrl.setMatrixSlot5Amount3(value: Int(sender.value))
            MatrixSlot5Dest3Slider.showValue()
            
        case "6a":
            patchCtrl.setMatrixSlot6Amount1(value: Int(sender.value))
            MatrixSlot6Dest1Slider.showValue()
        case "6b":
            patchCtrl.setMatrixSlot6Amount2(value: Int(sender.value))
            MatrixSlot6Dest2Slider.showValue()
        case "6c":
            patchCtrl.setMatrixSlot6Amount3(value: Int(sender.value))
            MatrixSlot6Dest3Slider.showValue()
        
        default:
            print("onbekend")
        }
        //print("slider nr ",sender.restorationIdentifier!)
    }
    
    
    //*****************************************************************
    // MARK: - MAtrix Slot 6
    //*****************************************************************
    
    @IBAction func Slot5SourceClick(_ sender: UIButton) {
        let matrixslot1SrcPopover = MatrixSourcePopover()
        matrixslot1SrcPopover.matrixSlotNr = 5
        matrixslot1SrcPopover.tableView.dataSource = matrixSrc!
        matrixslot1SrcPopover.tableView.delegate = matrixslot1SrcPopover
        matrixslot1SrcPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1SrcPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1SrcPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot5Dest1Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 5
        matrixslot1DestPopover.matrixSlotDestNr = 1
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot5Dest2Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 5
        matrixslot1DestPopover.matrixSlotDestNr = 2
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot5Dest3Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 5
        matrixslot1DestPopover.matrixSlotDestNr = 3
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }

    
    //*****************************************************************
    // MARK: - MAtrix Slot 6
    //*****************************************************************
    
    @IBAction func Slot6SourceClick(_ sender: UIButton) {
        let matrixslot1SrcPopover = MatrixSourcePopover()
        matrixslot1SrcPopover.matrixSlotNr = 6
        matrixslot1SrcPopover.tableView.dataSource = matrixSrc!
        matrixslot1SrcPopover.tableView.delegate = matrixslot1SrcPopover
        matrixslot1SrcPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1SrcPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1SrcPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot6Dest1Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 6
        matrixslot1DestPopover.matrixSlotDestNr = 1
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot6Dest2Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 6
        matrixslot1DestPopover.matrixSlotDestNr = 2
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Slot6Dest3Click(_ sender: UIButton) {
        let matrixslot1DestPopover = MatrixDestinationsPopover()
        matrixslot1DestPopover.matrixSlotNr = 6
        matrixslot1DestPopover.matrixSlotDestNr = 3
        matrixslot1DestPopover.tableView.dataSource = matrixDest!
        matrixslot1DestPopover.tableView.delegate = matrixslot1DestPopover
        matrixslot1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(matrixslot1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = matrixslot1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    //*****************************************************************
    // MARK: - Envelope 3
    //*****************************************************************
    
    @objc func updateEnv3Attack(notification: Notification){
        print("filter attack")
        patchCtrl.setEnvelope3Attack(value: Env3Control.attackTime!)
    }
    @objc func updateEnv3Decay(notification: Notification){
        print("filter decay")
        patchCtrl.setEnvelope3Decay(value: Env3Control.decayTime!)
    }
    @objc func updateEnv3Sustain(notification: Notification){
        print("filter sustain")
        patchCtrl.setEnvelope3Sustain(value: Env3Control.sustainLvl!)
    }
    @objc func updateEnv3SustainSlope(notification: Notification){
        print("filter sustainSlope")
        patchCtrl.setEnvelope3SustainSlope(value: Env3Control.sustainSlopeLvl!)
    }
    @objc func updateEnv3Release(notification: Notification){
        patchCtrl.setEnvelope3Release(value: Int(Env3Control.releaseTime!))
    }
    
    //*****************************************************************
    // MARK: - Envelope 4
    //*****************************************************************
    
    @objc func updateEnv4Attack(notification: Notification){
        print("amp attack ")
        patchCtrl.setEnvelope4Attack(value: Env4Control.attackTime!)
    }
    @objc func updateEnv4Decay(notification: Notification){
        print("amp decay")
        patchCtrl.setEnvelope4Decay(value: Env4Control.decayTime!)
    }
    @objc func updateEnv4Sustain(notification: Notification){
        print("amp sustain")
        patchCtrl.setEnvelope4Sustain(value: Env4Control.sustainLvl!)
    }
    @objc func updateEnv4SustainSlope(notification: Notification){
        print("amp sustainSlope")
        patchCtrl.setEnvelope4SustainSlope(value: Env4Control.sustainSlopeLvl!)
    }
    @objc func updateEnv4Release(notification: Notification){
        print("amp release")
        patchCtrl.setEnvelope4Release(value: Env4Control.releaseTime!)
    }
}
