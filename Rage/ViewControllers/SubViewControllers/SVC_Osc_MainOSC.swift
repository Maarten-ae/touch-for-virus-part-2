//
//  SVC_Osc_MainOSC.swift
//  Rage
//
//  Created by M. Lierop on 10-11-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class OscMainOscillatorsSubViewController: UIViewController {
    
    private var OscModelsLbls:OscModelsSrc?
    private var Osc1WaveLbls:Osc1WaveSrc?
    private var Osc2WaveLbls:Osc2WaveSrc?
    private var OscFMModeLbls:FMModesSrc?

    var OscKnobLabels: oscKnobs = oscKnobs()
    
    // Oscillator 1
    @IBOutlet weak var Osc1BtnModel: PMSuperButton!
    @IBOutlet weak var Osc1BtnWaveSelect: PMSuperButton!
    @IBOutlet weak var Osc1KnobWaveShape :Knob!
    @IBOutlet weak var Osc1KnobPulseWidth :Knob!
    @IBOutlet weak var Osc1KnobFormShift :Knob!
    @IBOutlet weak var Osc1KnobFormSpread :Knob!
    @IBOutlet weak var Osc1KnobLocalDetune :Knob!
    @IBOutlet weak var Osc1KnobInterpolation :Knob!
    @IBOutlet weak var Osc1KnobSemiTone :Knob!
    @IBOutlet weak var Osc1KnobKeyFollow :Knob!
    
    // Oscillator 2
    @IBOutlet weak var Osc2BtnModel: PMSuperButton!
    @IBOutlet weak var Osc2BtnWaveSelect: PMSuperButton!
    @IBOutlet weak var Osc2KnobWaveShape :Knob!
    @IBOutlet weak var Osc2KnobPulseWidth :Knob!
    @IBOutlet weak var Osc2KnobFormShift :Knob!
    @IBOutlet weak var Osc2KnobFormSpread :Knob!
    @IBOutlet weak var Osc2KnobLocalDetune :Knob!
    @IBOutlet weak var Osc2KnobInterpolation :Knob!
    @IBOutlet weak var Osc2KnobSemiTone :Knob!
    @IBOutlet weak var Osc2KnobKeyFollow :Knob!
    
    // Sub Oscillator
    @IBOutlet weak var OscSubKnobVolume: Knob!
    @IBOutlet weak var OscSubSwitchWaveForm: UISwitch!
    
    // Osc Section
    @IBOutlet weak var OscKnobInitPhase: Knob!
    @IBOutlet weak var OscKnobPortamento: Knob!
    @IBOutlet weak var OscKnobEnvelopeToOsc2: Knob!
    
    @IBOutlet weak var OscSyncBtn: ToggleButton!
    @IBOutlet weak var OscKnobFMAmount: Knob!
    @IBOutlet weak var OscKnobEnvToFM: Knob!
    
    @IBOutlet weak var OscSliderBalance: UISlider!
    @IBOutlet weak var OscBtnFMMode: PMSuperButton!
    
    @IBOutlet weak var OscKnobRingModVolume: Knob!
    //@IBOutlet weak var OscKnobRingModMix: Knob!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallbacks()

        OscSliderBalance.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        OscSliderBalance.setThumbImage( UIImage(named: "SliderHor-Tch"), for: .highlighted)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("OscModelChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("OscWaveSelectChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("FMModeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
        OscModelsLbls = OscModelsSrc()
        Osc1WaveLbls = Osc1WaveSrc()
        Osc2WaveLbls = Osc2WaveSrc()
        OscFMModeLbls = FMModesSrc()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        updateView()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        // Oscillator 1
        Osc1BtnModel.setTitle( OscModelLabels[safe: patchCtrl.CurrentPatch.osc1_Model], for: .normal)
        Osc1KnobWaveShape.value = Double (patchCtrl.CurrentPatch.osc1_WaveformShape)
        Osc1KnobWaveShape.label = OscKnobLabels.getWaveShapeLabel(oscNr: 1)
        
        Osc1KnobPulseWidth.value = Double(patchCtrl.CurrentPatch.osc1_Pulsewidth)
        Osc1KnobPulseWidth.label = self.OscKnobLabels.getPulseWidthtLabel(oscNr: 1)
        
        switch(patchCtrl.CurrentPatch.osc1_Model){
        case 0:
            // analog
            if ( patchCtrl.CurrentPatch.osc1_WaveformShape >= 64 ){
                Osc1BtnWaveSelect.isUserInteractionEnabled = false
                Osc1BtnWaveSelect.setTitle("None", for: .normal)
            } else {
                Osc1BtnWaveSelect.isUserInteractionEnabled = true
                Osc1BtnWaveSelect.setTitle( WaveLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
            }
            
            
            if ( patchCtrl.CurrentPatch.osc1_WaveformSelect >= WaveLabels.count ) {
                patchCtrl.CurrentPatch.osc1_WaveformSelect = (WaveLabels.count - 1)
            }
            
            Osc1KnobWaveShape.knobType = "oscwaveshape"
            Osc1KnobPulseWidth.enabled = true
            Osc1KnobFormShift.enabled = false
            Osc1KnobFormSpread.enabled = false
            Osc1KnobLocalDetune.enabled = false
            Osc1KnobInterpolation.enabled = false
            
            // Sub Oscillator
            OscSubKnobVolume.enabled = true
            OscSubSwitchWaveForm.isEnabled = true
            OscSubKnobVolume.value = Double(patchCtrl.CurrentPatch.oscS_Volume)
            OscSubSwitchWaveForm.setOn((patchCtrl.CurrentPatch.oscS_WaveformShape == 1), animated: true)
        case 1:
            // Hyper Saw
            Osc1KnobWaveShape.knobType = "volume"
            Osc1BtnWaveSelect.isUserInteractionEnabled = false
            Osc1BtnWaveSelect.setTitle( "Hyper Saw", for: .normal)
            Osc1KnobPulseWidth.enabled = true
            Osc1KnobFormShift.enabled = false
            Osc1KnobFormSpread.enabled = false
            Osc1KnobLocalDetune.enabled = false
            Osc1KnobInterpolation.enabled = false
            // Sub Oscillator
            OscSubKnobVolume.enabled = false
            OscSubSwitchWaveForm.isEnabled = false
        case 2:
            // Wavetable
            Osc1BtnWaveSelect.isUserInteractionEnabled = true
            Osc1BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
            Osc1KnobWaveShape.knobType = "volume"
            Osc1KnobPulseWidth.enabled = false
            Osc1KnobFormShift.enabled = false
            Osc1KnobFormSpread.enabled = false
            Osc1KnobLocalDetune.enabled = false
            Osc1KnobInterpolation.enabled = true
            // Sub Oscillator
            OscSubKnobVolume.enabled = false
            OscSubSwitchWaveForm.isEnabled = false
        case 3:
            // Wave PWM
            Osc1BtnWaveSelect.isUserInteractionEnabled = true
            Osc1BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
            Osc1KnobWaveShape.knobType = "volume"
            Osc1KnobPulseWidth.enabled = true
            Osc1KnobFormShift.enabled = false
            Osc1KnobFormSpread.enabled = false
            Osc1KnobLocalDetune.enabled = true
            Osc1KnobInterpolation.enabled = true
            // Sub Oscillator
            OscSubKnobVolume.enabled = false
            OscSubSwitchWaveForm.isEnabled = false
        case 4:
            // Grain Simple
            Osc1BtnWaveSelect.isUserInteractionEnabled = true
            Osc1BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
            Osc1KnobWaveShape.knobType = "volume"
            Osc1KnobPulseWidth.enabled = false
            Osc1KnobFormShift.enabled = true
            Osc1KnobFormSpread.enabled = true
            Osc1KnobLocalDetune.enabled = true
            Osc1KnobInterpolation.enabled = true
            // Sub Oscillator
            OscSubKnobVolume.enabled = false
            OscSubSwitchWaveForm.isEnabled = false
        case 5:
            // Grain complex
            Osc1BtnWaveSelect.isUserInteractionEnabled = true
            Osc1BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
            Osc1KnobWaveShape.knobType = "volume"
            Osc1KnobPulseWidth.enabled = false
            Osc1KnobFormShift.enabled = true
            Osc1KnobFormSpread.enabled = true
            Osc1KnobLocalDetune.enabled = true
            Osc1KnobInterpolation.enabled = true
            // Sub Oscillator
            OscSubKnobVolume.enabled = false
            OscSubSwitchWaveForm.isEnabled = false
        case 6:
            // Formant Simple
            Osc1BtnWaveSelect.isUserInteractionEnabled = true
            Osc1BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
            Osc1KnobWaveShape.knobType = "volume"
            Osc1KnobPulseWidth.enabled = false
            Osc1KnobFormShift.enabled = true
            Osc1KnobFormSpread.enabled = true
            Osc1KnobLocalDetune.enabled = true
            Osc1KnobInterpolation.enabled = true
            // Sub Oscillator
            OscSubKnobVolume.enabled = false
            OscSubSwitchWaveForm.isEnabled = false
        case 7:
            // Formant Complex
            Osc1BtnWaveSelect.isUserInteractionEnabled = true
            Osc1BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
            Osc1KnobWaveShape.knobType = "volume"
            Osc1KnobPulseWidth.enabled = false
            Osc1KnobFormShift.enabled = true
            Osc1KnobFormSpread.enabled = true
            Osc1KnobLocalDetune.enabled = true
            Osc1KnobInterpolation.enabled = true
            // Sub Oscillator
            OscSubKnobVolume.enabled = false
            OscSubSwitchWaveForm.isEnabled = false
        default:
            print("Foutje bedankt")
        }

        Osc1KnobFormShift.value = Double(patchCtrl.CurrentPatch.osc1_FormantShift)
        Osc1KnobFormSpread.value = Double(patchCtrl.CurrentPatch.osc1_FormantSpread)
        Osc1KnobLocalDetune.value = Double(patchCtrl.CurrentPatch.osc1_LocalDetune)
        Osc1KnobInterpolation.value = Double(patchCtrl.CurrentPatch.osc1_Interpolation)

        Osc1KnobSemiTone.value = Double(patchCtrl.CurrentPatch.osc1_DetuneInSemitones)
        Osc1KnobKeyFollow.value = Double(patchCtrl.CurrentPatch.osc1_Keyfollow)
        
        // Oscillator 2
        Osc2BtnModel.setTitle( OscModelLabels[safe: patchCtrl.CurrentPatch.osc2_Model] , for: .normal)
        Osc2KnobWaveShape.value = Double(patchCtrl.CurrentPatch.osc2_WaveformShape)
        Osc2KnobWaveShape.label = OscKnobLabels.getWaveShapeLabel(oscNr: 2)
        
        Osc2KnobPulseWidth.value = Double(patchCtrl.CurrentPatch.osc2_Pulsewidth)
        Osc2KnobPulseWidth.label = self.OscKnobLabels.getPulseWidthtLabel(oscNr: 2)
        
        switch(patchCtrl.CurrentPatch.osc2_Model){
        case 0:
            // analog
            if ( patchCtrl.CurrentPatch.osc1_WaveformShape >= 64 ){
                Osc2BtnWaveSelect.isUserInteractionEnabled = false
                Osc2BtnWaveSelect.setTitle("None", for: .normal)
            } else {
                Osc2BtnWaveSelect.isUserInteractionEnabled = true
                Osc2BtnWaveSelect.setTitle( WaveLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
            }
            
            
            if ( patchCtrl.CurrentPatch.osc2_WaveformSelect >= WaveLabels.count ) {
                patchCtrl.CurrentPatch.osc2_WaveformSelect = (WaveLabels.count - 1)
            }
            
            Osc2KnobWaveShape.knobType = "oscwaveshape"
            Osc2KnobPulseWidth.enabled = true
            Osc2KnobFormShift.enabled = false
            Osc2KnobFormSpread.enabled = false
            Osc2KnobLocalDetune.enabled = true
            Osc2KnobLocalDetune.label = "Fine Detune"
            Osc2KnobInterpolation.enabled = false
        case 1:
            // Hyper Saw
            Osc2BtnWaveSelect.isUserInteractionEnabled = false
            Osc2BtnWaveSelect.setTitle( "Hyper Saw", for: .normal)
            Osc2KnobWaveShape.knobType = "volume"
            Osc2KnobPulseWidth.enabled = true
            Osc2KnobFormShift.enabled = false
            Osc2KnobFormSpread.enabled = false
            Osc2KnobLocalDetune.enabled = false
            Osc2KnobLocalDetune.label = "Fine Detune"
            Osc2KnobInterpolation.enabled = false
        case 2:
            // Wavetable
            Osc2BtnWaveSelect.isUserInteractionEnabled = true
            Osc2BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
            Osc2KnobWaveShape.knobType = "volume"
            Osc2KnobPulseWidth.enabled = false
            Osc2KnobFormShift.enabled = false
            Osc2KnobFormSpread.enabled = false
            Osc2KnobLocalDetune.enabled = true
            Osc2KnobLocalDetune.label = "Fine Detune"
            Osc2KnobInterpolation.enabled = true
        case 3:
            // Wave PWM
            Osc2BtnWaveSelect.isUserInteractionEnabled = true
            Osc2BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
            Osc2KnobWaveShape.knobType = "volume"
            Osc2KnobPulseWidth.enabled = true
            Osc2KnobFormShift.enabled = false
            Osc2KnobFormSpread.enabled = false
            Osc2KnobLocalDetune.enabled = true
            Osc2KnobLocalDetune.label = "Fine Detune"
            Osc2KnobInterpolation.enabled = true
        case 4:
            // Grain Simple
            Osc2BtnWaveSelect.isUserInteractionEnabled = true
            Osc2BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
            Osc2KnobWaveShape.knobType = "volume"
            Osc2KnobPulseWidth.enabled = false
            Osc2KnobFormShift.enabled = true
            Osc2KnobFormSpread.enabled = true
            Osc2KnobLocalDetune.enabled = true
            Osc2KnobLocalDetune.label = "Local Detune"
            Osc2KnobInterpolation.enabled = true
        case 5:
            // Grain complex
            Osc2BtnWaveSelect.isUserInteractionEnabled = true
            Osc2BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
            Osc2KnobWaveShape.knobType = "volume"
            Osc2KnobPulseWidth.enabled = false
            Osc2KnobFormShift.enabled = true
            Osc2KnobFormSpread.enabled = true
            Osc2KnobLocalDetune.enabled = true
            Osc2KnobLocalDetune.label = "Local Detune"
            Osc2KnobInterpolation.enabled = true
        case 6:
            // Formant Simple
            Osc2BtnWaveSelect.isUserInteractionEnabled = true
            Osc2BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
                Osc2KnobWaveShape.knobType = "volume"
            Osc2KnobPulseWidth.enabled = false
            Osc2KnobFormShift.enabled = true
            Osc2KnobFormSpread.enabled = true
            Osc2KnobLocalDetune.enabled = true
            Osc2KnobLocalDetune.label = "Local Detune"
            Osc2KnobInterpolation.enabled = true
        case 7:
            // Formant Complex
            Osc2BtnWaveSelect.isUserInteractionEnabled = true
            Osc2BtnWaveSelect.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
            Osc2KnobWaveShape.knobType = "volume"
            Osc2KnobPulseWidth.enabled = false
            Osc2KnobFormShift.enabled = true
            Osc2KnobFormSpread.enabled = true
            Osc2KnobLocalDetune.enabled = true
            Osc2KnobLocalDetune.label = "Local Detune"
            Osc2KnobInterpolation.enabled = true
        default:
            print("Foutje bedankt")
        }
        
        Osc2KnobFormShift.value = Double(patchCtrl.CurrentPatch.osc2_FormantShift)
        Osc2KnobFormSpread.value = Double(patchCtrl.CurrentPatch.osc2_FormantSpread)
        
        switch(patchCtrl.CurrentPatch.osc2_Model){
        case 0:
            Osc2KnobLocalDetune.value = Double(patchCtrl.CurrentPatch.osc2_FineDetune)
        default:
            Osc2KnobLocalDetune.value = Double(patchCtrl.CurrentPatch.osc2_LocalDetune)
        }
        
        
        Osc2KnobInterpolation.value = Double(patchCtrl.CurrentPatch.osc2_Interpolation)
        
        Osc2KnobSemiTone.value = Double(patchCtrl.CurrentPatch.osc2_DetuneInSemitones)
        Osc2KnobKeyFollow.value = Double(patchCtrl.CurrentPatch.osc2_Keyfollow)
        
        
        // Osc Section
        OscSliderBalance.value = Float(patchCtrl.CurrentPatch.osc1_osc2_Balance)
        OscKnobInitPhase.value = Double(patchCtrl.CurrentPatch.osc_InitialPhase)
        OscKnobPortamento.value = Double(patchCtrl.CurrentPatch.osc_Portamento)
        OscKnobEnvelopeToOsc2.value = Double(patchCtrl.CurrentPatch.filterEnvelope_ToPitch)
        
        OscSyncBtn.setOn(val: (patchCtrl.CurrentPatch.osc1_osc2_Sync == 1))
        
        // FM
        OscKnobFMAmount.value = Double(patchCtrl.CurrentPatch.osc_FMAmount)
        OscKnobEnvToFM.value = Double(patchCtrl.CurrentPatch.filterEnvelope_ToFM)
        OscBtnFMMode.setTitle( FMModeLabels[safe: patchCtrl.CurrentPatch.osc_FMMode], for: .normal)
        
        // Ring Modulation
        OscKnobRingModVolume.value = Double(patchCtrl.CurrentPatch.osc_RingModulationVolume)
        //OscKnobRingModMix.value = Double(patchCtrl.CurrentPatch.osc_RingModulationMix)
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        
        // Oscillator 1

        Osc1KnobWaveShape.callback = { value in
            patchCtrl.setOsc1WaveformShape(value: Int(value))
            
            if (patchCtrl.CurrentPatch.osc1_Model == 0){
                if ( patchCtrl.CurrentPatch.osc1_WaveformShape >= 64 ){
                    self.Osc1BtnWaveSelect.isUserInteractionEnabled = false
                    self.Osc1BtnWaveSelect.setTitle("None", for: .normal)
                } else {
                    self.Osc1BtnWaveSelect.isUserInteractionEnabled = true
                    self.Osc1BtnWaveSelect.setTitle( WaveLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
                }
            }
            
        }
        Osc1KnobPulseWidth.callback = { value in
            patchCtrl.setOsc1PulseWidth(value: Int(value))
        }
        Osc1KnobFormShift.callback = { value in
            patchCtrl.setOsc1FormantShift(value: Int(value))
        }
        Osc1KnobFormSpread.callback = { value in
            patchCtrl.setOsc1FormantSpread(value: Int(value))
        }
        Osc1KnobLocalDetune.callback = { value in
            patchCtrl.setOsc1LocalDetune(value: Int(value))
        }
        Osc1KnobInterpolation.callback = { value in
            patchCtrl.setOsc1Interpolation(value: Int(value))
        }
        Osc1KnobSemiTone.callback = { value in
            patchCtrl.setOsc1SemiTone(value: Int(value))
        }
        Osc1KnobKeyFollow.callback = { value in
            patchCtrl.setOsc1KeyFollow(value: Int(value))
        }
        OscSubKnobVolume.callback = { value in
            patchCtrl.setOscSubVolume(value: Int(value))
        }
        
        Osc2KnobWaveShape.callback = { value in
            patchCtrl.setOsc2WaveformShape(value: Int(value))
            
            if (patchCtrl.CurrentPatch.osc2_Model == 0){
                if ( patchCtrl.CurrentPatch.osc2_WaveformShape >= 64 ){
                    self.Osc2BtnWaveSelect.isUserInteractionEnabled = false
                    self.Osc2BtnWaveSelect.setTitle("None", for: .normal)
                } else {
                    self.Osc2BtnWaveSelect.isUserInteractionEnabled = true
                    self.Osc2BtnWaveSelect.setTitle( WaveLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
                }
            }
        }
        Osc2KnobPulseWidth.callback = { value in
            patchCtrl.setOsc2PulseWidth(value: Int(value))
        }
        Osc2KnobFormShift.callback = { value in
            patchCtrl.setOsc2FormantShift(value: Int(value))
        }
        Osc2KnobFormSpread.callback = { value in
            patchCtrl.setOsc2FormantSpread(value: Int(value))
        }
        Osc2KnobLocalDetune.callback = { value in
            switch(patchCtrl.CurrentPatch.osc2_Model){
            case 0,2,3:
                patchCtrl.setOsc2FineDetune(value: Int(value))
            default:
                patchCtrl.setOsc2LocalDetune(value: Int(value))
            }
        }
        Osc2KnobInterpolation.callback = { value in
            patchCtrl.setOsc2Interpolation(value: Int(value))
        }
        Osc2KnobSemiTone.callback = { value in
            patchCtrl.setOsc2SemiTone(value: Int(value))
        }
        Osc2KnobKeyFollow.callback = { value in
            patchCtrl.setOsc2KeyFollow(value: Int(value))
        }
        
        OscKnobInitPhase.callback = { value in
            patchCtrl.setInitialPhase(value: Int(value))
        }
        OscKnobPortamento.callback = { value in
            patchCtrl.setPortamento(value: Int(value))
        }
        OscKnobEnvelopeToOsc2.callback = { value in
            patchCtrl.setFilterEnvelopeToPitch(value: Int(value))
        }
        OscKnobFMAmount.callback = { value in
            patchCtrl.setFMAmount(value: Int(value))
        }
        OscKnobEnvToFM.callback = { value in
            patchCtrl.setFilterEnvelopeToFM(value: Int(value))
        }
        
        OscKnobRingModVolume.callback = { value in
            patchCtrl.setRingModulation(value: Int(value))
        }
        
        OscSyncBtn.callback = { value in
            if (value == 1.0){
                patchCtrl.setOsc1Osc2Sync(value: 1)
            } else {
                patchCtrl.setOsc1Osc2Sync(value: 0)
           }
        }
        //OscKnobRingModMix.callback = { value in
        //    patchCtrl.setRingModulationMix(value: Int(value))
        //}
    }
    
    
    @IBAction func Osc1BtnModelEvent(_ sender: UIButton) {
        let Osc1ModelPopover = OscModelPopover()
        Osc1ModelPopover.OscNr = 1
        Osc1ModelPopover.tableView.dataSource = OscModelsLbls!
        Osc1ModelPopover.tableView.delegate = Osc1ModelPopover
        Osc1ModelPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc1ModelPopover, animated: true, completion: nil)
        let popoverPresentationController = Osc1ModelPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
        
    }
    
    @IBAction func Osc2BtnModelEvent(_ sender: UIButton) {
        let Osc2ModelPopover = OscModelPopover()
        Osc2ModelPopover.OscNr = 2
        Osc2ModelPopover.tableView.dataSource = OscModelsLbls!
        Osc2ModelPopover.tableView.delegate = Osc2ModelPopover
        Osc2ModelPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc2ModelPopover, animated: true, completion: nil)
        let popoverPresentationController = Osc2ModelPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
        
    }
    
    @IBAction func Osc1BtnWaveSelect(_ sender: UIButton) {
        let Osc1WavePopover = OscWavePopover()
        Osc1WavePopover.OscNr = 1
        Osc1WavePopover.tableView.dataSource = Osc1WaveLbls!
        Osc1WavePopover.tableView.delegate = Osc1WavePopover
        Osc1WavePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc1WavePopover, animated: true, completion: nil)
        let popoverPresentationController = Osc1WavePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Osc2BtnWaveSelect(_ sender: UIButton) {
        let Osc2WavePopover = OscWavePopover()
        Osc2WavePopover.OscNr = 2
        Osc2WavePopover.tableView.dataSource = Osc2WaveLbls!
        Osc2WavePopover.tableView.delegate = Osc2WavePopover
        Osc2WavePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc2WavePopover, animated: true, completion: nil)
        let popoverPresentationController = Osc2WavePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func OscFMModeEvent(_ sender: UIButton) {
        let FMModesPopover = FMModePopover()
        FMModesPopover.tableView.dataSource = OscFMModeLbls!
        FMModesPopover.tableView.delegate = FMModesPopover
        FMModesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(FMModesPopover, animated: true, completion: nil)
        let popoverPresentationController = FMModesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func OscSectionBalanceEvent(_ sender: UISlider) {
        patchCtrl.setOsc1Osc2Balance(value: Int(sender.value))
    }
    
    @IBAction func OscSubWaveShapeEvent(_ sender: UISwitch) {
        if (sender.isOn){
            patchCtrl.setOscSubWaveformShapee(value: 1)
        } else {
            patchCtrl.setOscSubWaveformShapee(value: 0)
        }
    }
    
    
}
