//
//  SVC_Osc_QuickPresets.swift
//  Rage
//
//  Created by M. Lierop on 10-11-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class OscQuickPresetsSubViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var OscModelsLbls:OscModelsSrc?
    private var Osc1WaveLbls:Osc1WaveSrc?
    private var Osc2WaveLbls:Osc2WaveSrc?
    private var Osc3ModeLbls:Osc3ModesSrc?
    private var UnisonModeLbls:UnisonModesSrc?
    
    //  Osc1
    
    @IBOutlet weak var Osc1BtnModel: UIButton!
    @IBOutlet weak var Osc1BtnWaveSelect: PMSuperButton!
    
    //  Osc2
    @IBOutlet weak var Osc2BtnModel: PMSuperButton!
    @IBOutlet weak var Osc2BtnWaveSelect: PMSuperButton!
    
    //  Osc3
    @IBOutlet weak var Osc3BtnMode: PMSuperButton!
    @IBOutlet weak var Osc3KnobSemitone: Knob!
    @IBOutlet weak var Osc3KnobDetune: Knob!
    @IBOutlet weak var Osc3KnobVolume: Knob!
    
    //  Unison
    @IBOutlet weak var OscBtnUnisonMode: PMSuperButton!
    @IBOutlet weak var UnisonKnobDetune: Knob!
    @IBOutlet weak var UnisonKnobSpread: Knob!
    @IBOutlet weak var UnisonKnobPhase: Knob!
    
    //  Misc
    @IBOutlet weak var MiscKnob_Volume: Knob!
    @IBOutlet weak var MiscKnob_Transposition: Knob!
    @IBOutlet weak var MiscKnob_Punch: Knob!
    @IBOutlet weak var MiscKnob_NoiseColor: Knob!
    @IBOutlet weak var MiscKnob_NoiseVolume: Knob!
    
    @IBOutlet weak var OscPresetsTable: UITableView!
    var currentSelectedRow: IndexPath!
    var currentScrollPosition: UITableView.ScrollPosition!
    
    let oscPresets = try! Realm(configuration: config).objects(subPatchOsc.self) //.sorted(byKeyPath: "name")
    
    @IBOutlet weak var SaveBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallbacks()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("OscModelChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("OscWaveSelectChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("Osc3ModeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UnisonModeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpdatePresets), name: Notification.Name("UpdatePresets"), object: nil)
        
        OscModelsLbls = OscModelsSrc()
        Osc1WaveLbls = Osc1WaveSrc()
        Osc2WaveLbls = Osc2WaveSrc()
        Osc3ModeLbls = Osc3ModesSrc()
        UnisonModeLbls = UnisonModesSrc()
        
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.OscPresetsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.OscPresetsTable.reloadData()
        
        self.updateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        
        super.viewWillAppear(animated)
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.OscPresetsTable.reloadData()
        self.OscPresetsTable.selectRow(at: currentSelectedRow, animated: true, scrollPosition: UITableView.ScrollPosition(rawValue: 0)!)
        self.updateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ OscPresetsTable: UITableView, numberOfRowsInSection section: Int) -> Int {
        return oscPresets.count
    }
    
    func tableView(_ OscPresetsTable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell:UITableViewCell = (self.OscPresetsTable.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.gray
        cell.textLabel?.text = oscPresets[indexPath.row].name
        //cell.textLabel?.text = patchLibrary.objects(subPatchOsc.self)[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ OscPresetsTable: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentSelectedRow = indexPath
        if (patchCtrl.LoadOscPatch(NewPatch: patchLibrary.objects(subPatchOsc.self)[indexPath.row]) )
        {
            updateView()
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.currentSelectedRow = indexPath
            let deleteObject = patchLibrary.objects(subPatchOsc.self)[indexPath.row]
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.delete(deleteObject)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    @objc func UpdatePresets(){
        let realm = try! Realm(configuration: config)
        realm.refresh()
        self.OscPresetsTable.reloadData()
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
   
    @objc func updateView(){
        self.Osc1BtnModel?.setTitle(  OscModelLabels[safe: patchCtrl.CurrentPatch.osc1_Model], for: .normal)
        switch(patchCtrl.CurrentPatch.osc1_Model){
        case 0:
            // analog
            self.Osc1BtnWaveSelect?.isUserInteractionEnabled = false
            Osc1BtnWaveSelect?.setTitle("None", for: .normal)
            
            if ( patchCtrl.CurrentPatch.osc1_WaveformSelect >= WaveLabels.count ) {
                patchCtrl.CurrentPatch.osc1_WaveformSelect = (WaveLabels.count - 1)
            }
            
        case 1:
            // Hyper Saw
            Osc1BtnWaveSelect?.isUserInteractionEnabled = false
            Osc1BtnWaveSelect?.setTitle( "Hyper Saw", for: .normal)
        default:
            Osc1BtnWaveSelect?.isUserInteractionEnabled = true
            Osc1BtnWaveSelect?.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc1_WaveformSelect ], for: .normal)
        }
        
        Osc2BtnModel?.setTitle(  OscModelLabels[safe: patchCtrl.CurrentPatch.osc2_Model], for: .normal)
        switch(patchCtrl.CurrentPatch.osc2_Model){
        case 0:
            // analog
            Osc2BtnWaveSelect?.isUserInteractionEnabled = false
            Osc2BtnWaveSelect?.setTitle("None", for: .normal)
            
            if ( patchCtrl.CurrentPatch.osc2_WaveformSelect >= WaveLabels.count ) {
                patchCtrl.CurrentPatch.osc2_WaveformSelect = (WaveLabels.count - 1)
            }
            
        case 1:
            // Hyper Saw
            Osc2BtnWaveSelect?.isUserInteractionEnabled = false
            Osc2BtnWaveSelect?.setTitle( "Hyper Saw", for: .normal)
        default:
            Osc2BtnWaveSelect?.isUserInteractionEnabled = true
            Osc2BtnWaveSelect?.setTitle( WavetableLabels[safe: patchCtrl.CurrentPatch.osc2_WaveformSelect ], for: .normal)
        }
        
        Osc3BtnMode?.setTitle( Oscillator3ModeLabels[safe: patchCtrl.CurrentPatch.osc3_Model], for: .normal)
        
        Osc3KnobSemitone?.value = Double(patchCtrl.CurrentPatch.osc3_DetuneInSemitones)
        Osc3KnobDetune?.value = Double(patchCtrl.CurrentPatch.osc3_FineDetune)
        Osc3KnobVolume?.value = Double(patchCtrl.CurrentPatch.osc3_Volume)
        
        MiscKnob_Volume?.value = Double(patchCtrl.CurrentPatch.osc_SectionVolume)
        MiscKnob_Transposition?.value = Double(patchCtrl.CurrentPatch.osc_SectionPatchTransposition)
        MiscKnob_Punch?.value = Double(patchCtrl.CurrentPatch.osc_PunchIntensity)
        MiscKnob_NoiseColor?.value = Double(patchCtrl.CurrentPatch.oscN_Color)
        MiscKnob_NoiseVolume?.value = Double(patchCtrl.CurrentPatch.oscN_Volume)
        OscBtnUnisonMode?.setTitle( UnisonLabels[patchCtrl.CurrentPatch.osc_Unison_Mode], for: .normal)
        UnisonKnobDetune?.value = Double( patchCtrl.CurrentPatch.osc_Unison_Detune )
        UnisonKnobSpread?.value = Double( patchCtrl.CurrentPatch.osc_Unison_PanoramaSpread )
        UnisonKnobPhase?.value = Double( patchCtrl.CurrentPatch.osc_Unison_LFOPhaseOffset )
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        
        // Oscillator 3
        Osc3KnobSemitone.callback = { value in
            patchCtrl.setOsc3SemiTone(value: Int(value))
        }
        Osc3KnobDetune.callback = { value in
            patchCtrl.setOsc3FineDetune(value: Int(value))
        }
        Osc3KnobVolume.callback = { value in
            patchCtrl.setOsc3Volume(value: Int(value))
        }
        
        // Misc
        MiscKnob_Volume.callback = { value in
            patchCtrl.setOscSectionVolume(value: Int(value))
        }
        MiscKnob_Punch.callback = { value in
            patchCtrl.setPunchIntensity(value: Int(value))
        }
        MiscKnob_Transposition.callback = { value in
            patchCtrl.setOscSectionPatchTransposition(value: Int(value))
        }
        MiscKnob_NoiseColor.callback = { value in
            patchCtrl.setOscNoiseColor(value: Int(value))
        }
        MiscKnob_NoiseVolume.callback = { value in
            patchCtrl.setOscNoiseVolume(value: Int(value))
        }
        
        //  Unison
        UnisonKnobDetune.callback = { value in
            patchCtrl.setOsc_Unison_Detune(value: Int(value))
        }
        UnisonKnobSpread.callback = { value in
            patchCtrl.setOsc_Unison_PanoramaSpread(value: Int(value))
        }
        UnisonKnobPhase.callback = { value in
            patchCtrl.setOsc_Unison_LFOPhaseOffset(value: Int(value))
        }
    }
    
    @IBAction func Osc1BtnModelEvent(_ sender: UIButton) {
        let Osc1ModelPopover = OscModelPopover()
        Osc1ModelPopover.OscNr = 1
        Osc1ModelPopover.tableView.dataSource = OscModelsLbls!
        Osc1ModelPopover.tableView.delegate = Osc1ModelPopover
        Osc1ModelPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc1ModelPopover, animated: true, completion: nil)
        let popoverPresentationController = Osc1ModelPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Osc2BtnModelEvent(_ sender: UIButton) {
        let Osc2ModelPopover = OscModelPopover()
        Osc2ModelPopover.OscNr = 2
        Osc2ModelPopover.tableView.dataSource = OscModelsLbls!
        Osc2ModelPopover.tableView.delegate = Osc2ModelPopover
        Osc2ModelPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc2ModelPopover, animated: true, completion: nil)
        let popoverPresentationController = Osc2ModelPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Osc1BtnWaveSelect(_ sender: UIButton) {
        let Osc1WavePopover = OscWavePopover()
        Osc1WavePopover.OscNr = 1
        Osc1WavePopover.tableView.dataSource = Osc1WaveLbls!
        Osc1WavePopover.tableView.delegate = Osc1WavePopover
        Osc1WavePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc1WavePopover, animated: true, completion: nil)
        let popoverPresentationController = Osc1WavePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func Osc2BtnWaveSelect(_ sender: UIButton) {
        let Osc2WavePopover = OscWavePopover()
        Osc2WavePopover.OscNr = 2
        Osc2WavePopover.tableView.dataSource = Osc2WaveLbls!
        Osc2WavePopover.tableView.delegate = Osc2WavePopover
        Osc2WavePopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc2WavePopover, animated: true, completion: nil)
        let popoverPresentationController = Osc2WavePopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    @IBAction func Osc3BtnModeSelectEvent(_ sender: UIButton) {
        let Osc3ModesPopover = Osc3ModePopover()
        Osc3ModesPopover.tableView.dataSource = Osc3ModeLbls!
        Osc3ModesPopover.tableView.delegate = Osc3ModesPopover
        Osc3ModesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Osc3ModesPopover, animated: true, completion: nil)
        let popoverPresentationController = Osc3ModesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func OscUnisonSelectEvent(_ sender: UIButton) {
        let UnisonModesPopover = UnisonModePopover()
        UnisonModesPopover.tableView.dataSource = UnisonModeLbls!
        UnisonModesPopover.tableView.delegate = UnisonModesPopover
        UnisonModesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(UnisonModesPopover, animated: true, completion: nil)
        let popoverPresentationController = UnisonModesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func SaveNewSubPatch(_ sender: UIButton) {
        self.OscPresetsTable.reloadData()
        showInputDialog(title: "Save new oscillator preset",
                        subtitle: "Save current oscillator settings as preset",
                        actionTitle: "Save",
                        cancelTitle: "Cancel",
                        inputPlaceholder: "New preset name",
                        inputKeyboardType: .alphabet,
                        actionHandler:  { (input:String?) in
                            print("The name is \(input ?? "")")
                            if (input != ""){
                                patchCtrl.OscPatch.name = input!
                                
                                let realm = try! Realm(configuration: config)
                                
                                try! realm.write {
                                    realm.create(subPatchOsc.self, value: patchCtrl.OscPatch)
                                    realm.refresh()
                                }
                            }
                            self.OscPresetsTable.reloadData()
                            let realm = try! Realm(configuration: config)
                            realm.refresh()
                            
                            self.UpdatePresets()
                        })
    }
}


