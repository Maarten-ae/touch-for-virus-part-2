//
//  FiltersSubViewController.swift
//  Rage
//
//  Created by M. Lierop on 06-12-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class FiltersSubViewController: UIViewController {
    
    private var SatModes:SaturationModes?
    
    //@IBOutlet weak var Filter1AnalogSwitch: UISwitch!
    @IBOutlet weak var Filter1AnalogModeBtn: ToggleButton!
    @IBOutlet weak var Filter1ModeSelect: UISegmentedControl!
    @IBOutlet weak var Filter1polaritySelect: UISegmentedControl!
    @IBOutlet weak var Filter1CutoffKnob: Knob!
    @IBOutlet weak var Filter1ResoKnob: Knob!
    @IBOutlet weak var Filter1EnvAmountKnob: Knob!
    @IBOutlet weak var Filter1EnvKeyFollowKnob: Knob!
    
    @IBOutlet weak var FilterSectionLinkBtn: ToggleButton!
    //@IBOutlet weak var FilterSectionLinkSwitch: UISwitch!
    @IBOutlet weak var FilterSectionCutoffLinkBtn: ToggleButton!
    //@IBOutlet weak var FilterSectionCutoffLinkSwitch: UISwitch!
    @IBOutlet weak var FilterSectionModeSelect: UISegmentedControl!
    @IBOutlet weak var FilterEnvControl: EnvelopeChart!
    @IBOutlet weak var FilterSectionBalanceSlider: UISlider!
    @IBOutlet weak var FilterSectionSaturation: PMSuperButton!
    
    @IBOutlet weak var Filter2ModeSelect: UISegmentedControl!
    @IBOutlet weak var Filter2polaritySelect: UISegmentedControl!
    @IBOutlet weak var Filter2CutoffKnob: Knob!
    
    @IBOutlet weak var Filter2ResoKnob: Knob!
    @IBOutlet weak var Filter2EnvAmountKnob: Knob!
    @IBOutlet weak var Filter2EnvKeyFollowKnob: Knob!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallbacks()
        
        FilterSectionBalanceSlider.setThumbImage( UIImage(named: "SliderHor"), for: .normal)
        FilterSectionBalanceSlider.setThumbImage( UIImage(named: "SliderHor-Tch"), for: .highlighted)
        
        self.FilterEnvControl.envelopePrefix = "Filtermain"
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvAttack(notification:)), name: Notification.Name("FiltermainEnvelope_Attack"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvDecay(notification:)), name: Notification.Name("FiltermainEnvelope_Decay"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvSustain(notification:)), name: Notification.Name("FiltermainEnvelope_Sustain"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvSustainSlope(notification:)), name: Notification.Name("FiltermainEnvelope_SustainSlope"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFilterEnvRelease(notification:)), name: Notification.Name("FiltermainEnvelope_Release"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("FilterSaturationChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
         SatModes = SaturationModes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        //updateView()
        updateView()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        if ( patchCtrl.CurrentPatch.filter1_Mode >= 4 ){
            Filter1ModeSelect.setTitle("LP 1", forSegmentAt: 0)
            Filter1ModeSelect.setTitle("LP 2", forSegmentAt: 1)
            Filter1ModeSelect.setTitle("LP 3", forSegmentAt: 2)
            Filter1ModeSelect.setTitle("LP 4", forSegmentAt: 3)
            
        } else {
            Filter1ModeSelect.setTitle("LP", forSegmentAt: 0)
            Filter1ModeSelect.setTitle("HP", forSegmentAt: 1)
            Filter1ModeSelect.setTitle("BP", forSegmentAt: 2)
            Filter1ModeSelect.setTitle("BS", forSegmentAt: 3)
        }
        Filter1ModeSelect.selectedSegmentIndex = (patchCtrl.CurrentPatch.filter1_Mode >= 4) ?patchCtrl.CurrentPatch.filter1_Mode - 4 : patchCtrl.CurrentPatch.filter1_Mode;
        Filter1polaritySelect.selectedSegmentIndex = patchCtrl.CurrentPatch.filter1_EnvelopePolarity
        Filter1AnalogModeBtn.setOn(val: (patchCtrl.CurrentPatch.filter1_Mode >= 4))
        Filter1CutoffKnob.value = Double(patchCtrl.CurrentPatch.filter1_Cutoff)
        Filter1ResoKnob.value = Double(patchCtrl.CurrentPatch.filter1_Resonance)
        Filter1EnvAmountKnob.value = Double(patchCtrl.CurrentPatch.filter1_EnvelopeAmount)
        Filter1EnvKeyFollowKnob.value = Double(patchCtrl.CurrentPatch.filter1_KeyFollow)
        
        Filter2ModeSelect.selectedSegmentIndex = patchCtrl.CurrentPatch.filter2_Mode
        Filter2polaritySelect.selectedSegmentIndex = patchCtrl.CurrentPatch.filter2_EnvelopePolarity
        Filter2CutoffKnob.value = Double(patchCtrl.CurrentPatch.filter2_Cutoff)
        Filter2ResoKnob.value = Double(patchCtrl.CurrentPatch.filter2_Resonance)
        Filter2EnvAmountKnob.value = Double(patchCtrl.CurrentPatch.filter2_EnvelopeAmount)
        Filter2EnvKeyFollowKnob.value = Double(patchCtrl.CurrentPatch.filter2_KeyFollow)
        
        FilterSectionBalanceSlider.value = Float(patchCtrl.CurrentPatch.filter_Balance)
        FilterSectionLinkBtn.setOn(val: (patchCtrl.CurrentPatch.filter_Link == 0))
        FilterSectionCutoffLinkBtn.setOn(val:(patchCtrl.CurrentPatch.filter_cutoffLink == 0))
        FilterSectionModeSelect.selectedSegmentIndex = patchCtrl.CurrentPatch.filter_routing

        FilterSectionSaturation.setTitle( FilterSaturationModes[safe: patchCtrl.CurrentPatch.filter_saturationType], for: .normal)
        
        self.FilterEnvControl.setEnvelopeValues(
            attack: patchCtrl.CurrentPatch.filterEnvelope_Attack,
            decay: patchCtrl.CurrentPatch.filterEnvelope_Decay,
            sustain: patchCtrl.CurrentPatch.filterEnvelope_Sustain,
            sustainSlope: patchCtrl.CurrentPatch.filterEnvelope_SustainSlope,
            release: patchCtrl.CurrentPatch.filterEnvelope_Release)
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        
        Filter1CutoffKnob.callback = { value in
            patchCtrl.setFilter1Cutoff(value: Int(value))
        }
        Filter1ResoKnob.callback = { value in
            patchCtrl.setFilter1Resonance(value: Int(value))
        }
        Filter1EnvAmountKnob.callback = { value in
            patchCtrl.setFilter1EnvelopeAmount(value: Int(value))
        }
        Filter1EnvKeyFollowKnob.callback = { value in
            patchCtrl.setFilter1KeyFollow(value: Int(value))
        }
        
        
        Filter2CutoffKnob.callback = { value in
            patchCtrl.setFilter2Cutoff(value: Int(value))
        }
        Filter2ResoKnob.callback = { value in
            patchCtrl.setFilter2Resonance(value: Int(value))
        }
        Filter2EnvAmountKnob.callback = { value in
            patchCtrl.setFilter2EnvelopeAmount(value: Int(value))
        }
        Filter2EnvKeyFollowKnob.callback = { value in
            patchCtrl.setFilter2KeyFollow(value: Int(value))
        }
        
        Filter1AnalogModeBtn.callback = { value in
            if ( value == 1.0){
                self.Filter1ModeSelect.setTitle("LP 1", forSegmentAt: 0)
                self.Filter1ModeSelect.setTitle("LP 2", forSegmentAt: 1)
                self.Filter1ModeSelect.setTitle("LP 3", forSegmentAt: 2)
                self.Filter1ModeSelect.setTitle("LP 4", forSegmentAt: 3)
                patchCtrl.CurrentPatch.filter1_Mode = patchCtrl.CurrentPatch.filter1_Mode + 4
            } else {
                self.Filter1ModeSelect.setTitle("LP", forSegmentAt: 0)
                self.Filter1ModeSelect.setTitle("HP", forSegmentAt: 1)
                self.Filter1ModeSelect.setTitle("BP", forSegmentAt: 2)
                self.Filter1ModeSelect.setTitle("BS", forSegmentAt: 3)
                patchCtrl.CurrentPatch.filter1_Mode = patchCtrl.CurrentPatch.filter1_Mode - 4
            }
        }
    
        FilterSectionLinkBtn.callback = { value in
            patchCtrl.CurrentPatch.filter_Link = Int(value)
        }
 
        FilterSectionCutoffLinkBtn.callback = { value in
            patchCtrl.CurrentPatch.filter_cutoffLink = Int(value)
        }
        
        
    }
    
 
    
    @objc func updateFilterEnvAttack(notification: Notification){
        print("filter attack")
        patchCtrl.setFilterEnvelopeAttack(value: FilterEnvControl.attackTime!)
    }
    @objc func updateFilterEnvDecay(notification: Notification){
        print("filter decay")
        patchCtrl.setFilterEnvelopeDecay(value: FilterEnvControl.decayTime!)
    }
    @objc func updateFilterEnvSustain(notification: Notification){
        print("filter sustain")
        patchCtrl.setFilterEnvelopeSustain(value: FilterEnvControl.sustainLvl!)
    }
    @objc func updateFilterEnvSustainSlope(notification: Notification){
        print("filter sustainSlope")
        patchCtrl.setFilterEnvelopeSustainSlope(value: FilterEnvControl.sustainSlopeLvl!)
    }
    @objc func updateFilterEnvRelease(notification: Notification){
        patchCtrl.setFilterEnvelopeRelease(value: Int(FilterEnvControl.releaseTime!))
    }
    
    @IBAction func FilterSectionModeChange(_ sender: UISegmentedControl) {
        patchCtrl.setFilterRouting(value: Int(sender.selectedSegmentIndex))
    }
    
    @IBAction func Filter1ModeSelectChange(_ sender: UISegmentedControl) {
        if ( Filter1AnalogModeBtn.isOn ){
            patchCtrl.setFilter1Mode(value: (Int(sender.selectedSegmentIndex) + 4 ))
        } else {
            patchCtrl.setFilter1Mode(value: Int(sender.selectedSegmentIndex))
        }
    }
    
    
    
    
    @IBAction func FilterSectionBalanceSliderChange(_ sender: UISlider) {
        patchCtrl.CurrentPatch.filter_Balance = Int(sender.value)
    }
    
    @IBAction func Filter2ModeSelectChange(_ sender: UISegmentedControl) {
        patchCtrl.setFilter2Mode(value: Int(sender.selectedSegmentIndex))
    }
    
    @IBAction func Filter2EnvPolarisationChange(_ sender: UISegmentedControl) {
        patchCtrl.setFilter2EnvelopePolarity(value: Int(sender.selectedSegmentIndex))
    }

    
    @IBAction func Filter1EnvPolarisationChange(_ sender: UISegmentedControl) {
        patchCtrl.setFilter1EnvelopePolarity(value: Int(sender.selectedSegmentIndex))
    }
    
    
   
    
    @IBAction func FilterSectionSaturationChanged(_ sender: PMSuperButton) {
        let filterSaturationChange = SaturationPopover()
        filterSaturationChange.tableView.dataSource = SatModes!
        filterSaturationChange.tableView.delegate = filterSaturationChange
        filterSaturationChange.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(filterSaturationChange, animated: true, completion: nil)
        let popoverPresentationController = filterSaturationChange.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    

}
