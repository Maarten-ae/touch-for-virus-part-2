//
//  SVC_Arp_PatternDesigner.swift
//  Rage
//
//  Created by M. Lierop on 04/03/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class ArpPatternDesignersSubView: UIViewController {

    
    @IBOutlet weak var ArpPAtternChart: ArpeggiatorChart!
   
    @IBOutlet weak var ActivatePatternBtn: PMSuperButton!
    
    private var ArpModeLbls: ArpModesSrc?
    @IBOutlet weak var ArpModeBtn: PMSuperButton!
    
    
    @IBOutlet weak var PatternLengthStepper: Stepper!
    private var ArpPatternLengthLbls: ArpPatternLengthSrc?
    
   
    @IBOutlet weak var ScrollView: UIScrollView!
    
    @IBOutlet weak var step_Toggle_01: ToggleButton!
    @IBOutlet weak var step_Toggle_02: ToggleButton!
    @IBOutlet weak var step_Toggle_03: ToggleButton!
    @IBOutlet weak var step_Toggle_04: ToggleButton!
    @IBOutlet weak var step_Toggle_05: ToggleButton!
    @IBOutlet weak var step_Toggle_06: ToggleButton!
    @IBOutlet weak var step_Toggle_07: ToggleButton!
    @IBOutlet weak var step_Toggle_08: ToggleButton!
    @IBOutlet weak var step_Toggle_09: ToggleButton!
    @IBOutlet weak var step_Toggle_10: ToggleButton!
    @IBOutlet weak var step_Toggle_11: ToggleButton!
    @IBOutlet weak var step_Toggle_12: ToggleButton!
    @IBOutlet weak var step_Toggle_13: ToggleButton!
    @IBOutlet weak var step_Toggle_14: ToggleButton!
    @IBOutlet weak var step_Toggle_15: ToggleButton!
    @IBOutlet weak var step_Toggle_16: ToggleButton!
    @IBOutlet weak var step_Toggle_17: ToggleButton!
    @IBOutlet weak var step_Toggle_18: ToggleButton!
    @IBOutlet weak var step_Toggle_19: ToggleButton!
    @IBOutlet weak var step_Toggle_20: ToggleButton!
    @IBOutlet weak var step_Toggle_21: ToggleButton!
    @IBOutlet weak var step_Toggle_22: ToggleButton!
    @IBOutlet weak var step_Toggle_23: ToggleButton!
    @IBOutlet weak var step_Toggle_24: ToggleButton!
    @IBOutlet weak var step_Toggle_25: ToggleButton!
    @IBOutlet weak var step_Toggle_26: ToggleButton!
    @IBOutlet weak var step_Toggle_27: ToggleButton!
    @IBOutlet weak var step_Toggle_28: ToggleButton!
    @IBOutlet weak var step_Toggle_29: ToggleButton!
    @IBOutlet weak var step_Toggle_30: ToggleButton!
    @IBOutlet weak var step_Toggle_31: ToggleButton!
    @IBOutlet weak var step_Toggle_32: ToggleButton!
    
    @IBOutlet weak var step_NoteLength_01: Knob!
    @IBOutlet weak var step_NoteLength_02: Knob!
    @IBOutlet weak var step_NoteLength_03: Knob!
    @IBOutlet weak var step_NoteLength_04: Knob!
    @IBOutlet weak var step_NoteLength_05: Knob!
    @IBOutlet weak var step_NoteLength_06: Knob!
    @IBOutlet weak var step_NoteLength_07: Knob!
    @IBOutlet weak var step_NoteLength_08: Knob!
    @IBOutlet weak var step_NoteLength_09: Knob!
    @IBOutlet weak var step_NoteLength_10: Knob!
    @IBOutlet weak var step_NoteLength_11: Knob!
    @IBOutlet weak var step_NoteLength_12: Knob!
    @IBOutlet weak var step_NoteLength_13: Knob!
    @IBOutlet weak var step_NoteLength_14: Knob!
    @IBOutlet weak var step_NoteLength_15: Knob!
    @IBOutlet weak var step_NoteLength_16: Knob!
    @IBOutlet weak var step_NoteLength_17: Knob!
    @IBOutlet weak var step_NoteLength_18: Knob!
    @IBOutlet weak var step_NoteLength_19: Knob!
    @IBOutlet weak var step_NoteLength_20: Knob!
    @IBOutlet weak var step_NoteLength_21: Knob!
    @IBOutlet weak var step_NoteLength_22: Knob!
    @IBOutlet weak var step_NoteLength_23: Knob!
    @IBOutlet weak var step_NoteLength_24: Knob!
    @IBOutlet weak var step_NoteLength_25: Knob!
    @IBOutlet weak var step_NoteLength_26: Knob!
    @IBOutlet weak var step_NoteLength_27: Knob!
    @IBOutlet weak var step_NoteLength_28: Knob!
    @IBOutlet weak var step_NoteLength_29: Knob!
    @IBOutlet weak var step_NoteLength_30: Knob!
    @IBOutlet weak var step_NoteLength_31: Knob!
    @IBOutlet weak var step_NoteLength_32: Knob!
    
    @IBOutlet weak var step_Velocity_01: Knob!
    @IBOutlet weak var step_Velocity_02: Knob!
    @IBOutlet weak var step_Velocity_03: Knob!
    @IBOutlet weak var step_Velocity_04: Knob!
    @IBOutlet weak var step_Velocity_05: Knob!
    @IBOutlet weak var step_Velocity_06: Knob!
    @IBOutlet weak var step_Velocity_07: Knob!
    @IBOutlet weak var step_Velocity_08: Knob!
    @IBOutlet weak var step_Velocity_09: Knob!
    @IBOutlet weak var step_Velocity_10: Knob!
    @IBOutlet weak var step_Velocity_11: Knob!
    @IBOutlet weak var step_Velocity_12: Knob!
    @IBOutlet weak var step_Velocity_13: Knob!
    @IBOutlet weak var step_Velocity_14: Knob!
    @IBOutlet weak var step_Velocity_15: Knob!
    @IBOutlet weak var step_Velocity_16: Knob!
    @IBOutlet weak var step_Velocity_17: Knob!
    @IBOutlet weak var step_Velocity_18: Knob!
    @IBOutlet weak var step_Velocity_19: Knob!
    @IBOutlet weak var step_Velocity_20: Knob!
    @IBOutlet weak var step_Velocity_21: Knob!
    @IBOutlet weak var step_Velocity_22: Knob!
    @IBOutlet weak var step_Velocity_23: Knob!
    @IBOutlet weak var step_Velocity_24: Knob!
    @IBOutlet weak var step_Velocity_25: Knob!
    @IBOutlet weak var step_Velocity_26: Knob!
    @IBOutlet weak var step_Velocity_27: Knob!
    @IBOutlet weak var step_Velocity_28: Knob!
    @IBOutlet weak var step_Velocity_29: Knob!
    @IBOutlet weak var step_Velocity_30: Knob!
    @IBOutlet weak var step_Velocity_31: Knob!
    @IBOutlet weak var step_Velocity_32: Knob!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallbacks()
        
        ArpModeLbls = ArpModesSrc()
        ArpPatternLengthLbls = ArpPatternLengthSrc()
        PatternLengthStepper.maxValue = 32
        PatternLengthStepper.minValue = 1
        PatternLengthStepper.text = "Pattern Length"
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("ArpSettingsChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
        
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        
        print("start update")
       
        ArpModeBtn.setTitle(ArpModeLabels[safe: patchCtrl.CurrentPatch.arp_mode], for: .normal)
       
        if ( patchCtrl.CurrentPatch.arp_pattern == 0){
            ActivatePatternBtn.isEnabled = false
            ActivatePatternBtn.isHidden = true
            if ( patchCtrl.CurrentPatch.arp_mode == 0){
                ArpPAtternChart.isHidden = true
            } else {
                ArpPAtternChart.isHidden = false
            }
        } else {
            ActivatePatternBtn.isEnabled = true
            ActivatePatternBtn.isHidden = false
            ArpPAtternChart.isHidden = true
        }
        
        ScrollView.contentSize.width = CGFloat( 62 + (patchCtrl.CurrentPatch.arp_patternLength * 62))
        
        PatternLengthStepper.value = Double(patchCtrl.CurrentPatch.arp_patternLength + 1)
        
        step_Toggle_01.setOn(val: (patchCtrl.CurrentPatch.arp_step_1 == 1) )
        step_Toggle_02.setOn(val: (patchCtrl.CurrentPatch.arp_step_2 == 1) )
        step_Toggle_03.setOn(val: (patchCtrl.CurrentPatch.arp_step_3 == 1) )
        step_Toggle_04.setOn(val: (patchCtrl.CurrentPatch.arp_step_4 == 1) )
        step_Toggle_05.setOn(val: (patchCtrl.CurrentPatch.arp_step_5 == 1) )
        step_Toggle_06.setOn(val: (patchCtrl.CurrentPatch.arp_step_6 == 1) )
        step_Toggle_07.setOn(val: (patchCtrl.CurrentPatch.arp_step_7 == 1) )
        step_Toggle_08.setOn(val: (patchCtrl.CurrentPatch.arp_step_8 == 1) )
        step_Toggle_09.setOn(val: (patchCtrl.CurrentPatch.arp_step_9 == 1) )
        step_Toggle_10.setOn(val: (patchCtrl.CurrentPatch.arp_step_10 == 1) )
        step_Toggle_11.setOn(val: (patchCtrl.CurrentPatch.arp_step_11 == 1) )
        step_Toggle_12.setOn(val: (patchCtrl.CurrentPatch.arp_step_12 == 1) )
        step_Toggle_13.setOn(val: (patchCtrl.CurrentPatch.arp_step_13 == 1) )
        step_Toggle_14.setOn(val: (patchCtrl.CurrentPatch.arp_step_14 == 1) )
        step_Toggle_15.setOn(val: (patchCtrl.CurrentPatch.arp_step_15 == 1) )
        step_Toggle_16.setOn(val: (patchCtrl.CurrentPatch.arp_step_16 == 1) )
        step_Toggle_17.setOn(val: (patchCtrl.CurrentPatch.arp_step_17 == 1) )
        step_Toggle_18.setOn(val: (patchCtrl.CurrentPatch.arp_step_18 == 1) )
        step_Toggle_19.setOn(val: (patchCtrl.CurrentPatch.arp_step_19 == 1) )
        step_Toggle_20.setOn(val: (patchCtrl.CurrentPatch.arp_step_20 == 1) )
        step_Toggle_21.setOn(val: (patchCtrl.CurrentPatch.arp_step_21 == 1) )
        step_Toggle_22.setOn(val: (patchCtrl.CurrentPatch.arp_step_22 == 1) )
        step_Toggle_23.setOn(val: (patchCtrl.CurrentPatch.arp_step_23 == 1) )
        step_Toggle_24.setOn(val: (patchCtrl.CurrentPatch.arp_step_24 == 1) )
        step_Toggle_25.setOn(val: (patchCtrl.CurrentPatch.arp_step_25 == 1) )
        step_Toggle_26.setOn(val: (patchCtrl.CurrentPatch.arp_step_26 == 1) )
        step_Toggle_27.setOn(val: (patchCtrl.CurrentPatch.arp_step_27 == 1) )
        step_Toggle_28.setOn(val: (patchCtrl.CurrentPatch.arp_step_28 == 1) )
        step_Toggle_29.setOn(val: (patchCtrl.CurrentPatch.arp_step_29 == 1) )
        step_Toggle_30.setOn(val: (patchCtrl.CurrentPatch.arp_step_30 == 1) )
        step_Toggle_31.setOn(val: (patchCtrl.CurrentPatch.arp_step_31 == 1) )
        step_Toggle_32.setOn(val: (patchCtrl.CurrentPatch.arp_step_32 == 1) )
        
        step_NoteLength_01.value = Double(patchCtrl.CurrentPatch.arp_stepLength_1)
        step_NoteLength_02.value = Double(patchCtrl.CurrentPatch.arp_stepLength_2)
        step_NoteLength_03.value = Double(patchCtrl.CurrentPatch.arp_stepLength_3)
        step_NoteLength_04.value = Double(patchCtrl.CurrentPatch.arp_stepLength_4)
        step_NoteLength_05.value = Double(patchCtrl.CurrentPatch.arp_stepLength_5)
        step_NoteLength_06.value = Double(patchCtrl.CurrentPatch.arp_stepLength_6)
        step_NoteLength_07.value = Double(patchCtrl.CurrentPatch.arp_stepLength_7)
        step_NoteLength_08.value = Double(patchCtrl.CurrentPatch.arp_stepLength_8)
        step_NoteLength_09.value = Double(patchCtrl.CurrentPatch.arp_stepLength_9)
        step_NoteLength_10.value = Double(patchCtrl.CurrentPatch.arp_stepLength_10)
        step_NoteLength_11.value = Double(patchCtrl.CurrentPatch.arp_stepLength_11)
        step_NoteLength_12.value = Double(patchCtrl.CurrentPatch.arp_stepLength_12)
        step_NoteLength_13.value = Double(patchCtrl.CurrentPatch.arp_stepLength_13)
        step_NoteLength_14.value = Double(patchCtrl.CurrentPatch.arp_stepLength_14)
        step_NoteLength_15.value = Double(patchCtrl.CurrentPatch.arp_stepLength_15)
        step_NoteLength_16.value = Double(patchCtrl.CurrentPatch.arp_stepLength_16)
        step_NoteLength_17.value = Double(patchCtrl.CurrentPatch.arp_stepLength_17)
        step_NoteLength_18.value = Double(patchCtrl.CurrentPatch.arp_stepLength_18)
        step_NoteLength_19.value = Double(patchCtrl.CurrentPatch.arp_stepLength_19)
        step_NoteLength_20.value = Double(patchCtrl.CurrentPatch.arp_stepLength_20)
        step_NoteLength_21.value = Double(patchCtrl.CurrentPatch.arp_stepLength_21)
        step_NoteLength_22.value = Double(patchCtrl.CurrentPatch.arp_stepLength_22)
        step_NoteLength_23.value = Double(patchCtrl.CurrentPatch.arp_stepLength_23)
        step_NoteLength_24.value = Double(patchCtrl.CurrentPatch.arp_stepLength_24)
        step_NoteLength_25.value = Double(patchCtrl.CurrentPatch.arp_stepLength_25)
        step_NoteLength_26.value = Double(patchCtrl.CurrentPatch.arp_stepLength_26)
        step_NoteLength_27.value = Double(patchCtrl.CurrentPatch.arp_stepLength_27)
        step_NoteLength_28.value = Double(patchCtrl.CurrentPatch.arp_stepLength_28)
        step_NoteLength_29.value = Double(patchCtrl.CurrentPatch.arp_stepLength_29)
        step_NoteLength_30.value = Double(patchCtrl.CurrentPatch.arp_stepLength_30)
        step_NoteLength_31.value = Double(patchCtrl.CurrentPatch.arp_stepLength_31)
        step_NoteLength_32.value = Double(patchCtrl.CurrentPatch.arp_stepLength_32)
        
        step_Velocity_01.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_1)
        step_Velocity_02.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_2)
        step_Velocity_03.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_3)
        step_Velocity_04.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_4)
        step_Velocity_05.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_5)
        step_Velocity_06.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_6)
        step_Velocity_07.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_7)
        step_Velocity_08.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_8)
        step_Velocity_09.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_9)
        step_Velocity_10.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_10)
        step_Velocity_11.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_11)
        step_Velocity_12.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_12)
        step_Velocity_13.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_13)
        step_Velocity_14.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_14)
        step_Velocity_15.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_15)
        step_Velocity_16.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_16)
        step_Velocity_17.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_17)
        step_Velocity_18.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_18)
        step_Velocity_19.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_19)
        step_Velocity_20.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_20)
        step_Velocity_21.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_21)
        step_Velocity_22.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_22)
        step_Velocity_23.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_23)
        step_Velocity_24.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_24)
        step_Velocity_25.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_25)
        step_Velocity_26.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_26)
        step_Velocity_27.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_27)
        step_Velocity_28.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_28)
        step_Velocity_29.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_29)
        step_Velocity_30.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_30)
        step_Velocity_31.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_31)
        step_Velocity_32.value = Double(patchCtrl.CurrentPatch.arp_stepVelocity_32)
        
        DispatchQueue.main.async {
            self.ArpPAtternChart.update()
        }
   
        //patchCtrl.CurrentPatch.arp_mode == 0
        //patchCtrl.CurrentPatch.arp_patternLength
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 0 && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_1 == 1){
            step_Toggle_01.isHidden = false
            step_Velocity_01.enabled = true
            step_NoteLength_01.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 0 && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_1 == 0) {
            step_Toggle_01.isHidden = false
            step_Velocity_01.enabled = false
            step_NoteLength_01.enabled = false
        } else {
            step_Toggle_01.isHidden = true
            step_Velocity_01.enabled = false
            step_NoteLength_01.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 1  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_2 == 1){
            step_Toggle_02.isHidden = false
            step_Velocity_02.enabled = true
            step_NoteLength_02.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 1  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_2 == 0){
            step_Toggle_02.isHidden = false
            step_Velocity_02.enabled = false
            step_NoteLength_02.enabled = false
        } else {
            step_Toggle_02.isHidden = true
            step_Velocity_02.enabled = false
            step_NoteLength_02.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 2  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_3 == 1){
            step_Toggle_03.isHidden = false
            step_Velocity_03.enabled = true
            step_NoteLength_03.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 2  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_3 == 0){
            step_Toggle_03.isHidden = false
            step_Velocity_03.enabled = false
            step_NoteLength_03.enabled = false
        } else {
            step_Toggle_03.isHidden = true
            step_Velocity_03.enabled = false
            step_NoteLength_03.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 3  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_4 == 1){
            step_Toggle_04.isHidden = false
            step_Velocity_04.enabled = true
            step_NoteLength_04.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 3  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_4 == 0){
            step_Toggle_04.isHidden = false
            step_Velocity_04.enabled = false
            step_NoteLength_04.enabled = false
        } else {
            step_Toggle_04.isHidden = true
            step_Velocity_04.enabled = false
            step_NoteLength_04.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 4  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_5 == 1){
            step_Toggle_05.isHidden = false
            step_Velocity_05.enabled = true
            step_NoteLength_05.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 4  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_5 == 0){
            step_Toggle_05.isHidden = false
            step_Velocity_05.enabled = false
            step_NoteLength_05.enabled = false
        } else {
            step_Toggle_05.isHidden = true
            step_Velocity_05.enabled = false
            step_NoteLength_05.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 5  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_6 == 1){
            step_Toggle_06.isHidden = false
            step_Velocity_06.enabled = true
            step_NoteLength_06.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 5  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_6 == 0){
            step_Toggle_06.isHidden = false
            step_Velocity_06.enabled = false
            step_NoteLength_06.enabled = false
        } else {
            step_Toggle_06.isHidden = true
            step_Velocity_06.enabled = false
            step_NoteLength_06.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 6  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_7 == 1){
            step_Toggle_07.isHidden = false
            step_Velocity_07.enabled = true
            step_NoteLength_07.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 6  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_7 == 0){
            step_Toggle_07.isHidden = false
            step_Velocity_07.enabled = false
            step_NoteLength_07.enabled = false
        } else {
            step_Toggle_07.isHidden = true
            step_Velocity_07.enabled = false
            step_NoteLength_07.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 7  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_8 == 1){
            step_Toggle_08.isHidden = false
            step_Velocity_08.enabled = true
            step_NoteLength_08.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 7  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_8 == 0){
            step_Toggle_08.isHidden = false
            step_Velocity_08.enabled = false
            step_NoteLength_08.enabled = false
        } else {
            step_Toggle_08.isHidden = true
            step_Velocity_08.enabled = false
            step_NoteLength_08.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 8  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_9 == 1){
            step_Toggle_09.isHidden = false
            step_Velocity_09.enabled = true
            step_NoteLength_09.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 8  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_9 == 0){
            step_Toggle_09.isHidden = false
            step_Velocity_09.enabled = false
            step_NoteLength_09.enabled = false
        } else {
            step_Toggle_09.isHidden = true
            step_Velocity_09.enabled = false
            step_NoteLength_09.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 9  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_10 == 1){
            step_Toggle_10.isHidden = false
            step_Velocity_10.enabled = true
            step_NoteLength_10.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 9  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_10 == 0){
            step_Toggle_10.isHidden = false
            step_Velocity_10.enabled = false
            step_NoteLength_10.enabled = false
        } else {
            step_Toggle_10.isHidden = true
            step_Velocity_10.enabled = false
            step_NoteLength_10.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 10  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_11 == 1){
            step_Toggle_11.isHidden = false
            step_Velocity_11.enabled = true
            step_NoteLength_11.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 10  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_11 == 0){
            step_Toggle_11.isHidden = false
            step_Velocity_11.enabled = false
            step_NoteLength_11.enabled = false
        } else {
            step_Toggle_11.isHidden = true
            step_Velocity_11.enabled = false
            step_NoteLength_11.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 11  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_12 == 1){
            step_Toggle_12.isHidden = false
            step_Velocity_12.enabled = true
            step_NoteLength_12.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 11  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_12 == 0){
            step_Toggle_12.isHidden = false
            step_Velocity_12.enabled = false
            step_NoteLength_12.enabled = false
        } else {
            step_Toggle_12.isHidden = true
            step_Velocity_12.enabled = false
            step_NoteLength_12.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 12  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_13 == 1){
            step_Toggle_13.isHidden = false
            step_Velocity_13.enabled = true
            step_NoteLength_13.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 12  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_13 == 0){
            step_Toggle_13.isHidden = false
            step_Velocity_13.enabled = false
            step_NoteLength_13.enabled = false
        } else {
            step_Toggle_13.isHidden = true
            step_Velocity_13.enabled = false
            step_NoteLength_13.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 13  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_14 == 1){
            step_Toggle_14.isHidden = false
            step_Velocity_14.enabled = true
            step_NoteLength_14.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 13  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_14 == 0){
            step_Toggle_14.isHidden = false
            step_Velocity_14.enabled = false
            step_NoteLength_14.enabled = false
        } else {
            step_Toggle_14.isHidden = true
            step_Velocity_14.enabled = false
            step_NoteLength_14.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 14  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_15 == 1){
            step_Toggle_15.isHidden = false
            step_Velocity_15.enabled = true
            step_NoteLength_15.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 14  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_15 == 0){
            step_Toggle_15.isHidden = false
            step_Velocity_15.enabled = false
            step_NoteLength_15.enabled = false
        } else {
            step_Toggle_15.isHidden = true
            step_Velocity_15.enabled = false
            step_NoteLength_15.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 15  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_16 == 1){
            step_Toggle_16.isHidden = false
            step_Velocity_16.enabled = true
            step_NoteLength_16.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 15  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_16 == 0){
            step_Toggle_16.isHidden = false
            step_Velocity_16.enabled = false
            step_NoteLength_16.enabled = false
        } else {
            step_Toggle_16.isHidden = true
            step_Velocity_16.enabled = false
            step_NoteLength_16.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 16  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_17 == 1){
            step_Toggle_17.isHidden = false
            step_Velocity_17.enabled = true
            step_NoteLength_17.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 16  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_17 == 0){
            step_Toggle_17.isHidden = false
            step_Velocity_17.enabled = false
            step_NoteLength_17.enabled = false
        } else {
            step_Toggle_17.isHidden = true
            step_Velocity_17.enabled = false
            step_NoteLength_17.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 17  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_18 == 1){
            step_Toggle_18.isHidden = false
            step_Velocity_18.enabled = true
            step_NoteLength_18.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 17  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_18 == 0){
            step_Toggle_18.isHidden = false
            step_Velocity_18.enabled = false
            step_NoteLength_18.enabled = false
        } else {
            step_Toggle_18.isHidden = true
            step_Velocity_18.enabled = false
            step_NoteLength_18.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 18  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_19 == 1){
            step_Toggle_19.isHidden = false
            step_Velocity_19.enabled = true
            step_NoteLength_19.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 18  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_19 == 0){
            step_Toggle_19.isHidden = false
            step_Velocity_19.enabled = false
            step_NoteLength_19.enabled = false
        } else {
            step_Toggle_19.isHidden = true
            step_Velocity_19.enabled = false
            step_NoteLength_19.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 19  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_20 == 1){
            step_Toggle_20.isHidden = false
            step_Velocity_20.enabled = true
            step_NoteLength_20.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 19  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_20 == 0){
            step_Toggle_20.isHidden = false
            step_Velocity_20.enabled = false
            step_NoteLength_20.enabled = false
        } else {
            step_Toggle_20.isHidden = true
            step_Velocity_20.enabled = false
            step_NoteLength_20.enabled = false
        }
        
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 20  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_21 == 1){
            step_Toggle_21.isHidden = false
            step_Velocity_21.enabled = true
            step_NoteLength_21.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 20  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_21 == 0){
            step_Toggle_21.isHidden = false
            step_Velocity_21.enabled = false
            step_NoteLength_21.enabled = false
        } else {
            step_Toggle_21.isHidden = true
            step_Velocity_21.enabled = false
            step_NoteLength_21.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 21  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_22 == 1){
            step_Toggle_22.isHidden = false
            step_Velocity_22.enabled = true
            step_NoteLength_22.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 21  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_22 == 0){
            step_Toggle_22.isHidden = false
            step_Velocity_22.enabled = false
            step_NoteLength_22.enabled = false
        } else {
            step_Toggle_22.isHidden = true
            step_Velocity_22.enabled = false
            step_NoteLength_22.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 22  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_23 == 1){
            step_Toggle_23.isHidden = false
            step_Velocity_23.enabled = true
            step_NoteLength_23.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 22  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_23 == 0){
            step_Toggle_23.isHidden = false
            step_Velocity_23.enabled = false
            step_NoteLength_23.enabled = false
        } else {
            step_Toggle_23.isHidden = true
            step_Velocity_23.enabled = false
            step_NoteLength_23.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 23  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_24 == 1){
            step_Toggle_24.isHidden = false
            step_Velocity_24.enabled = true
            step_NoteLength_24.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 23  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_24 == 0){
            step_Toggle_24.isHidden = false
            step_Velocity_24.enabled = false
            step_NoteLength_24.enabled = false
        } else {
            step_Toggle_24.isHidden = true
            step_Velocity_24.enabled = false
            step_NoteLength_24.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 24  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_25 == 1){
            step_Toggle_25.isHidden = false
            step_Velocity_25.enabled = true
            step_NoteLength_25.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 24  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_25 == 0){
            step_Toggle_25.isHidden = false
            step_Velocity_25.enabled = false
            step_NoteLength_25.enabled = false
        } else {
            step_Toggle_25.isHidden = true
            step_Velocity_25.enabled = false
            step_NoteLength_25.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 25  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_26 == 1){
            step_Toggle_26.isHidden = false
            step_Velocity_26.enabled = true
            step_NoteLength_26.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 25  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_26 == 0){
            step_Toggle_26.isHidden = false
            step_Velocity_26.enabled = false
            step_NoteLength_26.enabled = false
        } else {
            step_Toggle_26.isHidden = true
            step_Velocity_26.enabled = false
            step_NoteLength_26.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 26  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_27 == 1){
            step_Toggle_27.isHidden = false
            step_Velocity_27.enabled = true
            step_NoteLength_27.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 26  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_27 == 0){
            step_Toggle_27.isHidden = false
            step_Velocity_27.enabled = false
            step_NoteLength_27.enabled = false
        } else {
            step_Toggle_27.isHidden = true
            step_Velocity_27.enabled = false
            step_NoteLength_27.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 27  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_28 == 1){
            step_Toggle_28.isHidden = false
            step_Velocity_28.enabled = true
            step_NoteLength_28.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 27  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_28 == 0){
            step_Toggle_28.isHidden = false
            step_Velocity_28.enabled = false
            step_NoteLength_28.enabled = false
        } else {
            step_Toggle_28.isHidden = true
            step_Velocity_28.enabled = false
            step_NoteLength_28.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 28  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_29 == 1){
            step_Toggle_29.isHidden = false
            step_Velocity_29.enabled = true
            step_NoteLength_29.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 28  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_29 == 0){
            step_Toggle_29.isHidden = false
            step_Velocity_29.enabled = false
            step_NoteLength_29.enabled = false
        } else {
            step_Toggle_29.isHidden = true
            step_Velocity_29.enabled = false
            step_NoteLength_29.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 29  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_30 == 1){
            step_Toggle_30.isHidden = false
            step_Velocity_30.enabled = true
            step_NoteLength_30.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 29  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_30 == 0){
            step_Toggle_30.isHidden = false
            step_Velocity_30.enabled = false
            step_NoteLength_30.enabled = false
        } else {
            step_Toggle_30.isHidden = true
            step_Velocity_30.enabled = false
            step_NoteLength_30.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 30  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_31 == 1){
            step_Toggle_31.isHidden = false
            step_Velocity_31.enabled = true
            step_NoteLength_31.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 30  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_31 == 0){
            step_Toggle_31.isHidden = false
            step_Velocity_31.enabled = false
            step_NoteLength_31.enabled = false
        } else {
            step_Toggle_31.isHidden = true
            step_Velocity_31.enabled = false
            step_NoteLength_31.enabled = false
        }
        
        if (patchCtrl.CurrentPatch.arp_patternLength >= 31  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_32 == 1){
            step_Toggle_32.isHidden = false
            step_Velocity_32.enabled = true
            step_NoteLength_32.enabled = true
        } else if (patchCtrl.CurrentPatch.arp_patternLength >= 31  && patchCtrl.CurrentPatch.arp_mode != 0 && patchCtrl.CurrentPatch.arp_step_32 == 0){
            step_Toggle_32.isHidden = false
            step_Velocity_32.enabled = false
            step_NoteLength_32.enabled = false
        } else {
            step_Toggle_32.isHidden = true
            step_Velocity_32.enabled = false
            step_NoteLength_32.enabled = false
        }
    }
    
    func setupCallbacks() {
        
        PatternLengthStepper.callback = { value in
            patchCtrl.setArpPatternLength(value: Int(value - 1))
            self.updateView()
        }
        
        step_Toggle_01.callback = { value in
            patchCtrl.setArpStep_1(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_02.callback = { value in
            patchCtrl.setArpStep_2(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_03.callback = { value in
            patchCtrl.setArpStep_3(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_04.callback = { value in
            patchCtrl.setArpStep_4(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_05.callback = { value in
            patchCtrl.setArpStep_5(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_06.callback = { value in
            patchCtrl.setArpStep_6(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_07.callback = { value in
            patchCtrl.setArpStep_7(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_08.callback = { value in
            patchCtrl.setArpStep_8(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_09.callback = { value in
            patchCtrl.setArpStep_9(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_10.callback = { value in
            patchCtrl.setArpStep_10(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_11.callback = { value in
            patchCtrl.setArpStep_11(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_12.callback = { value in
            patchCtrl.setArpStep_12(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_13.callback = { value in
            patchCtrl.setArpStep_13(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_14.callback = { value in
            patchCtrl.setArpStep_14(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_15.callback = { value in
            patchCtrl.setArpStep_15(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_16.callback = { value in
            patchCtrl.setArpStep_16(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_17.callback = { value in
            patchCtrl.setArpStep_17(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_18.callback = { value in
            patchCtrl.setArpStep_18(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_19.callback = { value in
            patchCtrl.setArpStep_19(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_20.callback = { value in
            patchCtrl.setArpStep_20(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_21.callback = { value in
            patchCtrl.setArpStep_21(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_22.callback = { value in
            patchCtrl.setArpStep_22(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_23.callback = { value in
            patchCtrl.setArpStep_23(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_24.callback = { value in
            patchCtrl.setArpStep_24(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_25.callback = { value in
            patchCtrl.setArpStep_25(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_26.callback = { value in
            patchCtrl.setArpStep_26(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_27.callback = { value in
            patchCtrl.setArpStep_27(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_28.callback = { value in
            patchCtrl.setArpStep_28(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_29.callback = { value in
            patchCtrl.setArpStep_29(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_30.callback = { value in
            patchCtrl.setArpStep_30(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_31.callback = { value in
            patchCtrl.setArpStep_31(value: Int(value))
            self.updateView()
        }
        
        step_Toggle_32.callback = { value in
            patchCtrl.setArpStep_32(value: Int(value))
            self.updateView()
        }
        
        
        
        step_NoteLength_01.callback = { value in
            patchCtrl.setArpStepLength_1(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_02.callback = { value in
            patchCtrl.setArpStepLength_2(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_03.callback = { value in
            patchCtrl.setArpStepLength_3(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_04.callback = { value in
            patchCtrl.setArpStepLength_4(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_05.callback = { value in
            patchCtrl.setArpStepLength_5(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_06.callback = { value in
            patchCtrl.setArpStepLength_6(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_07.callback = { value in
            patchCtrl.setArpStepLength_7(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_08.callback = { value in
            patchCtrl.setArpStepLength_8(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_09.callback = { value in
            patchCtrl.setArpStepLength_9(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_10.callback = { value in
            patchCtrl.setArpStepLength_10(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_11.callback = { value in
            patchCtrl.setArpStepLength_11(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_12.callback = { value in
            patchCtrl.setArpStepLength_12(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_13.callback = { value in
            patchCtrl.setArpStepLength_13(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_14.callback = { value in
            patchCtrl.setArpStepLength_14(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_15.callback = { value in
            patchCtrl.setArpStepLength_15(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_16.callback = { value in
            patchCtrl.setArpStepLength_16(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_17.callback = { value in
            patchCtrl.setArpStepLength_17(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_18.callback = { value in
            patchCtrl.setArpStepLength_18(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_19.callback = { value in
            patchCtrl.setArpStepLength_19(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_20.callback = { value in
            patchCtrl.setArpStepLength_20(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_21.callback = { value in
            patchCtrl.setArpStepLength_21(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_22.callback = { value in
            patchCtrl.setArpStepLength_22(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_23.callback = { value in
            patchCtrl.setArpStepLength_23(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_24.callback = { value in
            patchCtrl.setArpStepLength_24(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_25.callback = { value in
            patchCtrl.setArpStepLength_25(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_26.callback = { value in
            patchCtrl.setArpStepLength_26(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_27.callback = { value in
            patchCtrl.setArpStepLength_27(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_28.callback = { value in
            patchCtrl.setArpStepLength_28(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_29.callback = { value in
            patchCtrl.setArpStepLength_29(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_30.callback = { value in
            patchCtrl.setArpStepLength_30(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_31.callback = { value in
            patchCtrl.setArpStepLength_31(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_NoteLength_32.callback = { value in
            patchCtrl.setArpStepLength_32(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        
        step_Velocity_01.callback = { value in
            patchCtrl.setArpStepVelocity_1(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_02.callback = { value in
            patchCtrl.setArpStepVelocity_2(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_03.callback = { value in
            patchCtrl.setArpStepVelocity_3(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_04.callback = { value in
            patchCtrl.setArpStepVelocity_4(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_05.callback = { value in
            patchCtrl.setArpStepVelocity_5(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_06.callback = { value in
            patchCtrl.setArpStepVelocity_6(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_07.callback = { value in
            patchCtrl.setArpStepVelocity_7(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_08.callback = { value in
            patchCtrl.setArpStepVelocity_8(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_09.callback = { value in
            patchCtrl.setArpStepVelocity_9(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_10.callback = { value in
            patchCtrl.setArpStepVelocity_10(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_11.callback = { value in
            patchCtrl.setArpStepVelocity_11(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_12.callback = { value in
            patchCtrl.setArpStepVelocity_12(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_13.callback = { value in
            patchCtrl.setArpStepVelocity_13(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_14.callback = { value in
            patchCtrl.setArpStepVelocity_14(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_15.callback = { value in
            patchCtrl.setArpStepVelocity_15(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_16.callback = { value in
            patchCtrl.setArpStepVelocity_16(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_17.callback = { value in
            patchCtrl.setArpStepVelocity_17(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_18.callback = { value in
            patchCtrl.setArpStepVelocity_18(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_19.callback = { value in
            patchCtrl.setArpStepVelocity_19(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_20.callback = { value in
            patchCtrl.setArpStepVelocity_20(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_21.callback = { value in
            patchCtrl.setArpStepVelocity_21(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_22.callback = { value in
            patchCtrl.setArpStepVelocity_22(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_23.callback = { value in
            patchCtrl.setArpStepVelocity_23(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_24.callback = { value in
            patchCtrl.setArpStepVelocity_24(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_25.callback = { value in
            patchCtrl.setArpStepVelocity_25(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_26.callback = { value in
            patchCtrl.setArpStepVelocity_26(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_27.callback = { value in
            patchCtrl.setArpStepVelocity_27(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_28.callback = { value in
            patchCtrl.setArpStepVelocity_28(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_29.callback = { value in
            patchCtrl.setArpStepVelocity_29(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_30.callback = { value in
            patchCtrl.setArpStepVelocity_30(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_31.callback = { value in
            patchCtrl.setArpStepVelocity_31(value: Int(value))
            self.ArpPAtternChart.update()
        }
        
        step_Velocity_32.callback = { value in
            patchCtrl.setArpStepVelocity_32(value: Int(value))
            self.ArpPAtternChart.update()
        }
    }
    
    
    @IBAction func ActivateUSerPAttern(_ sender: PMSuperButton) {
        patchCtrl.setArpPattern(value: 0)
        ActivatePatternBtn.isEnabled = false
        self.updateView()
    }
    
    @IBAction func ArpModechanged(_ sender: PMSuperButton) {
        let ArpModesPopover = ArpModePopover()
        ArpModesPopover.tableView.dataSource = ArpModeLbls!
        ArpModesPopover.tableView.delegate = ArpModesPopover
        ArpModesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ArpModesPopover, animated: true, completion: nil)
        let popoverPresentationController = ArpModesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
       
    }
    
}
