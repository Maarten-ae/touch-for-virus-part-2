//
//  SVC_Mods_LFO.swift
//  Rage
//
//  Created by M. Lierop on 09-01-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//


import Foundation
import UIKit

class ModsLFOSubView: UIViewController {
    
    private var LFOWaves:LFOWaveSources?
    private var LFOSrc:LFODestinationSources?
    private var LFO3Src:LFO3DestinationSources?
    
    
    @IBOutlet weak var LFO1ClockRateLbl: UILabel!
    @IBOutlet weak var LFO1WaveShapeSelector: UISegmentedControl!
    @IBOutlet weak var LFO1RateKnob: Knob!
    @IBOutlet weak var LFO1Contour: Knob!
    @IBOutlet weak var LFO1Phase: Knob!
    @IBOutlet weak var LFO1KeyFollow: Knob!
    @IBOutlet weak var LFO1Osc1: Knob!
    @IBOutlet weak var LFO1Osc2: Knob!
    @IBOutlet weak var LFO1PulseWidth: Knob!
    @IBOutlet weak var LFO1Resonance: Knob!
    @IBOutlet weak var LFO1FilterGain: Knob!
    @IBOutlet weak var LFO1Assign: Knob!
    @IBOutlet weak var LFO1ClockRateBtn: ToggleButton!
    @IBOutlet weak var LFO1DestinationBtn: PMSuperButton!
    @IBOutlet weak var LFO1EnvelopeModeBtn: ToggleButton!
    @IBOutlet weak var LFO1MonoPolyBtn: ToggleButton!
    
    @IBOutlet weak var LFO2ClockRateLbl: UILabel!
    @IBOutlet weak var LFO2ClockRateBtn: ToggleButton!
    @IBOutlet weak var LFO2WaveShapeSelector: UISegmentedControl!
    @IBOutlet weak var LFO2Rate: Knob!
    @IBOutlet weak var LFO2Contour: Knob!
    @IBOutlet weak var LFO2Phase: Knob!
    @IBOutlet weak var LFO2KeyFollow: Knob!
    @IBOutlet weak var LFO2Filter1: Knob!
    @IBOutlet weak var LFO2Filter2: Knob!
    @IBOutlet weak var LFO2Shape: Knob!
    @IBOutlet weak var LFO2Pan: Knob!
    @IBOutlet weak var LFO2FMAmount: Knob!
    @IBOutlet weak var LFO2Assign: Knob!
    @IBOutlet weak var LFO2DestinationBtn: PMSuperButton!
    @IBOutlet weak var LFO2EnvelopeModeBtn: ToggleButton!
    @IBOutlet weak var LFO2MonoPolyBtn: ToggleButton!
    
    @IBOutlet weak var LFO3ClockRateLbl: UILabel!
    @IBOutlet weak var LFO3ClockRateBtn: ToggleButton!
    @IBOutlet weak var LFO3WaveShapeSelector1: UISegmentedControl!
    @IBOutlet weak var LFO3WaveShapeSelector2: UISegmentedControl!
    @IBOutlet weak var LFO3Rate: Knob!
    @IBOutlet weak var LFO3Fade: Knob!
    @IBOutlet weak var LFO3KeyFollow: Knob!
    @IBOutlet weak var LFO3Assign: Knob!
    @IBOutlet weak var LFO3DestinationBtn: PMSuperButton!
    @IBOutlet weak var LFO3MonoPolyBtn: ToggleButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallbacks()
        LFOWaves = LFOWaveSources()
        LFOSrc = LFODestinationSources()
        LFO3Src = LFO3DestinationSources()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLFOWave(notification:)), name: Notification.Name("LFOWaveChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLFODestination(notification:)), name: Notification.Name("LFODestinationChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
       
        LFO1DestinationBtn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.lfo1_UserDestination], for: .normal)
        
        if ( patchCtrl.CurrentPatch.lfo1_Clock >= 1 ) {
            
            LFO1RateKnob.maximum = 21
            LFO1RateKnob.minimum = 1
            LFO1RateKnob.value = Double(patchCtrl.CurrentPatch.lfo1_Clock)
        } else {

            LFO1RateKnob.maximum = 127
            LFO1RateKnob.minimum = 0
            LFO1RateKnob.value = Double(patchCtrl.CurrentPatch.lfo1_Rate)
        }
        
        if ( patchCtrl.CurrentPatch.lfo1_WaveformShape <= 5){
            LFO1WaveShapeSelector.setTitle("Wave", forSegmentAt: 6)
            LFO1WaveShapeSelector.selectedSegmentIndex = patchCtrl.CurrentPatch.lfo1_WaveformShape
        } else {
            LFO1WaveShapeSelector.selectedSegmentIndex = 6
            let title:String = "W " + String( patchCtrl.CurrentPatch.lfo1_WaveformShape - 3 )
            LFO1WaveShapeSelector.setTitle(title, forSegmentAt: 6)
        }
        
        
        LFO1Contour.value = Double(patchCtrl.CurrentPatch.lfo1_WaveformContour)
        LFO1Phase.value = Double(patchCtrl.CurrentPatch.lfo1_TriggerPhase)
        LFO1KeyFollow.value = Double(patchCtrl.CurrentPatch.lfo1_Keyfollow)
        LFO1Osc1.value = Double(patchCtrl.CurrentPatch.lfo1_Osc1)
        LFO1Osc2.value = Double(patchCtrl.CurrentPatch.lfo1_Osc2)
        LFO1PulseWidth.value = Double(patchCtrl.CurrentPatch.lfo1_Pulsewidth)
        LFO1Resonance.value = Double(patchCtrl.CurrentPatch.lfo1_FilterResonance12)
        LFO1FilterGain.value = Double(patchCtrl.CurrentPatch.lfo1_FilterEnvelopeGain)
        LFO1Assign.value = Double(patchCtrl.CurrentPatch.lfo1_UserDestinationAmount)
        
        LFO1DestinationBtn.setTitle(LFODestinations[safe: patchCtrl.CurrentPatch.lfo1_UserDestination ], for: .normal)
        LFO1EnvelopeModeBtn.setOn(val: (patchCtrl.CurrentPatch.lfo1_EnvelopeMode == 1))
        LFO1MonoPolyBtn.setOn(val: (patchCtrl.CurrentPatch.lfo1_Mode == 1))
        LFO1ClockRateBtn.setOn(val: (patchCtrl.CurrentPatch.lfo1_Clock >= 1))
        self.setLFO1RateLabel()
        
        // -------------------------------
        
        LFO2DestinationBtn.setTitle( LFODestinations[safe: patchCtrl.CurrentPatch.lfo2_UserDestination], for: .normal)
        
        if ( patchCtrl.CurrentPatch.lfo2_Clock >= 1 ) {
            
            LFO2Rate.maximum = 21
            LFO2Rate.minimum = 1
            LFO2Rate.value = Double(patchCtrl.CurrentPatch.lfo2_Clock)
        } else {
            
            LFO2Rate.maximum = 127
            LFO2Rate.minimum = 0
            LFO2Rate.value = Double(patchCtrl.CurrentPatch.lfo2_Rate)
        }
        
        if ( patchCtrl.CurrentPatch.lfo2_WaveformShape <= 5){
            LFO2WaveShapeSelector.setTitle("Wave", forSegmentAt: 6)
            LFO2WaveShapeSelector.selectedSegmentIndex = patchCtrl.CurrentPatch.lfo2_WaveformShape
        } else {
            LFO2WaveShapeSelector.selectedSegmentIndex = 6
            let title:String = "W " + String( patchCtrl.CurrentPatch.lfo2_WaveformShape - 3 )
            LFO2WaveShapeSelector.setTitle(title, forSegmentAt: 6)
        }
        
        LFO2Contour.value = Double(patchCtrl.CurrentPatch.lfo2_WaveformContour)
        LFO2Phase.value = Double(patchCtrl.CurrentPatch.lfo2_TriggerPhase)
        LFO2KeyFollow.value = Double(patchCtrl.CurrentPatch.lfo2_Keyfollow)
        LFO2Filter1.value = Double(patchCtrl.CurrentPatch.lfo2_Cutoff12)
        LFO2Filter2.value = Double(patchCtrl.CurrentPatch.lfo2_Cutoff2)
        LFO2Shape.value = Double(patchCtrl.CurrentPatch.lfo2_Shape12)
        LFO2Pan.value = Double(patchCtrl.CurrentPatch.lfo2_Panorama)
        LFO2FMAmount.value = Double(patchCtrl.CurrentPatch.lfo2_FMAmount)
        LFO2Assign.value = Double(patchCtrl.CurrentPatch.lfo2_UserDestinationAmount)
        
        LFO2DestinationBtn.setTitle(LFODestinations[safe: patchCtrl.CurrentPatch.lfo2_UserDestination ], for: .normal)
        LFO2EnvelopeModeBtn.setOn(val: (patchCtrl.CurrentPatch.lfo2_EnvelopeMode == 1))
        LFO2MonoPolyBtn.setOn(val: (patchCtrl.CurrentPatch.lfo2_Mode == 1))
        LFO2ClockRateBtn.setOn(val: (patchCtrl.CurrentPatch.lfo2_Clock >= 1))
        self.setLFO2RateLabel()
        
        // -------------------------------
        
        LFO3DestinationBtn.setTitle( LFO3Destinations[safe: patchCtrl.CurrentPatch.lfo3_UserDestination], for: .normal)
        
        if ( patchCtrl.CurrentPatch.lfo3_Clock >= 1 ) {
            
            LFO3Rate.maximum = 21
            LFO3Rate.minimum = 1
            LFO3Rate.value = Double(patchCtrl.CurrentPatch.lfo3_Clock)
        } else {
            
            LFO3Rate.maximum = 127
            LFO3Rate.minimum = 0
            LFO3Rate.value = Double(patchCtrl.CurrentPatch.lfo3_Rate)
        }
        
        if ( patchCtrl.CurrentPatch.lfo3_WaveformShape <= 2){
            LFO3WaveShapeSelector2.selectedSegmentIndex = UISegmentedControl.noSegment
            LFO3WaveShapeSelector1.selectedSegmentIndex = patchCtrl.CurrentPatch.lfo3_WaveformShape
            LFO3WaveShapeSelector2.setTitle("Wave", forSegmentAt: 3)
            
        } else if (patchCtrl.CurrentPatch.lfo3_WaveformShape >= 3 && patchCtrl.CurrentPatch.lfo3_WaveformShape <= 5 ) {
            LFO3WaveShapeSelector1.selectedSegmentIndex = UISegmentedControl.noSegment
            LFO3WaveShapeSelector2.selectedSegmentIndex = Int(patchCtrl.CurrentPatch.lfo3_WaveformShape - 3)
            LFO3WaveShapeSelector2.setTitle("Wave", forSegmentAt: 3)
            
        } else {
            LFO3WaveShapeSelector2.selectedSegmentIndex = 3
            let title:String = "W " + String( patchCtrl.CurrentPatch.lfo3_WaveformShape - 3 )
            LFO3WaveShapeSelector2.setTitle(title, forSegmentAt: 3)
        }
        
        LFO3Fade.value = Double(patchCtrl.CurrentPatch.lfo3_FadeInTime)
        LFO3KeyFollow.value = Double(patchCtrl.CurrentPatch.lfo3_Keyfollow)
        LFO3Assign.value = Double(patchCtrl.CurrentPatch.lfo3_UserDestinationAmount)
        LFO3DestinationBtn.setTitle(LFO3Destinations[ patchCtrl.CurrentPatch.lfo3_UserDestination ], for: .normal)
        LFO3MonoPolyBtn.setOn(val: (patchCtrl.CurrentPatch.lfo3_Mode == 1))
        LFO3ClockRateBtn.setOn(val: (patchCtrl.CurrentPatch.lfo3_Clock >= 1 ))
        self.setLFO3RateLabel()
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {

        LFO1RateKnob.callback = { value in
            if ( self.LFO1ClockRateBtn.isOn ) {
                patchCtrl.setLFO1Clock(value: Int(value))
            } else {
                patchCtrl.setLFO1Rate(value: Int(value))
            }
            self.setLFO1RateLabel()
        }
        LFO1Contour.callback = { value in
            patchCtrl.setLFO1WaveformContour(value: Int(value))
        }
        LFO1Phase.callback = { value in
            patchCtrl.setLFO1TriggerPhase(value: Int(value))
        }
        LFO1KeyFollow.callback = { value in
            patchCtrl.setLFO1Keyfollow(value: Int(value))
        }
        LFO1Osc1.callback = { value in
            patchCtrl.setLFO1Osc1(value: Int(value))
        }
        LFO1Osc2.callback = { value in
            patchCtrl.setLFO1Osc2(value: Int(value))
        }
        LFO1PulseWidth.callback = { value in
            patchCtrl.setLFO1Pulsewidth(value: Int(value))
        }
        LFO1Resonance.callback = { value in
            patchCtrl.setLFO1FilterResonance12(value: Int(value))
        }
        LFO1FilterGain.callback = { value in
            patchCtrl.setLFO1FilterEnvelopeGain(value: Int(value))
        }
        LFO1Assign.callback = { value in
            patchCtrl.setLFO1UserDestinationAmount(value: Int(value))
        }
        
        LFO2Rate.callback = { value in
            if ( self.LFO2ClockRateBtn.isOn ) {
                patchCtrl.setLFO2Clock(value: Int(value))
            } else {
                patchCtrl.setLFO2Rate(value: Int(value))
            }
            self.setLFO2RateLabel()
        }
        LFO2Contour.callback = { value in
            patchCtrl.setLFO2WaveformContour(value: Int(value))
        }
        LFO2Phase.callback = { value in
            patchCtrl.setLFO2TriggerPhase(value: Int(value))
        }
        LFO2KeyFollow.callback = { value in
            patchCtrl.setLFO2Keyfollow(value: Int(value))
        }
        LFO2Filter1.callback = { value in
            patchCtrl.setLFO2Cutoff12(value: Int(value))
        }
        LFO2Filter2.callback = { value in
            patchCtrl.setLFO2Cutoff2(value: Int(value))
        }
        LFO2Shape.callback = { value in
            patchCtrl.setLFO2Shape12(value: Int(value))
        }
        LFO2Pan.callback = { value in
            patchCtrl.setLFO2Panorama(value: Int(value))
        }
        LFO2FMAmount.callback = { value in
            patchCtrl.setLFO2FMAmount(value: Int(value))
        }
        LFO2Assign.callback = { value in
            patchCtrl.setLFO2UserDestinationAmount(value: Int(value))
        }
        
        LFO3Rate.callback = { value in
            if ( self.LFO3ClockRateBtn.isOn ) {
                patchCtrl.setLFO3Clock(value: Int(value))
            } else {
                patchCtrl.setLFO3Rate(value: Int(value))
            }
            self.setLFO3RateLabel()
        }
        LFO3Fade.callback = { value in
            patchCtrl.setLFO3FadeInTime(value: Int(value))
        }
        LFO3KeyFollow.callback = { value in
            patchCtrl.setLFO3KeyFollow(value: Int(value))
        }
        LFO3Assign.callback = { value in
            patchCtrl.setLFO3UserDestinationAmount(value: Int(value))
        }
        
        //clockmodes
        LFO1ClockRateBtn.callback = { value in
            if ( value == 1.0 ) {
                patchCtrl.setLFO1Clock(value: 1)
            } else {
                patchCtrl.setLFO1Clock(value: 0)
            }
            self.updateView()
        }
        LFO2ClockRateBtn.callback = { value in
            if ( value == 1.0 ) {
                patchCtrl.setLFO2Clock(value: 1)
            } else {
                patchCtrl.setLFO2Clock(value: 0)
            }
            self.updateView()
        }
        LFO3ClockRateBtn.callback = { value in
            if ( value == 1.0 ) {
                patchCtrl.setLFO3Clock(value: 1)
            } else {
                patchCtrl.setLFO3Clock(value: 0)
            }
            self.updateView()
        }
        
        
        // env modes
        LFO1EnvelopeModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO1EnvelopeMode(value: 1)
            } else {
                patchCtrl.setLFO1EnvelopeMode(value: 0)
            }
        }
        LFO2EnvelopeModeBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO2EnvelopeMode(value: 1)
            } else {
                patchCtrl.setLFO2EnvelopeMode(value: 0)
            }
        }
 
        // poly modes
        LFO1MonoPolyBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO1Mode(value: 1)
            } else {
                patchCtrl.setLFO1Mode(value: 0)
            }
        }
        
        LFO2MonoPolyBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO2Mode(value: 1)
            } else {
                patchCtrl.setLFO2Mode(value: 0)
            }
        }
        
        LFO3MonoPolyBtn.callback = { value in
            if ( value == 1.0 ){
                patchCtrl.setLFO3Mode(value: 1)
            } else {
                patchCtrl.setLFO3Mode(value: 0)
            }
        }
    }
    
    func setLFO1RateLabel() {
        if ( self.LFO1ClockRateBtn.isOn ) {
            self.LFO1ClockRateLbl.text = LFOClockValues[Int(patchCtrl.CurrentPatch.lfo1_Clock)]
        } else {
            self.LFO1ClockRateLbl.text = String(patchCtrl.CurrentPatch.lfo1_Rate)
        }
    }
    
    func setLFO2RateLabel() {
        if ( self.LFO2ClockRateBtn.isOn ) {
            self.LFO2ClockRateLbl.text = LFOClockValues[Int(patchCtrl.CurrentPatch.lfo2_Clock)]
        } else {
            self.LFO2ClockRateLbl.text = String(patchCtrl.CurrentPatch.lfo2_Rate)
        }
    }
    
    func setLFO3RateLabel() {
        if ( self.LFO3ClockRateBtn.isOn ) {
            self.LFO3ClockRateLbl.text = LFOClockValues[Int(patchCtrl.CurrentPatch.lfo3_Clock)]
        } else {
            self.LFO3ClockRateLbl.text = String(patchCtrl.CurrentPatch.lfo3_Rate)
        }
    }
    
    
    
    @IBAction func ChangeLFO1WaveShape(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 0:
            patchCtrl.setLFO1WaveformShape(value: 0)
        case 1:
            patchCtrl.setLFO1WaveformShape(value: 1)
        case 2:
            patchCtrl.setLFO1WaveformShape(value: 2)
        case 3:
            patchCtrl.setLFO1WaveformShape(value: 3)
        case 4:
            patchCtrl.setLFO1WaveformShape(value: 4)
        case 5:
            patchCtrl.setLFO1WaveformShape(value: 5)
        case 6:
            
            let LFO1WavePopover = LFOWavePopover()
            LFO1WavePopover.LFONr = 1
            LFO1WavePopover.tableView.dataSource = LFOWaves!
            LFO1WavePopover.tableView.delegate = LFO1WavePopover
            LFO1WavePopover.modalPresentationStyle = UIModalPresentationStyle.popover
            
            present(LFO1WavePopover, animated: true, completion: nil)
            let popoverPresentationController = LFO1WavePopover.popoverPresentationController
            popoverPresentationController?.sourceView = sender
        default:
            print("foutje")
        }
    }
    
    @IBAction func ChangeLFO2WaveShape(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 0:
            patchCtrl.setLFO2WaveformShape(value: 0)
        case 1:
            patchCtrl.setLFO2WaveformShape(value: 1)
        case 2:
            patchCtrl.setLFO2WaveformShape(value: 2)
        case 3:
            patchCtrl.setLFO2WaveformShape(value: 3)
        case 4:
            patchCtrl.setLFO2WaveformShape(value: 4)
        case 5:
            patchCtrl.setLFO2WaveformShape(value: 5)
        case 6:
            
            let LFO2WavePopover = LFOWavePopover()
            LFO2WavePopover.LFONr = 2
            LFO2WavePopover.tableView.dataSource = LFOWaves!
            LFO2WavePopover.tableView.delegate = LFO2WavePopover
            LFO2WavePopover.modalPresentationStyle = UIModalPresentationStyle.popover
            
            present(LFO2WavePopover, animated: true, completion: nil)
            let popoverPresentationController = LFO2WavePopover.popoverPresentationController
            popoverPresentationController?.sourceView = sender
        default:
            print("foutje")
        }
    }
    
    @IBAction func ChangeLFO3WaveShape(_ sender: UISegmentedControl) {
        print("clicked shape ")
        if ( sender.restorationIdentifier == "control1" ){
            print("controlbar 1: ",sender.selectedSegmentIndex)
            switch (sender.selectedSegmentIndex){
            case 0:
                patchCtrl.setLFO3WaveformShape(value: 0)
            case 1:
                patchCtrl.setLFO3WaveformShape(value: 1)
            case 2:
                patchCtrl.setLFO3WaveformShape(value: 2)
            default:
                print("foutje")
            }
            LFO3WaveShapeSelector2.selectedSegmentIndex = UISegmentedControl.noSegment
        } else {
            print("controlbar 2: ",sender.selectedSegmentIndex)
            switch (sender.selectedSegmentIndex){
            case 0:
                patchCtrl.setLFO3WaveformShape(value: 3)
            case 1:
                patchCtrl.setLFO3WaveformShape(value: 4)
            case 2:
                patchCtrl.setLFO3WaveformShape(value: 5)
            case 3:
                
                let LFO3WavePopover = LFOWavePopover()
                LFO3WavePopover.LFONr = 3
                LFO3WavePopover.tableView.dataSource = LFOWaves!
                LFO3WavePopover.tableView.delegate = LFO3WavePopover
                LFO3WavePopover.modalPresentationStyle = UIModalPresentationStyle.popover
                
                present(LFO3WavePopover, animated: true, completion: nil)
                let popoverPresentationController = LFO3WavePopover.popoverPresentationController
                popoverPresentationController?.sourceView = sender
            default:
                print("foutje")
            }
            LFO3WaveShapeSelector1.selectedSegmentIndex = UISegmentedControl.noSegment
        }
        
    }
    
    @objc func updateLFOWave (notification: Notification){
        self.updateView()
    }
    
    @objc func updateLFODestination (notification: Notification){
        self.updateView()
    }
    
    
    @IBAction func LFO1DestinationPopover(_ sender: PMSuperButton) {
        let LFO1DestPopover = LFODestinationPopover()
        LFO1DestPopover.LFONr = 1
        LFO1DestPopover.tableView.dataSource = LFOSrc!
        LFO1DestPopover.tableView.delegate = LFO1DestPopover
        LFO1DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(LFO1DestPopover, animated: true, completion: nil)
        let popoverPresentationController = LFO1DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func LFO2DestinationPopover(_ sender: PMSuperButton) {
        let LFO2DestPopover = LFODestinationPopover()
        LFO2DestPopover.LFONr = 2
        LFO2DestPopover.tableView.dataSource = LFOSrc!
        LFO2DestPopover.tableView.delegate = LFO2DestPopover
        LFO2DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(LFO2DestPopover, animated: true, completion: nil)
        let popoverPresentationController = LFO2DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func LFO3DestinationPopover(_ sender: PMSuperButton) {
        let LFO3DestPopover = LFODestinationPopover()
        LFO3DestPopover.LFONr = 3
        LFO3DestPopover.tableView.dataSource = LFO3Src!
        LFO3DestPopover.tableView.delegate = LFO3DestPopover
        LFO3DestPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(LFO3DestPopover, animated: true, completion: nil)
        let popoverPresentationController = LFO3DestPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
}
