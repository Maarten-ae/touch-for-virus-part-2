//
//  SVC_Eff1Main.swift
//  Rage
//
//  Created by M. Lierop on 02-03-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

//


import Foundation
import UIKit

class Eff1AdvSubView: UIViewController {
    
    private var ChorusTypeLbls: ChorusTypeSrc?
    private var DistortionTypesLbls: DistortionTypeSrc?
    private var FilterBankTypeLbls: FilterBankTypeSrc?
    private var CharacterTypeLbls: CharacterTypeSrc?
    
    @IBOutlet weak var ChorusModeBtn: PMSuperButton!
    @IBOutlet weak var ChorusMixKnob: Knob!
    @IBOutlet weak var ChorusFeedbackKnob: Knob!
    @IBOutlet weak var ChorusDelayKnob: Knob!
    @IBOutlet weak var ChorusLFORateKnob: Knob!
    @IBOutlet weak var ChorusLFODepthKnob: Knob!
    @IBOutlet weak var ChorusLFOTypeSelect: UISegmentedControl!
    
    @IBOutlet weak var DistortionTypeBtn: PMSuperButton!
    @IBOutlet weak var DistortionMixKnob: Knob!
    @IBOutlet weak var DistortionIntensityKnob: Knob!
    @IBOutlet weak var DistortionDriveKnob: Knob!
    @IBOutlet weak var DistortionToneKnob: Knob!
    @IBOutlet weak var DistortionQualityKnob: Knob!
    @IBOutlet weak var DistortionTrebleKnob: Knob!
    @IBOutlet weak var DistortionHighCutKnob: Knob!
    
    @IBOutlet weak var PhaserMixKnob: Knob!
    @IBOutlet weak var PhaserFreqKnob: Knob!
    @IBOutlet weak var PhaserFeedbackKnob: Knob!
    @IBOutlet weak var PhaserSpreadKnob: Knob!
    @IBOutlet weak var PhaserStagesSelector: UISegmentedControl!
    @IBOutlet weak var PhaserLFORateKnob: Knob!
    @IBOutlet weak var PhaserLFODepthKnob: Knob!
    
    @IBOutlet weak var FilterBankModeBtn: PMSuperButton!
    @IBOutlet weak var FilterBankMixKnob: Knob!
    @IBOutlet weak var FilterBankFrequencyKnob: Knob!
    @IBOutlet weak var FilterBankKnob1: Knob!
    @IBOutlet weak var FilterBankKnob2: Knob!
    @IBOutlet weak var FilterBankKnob3: Knob!
    
    @IBOutlet weak var CharacterModeBtn: PMSuperButton!
    @IBOutlet weak var CharacterIntensityKnob: Knob!
    @IBOutlet weak var CharacterTuneKnob: Knob!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCallbacks()
        
        ChorusTypeLbls = ChorusTypeSrc()
        DistortionTypesLbls = DistortionTypeSrc()
        FilterBankTypeLbls = FilterBankTypeSrc()
        CharacterTypeLbls = CharacterTypeSrc()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("ChorusTypeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("DistortionTypeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("FilterBankTypeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("CharacterTypeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("UpdateView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        updateView()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //*****************************************************************
    // MARK: - Update all controls in this view
    //*****************************************************************
    
    @objc func updateView(){
        ChorusModeBtn.setTitle( ChorusTypes[safe:  patchCtrl.CurrentPatch.chorus_Type ], for: .normal)
        ChorusMixKnob.value = Double( patchCtrl.CurrentPatch.chorus_Mix )
        ChorusFeedbackKnob.value = Double( patchCtrl.CurrentPatch.chorus_Feedback )
        ChorusLFORateKnob.value = Double( patchCtrl.CurrentPatch.chorus_LFORate )
        ChorusLFODepthKnob.value = Double( patchCtrl.CurrentPatch.chorus_LFODepth )
        ChorusLFOTypeSelect.selectedSegmentIndex = patchCtrl.CurrentPatch.chorus_LFOShape
        
        switch patchCtrl.CurrentPatch.chorus_Type {
        case 1: // Classic
            ChorusMixKnob.enabled = true
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "Delay"
            ChorusDelayKnob.value = Double( patchCtrl.CurrentPatch.chorus_Delay )
            ChorusFeedbackKnob.enabled = true
            ChorusFeedbackKnob.label = "Feedback"
            ChorusLFORateKnob.enabled = true
            ChorusLFORateKnob.label = "Rate"
            ChorusLFODepthKnob.enabled = true
            ChorusLFODepthKnob.label = "Depth"
            ChorusLFOTypeSelect.isEnabled = true
        case 2: // Vintage
            ChorusMixKnob.enabled = true
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "X-over"
            ChorusDelayKnob.value = Double( patchCtrl.CurrentPatch.chorus_XOver )
            ChorusFeedbackKnob.enabled = false
            ChorusLFORateKnob.enabled = true
            ChorusLFODepthKnob.enabled = true
            ChorusLFODepthKnob.label = "Depth"
            ChorusLFOTypeSelect.isEnabled = false
        case 3: // Hyper Chorus
            ChorusMixKnob.enabled = true
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "X-over"
            ChorusDelayKnob.value = Double( patchCtrl.CurrentPatch.chorus_XOver )
            ChorusFeedbackKnob.enabled = true
            ChorusFeedbackKnob.label = "Amount"
            ChorusFeedbackKnob.value = Double( patchCtrl.CurrentPatch.chorus_Delay )
            ChorusLFORateKnob.enabled = false
            ChorusLFODepthKnob.enabled = true
            ChorusLFODepthKnob.label = "Depth"
            ChorusLFOTypeSelect.isEnabled = false
        case 4: // Air Chorus
            ChorusMixKnob.enabled = false
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "X-over"
            ChorusDelayKnob.value = Double( patchCtrl.CurrentPatch.chorus_XOver )
            ChorusFeedbackKnob.enabled = false
            ChorusLFORateKnob.enabled = false
            ChorusLFODepthKnob.enabled = true
            ChorusLFODepthKnob.label = "Depth"
            ChorusLFOTypeSelect.isEnabled = false
        case 5: // Vibrato
            ChorusMixKnob.enabled = false
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "X-over"
            ChorusDelayKnob.value = Double( patchCtrl.CurrentPatch.chorus_XOver )
            ChorusFeedbackKnob.enabled = false
            ChorusLFORateKnob.enabled = true
            ChorusLFODepthKnob.enabled = true
            ChorusLFODepthKnob.label = "Depth"
            ChorusLFOTypeSelect.isEnabled = false
        case 6:
            ChorusMixKnob.enabled = true
            ChorusDelayKnob.enabled = true
            ChorusDelayKnob.label = "Mic Angle"
            ChorusDelayKnob.value = Double( patchCtrl.CurrentPatch.chorus_Delay )
            ChorusFeedbackKnob.enabled = true
            ChorusFeedbackKnob.label = "Low/High Balance"
            ChorusLFORateKnob.enabled = false
            ChorusLFODepthKnob.enabled = true
            ChorusLFODepthKnob.label = "Distance"
            ChorusLFOTypeSelect.isEnabled = false
        default:
            ChorusMixKnob.enabled = false
            ChorusDelayKnob.enabled = false
            ChorusFeedbackKnob.enabled = false
            ChorusLFORateKnob.enabled = false
            ChorusLFODepthKnob.enabled = false
            ChorusLFOTypeSelect.isEnabled = false
        }
        
        
        DistortionTypeBtn.setTitle(DistortionTypes[safe:  patchCtrl.CurrentPatch.distortion_Type ], for: .normal)
        
        switch patchCtrl.CurrentPatch.distortion_Type {
        case 0:
            DistortionMixKnob.enabled = false
            DistortionIntensityKnob.enabled = false
            DistortionDriveKnob.enabled = false
            DistortionToneKnob.enabled = false
            DistortionQualityKnob.enabled = false
            DistortionTrebleKnob.enabled = false
            DistortionHighCutKnob.enabled = false
        case 1,2,3,4,5,6,7,12,13,14,15,16,17:
            DistortionMixKnob.enabled = true
            DistortionIntensityKnob.enabled = true
            DistortionDriveKnob.enabled = false
            DistortionToneKnob.enabled = false
            DistortionQualityKnob.enabled = false
            DistortionTrebleKnob.enabled = true
            DistortionHighCutKnob.enabled = true
        case 18,19:
            DistortionMixKnob.enabled = true
            DistortionIntensityKnob.enabled = true
            DistortionDriveKnob.enabled = false
            DistortionToneKnob.enabled = false
            DistortionQualityKnob.enabled = true
            DistortionTrebleKnob.enabled = false
            DistortionHighCutKnob.enabled = false
        case 8,9,10,11:
            DistortionMixKnob.enabled = true
            DistortionIntensityKnob.enabled = true
            DistortionDriveKnob.enabled = false
            DistortionToneKnob.enabled = false
            DistortionQualityKnob.enabled = false
            DistortionTrebleKnob.enabled = false
            DistortionHighCutKnob.enabled = false
        case 21,25:
            DistortionMixKnob.enabled = true
            DistortionIntensityKnob.enabled = false
            DistortionDriveKnob.enabled = true
            DistortionToneKnob.enabled = false
            DistortionQualityKnob.enabled = false
            DistortionTrebleKnob.enabled = false
            DistortionHighCutKnob.enabled = true
        case 20,22,23,24:
            DistortionMixKnob.enabled = true
            DistortionIntensityKnob.enabled = false
            DistortionDriveKnob.enabled = true
            DistortionToneKnob.enabled = true
            DistortionQualityKnob.enabled = false
            DistortionTrebleKnob.enabled = false
            DistortionHighCutKnob.enabled = true
        default:
            DistortionMixKnob.enabled = false
            DistortionIntensityKnob.enabled = false
            DistortionDriveKnob.enabled = false
            DistortionToneKnob.enabled = false
            DistortionQualityKnob.enabled = false
            DistortionTrebleKnob.enabled = false
            DistortionHighCutKnob.enabled = false
        }
        
        DistortionMixKnob.value = Double( patchCtrl.CurrentPatch.distortion_Mix )
        DistortionIntensityKnob.value = Double( patchCtrl.CurrentPatch.distortion_Intensity )
        DistortionDriveKnob.value = Double( patchCtrl.CurrentPatch.distortion_Intensity )
        DistortionToneKnob.value = Double( patchCtrl.CurrentPatch.distortion_ToneW )
        DistortionQualityKnob.value = Double( patchCtrl.CurrentPatch.distortion_Quality )
        DistortionTrebleKnob.value = Double( patchCtrl.CurrentPatch.distortion_TrebleBooster)
        DistortionHighCutKnob.value = Double( patchCtrl.CurrentPatch.distortion_HighCut )
        
        PhaserMixKnob.value = Double( patchCtrl.CurrentPatch.phaser_Mix )
        PhaserFreqKnob.value = Double( patchCtrl.CurrentPatch.phaser_Frequency )
        PhaserFeedbackKnob.value = Double( patchCtrl.CurrentPatch.phaser_Feedback )
        PhaserSpreadKnob.value = Double( patchCtrl.CurrentPatch.phaser_Spread )
        PhaserStagesSelector.selectedSegmentIndex = patchCtrl.CurrentPatch.phaser_Stages
        PhaserLFORateKnob.value = Double( patchCtrl.CurrentPatch.phaser_LFORate )
        PhaserLFODepthKnob.value = Double( patchCtrl.CurrentPatch.phaser_Depth )
        
        FilterBankModeBtn.setTitle( FilterBankTypes[safe:  patchCtrl.CurrentPatch.frequencyShifter_Type ], for: .normal)
        FilterBankMixKnob.value = Double( patchCtrl.CurrentPatch.frequencyShifter_Mix )
        FilterBankFrequencyKnob.value = Double( patchCtrl.CurrentPatch.frequencyShifter_Frequency )
        switch patchCtrl.CurrentPatch.frequencyShifter_Type {
        case 0:
            FilterBankMixKnob.enabled = false    // Mix
            FilterBankFrequencyKnob.enabled = false     // Frequency
            FilterBankKnob1.knobLabel = ""
            FilterBankKnob1.enabled = false
            FilterBankKnob2.knobLabel = ""// Resonance
            FilterBankKnob2.enabled = false
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 1:
            FilterBankMixKnob.enabled = true
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = ""
            FilterBankKnob1.enabled = false
            FilterBankKnob2.knobLabel = "Stereo Phase"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 2:
            FilterBankMixKnob.enabled = true
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Shape Left"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Stereo Phase"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = "Shape Right"
            FilterBankKnob3.enabled = true
        case 3:
            FilterBankMixKnob.enabled = true
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Stereo Phase"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 4:
            FilterBankMixKnob.enabled = true
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Stereo Phase"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 5:
            FilterBankMixKnob.enabled = false
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Filter type"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 6:
            FilterBankMixKnob.enabled = false
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Filter type"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 7:
            FilterBankMixKnob.enabled = false
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Filter type"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 8:
            FilterBankMixKnob.enabled = false
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Filter type"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = ""
            FilterBankKnob3.enabled = false
        case 9:
            FilterBankMixKnob.enabled = false
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Poles"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = "Slope"
            FilterBankKnob3.enabled = true
        case 10:
            FilterBankMixKnob.enabled = false
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Poles"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = "Slope"
            FilterBankKnob3.enabled = true
        case 11:
            FilterBankMixKnob.enabled = false
            FilterBankFrequencyKnob.enabled = true
            FilterBankKnob1.knobLabel = "Resonance"
            FilterBankKnob1.enabled = true
            FilterBankKnob2.knobLabel = "Poles"
            FilterBankKnob2.enabled = true
            FilterBankKnob3.knobLabel = "Slope"
            FilterBankKnob3.enabled = true
        default:
            print("helemaal fout")
        }
        
        switch patchCtrl.CurrentPatch.character_Type {
        case 0,7,8:
            CharacterIntensityKnob.enabled = true
            CharacterTuneKnob.enabled = true
        default:
            CharacterIntensityKnob.enabled = false
            CharacterTuneKnob.enabled = false
        }
        CharacterModeBtn.setTitle(CharacterTypes[safe:  patchCtrl.CurrentPatch.character_Type ], for: .normal)
        CharacterIntensityKnob.value = Double( patchCtrl.CurrentPatch.character_Intensity )
        CharacterTuneKnob.value = Double( patchCtrl.CurrentPatch.character_Tune )
    }
    
    //*****************************************************************
    // MARK: - Setup Knob 🎛 Callbacks
    //*****************************************************************
    
    func setupCallbacks() {
        ChorusMixKnob.callback = { value in
            if ( patchCtrl.CurrentPatch.chorus_Type == 1 ){
                patchCtrl.setChorusMixClassic(value: Int(value))
            } else {
                patchCtrl.setChorusMix(value: Int(value))
            }
        }
      
        self.ChorusFeedbackKnob.callback = { value in
            if ( patchCtrl.CurrentPatch.chorus_Type == 3 ){
                patchCtrl.setChorusDelay(value: Int(value))
            } else {
                patchCtrl.setChorusFeedback(value: Int(value))
            }
        }
        self.ChorusDelayKnob.callback = { value in
            switch patchCtrl.CurrentPatch.chorus_Type {
            case 1:
                patchCtrl.setChorusDelay(value: Int(value))
            case 6:
                patchCtrl.setChorusDelay(value: Int(value))
            default:
                patchCtrl.setChorusXOver(value: Int(value))
            }
        }
        self.ChorusLFORateKnob.callback = { value in patchCtrl.setChorusLFORate(value: Int(value))}
        self.ChorusLFODepthKnob.callback = { value in patchCtrl.setChorusLFODepth(value: Int(value))}
    
        self.DistortionMixKnob.callback = { value in patchCtrl.setDistortionMix(value: Int(value))}
        self.DistortionIntensityKnob.callback = { value in patchCtrl.setDistortionIntensity(value: Int(value))}
        self.DistortionDriveKnob.callback = { value in patchCtrl.setDistortionIntensity(value: Int(value))}
        self.DistortionToneKnob.callback = { value in patchCtrl.setDistortionToneW(value: Int(value))}
        self.DistortionQualityKnob.callback = { value in patchCtrl.setDistortionQuality(value: Int(value))}
        self.DistortionTrebleKnob.callback = { value in patchCtrl.setDistortionTrebleBooster(value: Int(value))}
        self.DistortionHighCutKnob.callback = { value in patchCtrl.setDistortionHighCut(value: Int(value))}
        
        self.PhaserMixKnob.callback = { value in patchCtrl.setPhaserMix(value: Int(value))}
        self.PhaserFreqKnob.callback = { value in patchCtrl.setPhaserFrequency(value: Int(value))}
        self.PhaserFeedbackKnob.callback = { value in patchCtrl.setPhaserFeedback(value: Int(value))}
        self.PhaserSpreadKnob.callback = { value in patchCtrl.setPhaserSpread(value: Int(value))}
        self.PhaserLFORateKnob.callback = { value in patchCtrl.setPhaserLFORate(value: Int(value))}
        self.PhaserLFODepthKnob.callback = { value in patchCtrl.setPhaserDepth(value: Int(value))}
        
        self.FilterBankMixKnob.callback = { value in patchCtrl.setFrequencyShifterMix(value: Int(value))}
        self.FilterBankFrequencyKnob.callback = { value in patchCtrl.setFrequencyShifterFrequency(value: Int(value))}
        self.FilterBankKnob1.callback = { value in
            switch patchCtrl.CurrentPatch.frequencyShifter_Type {
            case 2:
                patchCtrl.setFrequencyShifterLeftShape(value: Int(value))
            default:
                patchCtrl.setFrequencyShifterResonance(value: Int(value))
            }
        }
        
        self.FilterBankKnob2.callback = { value in
            switch patchCtrl.CurrentPatch.frequencyShifter_Type {
            case 1:
                patchCtrl.setFrequencyShifterStereoPhase(value: Int(value))
            case 2:
                patchCtrl.setFrequencyShifterStereoPhase(value: Int(value))
            case 3:
                patchCtrl.setFrequencyShifterStereoPhase(value: Int(value))
            case 4:
                patchCtrl.setFrequencyShifterStereoPhase(value: Int(value))
            case 5:
                patchCtrl.setFrequencyShifterFilterType(value: Int(value))
            case 6:
                patchCtrl.setFrequencyShifterFilterType(value: Int(value))
            case 7:
                patchCtrl.setFrequencyShifterFilterType(value: Int(value))
            case 8:
                patchCtrl.setFrequencyShifterFilterType(value: Int(value))
            case 9:
                patchCtrl.setFrequencyShifterPoles(value: Int(value))
            case 10:
                patchCtrl.setFrequencyShifterPoles(value: Int(value))
            case 11:
                patchCtrl.setFrequencyShifterPoles(value: Int(value))
            default:
                print("weer mislukt")
            }
        }
        
        self.FilterBankKnob3.callback = { value in
            switch patchCtrl.CurrentPatch.frequencyShifter_Type {
            case 2:
                patchCtrl.setFrequencyShifterRightShape(value: Int(value))
            case 9:
                patchCtrl.setFrequencyShifterSlope(value: Int(value))
            case 10:
                patchCtrl.setFrequencyShifterSlope(value: Int(value))
            case 11:
                patchCtrl.setFrequencyShifterSlope(value: Int(value))
            default:
                print("weer mislukt")
            }
        }
        
        self.CharacterIntensityKnob.callback = { value in patchCtrl.setCharacterIntensity(value: Int(value))}
        self.CharacterTuneKnob.callback = { value in patchCtrl.setCharacterTune(value: Int(value))}
    }
    
    @IBAction func ChorusLFOShapeChange(_ sender: UISegmentedControl) {
        patchCtrl.setChorusLFOShape(value: sender.selectedSegmentIndex )
    }
    
    @IBAction func PhaserStageChanged(_ sender: UISegmentedControl) {
        patchCtrl.setPhaserStages(value: sender.selectedSegmentIndex )
    }
    
    @IBAction func ChorustypeChange(_ sender: PMSuperButton) {
        let ChorusTypeSelectPopover = ChorusTypePopover()
        ChorusTypeSelectPopover.tableView.dataSource = ChorusTypeLbls!
        ChorusTypeSelectPopover.tableView.delegate = ChorusTypeSelectPopover
        ChorusTypeSelectPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(ChorusTypeSelectPopover, animated: true, completion: nil)
        let popoverPresentationController = ChorusTypeSelectPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    @IBAction func DistortionTypeChanged(_ sender: PMSuperButton) {
        let DistortionTypeSelectPopover = DistortionTypePopover()
        DistortionTypeSelectPopover.tableView.dataSource = DistortionTypesLbls!
        DistortionTypeSelectPopover.tableView.delegate = DistortionTypeSelectPopover
        DistortionTypeSelectPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(DistortionTypeSelectPopover, animated: true, completion: nil)
        let popoverPresentationController = DistortionTypeSelectPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func FilterBankTypeChanged(_ sender: PMSuperButton){
        let FilterBankTypeSelectPopover = FilterBankTypePopover()
        FilterBankTypeSelectPopover.tableView.dataSource = FilterBankTypeLbls!
        FilterBankTypeSelectPopover.tableView.delegate = FilterBankTypeSelectPopover
        FilterBankTypeSelectPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(FilterBankTypeSelectPopover, animated: true, completion: nil)
        let popoverPresentationController = FilterBankTypeSelectPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    @IBAction func CharacterTypeBtn(_ sender: PMSuperButton) {
        let CharacterTypesPopover = CharacterTypePopover()
        CharacterTypesPopover.tableView.dataSource = CharacterTypeLbls!
        CharacterTypesPopover.tableView.delegate = CharacterTypesPopover
        CharacterTypesPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(CharacterTypesPopover, animated: true, completion: nil)
        let popoverPresentationController = CharacterTypesPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
}
