//
//  VC_ToolBar.swift
//  Rage
//
//  Created by M. Lierop on 24-11-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class ToolbarViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    
    fileprivate let cellHeight: CGFloat = 210
    fileprivate let cellSpacing: CGFloat = 20
    fileprivate lazy var presentationAnimator = GuillotineTransitionAnimation()
 

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpdatePatchName), name: Notification.Name("UpdatePatchName"), object: nil)
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func UpdatePatchName (){
        //LoadPatchBtn.setTitle(patchCtrl.CurrentPatch.name, for: .normal)
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "KeyboardView" {
            let vc: UIViewController = segue.destination
            
            let pvc = vc.popoverPresentationController
            pvc?.delegate = self
            return
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
