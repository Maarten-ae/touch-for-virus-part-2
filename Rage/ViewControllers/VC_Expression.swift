//
//  VC_Expression.swift
//  Rage
//
//  Created by M. Lierop on 26-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class ExprViewController: UIViewController {

    @IBOutlet weak var ExprSubViewContainer: UIView!
   // @IBOutlet weak var ModsSubViewSegmentControl: UISegmentedControl!


    lazy var ExprQuickPresetsSubView: ExprQuickPresetsSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ExprQuickPresetsSubView") as! ExprQuickPresetsSubView
        self.addChild(viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ExprSubViewContainer.addSubview(ExprQuickPresetsSubView.view)
    }
}
