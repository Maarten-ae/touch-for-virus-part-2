//
//  VC_Library_tab.swift
//  Rage
//
//  Created by M. Lierop on 01/12/2020.
//  Copyright © 2020 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class LibraryTabController: UIViewController {

    
    @IBOutlet weak var LibraryTabPageContainer: UIView!
    
    lazy var LibrarySubView: LibraryViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "LibraryPage") as! LibraryViewController
        self.addChild(viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LibraryTabPageContainer.addSubview(LibrarySubView.view)
    }
}
