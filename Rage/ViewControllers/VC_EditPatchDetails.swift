//
//  VC_EditPatchDetails.swift
//  Rage
//
//  Created by M. Lierop on 05/01/2021.
//  Copyright © 2021 M. Lierop. All rights reserved.
//


import Foundation
import UIKit
import AudioKit
import RealmSwift
import CloudKit

class EditPatchDetailsViewControler: UIViewController {
    
    
    @IBOutlet weak var PatchNameInput: UITextField!
    @IBOutlet weak var AuthorNameInput: UITextField!
    @IBOutlet weak var Cat1Btn: PMSuperButton!
    @IBOutlet weak var Cat2Btn: PMSuperButton!
    var Categories:PatchCategories?
    @IBOutlet weak var DescriptionField: UITextView!
    
    @IBOutlet weak var SaveBtn: PMSuperButton!
    @IBOutlet weak var CancelBtn: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Categories = PatchCategories()
        
        self.setPatchName()
        self.setCategory()
        self.setAuthor()
        self.setDescription()
        self.PatchNameInput.text = patchCtrl.CurrentPatch.name
        
        // directly show the keyboard
        PatchNameInput.becomeFirstResponder()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setCategory), name: Notification.Name("CategorySelected"), object: nil)
    }
    
    func setPatchName(){
        self.PatchNameInput.text = PatchLib.CurrentPatch.name
    }
    
    @objc func setCategory(){
        Cat1Btn.setTitle( presetCategoriesUI[ patchCtrl.CurrentPatch.category1 ], for: .normal)
        Cat2Btn.setTitle( presetCategoriesUI[ patchCtrl.CurrentPatch.category2 ], for: .normal)
    }
    
    func setAuthor(){
        self.AuthorNameInput.text = PatchLib.CurrentPatch.authorName
    }
    
    func setDescription(){
        self.DescriptionField.text = PatchLib.CurrentPatch.descr
    }
    
    @IBAction func OnChangePatchName(_ sender: UITextField) {
        sender.layer.borderColor = UIColor( red: 255/255, green: 20/255, blue:20/255, alpha: 1.0 ).cgColor
        sender.layer.masksToBounds = true

        if ( Int(sender.text!.count) == 0 ){
            sender.layer.borderWidth = 2.0
            SaveBtn.isEnabled = false
           // SaveCloudBtn.isEnabled = false
        } else {
           // patchCtrl.CurrentPatch.name = sender.text!
            SaveBtn.isEnabled = true
           // SaveCloudBtn.isEnabled = true
            sender.layer.borderWidth = 0
        }
    }
    
    @IBAction func OnChangeAuthorName(_ sender: UITextField) {
        sender.layer.borderColor = UIColor( red: 255/255, green: 20/255, blue:20/255, alpha: 1.0 ).cgColor
        sender.layer.masksToBounds = true
        
        if ( Int(sender.text!.count) == 0 ){
            sender.layer.borderWidth = 2.0
            SaveBtn.isEnabled = false
            //SaveCloudBtn.isEnabled = false
        } else {
            app.setAuthorName(name: sender.text!)
            //patchCtrl.CurrentPatch.authorName = sender.text!
            SaveBtn.isEnabled = true
            //SaveCloudBtn.isEnabled = true
            sender.layer.borderWidth = 0
        }
    }
    
    @IBAction func OnSelectCat1(_ sender: PMSuperButton) {
        let Category1Popover = CategoryCollectionsPopover()
        Category1Popover.CategoryNR = 1
        Category1Popover.tableView.dataSource = Categories!
        Category1Popover.tableView.delegate = Category1Popover
        Category1Popover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Category1Popover, animated: true, completion: nil)
        let popoverPresentationController = Category1Popover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
        
    }
    
    @IBAction func OnSelectCat2(_ sender: PMSuperButton) {
        let Category2Popover = CategoryCollectionsPopover()
        Category2Popover.CategoryNR = 2
        Category2Popover.tableView.dataSource = Categories!
        Category2Popover.tableView.delegate = Category2Popover
        Category2Popover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Category2Popover, animated: true, completion: nil)
        let popoverPresentationController = Category2Popover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    
    
    @IBAction func onSaveChanges(_ sender: Any) {
        if PatchLib.savePatchDetails(Name: PatchNameInput.text!, Author: AuthorNameInput.text!, Descr: DescriptionField.text!) {
            DispatchQueue.main.async {
            // presentingViewController!.dismiss(animated: true, completion: nil)
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }
    
    
    @IBAction func CloseScreen(_ sender: UIButton) {
        DispatchQueue.main.async {
        // presentingViewController!.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
        }
    }
}
