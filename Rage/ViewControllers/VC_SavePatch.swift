//
//  VC_SavePatch.swift
//  Rage
//
//  Created by M. Lierop on 11-05-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//


import Foundation
import UIKit
import AudioKit
import RealmSwift
import CloudKit

class SavePatchViewControler: UIViewController {
    
    @IBOutlet weak var PatchNameInput: UITextField!
    @IBOutlet weak var AuthorNameInput: UITextField!
    
    @IBOutlet weak var SaveLocalBtn: PMSuperButton!
    @IBOutlet weak var CloseBnt: UIView!
    
    @IBOutlet weak var CollectionBtn: PMSuperButton!
    private var Collections:patchCollections?
    @IBOutlet weak var CollectionInput: UITextField!
    
    @IBOutlet weak var PatchDescription: UITextView!
    
    @IBOutlet weak var Cat1Btn: PMSuperButton!
    @IBOutlet weak var Cat2Btn: PMSuperButton!
    var Categories:PatchCategories?
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        Collections = patchCollections()
        Categories = PatchCategories()
        
        self.setCategory()
        self.setCollection()
        self.setAuthor()
        
        self.PatchNameInput.text = patchCtrl.CurrentPatch.name
                
        NotificationCenter.default.addObserver(self, selector: #selector(self.setCollection), name: Notification.Name("CollectionSelected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setCategory), name: Notification.Name("CategorySelected"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        
        self.setCategory()
        self.setCollection()
        self.setAuthor()
        
        PatchNameInput.text = patchCtrl.CurrentPatch.name
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @objc func setCollection(){
       
        CollectionInput.text = patchCtrl.CurrentPatch.collectionName
 
    }
    
    @objc func setCategory(){
        Cat1Btn.setTitle(presetCategoriesUI[patchCtrl.CurrentPatch.category1], for: .normal)
        Cat2Btn.setTitle(presetCategoriesUI[patchCtrl.CurrentPatch.category2], for: .normal)
    }
    
    func setAuthor(){
        self.AuthorNameInput.text = app.getAuthorName()
    }
    
    
    @IBAction func OnChangePatchName(_ sender: UITextField) {
        sender.layer.borderColor = UIColor( red: 255/255, green: 20/255, blue:20/255, alpha: 1.0 ).cgColor
        sender.layer.masksToBounds = true

        if ( Int(sender.text!.count) == 0 ){
            sender.layer.borderWidth = 2.0
            SaveLocalBtn.isEnabled = false
           // SaveCloudBtn.isEnabled = false
        } else {
            patchCtrl.CurrentPatch.name = sender.text!
            SaveLocalBtn.isEnabled = true
           // SaveCloudBtn.isEnabled = true
            sender.layer.borderWidth = 0
        }
    }
    
    @IBAction func OnChangeAuthorName(_ sender: UITextField) {
        sender.layer.borderColor = UIColor( red: 255/255, green: 20/255, blue:20/255, alpha: 1.0 ).cgColor
        sender.layer.masksToBounds = true
        
        if ( Int(sender.text!.count) == 0 ){
            sender.layer.borderWidth = 2.0
            SaveLocalBtn.isEnabled = false
            //SaveCloudBtn.isEnabled = false
        } else {
            app.setAuthorName(name: sender.text!)
            patchCtrl.CurrentPatch.authorName = sender.text!
            SaveLocalBtn.isEnabled = true
            //SaveCloudBtn.isEnabled = true
            sender.layer.borderWidth = 0
        }
    }
    
    @IBAction func OnChangeCollectionName(_ sender: UITextField) {
        sender.layer.borderColor = UIColor( red: 255/255, green: 20/255, blue:20/255, alpha: 1.0 ).cgColor
        sender.layer.masksToBounds = true
        
        
        if(Int(sender.text!.count) <= 3){
            sender.layer.borderWidth = 2.0
            SaveLocalBtn.isEnabled = false
            //SaveCloudBtn.isEnabled = false
        } else {
            patchCtrl.CurrentPatch.collectionName = sender.text!
            
            SaveLocalBtn.isEnabled = true
            //SaveCloudBtn.isEnabled = true
            sender.layer.borderWidth = 0
        }
        
    }
    
    @IBAction func OnSaveLocal(_ sender: PMSuperButton) {
        app.setAuthorName(name: AuthorNameInput.text!)
        patchCtrl.CurrentPatch.descr = self.PatchDescription.text
        patchCtrl.CurrentPatch.creationDate = Date()
        patchCtrl.CurrentPatch.lastUsage = Date()
        patchCtrl.CurrentPatch.usageCount = 1
        
        if (PatchNameInput.text != "" && (CollectionInput.text?.count)! >= 4 ){
            
            let realm = try! Realm(configuration: config)
            
            try! realm.write {
                realm.create(Patch.self, value: patchCtrl.CurrentPatch)
                realm.refresh()
                
                
                var styling = ToastStyle()
                styling.messageColor = .green
                let message:String = "Patch saved in local library!"
                DispatchQueue.main.async {
                    self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
                    
                    patchCtrl.SendName(newname: patchCtrl.CurrentPatch.name)
                    NotificationCenter.default.post(name: Notification.Name( "UpdatePatchName" ), object: nil)
                    PatchLib.update()
                }
                self.ExitPage()
            }
            
        }
    }
    
    @IBAction func OnSaveCloud(_ sender: PMSuperButton) {
        app.setAuthorName(name: AuthorNameInput.text!)
        patchCtrl.CurrentPatch.descr = self.PatchDescription.text
        
        if (PatchNameInput.text != "" && (CollectionInput.text?.count)! >= 4 ){
            
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try! encoder.encode(patchCtrl.CurrentPatch)
            
            let patchRecord = CKRecord(recordType: "iCPatch")
            patchRecord.setValue( PatchNameInput.text!, forKey: "name")
            patchRecord.setValue( data.base64EncodedString(), forKey: "params")
            patchRecord.setValue( CollectionInput.text!, forKey: "collection")
            patchRecord.setValue( patchCtrl.CurrentPatch.category1, forKey: "category1")
            patchRecord.setValue( patchCtrl.CurrentPatch.category2, forKey: "category2")
            patchRecord.setValue( AuthorNameInput.text!, forKey: "author")
            
            iCloudDB.save(patchRecord) { (results, error) -> Void in
                if error != nil
                {
                    var styling = ToastStyle()
                    styling.messageColor = .red
                    let message:String = "Failed to save in public library!"
                    DispatchQueue.main.async {
                        self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
                    }
                }
                else
                {
                    // Make sure cloudpatches are reload when library is opened
                    app.reloadCloudPatches = true
                    
                    var styling = ToastStyle()
                    styling.messageColor = .green
                    let message:String = "Patch saved in public library!"
                    DispatchQueue.main.async {
                        self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
                        
                        patchCtrl.SendName(newname: patchCtrl.CurrentPatch.name)
                        NotificationCenter.default.post(name: Notification.Name( "UpdatePatchName" ), object: nil)
                        
                    }
                     self.ExitPage()
                }
            }
            
        }
    }
    
    
    @IBAction func OnSelectCollection(_ sender: PMSuperButton) {
        let CollectionPopover = PatchCollectionsPopover()
        CollectionPopover.tableView.dataSource = Collections!
        CollectionPopover.tableView.delegate = CollectionPopover
        CollectionPopover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(CollectionPopover, animated: true, completion: nil)
        let popoverPresentationController = CollectionPopover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
        
    }
    
    
    @IBAction func OnSelectCat1(_ sender: PMSuperButton) {
        let Category1Popover = CategoryCollectionsPopover()
        Category1Popover.CategoryNR = 1
        Category1Popover.tableView.dataSource = Categories!
        Category1Popover.tableView.delegate = Category1Popover
        Category1Popover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Category1Popover, animated: true, completion: nil)
        let popoverPresentationController = Category1Popover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
        
    }
    
    @IBAction func OnSelectCat2(_ sender: PMSuperButton) {
        let Category2Popover = CategoryCollectionsPopover()
        Category2Popover.CategoryNR = 2
        Category2Popover.tableView.dataSource = Categories!
        Category2Popover.tableView.delegate = Category2Popover
        Category2Popover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Category2Popover, animated: true, completion: nil)
        let popoverPresentationController = Category2Popover.popoverPresentationController
        popoverPresentationController?.sourceView = sender
    }
    

    
    
    @IBAction func CloseScreen(_ sender: UIButton) {
        self.ExitPage()
    }
    
    func ExitPage(){
        DispatchQueue.main.async {
        // presentingViewController!.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)

        }
    }
}





/*  COLLECTION POPUP */

class PatchCollectionsPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            selectedRow = 0
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let realm = try! Realm(configuration: config)
        let distinctCollections = Set(realm.objects(Patch.self).value(forKey: "collectionName") as! [String])
        var collections: Array = Array(distinctCollections)
        collections.sort()
        print("GESELECTEERD: ",collections[indexPath.row])
        patchCtrl.CurrentPatch.collectionName = collections[indexPath.row]
        NotificationCenter.default.post(name: Notification.Name( "CollectionSelected" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}




class patchCollections: UIViewController, UITableViewDataSource {
    public var presetPatchCollections = ["My Patches", "Favorites", "Epic Patches"]
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let realm = try! Realm(configuration: config)
        let distinctCollections = Set(realm.objects(Patch.self).value(forKey: "collectionName") as! [String])
        return distinctCollections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let realm = try! Realm(configuration: config)
        let distinctCollections = Set(realm.objects(Patch.self).value(forKey: "collectionName") as! [String])
        var collections: Array = Array(distinctCollections)
        collections.sort()
        
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = collections[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}








import UIKit
private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
}
