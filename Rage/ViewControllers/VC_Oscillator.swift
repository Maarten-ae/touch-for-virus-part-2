//
//  VC_Oscillator.swift
//  Rage
//
//  Created by M. Lierop on 26-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import UIKit
import Realm
import AudioKit
import StoreKit



var midi = MIDI() //Manager.midi
var ViC = VirusConnect.init(ID: 1)



class OscViewController: UIViewController {

    @IBOutlet weak var OscSubViewContainer: UIView!
    @IBOutlet weak var OscSubViewSegmentControl: UISegmentedControl!
    
    lazy var OscQuickPresetsSubViewController: OscQuickPresetsSubViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "OscQuickPresetsSubViewController") as! OscQuickPresetsSubViewController
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var OscMainOscillatorsSubViewController: OscMainOscillatorsSubViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "OscMainOscillatorsSubViewController") as! OscMainOscillatorsSubViewController
        self.addChild(viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OscSubViewContainer.addSubview(OscQuickPresetsSubViewController.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func OscSubViewControlChange(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 0:
            OscQuickPresetsSubViewController.updateView()
            OscSubViewContainer.addSubview(OscQuickPresetsSubViewController.view)
        case 1:
            OscSubViewContainer.addSubview(OscMainOscillatorsSubViewController.view)
            OscMainOscillatorsSubViewController.updateView()
        default:
            OscQuickPresetsSubViewController.updateView()
            OscSubViewContainer.addSubview(OscQuickPresetsSubViewController.view)
        }
    }
}


extension OscViewController : SKStoreProductViewControllerDelegate {
    
    // ----------------------------------------------------------------------------------------
    // 5. Dismiss the StoreView
    // ----------------------------------------------------------------------------------------
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        print("RECEIVED a FINISH-Message from SKStoreProduktViewController")
        viewController.dismiss(animated: true, completion: nil)
    }
}
