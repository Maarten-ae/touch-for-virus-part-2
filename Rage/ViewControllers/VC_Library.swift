//
//  VC_Library.swift
//  Rage
//
//  Created by M. Lierop on 01/12/2020.
//  Copyright © 2020 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift


class LibraryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIContextMenuInteractionDelegate, UITextViewDelegate {
    
    @IBOutlet weak var CollectionTable: UITableView!
    @IBOutlet weak var PatchTable: UITableView!
    
    @IBOutlet weak var PatchCollectionLbl: UILabel!
    @IBOutlet weak var PatchTitle: UILabel!
    @IBOutlet weak var PatchActionsBtn: PMSuperButton!
    @IBOutlet weak var AuthorName: UILabel!
    @IBOutlet weak var Description: UITextView!
    @IBOutlet weak var FavoriteBtn: PMSuperButton!
    @IBOutlet weak var Categories: UILabel!

    
    private var FilterCategories:PatchCategories?
    fileprivate lazy var presentationAnimator = GuillotineTransitionAnimation()
    var currentSelectedRow: IndexPath!
    
    @IBOutlet weak var Searchbar: UISearchBar!
    let heartfilled =  UIImage(systemName: "heart.fill")
    let heart =  UIImage(systemName: "heart")
    
    enum CollectionSources {
        case Local
        case Cloud
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Searchbar.delegate = self
        Searchbar.setImage(UIImage(systemName: "line.horizontal.3.decrease.circle"), for: .bookmark, state: .normal)
        Searchbar.searchTextField.allowsDeletingTokens = true
        Searchbar.searchTextField.tokenBackgroundColor = UIColor.systemRed
        self.FilterCategories = PatchCategories()
       
        

        PatchActionsMenu()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTables), name: Notification.Name("UpdateLibraryTables"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCategoryToken), name: Notification.Name("UpdateCategoryToken"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatePatchInfo), name: Notification.Name("UpdatePatchInfo"), object: nil)
    }
    

    func createContextMenu() -> UIMenu {
        let shareAction = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { _ in
        print("Share")
        }
        let copy = UIAction(title: "Copy", image: UIImage(systemName: "doc.on.doc")) { _ in
        print("Copy")
        }
        let saveToPhotos = UIAction(title: "Add To Photos", image: UIImage(systemName: "photo")) { _ in
        print("Save to Photos")
        }
        return UIMenu(title: "", children: [shareAction, copy, saveToPhotos])
    }
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
    return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { _ -> UIMenu? in
    return self.createContextMenu()
        }
    }
    
    
    func PatchActionsMenu() {
        let deletePatch = UIAction(
            title: "Delete",
            image: UIImage(systemName: "minus.circle"),
            attributes: .destructive
            
            ) { (ToggleFavorite) in
            let dialogMessage = UIAlertController(title: "Are you sure?!", message: "Delete this patch FOR EVER?", preferredStyle: .alert)
                   
           // Create OK button with action handler
            let ok = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) -> Void in
            PatchLib.deletePatch()
           })
           
           // Create Cancel button with action handlder
           let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
               print("Cancel button tapped")
           }
           
           //Add OK and Cancel button to dialog message
           dialogMessage.addAction(ok)
           dialogMessage.addAction(cancel)
                   
           // Present dialog message to user
           self.present(dialogMessage, animated: true, completion: nil)
        }
        
        let deleteCollection = UIAction(
            title: "Delete Collection",
            image: UIImage(systemName: "folder.badge.minus"),
            attributes: .destructive ) { (action) in
            let collection:CollectionInfo = PatchLib.getCollection(name: PatchLib.CurrentCollection.name ?? "Unknonw")
            let txt:String = "...and all \(collection.count ?? "-ERROR-") patches this collection contains?"
            let dialogMessage = UIAlertController(title: "Delete: \(collection.name ?? "-ERROR-")?!", message: txt, preferredStyle: .alert)
                   
           // Create OK button with action handler
            let ok = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) -> Void in
                PatchLib.deleteCollection(name: collection.name!)
           })
           
           // Create Cancel button with action handlder
           let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
               print("Cancel button tapped")
           }
           
           //Add OK and Cancel button to dialog message
           dialogMessage.addAction(ok)
           dialogMessage.addAction(cancel)
                   
           // Present dialog message to user
           self.present(dialogMessage, animated: true, completion: nil)

        }
        
        let renameCollection = UIAction(
            title: "Rename collection",
            image: UIImage(systemName: "pencil.circle")) { (action) in
               
            //1. Create the alert controller.
            let renameCollectionAlert = UIAlertController(title: "Rename collection", message: "Everything between 4 and 25 characters will do.", preferredStyle: .alert)

            //2. Add the text field. You can configure it however you need.
            let collection:CollectionInfo = PatchLib.getCollection(name: PatchLib.CurrentCollection.name ?? "Unknonw")
            renameCollectionAlert.addTextField { (textField) in
                textField.text = collection.name
                textField.maxLength = 25
                textField.minLegth = 4
            }

            // 3. Grab the value from the text field, and print it when the user clicks OK.
            renameCollectionAlert.addAction(UIAlertAction(title: "Rename", style: .default, handler: { [weak renameCollectionAlert] (_) in
                
                let textField = renameCollectionAlert?.textFields![0] // Force unwrapping because we know it exists.
                
                PatchLib.renameCollection(old: collection.name!, new: textField!.text!)

            }))
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                print("Cancel button tapped")
            }
            renameCollectionAlert.addAction(cancel)

            // 4. Present the alert.
            self.present(renameCollectionAlert, animated: true, completion: nil)
            
            
            let menuViewController = self.storyboard!.instantiateViewController(withIdentifier: "EditPatchPopup2")
            menuViewController.modalPresentationStyle = .overFullScreen
            menuViewController.transitioningDelegate = self.parent as? UIViewControllerTransitioningDelegate
            menuViewController.modalTransitionStyle = .coverVertical
            self.presentationAnimator.animationDelegate = menuViewController as? GuillotineAnimationDelegate
            self.presentationAnimator.presentButton = self.PatchActionsBtn.imageView
            self.present(menuViewController, animated: true, completion: nil)
        }
        
        
        let editPatchDetails = UIAction(
            title: "Edit Patch details",
            image: UIImage(systemName: "pencil.circle")) { (action) in
               
            let menuViewController = self.storyboard!.instantiateViewController(withIdentifier: "EditPatchPopup2")
            menuViewController.modalPresentationStyle = .overFullScreen
            menuViewController.transitioningDelegate = self.parent as? UIViewControllerTransitioningDelegate
            menuViewController.modalTransitionStyle = .coverVertical
            self.presentationAnimator.animationDelegate = menuViewController as? GuillotineAnimationDelegate
            self.presentationAnimator.presentButton = self.PatchActionsBtn.imageView
            self.present(menuViewController, animated: true, completion: nil)
        }
        
        
     
        let moveToCollectionMenu = self.makeCollectionDestinationMenu()
        
        var menu = UIMenu(title: "Patch actions.", children: [deletePatch])
        if PatchLib.CurrentCollection.type == .custom {
            menu = UIMenu(title: "Patch actions.", children: [editPatchDetails,renameCollection,moveToCollectionMenu, deletePatch, deleteCollection])
        } else {
            menu = UIMenu(title: "Patch actions.", children: [editPatchDetails,moveToCollectionMenu, deletePatch])
        }
        
        PatchActionsBtn.menu = menu
        PatchActionsBtn.showsMenuAsPrimaryAction = true
    }
    
    func makeCollectionDestinationMenu() -> UIMenu {
         
        var collectionNames = [String]()
        var iterator = PatchLib.CustomCollections.makeIterator()
        while let Collection = iterator.next() {
            collectionNames.append(Collection.name!)
        }
        
        
       // let ratingButtonTitles = PatchLib.Collections.map( $0.name )
        
        let rateActions = collectionNames.enumerated()
          .map { index, name in
            return UIAction(
              title: name,
              identifier: UIAction.Identifier("\(index + 1)"),
                handler: movePatchTo)
          }
        
        return UIMenu(
          title: "Move Patch to...",
          image: UIImage(systemName: "folder"),
          children: rateActions)
      }

    func movePatchTo(from action: UIAction) {
        var collectionNr = Int(action.identifier.rawValue)
        if (collectionNr! >= 1) {collectionNr = collectionNr! - 1}
        PatchLib.moveCurrentPatchToCollection(nr:collectionNr ?? 0)
    }
   
    

    // MARK: Table dings
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (tableView){
        case self.PatchTable:
            return PatchLib.PatchList.count
            
        case self.CollectionTable:
            return PatchLib.Collections.count
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch (tableView){
        case self.PatchTable:
            
            let cell:UITableViewCell
            
            if ( PatchLib.PatchList[indexPath.row].isFavorite ){
                cell = (tableView.dequeueReusableCell(withIdentifier: "favoritePatchCell") as UITableViewCell?)!
            } else {
                cell = (tableView.dequeueReusableCell(withIdentifier: "patchCell") as UITableViewCell?)!
            }
            
            cell.textLabel?.text = PatchLib.PatchList[indexPath.row].name
            
            if ( PatchLib.CurrentCollection.type == .recentlyUsed ){
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEEE d MMMM YYYY H:mm"
                dateFormatter.dateFormat = "d MM YYYY H:mm"

                cell.detailTextLabel?.text =  dateFormatter.string(from: PatchLib.PatchList[indexPath.row].lastUsage)

            } else if ( PatchLib.CurrentCollection.type == .mostPopular){
                cell.detailTextLabel?.text = String( PatchLib.PatchList[indexPath.row].usageCount )
            } else {
                cell.detailTextLabel?.text = self.createCategoryString(cat1: PatchLib.PatchList[indexPath.row].category1, cat2: PatchLib.PatchList[indexPath.row].category2)
            }
            
            if( PatchLib.PatchList[indexPath.row] == PatchLib.CurrentPatch ){
                cell.isSelected = true
                cell.backgroundColor = .systemGray4
            } else {
                cell.backgroundColor = .systemGray5
            }
            
            return cell
            
        case self.CollectionTable:
            
            let cell:UITableViewCell
            
            switch ( PatchLib.Collections[indexPath.row].type ){
            case .all:
                cell = (tableView.dequeueReusableCell(withIdentifier: "AllPatchesCell") as UITableViewCell?)!
            case .favorites:
                cell = (tableView.dequeueReusableCell(withIdentifier: "FavoritePatchesCell") as UITableViewCell?)!
            case .recentlyUsed:
                cell = (tableView.dequeueReusableCell(withIdentifier: "recentlyUsedCell") as UITableViewCell?)!
            case .mostPopular:
                cell = (tableView.dequeueReusableCell(withIdentifier: "mostUsedCell") as UITableViewCell?)!
            case .custom:
                cell = (tableView.dequeueReusableCell(withIdentifier: "collectionCell") as UITableViewCell?)!
            case .none:
                cell = (tableView.dequeueReusableCell(withIdentifier: "collectionCell") as UITableViewCell?)!
            }

            var name:String = ""
            
            if (PatchLib.Collections[indexPath.row].name!.count > 28) {
                let endIndex = PatchLib.Collections[indexPath.row].name!.index(PatchLib.Collections[indexPath.row].name!.startIndex, offsetBy: 28)
                name = String( PatchLib.Collections[indexPath.row].name![..<endIndex] + "...")
              } else {
                name = PatchLib.Collections[indexPath.row].name!
              }
            
            cell.textLabel?.text = name
            cell.detailTextLabel?.text = PatchLib.Collections[indexPath.row].count
            cell.textLabel!.numberOfLines=2;
            cell.textLabel!.lineBreakMode = .byWordWrapping

            if( PatchLib.Collections[indexPath.row].name == PatchLib.CurrentCollection.name ){
                cell.isSelected = true
                cell.backgroundColor = .systemGray5
            } else {
                cell.backgroundColor = .clear
            }

            return cell
            
        default:
            
            let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
            return  cell

        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        switch (tableView){
        case self.PatchTable:
            // do nothing
            
            let _:Bool = PatchLib.selectPatchFromPatchlist(nr: Int(indexPath.row) )
            let selectedPatch = PatchLib.PatchList[indexPath.row]
            
            
            if ( patchCtrl.LoadPatch(NewPatch: selectedPatch,
                                     oscSection: true,
                                     filterSection: true,
                                     modSection: true,
                                     controlSection: true,
                                     arpSection: true,
                                     effSection: true,
                                     ambSection: true ))
            {
                NotificationCenter.default.post(name: Notification.Name( "UpdatePatchName" ), object: nil)
                self.updatePatchInfo()
                PatchLib.update()
            } else  {
                print("selected patch ended  UNsuccesfully")
            }
        
        
        case self.CollectionTable:
            PatchLib.setCollection(nr: Int(indexPath.row))
            self.PatchActionsMenu()
        default:
            // do nothing
        print("gek")
        }
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // let the controller to know that able to edit tableView's row
        switch (tableView){
        case self.PatchTable:
            return true
        case self.CollectionTable:
            return false
        default:
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
       
        switch (tableView){
        case self.PatchTable:
            let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in

                PatchLib.deleteFromPatchList(row: indexPath.row)
                
            }
            deleteAction.backgroundColor = .red
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            configuration.performsFirstActionWithFullSwipe = true
            return configuration
        case self.CollectionTable:
            return nil
        default:
            return nil
        }
        
    }
    
    
   @objc func updateTables (){
        self.CollectionTable.reloadData()
        self.PatchTable.reloadData()
    }
    
   
    
//    @IBAction func ontstclikc(_ sender: UIButton) {
//        print("sdfahsdfjhaskdjhas")
//    }
    
    
    // MARK: Favoritee
    @IBAction func toggleFavorite(_ sender: Any) {
        self.ToggleFavorite()
    }
    
    
    
    func ToggleFavorite() {
        
        // update patch
        let realm = try! Realm(configuration: config)
        realm.autorefresh = true
        DispatchQueue.main.async {
            try! realm.write {
               
                if (patchCtrl.liveEditablePatch.isFavorite) {
                    patchCtrl.liveEditablePatch.isFavorite = false
                    patchCtrl.CurrentPatch.isFavorite = false
                    self.FavoriteBtn.setTitleColor(.darkGray, for: .normal)
                    self.FavoriteBtn.tintColor = .darkGray
                } else {
                    patchCtrl.liveEditablePatch.isFavorite = true
                    patchCtrl.CurrentPatch.isFavorite = true
                    self.FavoriteBtn.setTitleColor(.systemRed, for: .normal)
                    self.FavoriteBtn.tintColor = .systemRed
                }
                
                try! realm.commitWrite()
                PatchLib.update()
            }
            
        }

        // update favorites count
        self.CollectionTable.reloadData()
        self.PatchTable.reloadData()
        
    }
    
    
    // MARK: Patch info update
    
    @objc func updatePatchInfo(){
        
        PatchTitle.text = patchCtrl.CurrentPatch.name
        AuthorName.text = patchCtrl.CurrentPatch.authorName.uppercased() + "   [ " + createCategoryString(cat1: patchCtrl.CurrentPatch.category1, cat2: patchCtrl.CurrentPatch.category2) + " ] "
        PatchCollectionLbl.text = patchCtrl.CurrentPatch.collectionName
        Description.text = patchCtrl.CurrentPatch.descr
       
        if (patchCtrl.CurrentPatch.isFavorite){
            self.FavoriteBtn.tintColor = .systemRed
            self.FavoriteBtn.setTitleColor(.systemRed, for: .normal)
        } else {
            self.FavoriteBtn.tintColor = .darkGray
            self.FavoriteBtn.setTitleColor(.darkGray, for: .normal)
        }
        
        //self.Categories.text = createCategoryString(cat1: patchCtrl.CurrentPatch.category1, cat2: patchCtrl.CurrentPatch.category2)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE d MMMM YYYY"
        
        
    }
    
    func createCategoryString(cat1: Int, cat2: Int) -> String {
        var catStr: String = ""
        
        if ( cat1 == 0 && cat2 == 0 ){ catStr = catStr + "-"}
        
        if ( cat1 != 0 ) { catStr = catStr + presetCategoriesUI[ cat1 ] }
        
        if ( cat1 != 0 && cat2 != 0 ){ catStr = catStr + " / "}
        
        if ( cat2 != 0 ) { catStr = catStr + presetCategoriesUI[ cat2]}
         
        return catStr
    }
    
    
    // MARK: Searchbar
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        PatchLib.setSearchString(srch: searchText)
        print("PatchLib.CurrentCategory.id ",PatchLib.CurrentCategory.id)
        
        if(searchBar.searchTextField.tokens == [] && PatchLib.CurrentCategory.id != 0){
            PatchLib.setCategory(cat: 0)
            print("Reset category")
        }
        print(searchText)
        print(searchBar.searchTextField.tokens)
        
    }
    
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        let Category1Popover = CategoryFilterPopover()
        Category1Popover.CategoryNR = 1
        Category1Popover.tableView.dataSource = FilterCategories!
        Category1Popover.tableView.delegate = Category1Popover
        Category1Popover.modalPresentationStyle = UIModalPresentationStyle.popover
        
        present(Category1Popover, animated: true, completion: nil)
        let popoverPresentationController = Category1Popover.popoverPresentationController
       popoverPresentationController?.sourceView = Searchbar
    }
    
    @objc func updateCategoryToken (){
        
        if (PatchLib.CurrentCategory.id != 0){
            let cat:String = PatchLib.CurrentCategory.label
            let globeImage = UIImage(systemName: "tag")?.withTintColor(UIColor.white, renderingMode: .alwaysOriginal)
            let token = UISearchToken(icon: globeImage, text: cat)
            Searchbar.searchTextField.insertToken(token, at: 0)
        }
    }
    
   
    func textFieldDidChange(txtField: UITextView) {
        print("textview didchange")
        PatchLib.editPatchDescription(descr: txtField.text)
    }


}
