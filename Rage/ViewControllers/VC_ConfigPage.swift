//
//  VC_ConfigPage.swift
//  Rage
//
//  Created by M. Lierop on 09-03-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import AudioKit

class ConfigPageViewControler: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var CloseBtn: PMSuperButton!
    
    @IBOutlet weak var TargetPatchTable: UITableView!
    @IBOutlet weak var MIDIOutputTable: UITableView!
    @IBOutlet weak var MIDITargetDeviceIDTable: UITableView!
    
    @IBOutlet weak var MIDIChannelStepper: Stepper!
    
    //@IBOutlet weak var MIDIThruToggleBtn: ToggleButton!
    @IBOutlet weak var BlueToothMenu: BluetoothMIDIButton!
    
    var outputTargets = midi.inputNames  //["All targets", "target 1"]
    
    var currentScrollPosition: UITableView.ScrollPosition!
    
    
    var targetPatches = ["Single Mode","Multi Mode, Part 1","Multi Mode, Part 2","Multi Mode, Part 3","Multi Mode, Part 4"
    ,"Multi Mode, Part 5","Multi Mode, Part 6","Multi Mode, Part 7","Multi Mode, Part 8"
    ,"Multi Mode, Part 9","Multi Mode, Part 10","Multi Mode, Part 11","Multi Mode, Part 12"
    ,"Multi Mode, Part 13","Multi Mode, Part 14","Multi Mode, Part 15","Multi Mode, Part 16"]
    var channels = ["All channels", "Channel 1", "Channel 2", "Channel 3", "Channel 4", "Channel 5",
                    "Channel 6", "Channel 7", "Channel 8", "Channel 9", "Channel 10", "Channel 11",
                    "Channel 12", "Channel 13", "Channel 14", "Channel 15", "Channel 16"]
    var DeviceIds = ["Any Device (Omni)","Device ID 1", "Device ID 2", "Device ID 3", "Device ID 4", "Device ID 5",
                    "Device ID 6", "Device ID 7", "Device ID 8", "Device ID 9", "Device ID 10", "Device ID 11",
                    "Device ID 12", "Device ID 13", "Device ID 14", "Device ID 15", "Device ID 16"]
     
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TargetPatchTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        MIDIOutputTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        MIDITargetDeviceIDTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
          MIDIChannelStepper.maxValue = Double(16)
          MIDIChannelStepper.minValue = Double(1)
        MIDIChannelStepper.value = Double( app.getAuditMIDIChannel() + 1 )

        self.BlueToothMenu.centerPopupIn(view: self.view)

        
        self.setupCallbacks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        //MIDISetupChanged
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateView), name: Notification.Name("MIDISetupChanged"), object: nil)
        
        //if app.GetMIDIThru() {
        //    MIDIThruToggleBtn.value = 1
        //} else {
        //    MIDIThruToggleBtn.value = 0
        //}
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @objc func updateView(){
        outputTargets = midi.inputNames
        self.MIDIOutputTable.reloadData()
    }
    
    @IBAction func CloseScreen(_ sender: PMSuperButton) {
        presentingViewController!.dismiss(animated: true, completion: nil)
    }
    
    func setupCallbacks() {
        
        // Oscillator 3
        MIDIChannelStepper.callback = { value in
            app.setAuditMIDIChannel(channel: Int(value - 1))
        }
        
    }
    
    //*****************************************************************
    // MARK: - functions for presets table
    //*****************************************************************
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (tableView == self.MIDIOutputTable) {
            return outputTargets.count
        } else if (tableView == self.TargetPatchTable) {
            return targetPatches.count
        } else {
            // if (tableView == self.MIDITargetDeviceIDTable)
            return DeviceIds.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell?)!
        
        if (tableView == self.MIDIOutputTable) {
            cell.textLabel?.text = self.outputTargets[indexPath.row]
            
            if( ViC.MIDITargetOutput == self.outputTargets[indexPath.row] ){
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
        } else if (tableView == self.TargetPatchTable) {
            cell.textLabel?.text = self.targetPatches[indexPath.row]
            
            switch (indexPath.row){
            case 0:
                if( ViC.partNumber == 64){tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)}
            case 1:
                if( ViC.partNumber == 0){tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)}
            case 2:
                if( ViC.partNumber == 1){tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)}
            case 3:
                if( ViC.partNumber == 2){tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)}
            case 4:
                if( ViC.partNumber == 3){tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)}
            default:
                print("error partnr")
            }
            
            
            if( ViC.partNumber == 64 && indexPath.row == 0){
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
        } else {
            // if (tableView == self.MIDITargetDeviceIDTable)
            cell.textLabel?.text = self.DeviceIds[indexPath.row]
            
            if(Int(ViC.MIDIDeviceId) == 16){
                if (indexPath.row == 0){tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)}
            } else {
                if (indexPath.row == Int(ViC.MIDIDeviceId) + 1) {tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)}
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if (tableView == self.MIDIOutputTable) {
            
            midi.openInput(index: 1)
            midi.openInput(index: 2)
            midi.openInput(index: 3)
            //midi.openInput(name: outputTargets[indexPath.row])
            ViC.MIDITargetOutput = outputTargets[indexPath.row]
            ViC.reconnectOutput(OutputId: outputTargets[indexPath.row])
            app.setMIDIOutput(Output: outputTargets[indexPath.row])
        } else if (tableView == self.TargetPatchTable) {
            // set and save target
            switch (indexPath.row){
            case 1:
                ViC.setPartNumber(nr: 0)
            case 2:
                ViC.setPartNumber(nr: 1)
            case 3:
                ViC.setPartNumber(nr: 2)
            case 4:
                ViC.setPartNumber(nr: 3)
            case 5:
                ViC.setPartNumber(nr: 4)
            case 6:
                ViC.setPartNumber(nr: 5)
            case 7:
                ViC.setPartNumber(nr: 6)
            case 8:
                ViC.setPartNumber(nr: 7)
            case 9:
                ViC.setPartNumber(nr: 8)
            case 10:
                ViC.setPartNumber(nr: 9)
            case 11:
                ViC.setPartNumber(nr: 10)
            case 12:
                ViC.setPartNumber(nr: 11)
            case 13:
                ViC.setPartNumber(nr: 12)
            case 14:
                ViC.setPartNumber(nr: 13)
            case 15:
                ViC.setPartNumber(nr: 14)
            case 16:
                ViC.setPartNumber(nr: 15)
            case 17:
                ViC.setPartNumber(nr: 16)
            default:
                ViC.setPartNumber(nr: 64)
            }
        } else {
            // if (tableView == self.MIDITargetDeviceIDTable)

            var DevID:Int = Int(16)
            if (indexPath.row == 0){
                DevID = 16
            } else {
                DevID = indexPath.row - 1
            }
            ViC.setTargetDevice(ID: DevID)
            print(DevID)
        }
    }

}

