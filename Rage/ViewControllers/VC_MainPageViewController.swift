//
//  VC_MainPageViewController.swift
//  Rage
//
//  Created by M. Lierop on 21-05-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import AudioKit
import RealmSwift


var patchCtrl = PatchControler()
var app = AppSettings()
var PatchLib: library = library()

class MainAppViewController: UIViewController, UIPopoverPresentationControllerDelegate, AKKeyboardDelegate {

    private var loaderd:MIDIFileHandler?
    
    @IBOutlet weak var AuditToggle: PMSuperButton!
    @IBOutlet weak var AuditKeyboardContainer: KeyboardView!
    
    fileprivate let cellHeight: CGFloat = 210
    fileprivate let cellSpacing: CGFloat = 20
    fileprivate lazy var presentationAnimator = GuillotineTransitionAnimation()
    
    @IBOutlet weak var CollectionName: UILabel!
    @IBOutlet weak var SavePatchBtn: UIButton!
    @IBOutlet weak var PatchBtn: UIButton!
    @IBOutlet weak var SettingsBtn: UIButton!
    @IBOutlet weak var SupportBtn: UIButton!
    @IBOutlet weak var GetPAtchBtn: PMSuperButton!
    @IBOutlet weak var ImportFile: PMSuperButton!
    
    @IBOutlet weak var NavigationMenu: UISegmentedControl!
    @IBOutlet weak var PageContainer: UIView!
    
    var SysexMsg:[Int] = []
    var SysexSubMsgCounter:Int = 1
    var MIDIHandlr:MIDIHandler!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpdatePatchName), name: Notification.Name("UpdatePatchName"), object: nil)
        
               
        PatchLib.Setup()
        let font = UIFont.systemFont(ofSize: 16)
        NavigationMenu.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
        PageContainer.addSubview(LibraryPage.view)
        
        MIDIHandlr = MIDIHandler()
        
        // init usersettings
        //let UserDefs = UserDefaults.standard
       
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //midi.openInput()
//        midi.openI
//        midi.openInput(uid: 111111)
       
        app.SetMIDIThru(Enable: false)
        midi.addListener(self)
        
        midi.openOutput()
        ViC.resetVirus()
        
        app.reloadCloudPatches = true
        self.AuditKeyboardContainer.isHidden = true

        AuditKeyboardContainer.delegate = self
        
        loaderd = MIDIFileHandler()
        loaderd?.GetFile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func UpdatePatchName (){
        PatchBtn.setTitle(patchCtrl.CurrentPatch.name, for: .normal)
        DispatchQueue.main.async{
            self.CollectionName.text = patchCtrl.CurrentPatch.collectionName
        }
        
    }
    @IBAction func ResetPatchPArameters(_ sender: Any) {
        
        let dialogMessage = UIAlertController(title: "Reload patch", message: "All parameters will be set to their original value. All changes will be lost...", preferredStyle: .alert)
               
       // Create OK button with action handler
        let ok = UIAlertAction(title: "Reload", style: .destructive, handler: { (action) -> Void in
            _ = patchCtrl.updatePatch()
       })
       
       // Create Cancel button with action handlder
       let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
           print("Cancel button tapped")
       }
       
       //Add OK and Cancel button to dialog message
       dialogMessage.addAction(ok)
       dialogMessage.addAction(cancel)
               
       // Present dialog message to user
       self.present(dialogMessage, animated: true, completion: nil)
        
    }
    
    @IBAction func onAuditStart(_ sender: PMSuperButton) {
        self.resizeKeyboard(initSize: false)
    }
    
    func resizeKeyboard(initSize: Bool){
        self.AuditKeyboardContainer.autoresizesSubviews = true
             
        if ( self.AuditKeyboardContainer.isHidden){
          self.AuditKeyboardContainer.setIsHidden(false, animated: true)
        } else {
          self.AuditKeyboardContainer.setIsHidden(true, animated: true)
        }
    }
    
    public func noteOn(note: MIDINoteNumber, velocity: MIDIVelocity) {
        midi.sendNoteOnMessage(noteNumber: note, velocity: velocity, channel: app.getAuditMIDIChannel() )
    }
    
    public func noteOff(note: MIDINoteNumber) {
        midi.sendNoteOffMessage(noteNumber: note, velocity: 0, channel: app.getAuditMIDIChannel())
    }
    

    
    @IBAction func OnGetPAtch(_ sender: PMSuperButton) {
        ViC.requestPatch()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "KeyboardView" {
            let vc: UIViewController = segue.destination
            
            let pvc = vc.popoverPresentationController
            pvc?.delegate = self
            return
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
    @IBAction func OpenSettingsDialog(_ sender: UIButton) {
        let menuViewController = storyboard!.instantiateViewController(withIdentifier: "SettingsPage")
        
        menuViewController.modalPresentationStyle = .custom
        menuViewController.transitioningDelegate = self.parent as? UIViewControllerTransitioningDelegate
        menuViewController.modalTransitionStyle = .crossDissolve
        presentationAnimator.animationDelegate = menuViewController as? GuillotineAnimationDelegate
        presentationAnimator.presentButton = sender
        present(menuViewController, animated: true, completion: nil)
    }
    
    @IBAction func onOpenFileImport(_ sender: UIButton) {
        print("import that file")

        let menuViewController = storyboard!.instantiateViewController(withIdentifier: "FileImport")
        
        menuViewController.modalPresentationStyle = .custom
        menuViewController.transitioningDelegate = self.parent as? UIViewControllerTransitioningDelegate
        menuViewController.modalTransitionStyle = .crossDissolve
        presentationAnimator.animationDelegate = menuViewController as? GuillotineAnimationDelegate
        presentationAnimator.presentButton = sender
        present(menuViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func showSupportPage(_ sender: UIButton) {
        
        if let url = URL(string: "https://www.maartenlierop.nl/touch-for-virus/support") {
            UIApplication.shared.open(url)
        }
    }
    
    
    @IBAction func SavePatch(_ sender: UIButton) {
        let menuViewController = storyboard!.instantiateViewController(withIdentifier: "SavePatchPage")
        
        menuViewController.modalPresentationStyle = .custom
        menuViewController.transitioningDelegate = self.parent as? UIViewControllerTransitioningDelegate
        menuViewController.modalTransitionStyle = .crossDissolve
        presentationAnimator.animationDelegate = menuViewController as? GuillotineAnimationDelegate
        presentationAnimator.presentButton = sender
        present(menuViewController, animated: true, completion: nil)
    }
    
    
    lazy var LibraryPage: LibraryTabController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "Library-Tab") as! LibraryTabController
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var OscillatorPage: OscViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "OSC-Tab") as! OscViewController
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var FilterPage: FilterViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "FLT-Tab") as! FilterViewController
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var ModulationPage: ModViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "MOD-Tab") as! ModViewController
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var ExpressionPage: ExprViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "CNT-Tab") as! ExprViewController
        self.addChild(viewController)
        return viewController
    }()

    lazy var ArpeggiatorPage: ArpViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ARP-Tab") as! ArpViewController
        self.addChild(viewController)
        return viewController
    }()
   
    lazy var Efx1Page: EfxCharViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "EFF-Tab") as! EfxCharViewController
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var Efx2Page: Efx2ViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "AMB-Tab") as! Efx2ViewController
        self.addChild(viewController)
        return viewController
    }()
    
    
    @IBAction func onNavigationEvent(_ sender: UISegmentedControl) {
        
        switch (NavigationMenu.selectedSegmentIndex){
        case 0:
            PageContainer.addSubview(LibraryPage.view)
        case 1:
            PageContainer.addSubview(OscillatorPage.view)
        case 2:
           PageContainer.addSubview(FilterPage.view)
        case 3:
            PageContainer.addSubview(ModulationPage.view)
        case 4:
            PageContainer.addSubview(ExpressionPage.view)
        case 5:
            PageContainer.addSubview(ArpeggiatorPage.view)
        case 6:
            PageContainer.addSubview(Efx1Page.view)
        case 7:
            PageContainer.addSubview(Efx2Page.view)
        default:
            PageContainer.addSubview(OscillatorPage.view)
        }
    }
}




class MenuViewController: UIViewController, GuillotineMenu {
    
    var dismissButton: UIButton?
    var titleLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dismissButton = {
            let button = UIButton(frame: .zero)
            button.setImage(UIImage(named: "ic_menu"), for: .normal)
            button.addTarget(self, action: #selector(dismissButtonTapped(_:)), for: .touchUpInside)
            return button
        }()
        
        titleLabel = {
            let label = UILabel()
            label.numberOfLines = 1;
            label.text = "Activity"
            label.font = UIFont.boldSystemFont(ofSize: 17)
            label.textColor = UIColor.white
            label.sizeToFit()
            return label
        }()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Menu: viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("Menu: viewDidAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Menu: viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("Menu: viewDidDisappear")
    }
    
    @objc func dismissButtonTapped(_ sender: UIButton) {
        presentingViewController!.dismiss(animated: true, completion: nil)
    }
    
//    @IBAction func menuButtonTapped(_ sender: UIButton) {
//        presentingViewController!.dismiss(animated: true, completion: nil)
//    }
//
//    @IBAction func closeMenu(_ sender: UIButton) {
//        presentingViewController!.dismiss(animated: true, completion: nil)
//    }
}

extension MenuViewController: GuillotineAnimationDelegate {
    
    func animatorDidFinishPresentation(_ animator: GuillotineTransitionAnimation) {
        print("menuDidFinishPresentation")
    }
    func animatorDidFinishDismissal(_ animator: GuillotineTransitionAnimation) {
        print("menuDidFinishDismissal")
    }
    
    func animatorWillStartPresentation(_ animator: GuillotineTransitionAnimation) {
        print("willStartPresentation")
    }
    
    func animatorWillStartDismissal(_ animator: GuillotineTransitionAnimation) {
        print("willStartDismissal")
    }
}

