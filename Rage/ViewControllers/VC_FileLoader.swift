//
//  VC_FileLoader.swift
//  Rage
//
//  Created by M. Lierop on 24/08/2020.
//  Copyright © 2020 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AudioKit
import UniformTypeIdentifiers

class FileLoaderViewControler:UIViewController {
    
    
    @IBOutlet weak var ImportLibraryTitle: UILabel!
    @IBOutlet weak var PatchnamePreview: UITextView!
    @IBOutlet weak var ImportPatchCount: UILabel!
    @IBOutlet weak var TargetCollection: UITextField!
    @IBOutlet weak var AuthorName: UITextField!
    @IBOutlet weak var PatchDescription: UITextView!
    
    @IBOutlet weak var ImportResult: UILabel!
    
    var ImportData: Array<AppleMIDIEvent>?
    
    var MIDIHandlr: MIDIHandler?
    var sequencerManager: SequencerManager?
    //let oscBank = FMOscillatorBank(waveform: Table(.triangle), attackDuration: 0.01, decayDuration: 0.03)
    var node: MIDINode!
    

    var documentPickerController: UIDocumentPickerViewController!
    weak var delegate: FileLoaderDelegate?

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
      sequencerManager = SequencerManager()
        
        ImportLibraryTitle.text = ""
        PatchnamePreview.text = ""
        ImportPatchCount.text = ""
        PatchDescription.text = ""
        ImportResult.text = ""
        
       // node = MIDINode(node: oscBank)
        MIDIHandlr = MIDIHandler()
        
      }
    
    @IBAction func StartImport(_ sender: Any) {
        var ignoredCounter:Int = 0
        var importedCounter:Int = 0
        if ( self.TargetCollection.text?.isEmpty ?? true ){
            
            self.ImportResult.text = "FAILED : Target collection cannot be empty"
            
        } else {
            
            if self.ImportData != nil && self.ImportData!.count > 0 {
                for index in 0..<self.ImportData!.count {
                    let Event = self.ImportData![index]
                    
                    if (index != 0){
                        
                        let rawData:UnsafeMutableRawPointer = UnsafeMutableRawPointer(mutating:(Event.data)!)
                        var dataArray = rawData.toArray(to: UInt8.self, capacity: Int( Event.dataSize ) )
                        dataArray = Array(dataArray.dropFirst(4))
                        
                        if (dataArray.count == 524) {
                            
                            // Event is a patchdump
                            let name:String = self.MIDIHandlr!.ParsePatchName(data: dataArray) + " / "
                            
                            if( !(name.contains("-Init")) && !(name.contains("- Init")) && !(name.contains("Empty"))) {
                                self.PatchnamePreview.text.append( name )
                                importedCounter += 1
                            } else {
                                ignoredCounter += 1
                            }
                        
                            self.savePatch(data: dataArray, nr : index)
                        }
                    }
                }
                
                PatchLib.update()
                
                if ignoredCounter == 0 {
                    self.ImportResult.text = "Import completed"
                } else {
                    self.ImportResult.text = "Import completed (" + String(ignoredCounter) + " init patches ignored)"
                }
                
                var styling = ToastStyle()
                styling.messageColor = .green
                let msg:String = "Ready: " + String( importedCounter ) + " patches imported"
                self.view.makeToast(msg, duration: 3.0, position: .top, style: styling)
                
                
            } else {
                self.ImportResult.text = "No file selected for import."
            }
        }
    }
    
    @IBAction func loadFileFromIcloud(_ sender: Any) {
        print("open picker")
        
        let supportedTypes: [UTType] = [UTType.midi]
        let picker = UIDocumentPickerViewController(forOpeningContentTypes: supportedTypes, asCopy: true)
        picker.delegate = self
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true, completion: nil)
    }
    
    
    
    @IBAction func CloseScreen(_ sender: UIButton) {
        PatchLib.update()
        presentingViewController!.dismiss(animated: true, completion: nil)
    }
    
}


extension FileLoaderViewControler: UIDocumentPickerDelegate {

    

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
              
        guard let manager = sequencerManager,
        let seq = manager.seq else { return }
        
        seq.loadMIDIFile(fromURL: urls[0])
        
        if seq.tracks.count >= 1 {
            if let Events = seq.tracks[0].eventData {
                
                self.ImportData = Events
                
                // ------- Update interface
                self.ImportPatchCount.text = "Contains " + String(Events.count) + " patches"
                self.ImportLibraryTitle.text = urls[0].lastPathComponent
                //self.AuthorName.text = "Unknown"
                self.PatchDescription.text = "Imported from " + urls[0].lastPathComponent
                self.PatchnamePreview.text = ""
                self.TargetCollection.text = urls[0].lastPathComponent.replacingOccurrences(of: ".mid", with: "")
                self.ImportResult.text = ""
                
                for index in 0..<Events.count {
                    let Event = Events[index]
                    
                    let rawData:UnsafeMutableRawPointer = UnsafeMutableRawPointer(mutating:(Event.data)!)
                    var dataArray = rawData.toArray(to: UInt8.self, capacity: Int( Event.dataSize ) )
                    dataArray = Array(dataArray.dropFirst(4))
                    
                    if (index == 0){
                        
                    // do nothing, i don't know what this first even of only 20 parameters means
                        
                    } else {
                        
                        if (dataArray.count == 524) {
                            
                            // Event is a patchdump
                            let name:String = self.MIDIHandlr!.ParsePatchName(data: dataArray) + " / "
                            //self.PatchnamePreview.text.append( name + "(" + String(dataArray.count) + ")" )
                            self.PatchnamePreview.text.append( name )
                            
                        } else {
                            
                            var styling = ToastStyle()
                            styling.messageColor = .red
                            let message:String = "Cannot read this file. It does not seem to be Virus TI compatible"
                            DispatchQueue.main.async {
                                self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
                            }
                            
                        }
                    }
                }
            }
        } else {
            var styling = ToastStyle()
            styling.messageColor = .red
            let message:String = "Empty file. No patches found."
            DispatchQueue.main.async {
                self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
            }
        }
        navigationController?.popViewController(animated: true)
    }

    
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        print("Entre a documentPicker")
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
       print("cancel  documentPicker")
    }

    
    func savePatch(data: Array<UInt8>, nr: Int){
 
        DispatchQueue.main.async {
            
            patchCtrl.CurrentPatch.collectionName = self.TargetCollection.text ?? "Imports"
            patchCtrl.CurrentPatch.authorName = self.AuthorName.text ?? "Unknown"
            patchCtrl.CurrentPatch.descr = self.PatchDescription.text ?? "Imported from file"
            patchCtrl.CurrentPatch.creationDate = Date()

            self.MIDIHandlr?.parseMIDIdump(data: data, triggerSaveAction: true)

        }
        
    }
}


extension UnsafeMutableRawPointer {
    func toArray<T>(to type: T.Type, capacity count: Int) -> [T]{
        let pointer = bindMemory(to: type, capacity: count)
        return Array(UnsafeBufferPointer(start: pointer, count: count))
    }
}



protocol FileLoaderDelegate: class {
    func loadFile(filename: String)
    func loadFile(url: URL)
}







//
//  SequencerManager.swift
//  MIDIFileEditAndSync
//
//  Created by Jeff Holtzkener on 2018/04/15.
//  Copyright © 2018 Jeff Holtzkener. All rights reserved.
//
import AudioKit
import Foundation

class SequencerManager {
    var seq: AppleSequencer?
   // let oscBank = FMOscillatorBank(waveform: Table(.triangle), attackDuration: 0.01, decayDuration: 0.03)
    let mixer = Mixer()
    var node: MIDINode!

    let minLoopLength = Duration(beats: 4)

    init() {
        setUpSequencer()
    }

    fileprivate func setUpSequencer() {
        seq = AppleSequencer(filename: "miditemp")
        seq?.setLength(minLoopLength)
        seq?.enableLooping()
       // node = MIDINode(node: oscBank)
        
    }

    fileprivate func startAudioKit() {
        let ae = AudioEngine()
        do {
            try ae.start()
        } catch {
            Log("Couldn't start AudioKit")
        }
    }

    // MARK: - Interface
    func play() {
        seq?.rewind()
        seq?.play()
    }

    func stop() {
        seq?.stop()
    }

    func getURLwithMIDIFileData() -> URL? {
        guard let seq = seq,
            let data = seq.genData() else { return nil }
        let fileName = "ExportedMIDI.mid"
        do {
            let tempPath = URL(fileURLWithPath: NSTemporaryDirectory().appending(fileName))
            try data.write(to: tempPath as URL)
            return tempPath
        } catch {
            Log("couldn't write to URL")
        }
        return nil
    }



}
