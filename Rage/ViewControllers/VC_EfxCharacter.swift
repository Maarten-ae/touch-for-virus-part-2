//
//  VC_EfxCharacter.swift
//  Rage
//
//  Created by M. Lierop on 26-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit


class EfxCharViewController: UIViewController {
    
    @IBOutlet weak var Eff1SubViewContainer: UIView!
    
    lazy var Eff1QuickPresetsSubView: Eff1QuickPresetsSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "Eff1QuickPresetsSubView") as! Eff1QuickPresetsSubView
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var Eff1AdvSubView: Eff1AdvSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "Eff1AdvSubView") as! Eff1AdvSubView
        self.addChild(viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Eff1SubViewContainer.addSubview(Eff1QuickPresetsSubView.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Eff1SubViewControlChange(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 0:
            Eff1QuickPresetsSubView.updateView()
            Eff1SubViewContainer.addSubview(Eff1QuickPresetsSubView.view)
        case 1:
            Eff1SubViewContainer.addSubview(Eff1AdvSubView.view)
            Eff1AdvSubView.updateView()
        default:
            Eff1QuickPresetsSubView.updateView()
            Eff1SubViewContainer.addSubview(Eff1QuickPresetsSubView.view)
        }
    }
}
