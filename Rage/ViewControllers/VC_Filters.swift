//
//  VC_Filters.swift
//  Rage
//
//  Created by M. Lierop on 26-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit


class FilterViewController: UIViewController {
    
    //FilterQuickPresetsSubViewController
    //FiltersSubViewController
    
    @IBOutlet weak var FilterSubViewContainer: UIView!
    @IBOutlet weak var FilterSubViewSegmentControl: UISegmentedControl!
    

    lazy var FilterQuickPresetsSubViewController: FilterQuickPresetsSubViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "FilterQuickPresetsSubViewController") as! FilterQuickPresetsSubViewController
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var FiltersSubViewController: FiltersSubViewController = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "FiltersSubViewController") as! FiltersSubViewController
        self.addChild(viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FilterSubViewSegmentControl.removeBorders()
        FilterSubViewContainer.addSubview(FilterQuickPresetsSubViewController.view)
        print("OSC patch")
        print(patchLibrary.objects(subPatchOsc.self).count)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func FilterSubViewControlChange(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 0:
            FilterQuickPresetsSubViewController.updateView()
            FilterSubViewContainer.addSubview(FilterQuickPresetsSubViewController.view)
        case 1:
            FilterSubViewContainer.addSubview(FiltersSubViewController.view)
            FiltersSubViewController.updateView()
        default:
            FilterQuickPresetsSubViewController.updateView()
            FilterSubViewContainer.addSubview(FilterQuickPresetsSubViewController.view)
        }
    }
    
}
