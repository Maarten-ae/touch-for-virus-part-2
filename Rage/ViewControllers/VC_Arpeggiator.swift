//
//  VC_Arpeggiator.swift
//  Rage
//
//  Created by M. Lierop on 26-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class ArpViewController: UIViewController {
    
    @IBOutlet weak var ArpSubViewContainer: UIView!
    
    
    lazy var ArpQuickPresetsSubView: ArpQuickPresetsSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ArpQuickPresetsSubView") as! ArpQuickPresetsSubView
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var ArpPatternDesignersSubView: ArpPatternDesignersSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ArpPatternDesignersSubView") as! ArpPatternDesignersSubView
        self.addChild(viewController)
        return viewController
    }()
    
    
    //ArpPatternDesignerSubView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ArpSubViewContainer.addSubview(ArpQuickPresetsSubView.view)
    }
    
    @IBAction func ArpSubViewControlChange(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 1:
            ArpSubViewContainer.addSubview(ArpPatternDesignersSubView.view)
            ArpPatternDesignersSubView.updateView()
        default:
            ArpQuickPresetsSubView.updateView()
            ArpSubViewContainer.addSubview(ArpQuickPresetsSubView.view)
        }
    }
}
