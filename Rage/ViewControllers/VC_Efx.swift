//
//  VC_Efx.swift
//  Rage
//
//  Created by M. Lierop on 26-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
//import RealmSwift


class Efx2ViewController: UIViewController {

    @IBOutlet weak var Eff2SubViewContainer: UIView!
    
    lazy var Eff2QuickPresetsSubView: Eff2QuickPresetsSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "Eff2QuickPresetsSubView") as! Eff2QuickPresetsSubView
        self.addChild(viewController)
        return viewController
    }()
    
    lazy var Eff1AdvSubView: Eff1AdvSubView = {
        let storyboard = UIStoryboard(name:"Main",bundle: Bundle.main)
        var viewController = storyboard.instantiateViewController(withIdentifier: "Eff1AdvSubView") as! Eff1AdvSubView
        self.addChild(viewController)
        return viewController
    }()
    //Eff1AdvSubView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Eff2SubViewContainer.addSubview(Eff2QuickPresetsSubView.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
    }
    
    
    
}
