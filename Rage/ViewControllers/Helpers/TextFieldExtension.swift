//
//  SGTextfieldExtension.swift
//
//  Created by Sudhir Gadhvi on 01/04/19.
//  Copyright © 2019 Sudhir Gadhvi. All rights reserved.
//
import UIKit

private var maxLengths = [UITextField: Int]()
private var minLengths = [UITextField: Int]()

extension UITextField {
    
    
    //MARk:- Minimum length
    @IBInspectable var minLegth: Int {
        get {
            guard let l = minLengths[self] else {
                return 0
            }
            return l
        }
        set {
            minLengths[self] = newValue
            addTarget(self, action: #selector(fixMin), for: .editingChanged)
        }
    }
    @objc func fixMin(textField: UITextField) {
        let text = textField.text
        textField.text = text?.safelyLimitedFrom(length: minLegth)
    }
}

extension String
{
    
    func safelyLimitedFrom(length n: Int)->String {
        if (self.count > n) {
            return self
        }
        return self + "-"
        //return String( Array(self).prefix(upTo: n) )
    }
}
