//
//  RageSelector_MainOsc1Mode.swift
//  Rage
//
//  Created by M. Lierop on 28-11-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class oscKnobs {
    
    let WaveShapeLabels: [String] = ["Waveform Shape", "Density", "Wavetable Index", "Wavetable Index", "Wavetable Index", "Wavetable Index", "Wavetable Index", "Wavetable Index"]
    
    func getWaveShapeLabel (oscNr: Int) -> String {
        switch(oscNr){
        case 1:
            return WaveShapeLabels[ patchCtrl.CurrentPatch.osc1_Model]
        case 2:
            return WaveShapeLabels[ patchCtrl.CurrentPatch.osc2_Model]
        default:
            return "Fout"
        }
    }
    
    
    let WaveSelectLabels: [String] = ["Waveform", "Spread", "Wavetable", "Waveform", "Waveform", "Waveform", "Waveform", "Waveform"]
    
    func getWaveSelectLabel (oscNr: Int) -> String {
        switch(oscNr){
        case 1:
            return WaveSelectLabels[ patchCtrl.CurrentPatch.osc1_Model]
        case 2:
            return WaveSelectLabels[ patchCtrl.CurrentPatch.osc2_Model]
        default:
            return "Fout"
        }
    }
    
    
    let PulseWidthLabels: [String] = ["Pulse Width", "Local Detune", "", "PWM", "", "", "", ""]
    
    func getPulseWidthtLabel (oscNr: Int) -> String {
        switch(oscNr){
        case 1:
            return PulseWidthLabels[ patchCtrl.CurrentPatch.osc1_Model]
        case 2:
            return PulseWidthLabels[ patchCtrl.CurrentPatch.osc2_Model]
        default:
            return "Fout"
        }
    }
}
