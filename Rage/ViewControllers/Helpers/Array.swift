//
//  Array.swift
//  Rage
//
//  Created by M. Lierop on 05/10/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import Foundation


extension Array {
    subscript(safe index: Index) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
