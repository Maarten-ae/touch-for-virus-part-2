//
//  VC_AuditKeyboard.swift
//  Rage
//
//  Created by M. Lierop on 21/02/2020.
//  Copyright © 2020 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import AudioKit
import RealmSwift
import CloudKit

class AuditKeyboardViewController:UIViewController, AKKeyboardDelegate {
    
   
    @IBOutlet weak var KeyBoardView: KeyboardView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        KeyBoardView.delegate = self
    }
    
    public func noteOn(note: MIDINoteNumber, velocity: MIDIVelocity) {
        print("NOTE ON ",note," channel: ",app.getAuditMIDIChannel())
        midi.sendNoteOnMessage(noteNumber: note, velocity: 120, channel: app.getAuditMIDIChannel() )
    }
    
    public func noteOff(note: MIDINoteNumber) {
        midi.sendNoteOffMessage(noteNumber: note, velocity: 120, channel: app.getAuditMIDIChannel())
    }

}
