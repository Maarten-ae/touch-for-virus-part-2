//
//  VC_SupportPage.swift
//  Rage
//
//  Created by M. Lierop on 23/12/2018.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import UIKit
import AudioKit
import WebKit

class SupportPageViewControler: UIViewController {
    
    @IBOutlet weak var WebViewer: WKWebView!
    @IBOutlet weak var CloseBtn: PMSuperButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        //https://www.maartenlierop.nl/touch-for-virus-app/InAppPages/index-v1-3.html
        let url = URL(string: "https://www.maartenlierop.nl/touch-for-virus-app/InAppPages/index-v1-4.html")!
        //WebViewer.allowsBackForwardNavigationGestures = true
        //WebViewer.customUserAgent = "Touch-for-VIRUS_1.3"
        WebViewer.load(URLRequest(url: url))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        
        
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    @IBAction func CloseScreen(_ sender: UIButton) {
        presentingViewController!.dismiss(animated: true, completion: nil)
    }
    
}
