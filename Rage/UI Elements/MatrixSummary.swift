//
//  MatrixSummary.swift
//  Rage
//
//  Created by M. Lierop on 16-04-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation


class MatrixSummary {
    
    init(){
        print("jaja")
    }
    
    func slot1Summary() -> String {
        
        var Summary: String = "Inactive"
        
        if ( patchCtrl.CurrentPatch.matrixSlot1_Source != 0){

            var counter: Int = 3
            
            if ( patchCtrl.CurrentPatch.matrixSlot1_Amount1 == 64 || patchCtrl.CurrentPatch.matrixSlot1_Destination1 == 0 ){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot1_Amount2 == 64 || patchCtrl.CurrentPatch.matrixSlot1_Destination2 == 0){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot1_Amount3 == 64 || patchCtrl.CurrentPatch.matrixSlot1_Destination3 == 0){
                counter = counter - 1
            }
            
            if (counter == 1) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot1_Source]! + "   ->   " + String(counter) + " destination"
            } else if (counter != 0) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot1_Source]! + "   ->   " + String(counter) + " destinations"
            }
        }
        
        return Summary
    }
    
    func slot2Summary() -> String {
        
        var Summary: String = "Inactive"
        
        if ( patchCtrl.CurrentPatch.matrixSlot2_Source != 0){
            
            var counter: Int = 3
            
            if ( patchCtrl.CurrentPatch.matrixSlot2_Amount1 == 64 || patchCtrl.CurrentPatch.matrixSlot2_Destination1 == 0 ){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot2_Amount2 == 64 || patchCtrl.CurrentPatch.matrixSlot2_Destination2 == 0){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot2_Amount3 == 64 || patchCtrl.CurrentPatch.matrixSlot2_Destination3 == 0){
                counter = counter - 1
            }
            
            if (counter == 1) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot2_Source]! + "   ->   " + String(counter) + " destination"
            } else if (counter != 0) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot2_Source]! + "   ->   " + String(counter) + " destinations"
            }
        }
        
        return Summary
    }
    
    
    func slot3Summary() -> String {
        
        var Summary: String = "Inactive"
        
        if ( patchCtrl.CurrentPatch.matrixSlot3_Source != 0){
            
            var counter: Int = 3
            
            if ( patchCtrl.CurrentPatch.matrixSlot3_Amount1 == 64 || patchCtrl.CurrentPatch.matrixSlot3_Destination1 == 0 ){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot3_Amount2 == 64 || patchCtrl.CurrentPatch.matrixSlot3_Destination2 == 0){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot3_Amount3 == 64 || patchCtrl.CurrentPatch.matrixSlot3_Destination3 == 0){
                counter = counter - 1
            }
            
            if (counter == 1) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot3_Source]! + "   ->   " + String(counter) + " destination"
            } else if (counter != 0) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot3_Source]! + "   ->   " + String(counter) + " destinations"
            }
        }
        
        return Summary
    }
    
    func slot4Summary() -> String {
        
        var Summary: String = "Inactive"
        
        if ( patchCtrl.CurrentPatch.matrixSlot4_Source != 0){
            
            var counter: Int = 3
            
            if ( patchCtrl.CurrentPatch.matrixSlot4_Amount1 == 64 || patchCtrl.CurrentPatch.matrixSlot4_Destination1 == 0 ){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot4_Amount2 == 64 || patchCtrl.CurrentPatch.matrixSlot4_Destination2 == 0){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot4_Amount3 == 64 || patchCtrl.CurrentPatch.matrixSlot4_Destination3 == 0){
                counter = counter - 1
            }
            
            if (counter == 1) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot4_Source]! + "   ->   " + String(counter) + " destination"
            } else if (counter != 0) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot4_Source]! + "   ->   " + String(counter) + " destinations"
            }
        }
        
        return Summary
    }
    
    func slot5Summary() -> String {
        
        var Summary: String = "Inactive"
        
        if ( patchCtrl.CurrentPatch.matrixSlot5_Source != 0){
            
            var counter: Int = 3
            
            if ( patchCtrl.CurrentPatch.matrixSlot5_Amount1 == 64 || patchCtrl.CurrentPatch.matrixSlot5_Destination1 == 0 ){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot5_Amount2 == 64 || patchCtrl.CurrentPatch.matrixSlot5_Destination2 == 0){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot5_Amount3 == 64 || patchCtrl.CurrentPatch.matrixSlot5_Destination3 == 0){
                counter = counter - 1
            }
            
            if (counter == 1) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot5_Source]! + "   ->   " + String(counter) + " destination"
            } else if (counter != 0) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot5_Source]! + "   ->   " + String(counter) + " destinations"
            }
        }
        
        return Summary
    }
    
    func slot6Summary() -> String {
        
        var Summary: String = "Inactive"
        
        if ( patchCtrl.CurrentPatch.matrixSlot6_Source != 0){
            
            var counter: Int = 3
            
            if ( patchCtrl.CurrentPatch.matrixSlot6_Amount1 == 64 || patchCtrl.CurrentPatch.matrixSlot6_Destination1 == 0 ){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot6_Amount2 == 64 || patchCtrl.CurrentPatch.matrixSlot6_Destination2 == 0){
                counter = counter - 1
            }
            
            if ( patchCtrl.CurrentPatch.matrixSlot6_Amount3 == 64 || patchCtrl.CurrentPatch.matrixSlot6_Destination3 == 0){
                counter = counter - 1
            }
            
            if (counter == 1) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot6_Source]! + "   ->   " + String(counter) + " destination"
            } else if (counter != 0) {
                Summary = MatrixSourceLabels[safe: patchCtrl.CurrentPatch.matrixSlot6_Source]! + "   ->   " + String(counter) + " destinations"
            }
        }
        
        return Summary
    }
    
}
