//
//  Arpeggiator.swift
//  Rage
//
//  Created by M. Lierop on 22/02/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

public class ArpeggiatorChartIO: UIView
{
    let StepSize: Int? = 62
    
    let layer_step01: CAShapeLayer
    private var step01_Handle: UIView!
    private var step01_Point: CGPoint! = CGPoint(x: 0, y: 0)
    
    let layer_step02: CAShapeLayer
    private var step02_Handle: UIView!
    private var step02_Point: CGPoint! = CGPoint(x: (1 * 127), y: 0)
    
    let layer_step03: CAShapeLayer
    private var step03_Handle: UIView!
    private var step03_Point: CGPoint! = CGPoint(x: (2 * 127), y: 0)
    
    let layer_step04: CAShapeLayer
    private var step04_Handle: UIView!
    private var step04_Point: CGPoint! = CGPoint(x: (3 * 127), y: 0)
    
    let layer_step05: CAShapeLayer
    private var step05_Handle: UIView!
    private var step05_Point: CGPoint! = CGPoint(x: 0, y: 0)
    
    let layer_step06: CAShapeLayer
    private var step06_Handle: UIView!
    private var step06_Point: CGPoint! = CGPoint(x: (1 * 127), y: 0)
    
    let layer_step07: CAShapeLayer
    private var step07_Handle: UIView!
    private var step07_Point: CGPoint! = CGPoint(x: (2 * 127), y: 0)
    
    let layer_step08: CAShapeLayer
    private var step08_Handle: UIView!
    private var step08_Point: CGPoint! = CGPoint(x: (3 * 127), y: 0)
    
    let layer_step09: CAShapeLayer
    private var step09_Handle: UIView!
    private var step09_Point: CGPoint! = CGPoint(x: 0, y: 0)
    
    let layer_step10: CAShapeLayer
    private var step10_Handle: UIView!
    private var step10_Point: CGPoint! = CGPoint(x: (1 * 127), y: 0)
    
    let layer_step11: CAShapeLayer
    private var step11_Handle: UIView!
    private var step11_Point: CGPoint! = CGPoint(x: (2 * 127), y: 0)
    
    let layer_step12: CAShapeLayer
    private var step12_Handle: UIView!
    private var step12_Point: CGPoint! = CGPoint(x: (3 * 127), y: 0)
    
    let layer_step13: CAShapeLayer
    private var step13_Handle: UIView!
    private var step13_Point: CGPoint! = CGPoint(x: 0, y: 0)
    
    let layer_step14: CAShapeLayer
    private var step14_Handle: UIView!
    private var step14_Point: CGPoint! = CGPoint(x: (1 * 127), y: 0)
    
    let layer_step15: CAShapeLayer
    private var step15_Handle: UIView!
    private var step15_Point: CGPoint! = CGPoint(x: (2 * 127), y: 0)
    
    let layer_step16: CAShapeLayer
    private var step16_Handle: UIView!
    private var step16_Point: CGPoint! = CGPoint(x: (3 * 127), y: 0)
    
    
    
    var TouchedStep: Int = 0
    
    var fullPattern: [ArpStep] = []
    var pattern: [ArpStep] = []
    
    
    // required for IBDesignable class to properly render
    required public init?(coder aDecoder: NSCoder) {
        
        
        layer_step01 = CAShapeLayer()
        layer_step02 = CAShapeLayer()
        layer_step03 = CAShapeLayer()
        layer_step04 = CAShapeLayer()
        layer_step05 = CAShapeLayer()
        layer_step06 = CAShapeLayer()
        layer_step07 = CAShapeLayer()
        layer_step08 = CAShapeLayer()
        layer_step09 = CAShapeLayer()
        layer_step10 = CAShapeLayer()
        layer_step11 = CAShapeLayer()
        layer_step12 = CAShapeLayer()
        layer_step13 = CAShapeLayer()
        layer_step14 = CAShapeLayer()
        layer_step15 = CAShapeLayer()
        layer_step16 = CAShapeLayer()
        
        super.init(coder: aDecoder)
        initialize()
    }
    
    // required for IBDesignable class to properly render
    required override public init(frame: CGRect) {
        
        
        layer_step01 = CAShapeLayer()
        layer_step02 = CAShapeLayer()
        layer_step03 = CAShapeLayer()
        layer_step04 = CAShapeLayer()
        layer_step05 = CAShapeLayer()
        layer_step06 = CAShapeLayer()
        layer_step07 = CAShapeLayer()
        layer_step08 = CAShapeLayer()
        layer_step09 = CAShapeLayer()
        layer_step10 = CAShapeLayer()
        layer_step11 = CAShapeLayer()
        layer_step12 = CAShapeLayer()
        layer_step13 = CAShapeLayer()
        layer_step14 = CAShapeLayer()
        layer_step15 = CAShapeLayer()
        layer_step16 = CAShapeLayer()
        
        
        super.init(frame: frame)
        initialize()
    }
    
    
    fileprivate func initialize() {
        print("> hello arp patternbuilder!")
        
        self.step01_Handle = DrawArpHandlePnt(point: step01_Point, id: "step01_Handle")
        self.step02_Handle = DrawArpHandlePnt(point: step02_Point, id: "step02_Handle")
        self.step03_Handle = DrawArpHandlePnt(point: step03_Point, id: "step03_Handle")
        self.step04_Handle = DrawArpHandlePnt(point: step04_Point, id: "step04_Handle")
        self.step05_Handle = DrawArpHandlePnt(point: step01_Point, id: "step01_Handle")
        self.step06_Handle = DrawArpHandlePnt(point: step02_Point, id: "step02_Handle")
        self.step07_Handle = DrawArpHandlePnt(point: step03_Point, id: "step03_Handle")
        self.step08_Handle = DrawArpHandlePnt(point: step04_Point, id: "step04_Handle")
        self.step09_Handle = DrawArpHandlePnt(point: step01_Point, id: "step01_Handle")
        self.step10_Handle = DrawArpHandlePnt(point: step02_Point, id: "step02_Handle")
        self.step11_Handle = DrawArpHandlePnt(point: step03_Point, id: "step03_Handle")
        self.step12_Handle = DrawArpHandlePnt(point: step04_Point, id: "step04_Handle")
        self.step13_Handle = DrawArpHandlePnt(point: step01_Point, id: "step01_Handle")
        self.step14_Handle = DrawArpHandlePnt(point: step02_Point, id: "step02_Handle")
        self.step15_Handle = DrawArpHandlePnt(point: step03_Point, id: "step03_Handle")
        self.step16_Handle = DrawArpHandlePnt(point: step04_Point, id: "step04_Handle")
        //rebuildPattern()
    }
    
    func update(){
        self.rebuildPattern()
    }
    
    func clearLayers(){
        layer_step01.removeFromSuperlayer()
        layer_step02.removeFromSuperlayer()
        layer_step03.removeFromSuperlayer()
        layer_step04.removeFromSuperlayer()
        layer_step05.removeFromSuperlayer()
        layer_step06.removeFromSuperlayer()
        layer_step07.removeFromSuperlayer()
        layer_step08.removeFromSuperlayer()
        layer_step09.removeFromSuperlayer()
        layer_step10.removeFromSuperlayer()
        layer_step11.removeFromSuperlayer()
        layer_step12.removeFromSuperlayer()
        layer_step13.removeFromSuperlayer()
        layer_step14.removeFromSuperlayer()
        layer_step15.removeFromSuperlayer()
        layer_step16.removeFromSuperlayer()
        
        self.step01_Handle.isHidden = true
        self.step02_Handle.isHidden = true
        self.step03_Handle.isHidden = true
        self.step04_Handle.isHidden = true
        self.step05_Handle.isHidden = true
        self.step06_Handle.isHidden = true
        self.step07_Handle.isHidden = true
        self.step08_Handle.isHidden = true
        self.step09_Handle.isHidden = true
        self.step10_Handle.isHidden = true
        self.step11_Handle.isHidden = true
        self.step12_Handle.isHidden = true
        self.step13_Handle.isHidden = true
        self.step14_Handle.isHidden = true
        self.step15_Handle.isHidden = true
        self.step16_Handle.isHidden = true
    }
    
    
    
    func rebuildPattern (){
        self.clearLayers()
        
        fullPattern = []
        pattern = []
        
        //self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        
        // eerst array maken met de steps die wel aan staan
        let State: Patch = patchCtrl.CurrentPatch
        
        // fullPattern.append(ArpStep(start: 0, size: 1, noteLength: 0, noteVelocity: 0, x:0, y:0, width:0, height:0))  // dummy, to make index match to start nr's
        
        if( State.arp_step_1 == 1 ){fullPattern.append(ArpStep(start: 1, size: 1, noteLength: State.arp_stepLength_1, noteVelocity: State.arp_stepVelocity_1, x:0, y:0, width:0, height:0))}
        if( State.arp_step_2 == 1 ){fullPattern.append(ArpStep(start: 2, size: 1, noteLength: State.arp_stepLength_2, noteVelocity: State.arp_stepVelocity_2, x:0, y:0, width:0, height:0))}
        if( State.arp_step_3 == 1 ){fullPattern.append(ArpStep(start: 3, size: 1, noteLength: State.arp_stepLength_3, noteVelocity: State.arp_stepVelocity_3, x:0, y:0, width:0, height:0))}
        if( State.arp_step_4 == 1 ){fullPattern.append(ArpStep(start: 4, size: 1, noteLength: State.arp_stepLength_4, noteVelocity: State.arp_stepVelocity_4, x:0, y:0, width:0, height:0))}
        if( State.arp_step_5 == 1 ){fullPattern.append(ArpStep(start: 5, size: 1, noteLength: State.arp_stepLength_5, noteVelocity: State.arp_stepVelocity_5, x:0, y:0, width:0, height:0))}
        if( State.arp_step_6 == 1 ){fullPattern.append(ArpStep(start: 6, size: 1, noteLength: State.arp_stepLength_6, noteVelocity: State.arp_stepVelocity_6, x:0, y:0, width:0, height:0))}
        if( State.arp_step_7 == 1 ){fullPattern.append(ArpStep(start: 7, size: 1, noteLength: State.arp_stepLength_7, noteVelocity: State.arp_stepVelocity_7, x:0, y:0, width:0, height:0))}
        if( State.arp_step_8 == 1 ){fullPattern.append(ArpStep(start: 8, size: 1, noteLength: State.arp_stepLength_8, noteVelocity: State.arp_stepVelocity_8, x:0, y:0, width:0, height:0))}
        if( State.arp_step_9 == 1 ){fullPattern.append(ArpStep(start: 9, size: 1, noteLength: State.arp_stepLength_9, noteVelocity: State.arp_stepVelocity_9, x:0, y:0, width:0, height:0))}
        if( State.arp_step_10 == 1 ){fullPattern.append(ArpStep(start: 10, size: 1, noteLength: State.arp_stepLength_10, noteVelocity: State.arp_stepVelocity_10, x:0, y:0, width:0, height:0))}
        if( State.arp_step_11 == 1 ){fullPattern.append(ArpStep(start: 11, size: 1, noteLength: State.arp_stepLength_11, noteVelocity: State.arp_stepVelocity_11, x:0, y:0, width:0, height:0))}
        if( State.arp_step_12 == 1 ){fullPattern.append(ArpStep(start: 12, size: 1, noteLength: State.arp_stepLength_12, noteVelocity: State.arp_stepVelocity_12, x:0, y:0, width:0, height:0))}
        if( State.arp_step_13 == 1 ){fullPattern.append(ArpStep(start: 13, size: 1, noteLength: State.arp_stepLength_13, noteVelocity: State.arp_stepVelocity_13, x:0, y:0, width:0, height:0))}
        if( State.arp_step_14 == 1 ){fullPattern.append(ArpStep(start: 14, size: 1, noteLength: State.arp_stepLength_14, noteVelocity: State.arp_stepVelocity_14, x:0, y:0, width:0, height:0))}
        if( State.arp_step_15 == 1 ){fullPattern.append(ArpStep(start: 15, size: 1, noteLength: State.arp_stepLength_15, noteVelocity: State.arp_stepVelocity_15, x:0, y:0, width:0, height:0))}
        if( State.arp_step_16 == 1 ){fullPattern.append(ArpStep(start: 16, size: 1, noteLength: State.arp_stepLength_16, noteVelocity: State.arp_stepVelocity_16, x:0, y:0, width:0, height:0))}
        if( State.arp_step_17 == 1 ){fullPattern.append(ArpStep(start: 17, size: 1, noteLength: State.arp_stepLength_17, noteVelocity: State.arp_stepVelocity_17, x:0, y:0, width:0, height:0))}
        if( State.arp_step_18 == 1 ){fullPattern.append(ArpStep(start: 18, size: 1, noteLength: State.arp_stepLength_18, noteVelocity: State.arp_stepVelocity_18, x:0, y:0, width:0, height:0))}
        if( State.arp_step_19 == 1 ){fullPattern.append(ArpStep(start: 19, size: 1, noteLength: State.arp_stepLength_19, noteVelocity: State.arp_stepVelocity_19, x:0, y:0, width:0, height:0))}
        if( State.arp_step_20 == 1 ){fullPattern.append(ArpStep(start: 20, size: 1, noteLength: State.arp_stepLength_20, noteVelocity: State.arp_stepVelocity_20, x:0, y:0, width:0, height:0))}
        if( State.arp_step_21 == 1 ){fullPattern.append(ArpStep(start: 21, size: 1, noteLength: State.arp_stepLength_21, noteVelocity: State.arp_stepVelocity_21, x:0, y:0, width:0, height:0))}
        if( State.arp_step_22 == 1 ){fullPattern.append(ArpStep(start: 22, size: 1, noteLength: State.arp_stepLength_22, noteVelocity: State.arp_stepVelocity_22, x:0, y:0, width:0, height:0))}
        if( State.arp_step_23 == 1 ){fullPattern.append(ArpStep(start: 23, size: 1, noteLength: State.arp_stepLength_23, noteVelocity: State.arp_stepVelocity_23, x:0, y:0, width:0, height:0))}
        if( State.arp_step_24 == 1 ){fullPattern.append(ArpStep(start: 24, size: 1, noteLength: State.arp_stepLength_24, noteVelocity: State.arp_stepVelocity_24, x:0, y:0, width:0, height:0))}
        if( State.arp_step_25 == 1 ){fullPattern.append(ArpStep(start: 25, size: 1, noteLength: State.arp_stepLength_25, noteVelocity: State.arp_stepVelocity_25, x:0, y:0, width:0, height:0))}
        if( State.arp_step_26 == 1 ){fullPattern.append(ArpStep(start: 26, size: 1, noteLength: State.arp_stepLength_26, noteVelocity: State.arp_stepVelocity_26, x:0, y:0, width:0, height:0))}
        if( State.arp_step_27 == 1 ){fullPattern.append(ArpStep(start: 27, size: 1, noteLength: State.arp_stepLength_27, noteVelocity: State.arp_stepVelocity_27, x:0, y:0, width:0, height:0))}
        if( State.arp_step_28 == 1 ){fullPattern.append(ArpStep(start: 28, size: 1, noteLength: State.arp_stepLength_28, noteVelocity: State.arp_stepVelocity_28, x:0, y:0, width:0, height:0))}
        if( State.arp_step_29 == 1 ){fullPattern.append(ArpStep(start: 29, size: 1, noteLength: State.arp_stepLength_29, noteVelocity: State.arp_stepVelocity_29, x:0, y:0, width:0, height:0))}
        if( State.arp_step_30 == 1 ){fullPattern.append(ArpStep(start: 30, size: 1, noteLength: State.arp_stepLength_30, noteVelocity: State.arp_stepVelocity_30, x:0, y:0, width:0, height:0))}
        if( State.arp_step_31 == 1 ){fullPattern.append(ArpStep(start: 31, size: 1, noteLength: State.arp_stepLength_31, noteVelocity: State.arp_stepVelocity_31, x:0, y:0, width:0, height:0))}
        if( State.arp_step_32 == 1 ){fullPattern.append(ArpStep(start: 32, size: 1, noteLength: State.arp_stepLength_32, noteVelocity: State.arp_stepVelocity_32, x:0, y:0, width:0, height:0))}
        
        
        
        // calculate step length
        var i:Int = 0
        
        for step in fullPattern {
            // maak een array met alleen de steps die aan staan
            var length:Int = 1
            if ( (i + 1) <= (fullPattern.count - 1) ){
                length = ( fullPattern[ (i) + 1].start - step.start)
            } else {
                length = ( 32 - step.start)
            }
            
            pattern.append(ArpStep(start: step.start,
                                   size: length,
                                   noteLength: step.noteLength,
                                   noteVelocity: step.noteVelocity,
                                   x: Int (( Double(step.start) * Double(self.StepSize!) ) - Double(self.StepSize!) ),
                                   y: 130,
                                   width: Int(  (Double(step.noteLength) / Double(127)) * (Double(length) * Double(self.StepSize!)) ),
                                   height: Int(-1 * step.noteVelocity)
            ))
            i = i + 1
        }
        
        print("Aantal actieve steps ",pattern.count)
        
        for step in pattern {
            // loop door de steps die aan staan om deze te tekenen
            
            switch(step.start){
            case 1:
                self.initStep01(step: step)
            case 2:
                self.initStep02(step: step)
            case 3:
                self.initStep03(step: step)
            case 4:
                self.initStep04(step: step)
            case 5:
                self.initStep05(step: step)
            case 6:
                self.initStep06(step: step)
            case 7:
                self.initStep07(step: step)
            case 8:
                self.initStep08(step: step)
            case 9:
                self.initStep09(step: step)
            case 10:
                self.initStep10(step: step)
            case 11:
                self.initStep11(step: step)
            case 12:
                self.initStep12(step: step)
            case 13:
                self.initStep13(step: step)
            case 14:
                self.initStep14(step: step)
            case 15:
                self.initStep15(step: step)
            case 16:
                self.initStep16(step: step)
            default:
                break
            }
            
        }
        self.bringHandlesToFront()
    }
    
    func reDrawPattern(){
        for step in pattern {
            // loop door de steps die aan staan om deze te tekenen
            
            switch(step.start){
            case 1:
                self.drawStep01(step: step)
            case 2:
                self.drawStep02(step: step)
            case 3:
                self.drawStep03(step: step)
            case 4:
                self.drawStep04(step: step)
            case 5:
                self.drawStep05(step: step)
            case 6:
                self.drawStep06(step: step)
            case 7:
                self.drawStep07(step: step)
            case 8:
                self.drawStep08(step: step)
            case 9:
                self.drawStep09(step: step)
            case 10:
                self.drawStep10(step: step)
            case 11:
                self.drawStep11(step: step)
            case 12:
                self.drawStep12(step: step)
            case 13:
                self.drawStep13(step: step)
            case 14:
                self.drawStep14(step: step)
            case 15:
                self.drawStep15(step: step)
            case 16:
                self.drawStep16(step: step)
            default:
                break
            }
            
        }
    }
    
    func initStep01(step: ArpStep){
        self.step01_Point = CGPoint(x: (step.x + (step.width / 2) ), y: (127 + step.height) )
        self.step01_Handle.frame.origin.x = CGFloat( self.step01_Point.x - 20)
        self.step01_Handle.frame.origin.y = CGFloat( self.step01_Point.y - 20)
        self.addSubview(self.step01_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep01(_:)))
        self.step01_Handle.isHidden = false
        self.step01_Handle.isUserInteractionEnabled = true
        self.step01_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep01(step: step)
    }
    
    func initStep02(step: ArpStep){
        // teken handle
        self.step02_Point = CGPoint(x: (step.x + (step.width / 2) ), y: (127 + step.height) )
        self.step02_Handle.frame.origin.x = CGFloat( self.step02_Point.x - 20)
        self.step02_Handle.frame.origin.y = CGFloat( self.step02_Point.y - 20)
        self.addSubview(self.step02_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep02(_:)))
        self.step02_Handle.isHidden = false
        self.step02_Handle.isUserInteractionEnabled = true
        self.step02_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep02(step: step)
    }
    
    func initStep03(step: ArpStep){
        // teken handle
        self.step03_Point = CGPoint(x: (step.x + ( step.width / 2) ), y: (127 + step.height) )
        self.step03_Handle.frame.origin.x = CGFloat( self.step03_Point.x - 20)
        self.step03_Handle.frame.origin.y = CGFloat( self.step03_Point.y - 20)
        self.addSubview(self.step03_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep03(_:)))
        self.step03_Handle.isHidden = false
        self.step03_Handle.isUserInteractionEnabled = true
        self.step03_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep03(step: step)
    }
    
    func initStep04(step: ArpStep){
        // teken handle
        self.step04_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step04_Handle.frame.origin.x = CGFloat( self.step04_Point.x - 20)
        self.step04_Handle.frame.origin.y = CGFloat( self.step04_Point.y - 20)
        self.addSubview(self.step04_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep04(_:)))
        self.step04_Handle.isHidden = false
        self.step04_Handle.isUserInteractionEnabled = true
        self.step04_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep04(step: step)
    }
    func initStep05(step: ArpStep){
        // teken handle
        self.step05_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step05_Handle.frame.origin.x = CGFloat( self.step05_Point.x - 20)
        self.step05_Handle.frame.origin.y = CGFloat( self.step05_Point.y - 20)
        self.addSubview(self.step05_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep05(_:)))
        self.step05_Handle.isHidden = false
        self.step05_Handle.isUserInteractionEnabled = true
        self.step05_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep05(step: step)
    }
    func initStep06(step: ArpStep){
        // teken handle
        self.step06_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step06_Handle.frame.origin.x = CGFloat( self.step06_Point.x - 20)
        self.step06_Handle.frame.origin.y = CGFloat( self.step06_Point.y - 20)
        self.addSubview(self.step06_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep06(_:)))
        self.step06_Handle.isHidden = false
        self.step06_Handle.isUserInteractionEnabled = true
        self.step06_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep06(step: step)
    }
    func initStep07(step: ArpStep){
        // teken handle
        self.step07_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step07_Handle.frame.origin.x = CGFloat( self.step07_Point.x - 20)
        self.step07_Handle.frame.origin.y = CGFloat( self.step07_Point.y - 20)
        self.addSubview(self.step07_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep07(_:)))
        self.step07_Handle.isHidden = false
        self.step07_Handle.isUserInteractionEnabled = true
        self.step07_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep07(step: step)
    }
    func initStep08(step: ArpStep){
        // teken handle
        self.step08_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step08_Handle.frame.origin.x = CGFloat( self.step08_Point.x - 20)
        self.step08_Handle.frame.origin.y = CGFloat( self.step08_Point.y - 20)
        self.addSubview(self.step08_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep08(_:)))
        self.step08_Handle.isHidden = false
        self.step08_Handle.isUserInteractionEnabled = true
        self.step08_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep08(step: step)
    }
    func initStep09(step: ArpStep){
        // teken handle
        self.step09_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step09_Handle.frame.origin.x = CGFloat( self.step09_Point.x - 20)
        self.step09_Handle.frame.origin.y = CGFloat( self.step09_Point.y - 20)
        self.addSubview(self.step09_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep09(_:)))
        self.step09_Handle.isHidden = false
        self.step09_Handle.isUserInteractionEnabled = true
        self.step09_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep09(step: step)
    }
    func initStep10(step: ArpStep){
        // teken handle
        self.step10_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step10_Handle.frame.origin.x = CGFloat( self.step10_Point.x - 20)
        self.step10_Handle.frame.origin.y = CGFloat( self.step10_Point.y - 20)
        self.addSubview(self.step10_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep10(_:)))
        self.step10_Handle.isHidden = false
        self.step10_Handle.isUserInteractionEnabled = true
        self.step10_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep10(step: step)
    }
    func initStep11(step: ArpStep){
        // teken handle
        self.step11_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step11_Handle.frame.origin.x = CGFloat( self.step11_Point.x - 20)
        self.step11_Handle.frame.origin.y = CGFloat( self.step11_Point.y - 20)
        self.addSubview(self.step11_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep11(_:)))
        self.step11_Handle.isHidden = false
        self.step11_Handle.isUserInteractionEnabled = true
        self.step11_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep11(step: step)
    }
    func initStep12(step: ArpStep){
        // teken handle
        self.step12_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step12_Handle.frame.origin.x = CGFloat( self.step12_Point.x - 20)
        self.step12_Handle.frame.origin.y = CGFloat( self.step12_Point.y - 20)
        self.addSubview(self.step12_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep12(_:)))
        self.step12_Handle.isHidden = false
        self.step12_Handle.isUserInteractionEnabled = true
        self.step12_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep12(step: step)
    }
    func initStep13(step: ArpStep){
        // teken handle
        self.step13_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step13_Handle.frame.origin.x = CGFloat( self.step13_Point.x - 20)
        self.step13_Handle.frame.origin.y = CGFloat( self.step13_Point.y - 20)
        self.addSubview(self.step13_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep13(_:)))
        self.step13_Handle.isHidden = false
        self.step13_Handle.isUserInteractionEnabled = true
        self.step13_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep13(step: step)
    }
    func initStep14(step: ArpStep){
        // teken handle
        self.step14_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step14_Handle.frame.origin.x = CGFloat( self.step14_Point.x - 20)
        self.step14_Handle.frame.origin.y = CGFloat( self.step14_Point.y - 20)
        self.addSubview(self.step14_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep14(_:)))
        self.step14_Handle.isHidden = false
        self.step14_Handle.isUserInteractionEnabled = true
        self.step14_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep14(step: step)
    }
    func initStep15(step: ArpStep){
        // teken handle
        self.step15_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step15_Handle.frame.origin.x = CGFloat( self.step15_Point.x - 20)
        self.step15_Handle.frame.origin.y = CGFloat( self.step15_Point.y - 20)
        self.addSubview(self.step15_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep15(_:)))
        self.step15_Handle.isHidden = false
        self.step15_Handle.isUserInteractionEnabled = true
        self.step15_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep15(step: step)
    }
    func initStep16(step: ArpStep){
        // teken handle
        self.step16_Point = CGPoint(x: (step.x + step.width ), y: (127 + step.height) )
        self.step16_Handle.frame.origin.x = CGFloat( self.step16_Point.x - 20)
        self.step16_Handle.frame.origin.y = CGFloat( self.step16_Point.y - 20)
        self.addSubview(self.step16_Handle)
        
        let Gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeStep16(_:)))
        self.step16_Handle.isHidden = false
        self.step16_Handle.isUserInteractionEnabled = true
        self.step16_Handle.addGestureRecognizer(Gesture)
        
        self.drawStep16(step: step)
    }
    
    
    
    func drawStep01(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step01.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step01.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step01)
    }
    
    func drawStep02(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step02.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step02.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step02)
    }
    
    func drawStep03(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step03.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step03.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step03)
    }
    
    func drawStep04(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step04.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step04.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step04)
    }
    
    func drawStep05(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step05.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step05.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step05)
    }
    func drawStep06(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step06.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step06.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step06)
    }
    func drawStep07(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step07.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step07.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step07)
    }
    func drawStep08(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step08.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step08.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step08)
    }
    func drawStep09(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step09.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step09.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step09)
    }
    func drawStep10(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step10.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step10.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step10)
    }
    func drawStep11(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step11.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step11.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step11)
    }
    func drawStep12(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step12.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step12.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step12)
    }
    func drawStep13(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step13.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step13.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step13)
    }
    func drawStep14(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step14.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step14.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step14)
    }
    func drawStep15(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step15.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step15.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step15)
    }
    func drawStep16(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step16.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step16.fillColor = UIColor.green.cgColor
        self.layer.addSublayer(layer_step16)
    }
    
    
    @objc func onChangeStep01(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_1, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_1(value: Int(newYValue))
        
        self.step01_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep02(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_2, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_2(value: Int(newYValue))
        
        self.step02_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep03(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_3, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_3(value: Int(newYValue))
        
        self.step03_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep04(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_4, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_4(value: Int(newYValue))
        
        self.step04_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep05(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_5, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_5(value: Int(newYValue))
        
        self.step05_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep06(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_6, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_6(value: Int(newYValue))
        
        self.step06_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep07(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_7, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_7(value: Int(newYValue))
        
        self.step07_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep08(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_8, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_8(value: Int(newYValue))
        
        self.step08_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep09(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_9, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_9(value: Int(newYValue))
        
        self.step09_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep10(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_10, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_10(value: Int(newYValue))
        
        self.step10_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep11(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_11, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_11(value: Int(newYValue))
        
        self.step11_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep12(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_12, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_12(value: Int(newYValue))
        
        self.step12_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep13(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_13, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_13(value: Int(newYValue))
        
        self.step13_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep14(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_14, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_14(value: Int(newYValue))
        
        self.step14_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    @objc func onChangeStep15(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_15, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_15(value: Int(newYValue))
        
        self.step15_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    @objc func onChangeStep16(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        sender.setTranslation(CGPoint.zero, in: self)
        
        let newYValue: Int = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_16, change: Int(translationGlobal.y))
        patchCtrl.setArpStepVelocity_16(value: Int(newYValue))
        
        self.step16_Handle.frame.origin.y = CGFloat( 130 - newYValue )
        self.rebuildPattern()
    }
    
    
    
    func CalculateVelocity(old: Int, change: Int) -> Int {
        
        var new:Int = 0
        
        new = old - change
        
        if ( new < 0 ){
            return 0
        } else if ( new > 127) {
            return 127
        } else {
            return new
        }
        
    }
    
    func setVelocity(StepNR: Int, value: Int ){
        var newValue: Int = 0
        switch(StepNR){
        case 1:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_1, change: value)
            patchCtrl.setArpStepVelocity_1(value: Int(newValue))
        case 2:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_2, change: value)
            patchCtrl.setArpStepVelocity_2(value: Int(newValue))
        case 3:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_3, change: value)
            patchCtrl.setArpStepVelocity_3(value: Int(newValue))
        case 4:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_4, change: value)
            patchCtrl.setArpStepVelocity_4(value: Int(newValue))
        case 5:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_5, change: value)
            patchCtrl.setArpStepVelocity_5(value: Int(newValue))
        case 6:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_6, change: value)
            patchCtrl.setArpStepVelocity_6(value: Int(newValue))
        case 7:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_7, change: value)
            patchCtrl.setArpStepVelocity_7(value: Int(newValue))
        case 8:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_8, change: value)
            patchCtrl.setArpStepVelocity_8(value: Int(newValue))
        case 9:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_9, change: value)
            patchCtrl.setArpStepVelocity_9(value: Int(newValue))
        case 10:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_10, change: value)
            patchCtrl.setArpStepVelocity_10(value: Int(newValue))
        case 11:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_11, change: value)
            patchCtrl.setArpStepVelocity_11(value: Int(newValue))
        case 12:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_12, change: value)
            patchCtrl.setArpStepVelocity_12(value: Int(newValue))
        case 13:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_13, change: value)
            patchCtrl.setArpStepVelocity_13(value: Int(newValue))
        case 14:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_14, change: value)
            patchCtrl.setArpStepVelocity_14(value: Int(newValue))
        case 15:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_15, change: value)
            patchCtrl.setArpStepVelocity_15(value: Int(newValue))
        case 16:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_16, change: value)
            patchCtrl.setArpStepVelocity_16(value: Int(newValue))
        case 17:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_17, change: value)
            patchCtrl.setArpStepVelocity_17(value: Int(newValue))
        case 18:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_18, change: value)
            patchCtrl.setArpStepVelocity_18(value: Int(newValue))
        case 19:
            newValue = CalculateVelocity(old: patchCtrl.CurrentPatch.arp_stepVelocity_19, change: value)
            patchCtrl.setArpStepVelocity_19(value: Int(newValue))
        default:
            break
        }
        self.rebuildPattern()
    }
    
    
    private func bringHandlesToFront(){
        self.bringSubviewToFront(step01_Handle)
        self.bringSubviewToFront(step02_Handle)
        self.bringSubviewToFront(step03_Handle)
    }
}

struct ArpStep {
    //var index: Int
    var start: Int
    var size: Int
    var noteLength: Int
    var noteVelocity: Int
    var x:Int
    var y:Int
    var width:Int
    var height:Int
}

class DrawArpHandlePnt: UIView {
    var posX: Int
    var posY: Int
    var id: String
    
    init(point: CGPoint, id: String) {
        let frameRect = CGRect(x: (point.x - 15), y: (point.y ), width: 80, height: 30)
        
        self.posX = Int(point.x)
        self.posY = Int(point.y)
        self.id = id
        super.init(frame: frameRect)
        
        let promobox =  UIView()
        
        promobox.frame = CGRect(x: 15, y: 15, width: 10, height: 10)
        promobox.layer.borderColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 1).cgColor
        promobox.layer.borderWidth = 2.0
        promobox.layer.backgroundColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 1).cgColor
        
        self.addSubview(promobox)
        
        //let rect = CGRect(x: (point.x - 5), y: (point.y - 5), width: 10, height: 10)
        
        //self.frame = rect // inherited stuff from super class -> UIView
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0) // inherited stuff from super class -> UIView
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
