//
//  AppHeader.swift
//  RAGE
//
//  Created by M. Lierop on 24-08-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

public class AppToolbar: UIView {
    
    var HeaderWidth: Int = 0
    let HeaderHeight: Int = 57
    
    var PatchNameViewObject :UILabel
    
    override init(frame: CGRect) {
        let rect = CGRect(x: 0, y: 0, width: HeaderWidth, height: HeaderHeight)
        self.PatchNameViewObject = UILabel()
        
        super.init(frame: rect)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.green

        self.frame = rect // inherited stuff from super class -> UIView
        
        
    }
    
    init(ScreenWidth: Int){
        HeaderWidth = ScreenWidth
        self.PatchNameViewObject = UILabel()
        
        let rect2 = CGRect(x: 0, y: 0, width: HeaderWidth, height: HeaderHeight)
        super.init(frame: rect2)
        
        self.frame = rect2
        self.drawHeader()
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func drawHeader(){
        var imageViewObject :UIImageView
        
        imageViewObject = UIImageView(frame:CGRect(x: 0, y: 0, width: CGFloat(HeaderWidth), height: CGFloat(HeaderHeight)))
        imageViewObject.image = UIImage(named:"ui-topBar")
        self.addSubview(imageViewObject)
        self.sendSubviewToBack(imageViewObject)
        
        
        var wordMarkObject :UIImageView
        
        wordMarkObject = UIImageView(frame:CGRect(x: 0, y: 0, width: CGFloat(217), height: CGFloat(40)))
        wordMarkObject.image = UIImage(named:"ui-WordMark")
        self.addSubview(wordMarkObject)
        
        
        var displayImage :UIImageView
        
        displayImage = UIImageView(frame:CGRect(x: (self.center.x - 157), y: 0, width: CGFloat(314), height: CGFloat(88)))
        displayImage.image = UIImage(named:"UI-Display")
        self.addSubview(displayImage)
        
        
        let SaveBtn      = UIButton(type: UIButton.ButtonType.custom) as UIButton
        SaveBtn.setTitle("Save", for: [])
        SaveBtn.setTitleColor(UIColor.white, for: [])
        SaveBtn.frame = CGRect(x: (self.HeaderWidth - 210), y: 0, width: 200,height: 40)
        self.addSubview(SaveBtn)
        
        
        PatchNameViewObject = UILabel(frame: CGRect(x: (self.center.x - 157),y: 0, width: 314, height: 50))
        PatchNameViewObject.textAlignment = NSTextAlignment.center
        PatchNameViewObject.text = String(describing: patchCtrl.CurrentPatch.name)
        PatchNameViewObject.textColor = UIColor.white
        self.addSubview(PatchNameViewObject)
        
    }
    
}
