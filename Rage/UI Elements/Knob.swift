//
//  KnobView.swift
//  AudioKit Synth One
//
//  Created by Matthew Fecher on 7/20/17.
//  Copyright © 2017 AudioKit Pro. All rights reserved.
//
import UIKit

@IBDesignable public class Knob: UIView {
    
    var callback: (Double)->Void = { _ in }
    let tap = UITapGestureRecognizer()
    
    @IBInspectable var minValue: Int = 0
    @IBInspectable var maxValue: Int = 127
    @IBInspectable var knobType: String = "" {
        didSet {
            self.setKnobLines()
        }
    }
    
    @IBInspectable var hideValueIndicator: Bool = false {
        didSet {
            self.showValue()
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var hideKnoblines: Bool = false {
        didSet {
            self.setKnobLines()
            setNeedsDisplay()
        }
    }
    
    var minimum = 0.0 {
        didSet {
            self.knobValue = CGFloat((value - minimum) / (maximum - minimum))
        }
    }
    @IBInspectable var maximum = 127.0 {
        didSet {
            self.knobValue = CGFloat((value - minimum) / (maximum - minimum))
        }
    }
    
    var value: Double = 0.5 {
        didSet {
            if value > maximum {
                value = maximum
            }
            if value < minimum {
                value = minimum
            }
            self.knobValue = CGFloat((value - minimum) / (maximum - minimum))
        }
    }
    @IBInspectable var label: String = "" {
        didSet{
            //print("var label.didset",label)
            self.knobLabel = label
        }
    }
    
    // Knob properties
    var knobValue: CGFloat = 0.5 {
        didSet {
            self.showValue()
            setNeedsDisplay()
        }
    }
    var knobLabel: String = "" {
        didSet {
            self.updateLbl()
        }
    }
    var enabled: Bool = true {
        didSet {
            if enabled {
                self.isUserInteractionEnabled = true
                self.alpha = 1
            } else {
                self.isUserInteractionEnabled = false
                self.alpha = 0.6
    
            }
            self.updateLbl()
            setNeedsDisplay()
        }
    }
    var knobFill: CGFloat = 0
    var knobSensitivity = 0.004//0.005
    var lastX: CGFloat = 0
    var lastY: CGFloat = 0
    let labelView = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 21))
    
    let valueIndicatorView = UILabel(frame: CGRect(x: 0, y: 0, width: 50 , height: 10))
    var imageView = UIImageView(image: UIImage(named: "knoblines-neutraal"))
    
    //let bgImage = #imageLiteral(resourceName: "Image")
    // Init / Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentMode = .redraw
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        self.isUserInteractionEnabled = true
        self.setKnobLines()
        self.showValue()
        self.tap.numberOfTapsRequired = 2
        self.tap.addTarget(self, action: #selector(self.ResetValue))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(self.tap)
        
        contentMode = .redraw
    }
    
    @objc func ResetValue(){
        switch (knobType){
        case "balance","saturation":
            self.value = 64
            self.callback(64)
        case "keyfollow":
            self.value = 96
            self.callback(96)
        default:
            self.value = 0
            self.callback(0)
        }
    }
    
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setKnobLines()
        self.showValue()
        contentMode = .scaleAspectFit
        clipsToBounds = true
    }
    
    public class override var requiresConstraintBasedLayout: Bool {
        return true
    }
    
    public override func draw(_ rect: CGRect) {
        //setKnobLines()
        
        KnobStyleKit.drawFMKnob(frame: CGRect(x:0,y:0, width: self.bounds.width, height: self.bounds.height), knobValue: knobValue, enabled: enabled)
        self.showValue()
        
    }
    
    func updateLbl (){
        if(labelView.tag==13){
            labelView.removeFromSuperview()
        }
        if (self.knobLabel != "" && self.enabled == true ){
            labelView.font = UIFont.preferredFont(forTextStyle: .footnote)
            labelView.font.withSize(CGFloat(14))
            labelView.textColor = UIColor(red:0.46,green:0.46,blue:0.46,alpha:1)
            labelView.center = CGPoint(x: (self.frame.size.width / 2), y: -20)
            labelView.textAlignment = .center
            labelView.text = self.knobLabel
            labelView.tag = 13
            labelView.autoresizingMask = .init(arrayLiteral: .flexibleHeight, .flexibleWidth)
            self.addSubview(labelView)
        }
    }
    
    func showValue (){
        if(valueIndicatorView.tag==15){
            valueIndicatorView.removeFromSuperview()
        }
        
        var displayValue: String = ""
        
        switch (knobType){
        case "balance","saturation":
            displayValue = String( Int(self.value) - 64 )
        case "keyfollow":
            if (Int(self.value) == 96) {
                displayValue = "normal"
            } else {
                displayValue = String( Int(self.value) )
            }
        case "oscwaveshape":
            if (Int(self.value) == 0) {
                displayValue = "wave"
            } else if (Int(self.value) == 64) {
                displayValue = "saw"
            } else if (Int(self.value) == 127) {
                displayValue = "pulse"
            } else {
                displayValue = String( Int(self.value) )
            }
        default:
            displayValue = String( Int(self.value) )
        }
        
        if (self.enabled == true && !hideValueIndicator ){
            valueIndicatorView.autoresizingMask = .init(arrayLiteral: .flexibleHeight, .flexibleWidth, .flexibleTopMargin)
            //valueIndicatorView.frame.size = CGSize(width: self.frame.width, height: 10)
            valueIndicatorView.font = UIFont.preferredFont(forTextStyle: .footnote)
            valueIndicatorView.font.withSize(CGFloat(12))
            valueIndicatorView.textColor = UIColor(red:1,green:1,blue:1,alpha:1)
            valueIndicatorView.textAlignment = .center
            valueIndicatorView.text = displayValue
            valueIndicatorView.tag = 15
            valueIndicatorView.center = CGPoint(x:((imageView.frame.size.width / 2)-2),y:((imageView.frame.size.height / 2) - 6))
//            valueIndicatorView.backgroundColor = .magenta
            self.addSubview(valueIndicatorView)
        }
    }
    
    func setKnobLines(){
        if(imageView.tag==14){
            imageView.removeFromSuperview()
        }
        
        if ( self.enabled && !hideKnoblines){
            var imageName = ""
            switch (knobType){
            case "volume":
                imageName = "knoblines-volume"
            case "balance":
                imageName = "knoblines-balance"
            case "keyfollow":
                imageName = "knoblines-keyfollow"
            case "oscwaveshape":
                imageName = "knoblines-waveshape"
            case "saturation":
                imageName = "knoblines-saturation"
            default:
                imageName = "knoblines-neutraal"
            }
            
            let image = UIImage(named: imageName)
            imageView = UIImageView(image: image!)
           
            
//            imageView.frame = CGRect(x: -2, y: -7, width: 55, height: 59)
            imageView.frame = CGRect(x: -2, y: -8, width: (self.frame.width * 1.1), height: (self.frame.height * 1.2))
            
            imageView.autoresizingMask = .init(arrayLiteral: .flexibleHeight, .flexibleWidth)

            imageView.tag = 14
            self.addSubview(imageView)
            self.sendSubviewToBack(imageView)
        }
    }
    
    // Helper
    func setPercentagesWithTouchPoint(_ touchPoint: CGPoint) {
        // Knobs assume up or right is increasing, and down or left is decreasing
        
        let horizontalChange = Double(touchPoint.x - lastX) * knobSensitivity
        value += horizontalChange * 127 //(maximum - minimum)
        
        let verticalChange = Double(touchPoint.y - lastY) * knobSensitivity
        value -= verticalChange * 127 //(maximum - minimum)
        
        callback(value)
        lastX = touchPoint.x
        lastY = touchPoint.y
        self.showValue()
    }
}
