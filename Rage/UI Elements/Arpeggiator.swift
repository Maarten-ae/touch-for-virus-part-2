//
//  Arpeggiator.swift
//  Rage
//
//  Created by M. Lierop on 22/02/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

public class ArpeggiatorChart: UIView
{
    let StepSize: Int? = 62
    
    let layer_step01: CAShapeLayer
    private var step01_Point: CGPoint! = CGPoint(x: 0, y: 0)
    
    let layer_step02: CAShapeLayer
    private var step02_Point: CGPoint! = CGPoint(x: (1 * 127), y: 0)
    
    let layer_step03: CAShapeLayer
    private var step03_Point: CGPoint! = CGPoint(x: (2 * 127), y: 0)
    
    let layer_step04: CAShapeLayer
    private var step04_Point: CGPoint! = CGPoint(x: (3 * 127), y: 0)
    
    let layer_step05: CAShapeLayer
    private var step05_Point: CGPoint! = CGPoint(x: (4 * 127), y: 0)
    
    let layer_step06: CAShapeLayer
    private var step06_Point: CGPoint! = CGPoint(x: (5 * 127), y: 0)
    
    let layer_step07: CAShapeLayer
    private var step07_Point: CGPoint! = CGPoint(x: (6 * 127), y: 0)
    
    let layer_step08: CAShapeLayer
    private var step08_Point: CGPoint! = CGPoint(x: (7 * 127), y: 0)
    
    let layer_step09: CAShapeLayer
    private var step09_Point: CGPoint! = CGPoint(x: (8 * 127), y: 0)
    
    let layer_step10: CAShapeLayer
    private var step10_Point: CGPoint! = CGPoint(x: (9 * 127), y: 0)
    
    let layer_step11: CAShapeLayer
    private var step11_Point: CGPoint! = CGPoint(x: (10 * 127), y: 0)
    
    let layer_step12: CAShapeLayer
    private var step12_Point: CGPoint! = CGPoint(x: (11 * 127), y: 0)
    
    let layer_step13: CAShapeLayer
    private var step13_Point: CGPoint! = CGPoint(x: (12 * 127), y: 0)
    
    let layer_step14: CAShapeLayer
    private var step14_Point: CGPoint! = CGPoint(x: (13 * 127), y: 0)
    
    let layer_step15: CAShapeLayer
    private var step15_Point: CGPoint! = CGPoint(x: (14 * 127), y: 0)
    
    let layer_step16: CAShapeLayer
    private var step16_Point: CGPoint! = CGPoint(x: (15 * 127), y: 0)
    
    let layer_step17: CAShapeLayer
    private var step17_Point: CGPoint! = CGPoint(x: (16 * 127), y: 0)
    
    let layer_step18: CAShapeLayer
    private var step18_Point: CGPoint! = CGPoint(x: (17 * 127), y: 0)
    
    let layer_step19: CAShapeLayer
    private var step19_Point: CGPoint! = CGPoint(x: (18 * 127), y: 0)
    
    let layer_step20: CAShapeLayer
    private var step20_Point: CGPoint! = CGPoint(x: (19 * 127), y: 0)
    
    let layer_step21: CAShapeLayer
    private var step21_Point: CGPoint! = CGPoint(x: (20 * 127), y: 0)
    
    let layer_step22: CAShapeLayer
    private var step22_Point: CGPoint! = CGPoint(x: (21 * 127), y: 0)
    
    let layer_step23: CAShapeLayer
    private var step23_Point: CGPoint! = CGPoint(x: (22 * 127), y: 0)
    
    let layer_step24: CAShapeLayer
    private var step24_Point: CGPoint! = CGPoint(x: (23 * 127), y: 0)
    
    let layer_step25: CAShapeLayer
    private var step25_Point: CGPoint! = CGPoint(x: (24 * 127), y: 0)
    
    let layer_step26: CAShapeLayer
    private var step26_Point: CGPoint! = CGPoint(x: (25 * 127), y: 0)
    
    let layer_step27: CAShapeLayer
    private var step27_Point: CGPoint! = CGPoint(x: (26 * 127), y: 0)
    
    let layer_step28: CAShapeLayer
    private var step28_Point: CGPoint! = CGPoint(x: (27 * 127), y: 0)
    
    let layer_step29: CAShapeLayer
    private var step29_Point: CGPoint! = CGPoint(x: (28 * 127), y: 0)
    
    let layer_step30: CAShapeLayer
    private var step30_Point: CGPoint! = CGPoint(x: (29 * 127), y: 0)
    
    let layer_step31: CAShapeLayer
    private var step31_Point: CGPoint! = CGPoint(x: (30 * 127), y: 0)
    
    let layer_step32: CAShapeLayer
    private var step32_Point: CGPoint! = CGPoint(x: (31 * 127), y: 0)
    var TouchedStep: Int = 0
    
    var fullPattern: [ArpStep] = []
    var pattern: [ArpStep] = []
    
    
    // required for IBDesignable class to properly render
    required public init?(coder aDecoder: NSCoder) {
        
        
        layer_step01 = CAShapeLayer()
        layer_step02 = CAShapeLayer()
        layer_step03 = CAShapeLayer()
        layer_step04 = CAShapeLayer()
        layer_step05 = CAShapeLayer()
        layer_step06 = CAShapeLayer()
        layer_step07 = CAShapeLayer()
        layer_step08 = CAShapeLayer()
        layer_step09 = CAShapeLayer()
        layer_step10 = CAShapeLayer()
        layer_step11 = CAShapeLayer()
        layer_step12 = CAShapeLayer()
        layer_step13 = CAShapeLayer()
        layer_step14 = CAShapeLayer()
        layer_step15 = CAShapeLayer()
        layer_step16 = CAShapeLayer()
        layer_step17 = CAShapeLayer()
        layer_step18 = CAShapeLayer()
        layer_step19 = CAShapeLayer()
        layer_step20 = CAShapeLayer()
        layer_step21 = CAShapeLayer()
        layer_step22 = CAShapeLayer()
        layer_step23 = CAShapeLayer()
        layer_step24 = CAShapeLayer()
        layer_step25 = CAShapeLayer()
        layer_step26 = CAShapeLayer()
        layer_step27 = CAShapeLayer()
        layer_step28 = CAShapeLayer()
        layer_step29 = CAShapeLayer()
        layer_step30 = CAShapeLayer()
        layer_step31 = CAShapeLayer()
        layer_step32 = CAShapeLayer()
        
        super.init(coder: aDecoder)
        initialize()
    }
    
    // required for IBDesignable class to properly render
    required override public init(frame: CGRect) {
        
        
        layer_step01 = CAShapeLayer()
        layer_step02 = CAShapeLayer()
        layer_step03 = CAShapeLayer()
        layer_step04 = CAShapeLayer()
        layer_step05 = CAShapeLayer()
        layer_step06 = CAShapeLayer()
        layer_step07 = CAShapeLayer()
        layer_step08 = CAShapeLayer()
        layer_step09 = CAShapeLayer()
        layer_step10 = CAShapeLayer()
        layer_step11 = CAShapeLayer()
        layer_step12 = CAShapeLayer()
        layer_step13 = CAShapeLayer()
        layer_step14 = CAShapeLayer()
        layer_step15 = CAShapeLayer()
        layer_step16 = CAShapeLayer()
        layer_step17 = CAShapeLayer()
        layer_step18 = CAShapeLayer()
        layer_step19 = CAShapeLayer()
        layer_step20 = CAShapeLayer()
        layer_step21 = CAShapeLayer()
        layer_step22 = CAShapeLayer()
        layer_step23 = CAShapeLayer()
        layer_step24 = CAShapeLayer()
        layer_step25 = CAShapeLayer()
        layer_step26 = CAShapeLayer()
        layer_step27 = CAShapeLayer()
        layer_step28 = CAShapeLayer()
        layer_step29 = CAShapeLayer()
        layer_step30 = CAShapeLayer()
        layer_step31 = CAShapeLayer()
        layer_step32 = CAShapeLayer()
        
        super.init(frame: frame)
        initialize()
    }
    
    
    fileprivate func initialize() {
        print("> hello arp patternbuilder!")
        
        self.contentScaleFactor = 2
      rebuildPattern()
    }
    
    func update(){
        self.rebuildPattern()
    }
    
    func clearLayers(){
        layer_step01.removeFromSuperlayer()
        layer_step02.removeFromSuperlayer()
        layer_step03.removeFromSuperlayer()
        layer_step04.removeFromSuperlayer()
        layer_step05.removeFromSuperlayer()
        layer_step06.removeFromSuperlayer()
        layer_step07.removeFromSuperlayer()
        layer_step08.removeFromSuperlayer()
        layer_step09.removeFromSuperlayer()
        layer_step10.removeFromSuperlayer()
        layer_step11.removeFromSuperlayer()
        layer_step12.removeFromSuperlayer()
        layer_step13.removeFromSuperlayer()
        layer_step14.removeFromSuperlayer()
        layer_step15.removeFromSuperlayer()
        layer_step16.removeFromSuperlayer()
        layer_step17.removeFromSuperlayer()
        layer_step18.removeFromSuperlayer()
        layer_step19.removeFromSuperlayer()
        layer_step20.removeFromSuperlayer()
        layer_step21.removeFromSuperlayer()
        layer_step22.removeFromSuperlayer()
        layer_step23.removeFromSuperlayer()
        layer_step24.removeFromSuperlayer()
        layer_step25.removeFromSuperlayer()
        layer_step26.removeFromSuperlayer()
        layer_step27.removeFromSuperlayer()
        layer_step28.removeFromSuperlayer()
        layer_step29.removeFromSuperlayer()
        layer_step30.removeFromSuperlayer()
        layer_step31.removeFromSuperlayer()
        layer_step32.removeFromSuperlayer()
    }
    
    
    
    func rebuildPattern (){
        self.clearLayers()
        
        fullPattern = []
        pattern = []
        
        //self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        
        // eerst array maken met de steps die wel aan staan
        let State: Patch = patchCtrl.CurrentPatch
        
       // fullPattern.append(ArpStep(start: 0, size: 1, noteLength: 0, noteVelocity: 0, x:0, y:0, width:0, height:0))  // dummy, to make index match to start nr's
        print("PatternLength = ",State.arp_patternLength)
        if( State.arp_step_1 == 1 && State.arp_patternLength >= 0 ){fullPattern.append(ArpStep(start: 1, size: 1, noteLength: State.arp_stepLength_1, noteVelocity: State.arp_stepVelocity_1, x:0, y:0, width:0, height:0))}
        if( State.arp_step_2 == 1 && State.arp_patternLength >= 1  ){fullPattern.append(ArpStep(start: 2, size: 1, noteLength: State.arp_stepLength_2, noteVelocity: State.arp_stepVelocity_2, x:0, y:0, width:0, height:0))}
        if( State.arp_step_3 == 1 && State.arp_patternLength >= 2  ){fullPattern.append(ArpStep(start: 3, size: 1, noteLength: State.arp_stepLength_3, noteVelocity: State.arp_stepVelocity_3, x:0, y:0, width:0, height:0))}
        if( State.arp_step_4 == 1 && State.arp_patternLength >= 3  ){fullPattern.append(ArpStep(start: 4, size: 1, noteLength: State.arp_stepLength_4, noteVelocity: State.arp_stepVelocity_4, x:0, y:0, width:0, height:0))}
        if( State.arp_step_5 == 1 && State.arp_patternLength >= 4  ){fullPattern.append(ArpStep(start: 5, size: 1, noteLength: State.arp_stepLength_5, noteVelocity: State.arp_stepVelocity_5, x:0, y:0, width:0, height:0))}
        if( State.arp_step_6 == 1 && State.arp_patternLength >= 5  ){fullPattern.append(ArpStep(start: 6, size: 1, noteLength: State.arp_stepLength_6, noteVelocity: State.arp_stepVelocity_6, x:0, y:0, width:0, height:0))}
        if( State.arp_step_7 == 1 && State.arp_patternLength >= 6  ){fullPattern.append(ArpStep(start: 7, size: 1, noteLength: State.arp_stepLength_7, noteVelocity: State.arp_stepVelocity_7, x:0, y:0, width:0, height:0))}
        if( State.arp_step_8 == 1 && State.arp_patternLength >= 7  ){fullPattern.append(ArpStep(start: 8, size: 1, noteLength: State.arp_stepLength_8, noteVelocity: State.arp_stepVelocity_8, x:0, y:0, width:0, height:0))}
        if( State.arp_step_9 == 1 && State.arp_patternLength >= 8  ){fullPattern.append(ArpStep(start: 9, size: 1, noteLength: State.arp_stepLength_9, noteVelocity: State.arp_stepVelocity_9, x:0, y:0, width:0, height:0))}
        if( State.arp_step_10 == 1 && State.arp_patternLength >= 9  ){fullPattern.append(ArpStep(start: 10, size: 1, noteLength: State.arp_stepLength_10, noteVelocity: State.arp_stepVelocity_10, x:0, y:0, width:0, height:0))}
        if( State.arp_step_11 == 1 && State.arp_patternLength >= 10  ){fullPattern.append(ArpStep(start: 11, size: 1, noteLength: State.arp_stepLength_11, noteVelocity: State.arp_stepVelocity_11, x:0, y:0, width:0, height:0))}
        if( State.arp_step_12 == 1 && State.arp_patternLength >= 11  ){fullPattern.append(ArpStep(start: 12, size: 1, noteLength: State.arp_stepLength_12, noteVelocity: State.arp_stepVelocity_12, x:0, y:0, width:0, height:0))}
        if( State.arp_step_13 == 1 && State.arp_patternLength >= 12  ){fullPattern.append(ArpStep(start: 13, size: 1, noteLength: State.arp_stepLength_13, noteVelocity: State.arp_stepVelocity_13, x:0, y:0, width:0, height:0))}
        if( State.arp_step_14 == 1 && State.arp_patternLength >= 13  ){fullPattern.append(ArpStep(start: 14, size: 1, noteLength: State.arp_stepLength_14, noteVelocity: State.arp_stepVelocity_14, x:0, y:0, width:0, height:0))}
        if( State.arp_step_15 == 1 && State.arp_patternLength >= 14  ){fullPattern.append(ArpStep(start: 15, size: 1, noteLength: State.arp_stepLength_15, noteVelocity: State.arp_stepVelocity_15, x:0, y:0, width:0, height:0))}
        if( State.arp_step_16 == 1 && State.arp_patternLength >= 15  ){fullPattern.append(ArpStep(start: 16, size: 1, noteLength: State.arp_stepLength_16, noteVelocity: State.arp_stepVelocity_16, x:0, y:0, width:0, height:0))}
        if( State.arp_step_17 == 1 && State.arp_patternLength >= 16  ){fullPattern.append(ArpStep(start: 17, size: 1, noteLength: State.arp_stepLength_17, noteVelocity: State.arp_stepVelocity_17, x:0, y:0, width:0, height:0))}
        if( State.arp_step_18 == 1 && State.arp_patternLength >= 17  ){fullPattern.append(ArpStep(start: 18, size: 1, noteLength: State.arp_stepLength_18, noteVelocity: State.arp_stepVelocity_18, x:0, y:0, width:0, height:0))}
        if( State.arp_step_19 == 1 && State.arp_patternLength >= 18  ){fullPattern.append(ArpStep(start: 19, size: 1, noteLength: State.arp_stepLength_19, noteVelocity: State.arp_stepVelocity_19, x:0, y:0, width:0, height:0))}
        if( State.arp_step_20 == 1 && State.arp_patternLength >= 19  ){fullPattern.append(ArpStep(start: 20, size: 1, noteLength: State.arp_stepLength_20, noteVelocity: State.arp_stepVelocity_20, x:0, y:0, width:0, height:0))}
        if( State.arp_step_21 == 1 && State.arp_patternLength >= 20  ){fullPattern.append(ArpStep(start: 21, size: 1, noteLength: State.arp_stepLength_21, noteVelocity: State.arp_stepVelocity_21, x:0, y:0, width:0, height:0))}
        if( State.arp_step_22 == 1 && State.arp_patternLength >= 21  ){fullPattern.append(ArpStep(start: 22, size: 1, noteLength: State.arp_stepLength_22, noteVelocity: State.arp_stepVelocity_22, x:0, y:0, width:0, height:0))}
        if( State.arp_step_23 == 1 && State.arp_patternLength >= 22  ){fullPattern.append(ArpStep(start: 23, size: 1, noteLength: State.arp_stepLength_23, noteVelocity: State.arp_stepVelocity_23, x:0, y:0, width:0, height:0))}
        if( State.arp_step_24 == 1  && State.arp_patternLength >= 23 ){fullPattern.append(ArpStep(start: 24, size: 1, noteLength: State.arp_stepLength_24, noteVelocity: State.arp_stepVelocity_24, x:0, y:0, width:0, height:0))}
        if( State.arp_step_25 == 1 && State.arp_patternLength >= 24  ){fullPattern.append(ArpStep(start: 25, size: 1, noteLength: State.arp_stepLength_25, noteVelocity: State.arp_stepVelocity_25, x:0, y:0, width:0, height:0))}
        if( State.arp_step_26 == 1 && State.arp_patternLength >= 25 ){fullPattern.append(ArpStep(start: 26, size: 1, noteLength: State.arp_stepLength_26, noteVelocity: State.arp_stepVelocity_26, x:0, y:0, width:0, height:0))}
        if( State.arp_step_27 == 1  && State.arp_patternLength >= 26 ){fullPattern.append(ArpStep(start: 27, size: 1, noteLength: State.arp_stepLength_27, noteVelocity: State.arp_stepVelocity_27, x:0, y:0, width:0, height:0))}
        if( State.arp_step_28 == 1  && State.arp_patternLength >= 27 ){fullPattern.append(ArpStep(start: 28, size: 1, noteLength: State.arp_stepLength_28, noteVelocity: State.arp_stepVelocity_28, x:0, y:0, width:0, height:0))}
        if( State.arp_step_29 == 1 && State.arp_patternLength >= 28  ){fullPattern.append(ArpStep(start: 29, size: 1, noteLength: State.arp_stepLength_29, noteVelocity: State.arp_stepVelocity_29, x:0, y:0, width:0, height:0))}
        if( State.arp_step_30 == 1 && State.arp_patternLength >= 29  ){fullPattern.append(ArpStep(start: 30, size: 1, noteLength: State.arp_stepLength_30, noteVelocity: State.arp_stepVelocity_30, x:0, y:0, width:0, height:0))}
        if( State.arp_step_31 == 1 && State.arp_patternLength >= 30  ){fullPattern.append(ArpStep(start: 31, size: 1, noteLength: State.arp_stepLength_31, noteVelocity: State.arp_stepVelocity_31, x:0, y:0, width:0, height:0))}
        if( State.arp_step_32 == 1 && State.arp_patternLength >= 31  ){fullPattern.append(ArpStep(start: 32, size: 1, noteLength: State.arp_stepLength_32, noteVelocity: State.arp_stepVelocity_32, x:0, y:0, width:0, height:0))}
        
       print("Aantal steps ",fullPattern.count)
        print(fullPattern)
        // calculate step length
        var i:Int = 0
        
        for step in fullPattern {
            // maak een array met alleen de steps die aan staan
            var length:Int = 1
            if ( (i + 1) <= (fullPattern.count - 1) ){
                length = ( fullPattern[ (i) + 1].start - step.start)
            } else {
                length = ( (patchCtrl.CurrentPatch.arp_patternLength + 2) - step.start)
            }

            pattern.append(ArpStep(start: step.start,
                                   size: length,
                                   noteLength: step.noteLength,
                                   noteVelocity: step.noteVelocity,
                                   x: Int (( Double(step.start) * Double(self.StepSize!) ) - Double(self.StepSize!) ),
                                   y: 130,
                                   width: Int(  (Double(step.noteLength) / Double(127)) * (Double(length) * Double(self.StepSize!)) ),
                                   height: Int(-1 * step.noteVelocity)
            ))
            i = i + 1
        }
        
       print("Aantal actieve steps ",pattern.count)
        print(pattern)
        for step in pattern {
            // loop door de steps die aan staan om deze te tekenen

            switch(step.start){
            case 1:
                self.initStep01(step: step)
            case 2:
                self.initStep02(step: step)
            case 3:
                self.initStep03(step: step)
            case 4:
                self.initStep04(step: step)
            case 5:
                self.initStep05(step: step)
            case 6:
                self.initStep06(step: step)
            case 7:
                self.initStep07(step: step)
            case 8:
                self.initStep08(step: step)
            case 9:
                self.initStep09(step: step)
            case 10:
                self.initStep10(step: step)
            case 11:
                self.initStep11(step: step)
            case 12:
                self.initStep12(step: step)
            case 13:
                self.initStep13(step: step)
            case 14:
                self.initStep14(step: step)
            case 15:
                self.initStep15(step: step)
            case 16:
                self.initStep16(step: step)
            case 17:
                self.initStep17(step: step)
            case 18:
                self.initStep18(step: step)
            case 19:
                self.initStep19(step: step)
                
            case 20:
                self.initStep20(step: step)
            case 21:
                self.initStep21(step: step)
            case 22:
                self.initStep22(step: step)
            case 23:
                self.initStep23(step: step)
            case 24:
                self.initStep24(step: step)
            case 25:
                self.initStep25(step: step)
            case 26:
                self.initStep26(step: step)
            case 27:
                self.initStep27(step: step)
            case 28:
                self.initStep28(step: step)
            case 29:
                self.initStep29(step: step)
            case 30:
                self.initStep30(step: step)
            case 31:
                self.initStep31(step: step)
            case 32:
                self.initStep32(step: step)
            default:
                break
            }
            
        }
    }
    
    func reDrawPattern(){
        for step in pattern {
            // loop door de steps die aan staan om deze te tekenen
            
            switch(step.start){
            case 1:
                self.drawStep01(step: step)
            case 2:
                self.drawStep02(step: step)
            case 3:
                self.drawStep03(step: step)
            case 4:
                self.drawStep04(step: step)
            case 5:
                self.drawStep05(step: step)
            case 6:
                self.drawStep06(step: step)
            case 7:
                self.drawStep07(step: step)
            case 8:
                self.drawStep08(step: step)
            case 9:
                self.drawStep09(step: step)
            case 10:
                self.drawStep10(step: step)
            case 11:
                self.drawStep11(step: step)
            case 12:
                self.drawStep12(step: step)
            case 13:
                self.drawStep13(step: step)
            case 14:
                self.drawStep14(step: step)
            case 15:
                self.drawStep15(step: step)
            case 16:
                self.drawStep16(step: step)
            case 17:
                self.drawStep17(step: step)
            case 18:
                self.drawStep18(step: step)
            case 19:
                self.drawStep19(step: step)
            case 20:
                self.drawStep20(step: step)
            case 21:
                self.drawStep21(step: step)
            case 22:
                self.drawStep22(step: step)
            case 23:
                self.drawStep23(step: step)
            case 24:
                self.drawStep24(step: step)
            case 25:
                self.drawStep25(step: step)
            case 26:
                self.drawStep26(step: step)
            case 27:
                self.drawStep27(step: step)
            case 28:
                self.drawStep28(step: step)
            case 29:
                self.drawStep29(step: step)
            case 30:
                self.drawStep30(step: step)
            case 31:
                self.drawStep31(step: step)
            case 32:
                self.drawStep32(step: step)
            default:
                break
            }
    
        }
    }
    
    func initStep01(step: ArpStep){
        self.drawStep01(step: step)
    }
    
    func initStep02(step: ArpStep){
        self.drawStep02(step: step)
    }
    
    func initStep03(step: ArpStep){
        self.drawStep03(step: step)
    }
    
    func initStep04(step: ArpStep){
        self.drawStep04(step: step)
    }
    func initStep05(step: ArpStep){
        self.drawStep05(step: step)
    }
    func initStep06(step: ArpStep){
        self.drawStep06(step: step)
    }
    func initStep07(step: ArpStep){
        self.drawStep07(step: step)
    }
    func initStep08(step: ArpStep){
        self.drawStep08(step: step)
    }
    func initStep09(step: ArpStep){
        self.drawStep09(step: step)
    }
    func initStep10(step: ArpStep){
        self.drawStep10(step: step)
    }
    func initStep11(step: ArpStep){
        self.drawStep11(step: step)
    }
    func initStep12(step: ArpStep){
        self.drawStep12(step: step)
    }
    func initStep13(step: ArpStep){
        self.drawStep13(step: step)
    }
    func initStep14(step: ArpStep){

        self.drawStep14(step: step)
    }
    func initStep15(step: ArpStep){
     
        self.drawStep15(step: step)
    }
    func initStep16(step: ArpStep){
       
        self.drawStep16(step: step)
    }
    func initStep17(step: ArpStep){
        
        self.drawStep17(step: step)
    }
    func initStep18(step: ArpStep){
        
        self.drawStep18(step: step)
    }
    func initStep19(step: ArpStep){
        
        self.drawStep19(step: step)
    }
    func initStep20(step: ArpStep){
        self.drawStep20(step: step)
    }
    func initStep21(step: ArpStep){
        self.drawStep21(step: step)
    }
    func initStep22(step: ArpStep){
        self.drawStep22(step: step)
    }
    func initStep23(step: ArpStep){
        self.drawStep23(step: step)
    }
    func initStep24(step: ArpStep){
        
        self.drawStep24(step: step)
    }
    func initStep25(step: ArpStep){
        
        self.drawStep25(step: step)
    }
    func initStep26(step: ArpStep){
        
        self.drawStep26(step: step)
    }
    func initStep27(step: ArpStep){
        
        self.drawStep27(step: step)
    }
    func initStep28(step: ArpStep){
        
        self.drawStep28(step: step)
    }
    func initStep29(step: ArpStep){
        
        self.drawStep29(step: step)
    }
    func initStep30(step: ArpStep){
        self.drawStep30(step: step)
    }
    func initStep31(step: ArpStep){
        self.drawStep31(step: step)
    }
    func initStep32(step: ArpStep){
        self.drawStep32(step: step)
    }
    
    
    
    func drawStep01(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step01.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        //layer_step01.fillColor = UIColor.green.cgColor
        layer_step01.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor

        self.layer.addSublayer(layer_step01)
    }
    
    func drawStep02(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step02.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step02.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step02)
    }
    
    func drawStep03(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step03.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step03.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step03)
    }
    
    func drawStep04(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step04.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step04.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step04)
    }
    
    func drawStep05(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step05.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step05.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step05)
    }
    func drawStep06(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step06.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step06.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step06)
    }
    func drawStep07(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step07.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step07.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step07)
    }
    func drawStep08(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step08.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step08.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step08)
    }
    func drawStep09(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step09.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step09.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step09)
    }
    func drawStep10(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step10.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step10.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step10)
    }
    func drawStep11(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step11.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step11.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step11)
    }
    func drawStep12(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step12.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step12.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step12)
    }
    func drawStep13(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step13.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step13.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step13)
    }
    func drawStep14(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step14.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step14.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step14)
    }
    func drawStep15(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step15.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step15.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step15)
    }
    func drawStep16(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step16.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step16.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step16)
    }
    
    func drawStep17(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step17.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step17.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step17)
    }
    
    func drawStep18(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step18.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step18.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step18)
    }
    
    func drawStep19(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step19.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step19.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step19)
    }
    
    func drawStep20(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step20.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step20.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step20)
    }
    
    func drawStep21(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step21.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step21.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step21)
    }
    
    func drawStep22(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step22.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step22.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step22)
    }
    
    func drawStep23(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step23.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step23.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step23)
    }
    
    func drawStep24(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step24.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step24.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step24)
    }
    
    func drawStep25(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step25.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step25.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step25)
    }
    
    func drawStep26(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step26.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step26.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step26)
    }
    
    func drawStep27(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step27.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step27.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step27)
    }
    
    func drawStep28(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step28.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step28.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step28)
    }
    
    func drawStep29(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step29.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step29.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step29)
    }
    
    func drawStep30(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step30.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step30.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step30)
    }
    
    func drawStep31(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step31.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step31.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step31)
    }
    
    func drawStep32(step: ArpStep){
        print(step)
        // teken het vlak
        layer_step32.path = UIBezierPath(rect: CGRect(x: step.x, y: step.y, width: step.width, height: step.height)).cgPath
        layer_step32.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.layer.addSublayer(layer_step32)
    }
    
    

    
    
    func CalculateVelocity(old: Int, change: Int) -> Int {
        
        var new:Int = 0
        
        new = old - change
        
        if ( new < 0 ){
            return 0
        } else if ( new > 127) {
            return 127
        } else {
            return new
        }
        
    }
    
   
}

