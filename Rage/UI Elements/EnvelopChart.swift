//
//  EnvelopChart.swift
//  Rage
//
//  Created by M. Lierop on 12-12-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

public class EnvelopeChart: UIView
{
    
    var attackTime: Int? = 64
    private let attackLvl: Int? = 127
    private var attackPoint: CGPoint! = CGPoint(x: 0, y: 0)
    private var attackHandle: UIView!
    
    var decayTime: Int? = 127
    private var decayLvl: Int? = 127
    private var decayPoint: CGPoint! = CGPoint(x: 127, y: 0)
    private var decayHandle: UIView!
    
    private var sustainTime: Int? = 60
    var sustainLvl: Int? = 127
    private var sustainPoint: CGPoint! = CGPoint(x: 300, y: 0)
    private var sustainHandle: UIView!
    
    private let sustainSlopeTime: Int? = 127
    var sustainSlopeLvl: Int? = 0
    private var sustainSlopePoint: CGPoint! = CGPoint(x: 350, y: 0)
    private var sustainSlopeHandle: UIView!
    
    var releaseTime: Int? = 60
    private let releaseLvl: Int? = 0
    private var releasePoint: CGPoint! = CGPoint(x: 478, y: 127)
    private var releaseHandle: UIView!
    
    private var line = CAShapeLayer()
    var envelopePrefix = ""
    
    // required for IBDesignable class to properly render
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    // required for IBDesignable class to properly render
    required override public init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    fileprivate func initialize() {
        self.attackHandle = DrawHandlePnt(point: attackPoint, id: "attack")
        self.decayHandle = DrawHandlePnt(point: decayPoint, id: "decay")
        self.sustainHandle = DrawHandlePnt(point: sustainPoint, id: "sustain")
        self.sustainSlopeHandle = DrawHandlePnt(point: sustainSlopePoint, id: "sustainSlope")
        self.releaseHandle = DrawHandlePnt(point: releasePoint, id: "release")
        
        //self.autoresizingMask = .init(arrayLiteral: .flexibleHeight, .flexibleWidth)

        self.contentScaleFactor = 2
        
        let imageName = "EnvelopeGrid"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
       

        imageView.frame = CGRect(x: 20, y: 10, width: 455, height: 137)
        self.addSubview(imageView)
        
        updateChart()
    }
    
    fileprivate func updateChart() {
        redraw()
    }
    
    public func setEnvelopeValues(attack: Int, decay: Int, sustain: Int, sustainSlope: Int,release: Int){
        self.setAttack(value: attack)
        self.setDecay(value: decay)
        self.setSustain(value: sustain)
        self.setSustainSlope(value: sustainSlope)
        self.setRelease(value: release)
    }
    
    public func setAttack (value: Int){
        if (value <= 0 ){
            self.attackTime = 0
        } else if (value >= 127) {
            self.attackTime = 127
        } else {
            self.attackTime = value
        }
        self.drawAttack()
        self.drawDecay()
    }
    
    private func drawAttack(){
        self.attackPoint = CGPoint(x: (self.attackTime! + 20), y: (0 + 20))
        self.attackHandle.frame.origin.x = CGFloat( self.attackPoint.x - 20)
        self.attackHandle.frame.origin.y = CGFloat( self.attackPoint.y - 20)
        self.drawEnvelope()
    }
    
    public func setDecay (value: Int){
        if (value <= 0 ){
            self.decayTime = 0
        } else if (value >= 127) {
            self.decayTime = 127
        } else {
            self.decayTime = value
        }
        self.drawDecay()
        self.drawSustain()
    }
    
    private func drawDecay(){
        self.decayPoint = CGPoint(x: (self.attackTime! + self.decayTime! + 20), y: (127 - self.sustainLvl! + 20) )
        self.decayHandle.frame.origin.x = CGFloat( self.decayPoint.x - 20)
        self.decayHandle.frame.origin.y = CGFloat( self.decayPoint.y - 20)
        self.drawEnvelope()
    }
    
    public func setSustain (value: Int){
        if (value <= 0 ){
            self.sustainLvl = 0
        } else if (value >= 127) {
            self.sustainLvl = 127
        } else {
            self.sustainLvl = value
        }
        self.drawSustain()
        self.drawDecay()
    }
    
    private func drawSustain(){
        self.sustainPoint = CGPoint(x: (276 + 20), y: (127 - self.sustainLvl! + 20) )
        self.sustainHandle.frame.origin.x = CGFloat(self.sustainPoint.x - 20)
        self.sustainHandle.frame.origin.y = CGFloat(self.sustainPoint.y - 20)
        self.drawEnvelope()
    }
    
    public func setSustainSlope (value: Int){
        
        if (value <= 0 ){
            self.sustainSlopeLvl = 0
        } else if (value >= 127) {
            self.sustainSlopeLvl = 127
        } else {
            self.sustainSlopeLvl = value
        }
        self.drawSustainSlope()
    }
    
    private func drawSustainSlope(){
        
        var relativeValue: Int = self.sustainLvl! + ((64 - self.sustainSlopeLvl!) / 2)
        
        if (relativeValue <= 0) {
            relativeValue = 0
        } else if (relativeValue >= 127) {
            relativeValue = 127
        }
        self.sustainSlopePoint = CGPoint(x: (326 + 20), y: (127 - relativeValue + 20) )
        self.sustainSlopeHandle.frame.origin.x = CGFloat(self.sustainSlopePoint.x - 20)
        self.sustainSlopeHandle.frame.origin.y = CGFloat(self.sustainSlopePoint.y - 20)
        self.drawEnvelope()
    }
    
    public func setRelease (value: Int){
        if (value <= 0 ){
            self.releaseTime = 0
        } else if (value >= 127) {
            self.releaseTime = 127
        } else {
            self.releaseTime = value
        }
        self.drawRelease()
    }
    
    private func drawRelease(){
        self.releasePoint = CGPoint(x: (326 + self.releaseTime! + 20), y: (127 + 20) )
        self.releaseHandle.frame.origin.x = CGFloat(self.releasePoint.x - 20)
        self.releaseHandle.frame.origin.y = CGFloat(self.releasePoint.y - 20)
        self.drawEnvelope()
    }
    
    private func redraw (){
        drawEnvelope()
        drawGripPnts()
    }
    
    private func drawEnvelope(){
        let linePath = UIBezierPath()
        //linePath.fill()
        linePath.move(to: CGPoint(x: 20, y: 147))
        linePath.addLine(to: self.attackPoint)
        linePath.addCurve(to: self.decayPoint,
                          controlPoint1: CGPoint(x: (self.decayPoint.x - ((self.decayPoint.x - self.attackPoint.x ) / 2)) ,y: self.decayPoint.y),
                          controlPoint2: self.decayPoint)
        linePath.addLine(to: self.sustainPoint)
        linePath.addLine(to: self.sustainSlopePoint)
        linePath.addCurve(to: self.releasePoint, controlPoint1:  CGPoint(x: self.sustainSlopePoint.x, y: self.releasePoint.y), controlPoint2: self.releasePoint)
        
        self.line.path = linePath.cgPath
        self.line.strokeColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 1).cgColor
        
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0.5, y: 1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0)
        gradient.frame = CGRect(x: 0, y: 0, width: 540, height: 153)
        gradient.colors = [
            UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 0.4).cgColor,
            UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.9).cgColor
        ]
        gradient.locations = [
            0.0,
            0.5
        ]
        
        self.line.fillColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 0.4).cgColor
        self.line.mask = gradient
        self.line.lineWidth = 1
        self.line.lineJoin = CAShapeLayerLineJoin.round
        
        self.layer.addSublayer(self.line)
        bringHandlesToFront()
    }
    
    private func drawGripPnts(){
        
        self.addSubview(attackHandle)
        
        let attackGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeAttack(_:)))
        attackHandle.isUserInteractionEnabled = true
        attackHandle.addGestureRecognizer(attackGesture)
        
        self.addSubview(decayHandle)
        let decayGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeDecay(_:)))
        decayHandle.isUserInteractionEnabled = true
        decayHandle.addGestureRecognizer(decayGesture)
        
        self.addSubview(sustainHandle)
        let sustainLevelGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeSustainLevel(_:)))
        sustainHandle.isUserInteractionEnabled = true
        sustainHandle.addGestureRecognizer(sustainLevelGesture)
        
        self.addSubview(sustainSlopeHandle)
        let sustainSlopeGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeSustainSlope(_:)))
        sustainSlopeHandle.isUserInteractionEnabled = true
        sustainSlopeHandle.addGestureRecognizer(sustainSlopeGesture)
        
        self.addSubview(releaseHandle)
        let releaseGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onChangeRelease(_:)))
        releaseHandle.isUserInteractionEnabled = true
        releaseHandle.addGestureRecognizer(releaseGesture)
    }
    
    private func bringHandlesToFront(){
        self.bringSubviewToFront(attackHandle)
        self.bringSubviewToFront(decayHandle)
        self.bringSubviewToFront(sustainHandle)
    }
    
    
    
    @objc func onChangeAttack(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self.attackHandle)
        let time = self.attackTime! + Int(translationGlobal.x)

        self.setAttack(value: time)
        
        sender.setTranslation(CGPoint.zero, in: self)
        
        NotificationCenter.default.post(name: Notification.Name( self.envelopePrefix + "Envelope_Attack" ), object: nil)
    }
    
    @objc func onChangeDecay(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        let time = self.decayTime! + Int(translationGlobal.x)
        let level = self.sustainLvl! - Int(translationGlobal.y)
        
        self.setDecay(value: time)
        self.setSustain(value: level)
        self.drawSustainSlope()
        
        sender.setTranslation(CGPoint.zero, in: self)
        
        NotificationCenter.default.post(name: Notification.Name( self.envelopePrefix + "Envelope_Decay" ), object: nil)
        NotificationCenter.default.post(name: Notification.Name( self.envelopePrefix + "Envelope_Sustain" ), object: nil)
    }
    
    @objc func onChangeSustainLevel(_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        let level = self.sustainLvl! - Int(translationGlobal.y)
        
        self.setSustain(value: level)
        self.drawSustainSlope()
        sender.setTranslation(CGPoint.zero, in: self)
        
        NotificationCenter.default.post(name: Notification.Name( self.envelopePrefix + "Envelope_Sustain" ), object: nil)
    }
    
    @objc func onChangeSustainSlope (_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        let level = self.sustainSlopeLvl! + Int(translationGlobal.y)
        
        self.setSustainSlope(value: level)
        
        sender.setTranslation(CGPoint.zero, in: self)
        
        NotificationCenter.default.post(name: Notification.Name( self.envelopePrefix + "Envelope_SustainSlope" ), object: nil)
    }
    
    @objc func onChangeRelease (_ sender:UIPanGestureRecognizer){
        let translationGlobal = sender.translation(in: self)
        let time = self.releaseTime! + Int(translationGlobal.x)
        
        self.setRelease(value: time)
        
        sender.setTranslation(CGPoint.zero, in: self)
        
        NotificationCenter.default.post(name: Notification.Name( self.envelopePrefix + "Envelope_Release" ), object: nil)
    }
}


class DrawHandlePnt: UIView {
    var posX: Int
    var posY: Int
    var id: String
    
    init(point: CGPoint, id: String) {
        let frameRect = CGRect(x: (point.x - 20), y: (point.y ), width: 40, height: 40)
        
        self.posX = Int(point.x)
        self.posY = Int(point.y)
        self.id = id
        super.init(frame: frameRect)
        
        let promobox =  UIView()
        
        promobox.frame = CGRect(x: 15, y: 15, width: 10, height: 10)
        promobox.layer.borderColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 1).cgColor
        promobox.layer.borderWidth = 2.0
        promobox.layer.backgroundColor = UIColor(red: 1.00, green: 0.15, blue: 0.37, alpha: 1).cgColor
        
        self.addSubview(promobox)
        
        //let rect = CGRect(x: (point.x - 5), y: (point.y - 5), width: 10, height: 10)
        
        //self.frame = rect // inherited stuff from super class -> UIView
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0) // inherited stuff from super class -> UIView
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


@objc(KnobStyleKit2ResizingBehavior)
public enum ResizingBehavior: Int {
    case aspectFit /// The content is proportionally resized to fit into the target rectangle.
    case aspectFill /// The content is proportionally resized to completely fill the target rectangle.
    case stretch /// The content is stretched to match the entire target rectangle.
    case center /// The content is centered in the target rectangle, but it is NOT resized.
    public func apply(rect: CGRect, target: CGRect) -> CGRect {
        if rect == target || target == CGRect.zero {
            return rect
        }
        
        var scales = CGSize.zero
        scales.width = abs(target.width / rect.width)
        scales.height = abs(target.height / rect.height)
        
        switch self {
        case .aspectFit:
            scales.width = min(scales.width, scales.height)
            scales.height = scales.width
        case .aspectFill:
            scales.width = max(scales.width, scales.height)
            scales.height = scales.width
        case .stretch:
            break
        case .center:
            scales.width = 1
            scales.height = 1
        }
        
        var result = rect.standardized
        result.size.width *= scales.width
        result.size.height *= scales.height
        result.origin.x = target.minX + (target.width - result.width) / 2
        result.origin.y = target.minY + (target.height - result.height) / 2
        return result
    }
}

