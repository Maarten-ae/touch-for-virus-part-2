//
//  PopoverTableSelectors.swift
//  Rage
//
//  Created by M. Lierop on 19-01-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import UIKit

class LFODestinationPopover: UITableViewController {
    
    public var LFONr: Int = 0
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            switch (LFONr){
            case 1:
                selectedRow = patchCtrl.CurrentPatch.lfo1_UserDestination
            case 2:
                selectedRow = patchCtrl.CurrentPatch.lfo2_UserDestination
            case 3:
                selectedRow = patchCtrl.CurrentPatch.lfo3_UserDestination
            default:
                break
            }
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (LFONr){
        case 0:
            break
        case 1:
            patchCtrl.setLFO1UserDestination(value: indexPath.row)
        case 2:
            patchCtrl.setLFO2UserDestination(value: indexPath.row)
        case 3:
            patchCtrl.setLFO3UserDestination(value: indexPath.row)
        default:
            break
        }
        NotificationCenter.default.post(name: Notification.Name( "LFODestinationChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class LFODestinationSources: UIViewController, UITableViewDataSource {
    private var sources = LFODestinations
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = sources[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


class LFO3DestinationSources: UIViewController, UITableViewDataSource {
    private var sources = LFO3Destinations
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = sources[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}



class LFOWavePopover: UITableViewController {
    
    public var LFONr: Int = 0
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            switch (LFONr){
            case 1:
                selectedRow = patchCtrl.CurrentPatch.lfo1_WaveformShape
            case 2:
                selectedRow = patchCtrl.CurrentPatch.lfo2_WaveformShape
            case 3:
                selectedRow = patchCtrl.CurrentPatch.lfo3_WaveformShape
            default:
                break
            }
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (LFONr){
        case 0:
            break
        case 1:
            patchCtrl.setLFO1WaveformShape(value: (indexPath.row + 6))
        case 2:
            patchCtrl.setLFO2WaveformShape(value: (indexPath.row + 6))
        case 3:
            patchCtrl.setLFO3WaveformShape(value: (indexPath.row + 6))
        default:
            break
        }
        NotificationCenter.default.post(name: Notification.Name( "LFOWaveChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class LFOWaveSources: UIViewController, UITableViewDataSource {
    private var sources = ["Wave 3","Wave 4","Wave 5","Wave 6","Wave 7","Wave 8","Wave 9","Wave 10","Wave 11","Wave 12","Wave 13","Wave 14","Wave 15","Wave 16","Wave 17","Wave 18","Wave 19","Wave 20","Wave 21","Wave 22","Wave 23","Wave 24","Wave 25","Wave 26","Wave 27","Wave 28","Wave 29","Wave 30","Wave 31","Wave 32","Wave 33","Wave 34","Wave 35","Wave 36","Wave 37","Wave 38","Wave 39","Wave 40","Wave 41","Wave 42","Wave 43","Wave 44","Wave 45","Wave 46","Wave 47","Wave 48","Wave 49","Wave 50","Wave 51","Wave 52","Wave 53","Wave 54","Wave 55","Wave 56","Wave 57","Wave 58","Wave 59","Wave 60","Wave 61","Wave 62","Wave 63","Wave 64"]
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = sources[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


class MatrixSourcePopover: UITableViewController {
    
    // TablePopover for selecting a Matrix slot source

    public var matrixSlotNr: Int = 0
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            switch (matrixSlotNr){
            case 1:
                selectedRow = patchCtrl.CurrentPatch.matrixSlot1_Source
            case 2:
                selectedRow = patchCtrl.CurrentPatch.matrixSlot2_Source
            case 3:
                selectedRow = patchCtrl.CurrentPatch.matrixSlot3_Source
            case 4:
                selectedRow = patchCtrl.CurrentPatch.matrixSlot4_Source
            case 5:
                selectedRow = patchCtrl.CurrentPatch.matrixSlot5_Source
            case 6:
                selectedRow = patchCtrl.CurrentPatch.matrixSlot6_Source
            default:
                break
            }
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (matrixSlotNr){
        case 0:
            break
        case 1:
            patchCtrl.setMatrixSlot1Source(value: indexPath.row)
        case 2:
            patchCtrl.setMatrixSlot2Source(value: indexPath.row)
        case 3:
            patchCtrl.setMatrixSlot3Source(value: indexPath.row)
        case 4:
            patchCtrl.setMatrixSlot4Source(value: indexPath.row)
        case 5:
            patchCtrl.setMatrixSlot5Source(value: indexPath.row)
        case 6:
            patchCtrl.setMatrixSlot6Source(value: indexPath.row)
        default:
            break
        }
        NotificationCenter.default.post(name: Notification.Name( "MatrixSlotSourceChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class MatrixSources: UIViewController, UITableViewDataSource {
    private var sources = MatrixSourceLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = sources[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}



class MatrixDestinationsPopover: UITableViewController {
    
    // TablePopover for selecting a Matrix slot destination
    
    public var matrixSlotNr: Int = 0
    public var matrixSlotDestNr: Int = 0
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            switch (matrixSlotNr){
            case 1:
                
                switch (matrixSlotDestNr){
                case 1:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot1_Destination1
                case 2:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot1_Destination2
                case 3:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot1_Destination3
                default:
                    break
                }
                
            case 2:
                
                switch (matrixSlotDestNr){
                case 1:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot2_Destination1
                case 2:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot2_Destination2
                case 3:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot2_Destination3
                default:
                    break
                }
                
            case 3:
                
                switch (matrixSlotDestNr){
                case 1:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot3_Destination1
                case 2:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot3_Destination2
                case 3:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot3_Destination3
                default:
                    break
                }
                
            case 4:
                
                switch (matrixSlotDestNr){
                case 1:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot4_Destination1
                case 2:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot4_Destination2
                case 3:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot4_Destination3
                default:
                    break
                }
                
            case 5:
                
                switch (matrixSlotDestNr){
                case 1:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot5_Destination1
                case 2:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot5_Destination2
                case 3:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot5_Destination3
                default:
                    break
                }
                
            case 6:
                
                switch (matrixSlotDestNr){
                case 1:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot6_Destination1
                case 2:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot6_Destination2
                case 3:
                    selectedRow = patchCtrl.CurrentPatch.matrixSlot6_Destination3
                default:
                    break
                }
                
            default:
                break
            }
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch (matrixSlotNr){
        case 1:
            
            switch (matrixSlotDestNr){
            case 1:
                patchCtrl.setMatrixSlot1Destination1(value: indexPath.row)
            case 2:
                patchCtrl.setMatrixSlot1Destination2(value: indexPath.row)
            case 3:
                patchCtrl.setMatrixSlot1Destination3(value: indexPath.row)
            default:
                break
            }
            
        case 2:
            
            switch (matrixSlotDestNr){
            case 1:
                patchCtrl.setMatrixSlot2Destination1(value: indexPath.row)
            case 2:
                patchCtrl.setMatrixSlot2Destination2(value: indexPath.row)
            case 3:
                patchCtrl.setMatrixSlot2Destination3(value: indexPath.row)
            default:
                break
            }
            
        case 3:
            
            switch (matrixSlotDestNr){
            case 1:
                patchCtrl.setMatrixSlot3Destination1(value: indexPath.row)
            case 2:
                patchCtrl.setMatrixSlot3Destination2(value: indexPath.row)
            case 3:
                patchCtrl.setMatrixSlot3Destination3(value: indexPath.row)
            default:
                break
            }
            
        case 4:
            
            switch (matrixSlotDestNr){
            case 1:
                patchCtrl.setMatrixSlot4Destination1(value: indexPath.row)
            case 2:
                patchCtrl.setMatrixSlot4Destination2(value: indexPath.row)
            case 3:
                patchCtrl.setMatrixSlot4Destination3(value: indexPath.row)
            default:
                break
            }
            
        case 5:
            
            switch (matrixSlotDestNr){
            case 1:
                patchCtrl.setMatrixSlot5Destination1(value: indexPath.row)
            case 2:
                patchCtrl.setMatrixSlot5Destination2(value: indexPath.row)
            case 3:
                patchCtrl.setMatrixSlot5Destination3(value: indexPath.row)
            default:
                break
            }
            
        case 6:
            
            switch (matrixSlotDestNr){
            case 1:
                patchCtrl.setMatrixSlot6Destination1(value: indexPath.row)
            case 2:
                patchCtrl.setMatrixSlot6Destination2(value: indexPath.row)
            case 3:
                patchCtrl.setMatrixSlot6Destination3(value: indexPath.row)
            default:
                break
            }
            
        default:
            break
        }
        NotificationCenter.default.post(name: Notification.Name( "MatrixSlotDestinationChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class MatrixDestinations: NSObject, UITableViewDataSource {
    private var destinations = LFODestinations
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return destinations.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = destinations[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// ***********************************************************************************
// ***********************************************************************************

class SaturationPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            selectedRow = patchCtrl.CurrentPatch.filter_saturationType
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        patchCtrl.setFilterSaturationType(value: (indexPath.row))
        
        NotificationCenter.default.post(name: Notification.Name( "FilterSaturationChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class SaturationModes: UIViewController, UITableViewDataSource {
    private var sources = FilterSaturationModes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = sources[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// ***********************************************************************************
// ***********************************************************************************

class OscModelPopover: UITableViewController {
    
    public var OscNr = 0
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            switch(OscNr){
            case 1:
                selectedRow = patchCtrl.CurrentPatch.osc1_Model
            case 2:
                selectedRow = patchCtrl.CurrentPatch.osc2_Model
            default:
                selectedRow = 1
            }
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch(OscNr){
        case 1:
            patchCtrl.setOsc1Model(model: (indexPath.row))
        case 2:
            patchCtrl.setOsc2Model(model: (indexPath.row))
        default:
            break
        }
        
        NotificationCenter.default.post(name: Notification.Name( "OscModelChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class OscModelsSrc: UIViewController, UITableViewDataSource {
    private var sources = OscModelLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = sources[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// ***********************************************************************************
// ***********************************************************************************

class OscWavePopover: UITableViewController {
    
    public var waveType = ""
    public var OscNr = 0
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            switch(OscNr){
            case 1:
                
                switch( patchCtrl.CurrentPatch.osc1_Model){
                case 0:
                    // analog
                    selectedRow = patchCtrl.CurrentPatch.osc1_WaveformSelect
                case 1:
                    //hypersaw
                    selectedRow = 1
                default:
                    // other modes
                    selectedRow = patchCtrl.CurrentPatch.osc1_WaveformSelect
                }
                
            case 2:
                
                switch( patchCtrl.CurrentPatch.osc2_Model){
                case 0:
                    selectedRow = patchCtrl.CurrentPatch.osc2_WaveformSelect
                case 1:
                    selectedRow = 1
                default:
                    selectedRow = patchCtrl.CurrentPatch.osc2_WaveformSelect
                }
            default:
                selectedRow = 1
            }
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch(OscNr){
        case 1:
            patchCtrl.setOsc1WaveformSelect(value: indexPath.row)
            
        case 2:
            patchCtrl.setOsc2WaveformSelect(value: indexPath.row)
            
        default:
            selectedRow = 1
        }
        
        NotificationCenter.default.post(name: Notification.Name( "OscWaveSelectChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class Osc1WaveSrc: UIViewController, UITableViewDataSource {
    private var AnalogWaves = WaveLabels
    private var Waveforms = WavetableLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ( patchCtrl.CurrentPatch.osc1_Model == 0 ){
            return AnalogWaves.count
        } else {
            return Waveforms.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        
        if ( patchCtrl.CurrentPatch.osc1_Model == 0 ){
            cell.textLabel?.text = AnalogWaves[indexPath.row]
        } else {
            cell.textLabel?.text = Waveforms[indexPath.row]
        }

        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

class Osc2WaveSrc: UIViewController, UITableViewDataSource {
    private var AnalogWaves = WaveLabels
    private var Waveforms = WavetableLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ( patchCtrl.CurrentPatch.osc2_Model == 0 ){
            return AnalogWaves.count
        } else {
            return Waveforms.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        
        if ( patchCtrl.CurrentPatch.osc2_Model == 0 ){
            cell.textLabel?.text = AnalogWaves[indexPath.row]
        } else {
            cell.textLabel?.text = Waveforms[indexPath.row]
        }
        
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// ***********************************************************************************
// ***********************************************************************************

class Osc3ModePopover: UITableViewController {

    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            
            selectedRow = patchCtrl.CurrentPatch.osc3_Model
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setOsc3Model(model: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "Osc3ModeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class Osc3ModesSrc: UIViewController, UITableViewDataSource {
    private var osc3Modes = Oscillator3ModeLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return osc3Modes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = osc3Modes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

//
// ***********************************************************************************
// ***********************************************************************************

class UnisonModePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.osc_Unison_Mode
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setOsc_Unison_Mode(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "UnisonModeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class UnisonModesSrc: UIViewController, UITableViewDataSource {
    private var unisonModes = UnisonLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return unisonModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = unisonModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

//
// ***********************************************************************************
// ***********************************************************************************

class FMModePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.osc_FMMode
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setFMMode(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "FMModeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class FMModesSrc: UIViewController, UITableViewDataSource {
    private var FMModes = FMModeLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


// Soft Button Destionations
// ***********************************************************************************
// ***********************************************************************************

class SoftKnobDestinationsPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    public var SoftKnobNr: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            switch (SoftKnobNr){
            case 1:
                selectedRow = patchCtrl.CurrentPatch.control_Softknob1Destination
            case 2:
                selectedRow = patchCtrl.CurrentPatch.control_Softknob2Destination
            case 3:
                selectedRow = patchCtrl.CurrentPatch.control_Softknob3Destination
            default:
                print("wrong SoftKnob NR")
            }
            
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        switch (SoftKnobNr){
        case 1:
            patchCtrl.setSoftknob1Destination(value: indexPath.row)
        case 2:
            patchCtrl.setSoftknob2Destination(value: indexPath.row)
        case 3:
            patchCtrl.setSoftknob3Destination(value: indexPath.row)
        default:
            print("wrong SoftKnob NR")
        }
        
        NotificationCenter.default.post(name: Notification.Name( "SoftKnobChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class SoftKnobDestinationsSrc: UIViewController, UITableViewDataSource {
    private var FMModes = SoftKnobDestinations
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Soft Button Names
// ***********************************************************************************
// ***********************************************************************************

class SoftKnobNamesPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    public var SoftKnobNr: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            switch (SoftKnobNr){
            case 1:
                selectedRow = patchCtrl.CurrentPatch.control_Softknob1Name
            case 2:
                selectedRow = patchCtrl.CurrentPatch.control_Softknob2Name
            case 3:
                selectedRow = patchCtrl.CurrentPatch.control_Softknob3Name
            default:
                print("wrong SoftKnob NR")
            }
            
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        switch (SoftKnobNr){
        case 1:
            patchCtrl.setSoftknob1Name(value: indexPath.row)
        case 2:
            patchCtrl.setSoftknob2Name(value: indexPath.row)
        case 3:
            patchCtrl.setSoftknob3Name(value: indexPath.row)
        default:
            print("wrong SoftKnob NR")
        }
        
        NotificationCenter.default.post(name: Notification.Name( "SoftKnobChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class SoftKnobNamesSrc: UIViewController, UITableViewDataSource {
    private var FMModes = SoftKnobNames
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Arp mode
// ***********************************************************************************
// ***********************************************************************************

class ArpModePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.arp_mode
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setArpMode(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ArpSettingsChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ArpModesSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ArpModeLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


// Arp pattern length
// ***********************************************************************************
// ***********************************************************************************

class ArpPatternLengthPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.arp_patternLength
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setArpPatternLength(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ArpSettingsChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ArpPatternLengthSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ArpPAtternLengthLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


// Arp Pattern
// ***********************************************************************************
// ***********************************************************************************

class ArpPatternPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.arp_pattern
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setArpPattern(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ArpSettingsChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ArpPatternsSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ArpPatternLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Arp Oct
// ***********************************************************************************
// ***********************************************************************************

class ArpOctPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.arp_range
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setArpRange(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ArpSettingsChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}


class ArpOctSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ArpOctaveRangeLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Arp Resolution
// ***********************************************************************************
// ***********************************************************************************

class ArpResolutionPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.arp_clock
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setArpClock(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ArpSettingsChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ArpResolutionsSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ArpClockLabels
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}



// Character Type
// ***********************************************************************************
// ***********************************************************************************

class CharacterTypePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.character_Type
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setCharacterType(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "CharacterTypeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class CharacterTypeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = CharacterTypes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Reverb Modes
// ***********************************************************************************
// ***********************************************************************************

class ReverbModePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.reverb_Mode
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setReverbMode(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ReverbModeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ReverbModeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ReverbModes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


// Reverb Type
// ***********************************************************************************
// ***********************************************************************************

class ReverbTypePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.reverb_Type
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setReverbType(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ReverbModeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ReverbTypeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ReverbTypes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


// Reverb Clock
// ***********************************************************************************
// ***********************************************************************************

class ReverbClockPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.reverb_Clock
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setReverbClock(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ReverbClockChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ReverbClockSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ReverbClock
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Delay Type
// ***********************************************************************************
// ***********************************************************************************

class DelayTypePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.delay_Type
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setDelayType(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "DelayTypeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class DelayTypeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = DelayTypes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Delay Mode
// ***********************************************************************************
// ***********************************************************************************

class DelayModePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.delay_Mode
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setDelayMode(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "DelayModeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class DelayModeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = DelayModes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Delay Clock
// ***********************************************************************************
// ***********************************************************************************

class DelayClockPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.delay_Clock
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setDelayClock(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "DelayClockChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class DelayClockSrc: UIViewController, UITableViewDataSource {
    private var FMModes = DelayClock
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// DelayLFOWave
// ***********************************************************************************
// ***********************************************************************************

class DelayLFOWavePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.delay_LFOShape
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setDelayLFOShape(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "DelayLFOShapeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class DelayLFOWaveSrc: UIViewController, UITableViewDataSource {
    private var FMModes = DelayLFOWave
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


//
// Chorus Types
// ***********************************************************************************
// ***********************************************************************************

class ChorusTypePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.chorus_Type
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setChorusType(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "ChorusTypeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class ChorusTypeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = ChorusTypes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


// Distortion Types
// ***********************************************************************************
// ***********************************************************************************

class DistortionTypePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.distortion_Type
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setDistortionType(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "DistortionTypeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class DistortionTypeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = DistortionTypes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


// FilterBank Types
// ***********************************************************************************
// ***********************************************************************************

class FilterBankTypePopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            selectedRow = patchCtrl.CurrentPatch.frequencyShifter_Type
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setFrequencyShifterType(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "FilterBankTypeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class FilterBankTypeSrc: UIViewController, UITableViewDataSource {
    private var FMModes = FilterBankTypes
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FMModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = FMModes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// ***********************************************************************************
// ***********************************************************************************

class KeyboardModesPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            
            
            selectedRow = patchCtrl.CurrentPatch.control_KeyboardMode
            
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        patchCtrl.setKeyboardMode(value: indexPath.row)
        
        NotificationCenter.default.post(name: Notification.Name( "KeyboardModeChanged" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

class KeyboardModesSrc: UIViewController, UITableViewDataSource {
    private var osc3Modes = keyboardModeNames
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return osc3Modes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = osc3Modes[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}

// Categories popover
// ***********************************************************************************
// ***********************************************************************************

class PatchCategories: UIViewController, UITableViewDataSource {
    private var currentSelectedRow: IndexPath!
    private var currentScrollPosition: UITableView.ScrollPosition!
    public var matrixSlotNr: Int = 0
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presetCategoriesUI.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = UIFont(name:"Avenir", size:14);
        cell.textLabel?.textColor = UIColor.lightText
        cell.textLabel?.text = presetCategoriesUI[indexPath.row]
        cell.backgroundColor = UIColor.clear
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.gray
        cell.selectedBackgroundView = bgColorView
        return cell
    }
}


class CategoryCollectionsPopover: UITableViewController {
    
    public var selectedRow: Int = 0
    public var CategoryNR = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        if ( selectedRow != 0 ){
            selectedRow = 1
        } else {
            if (CategoryNR == 1 ){
                selectedRow = patchCtrl.CurrentPatch.category1
            } else if (CategoryNR == 2) {
                selectedRow = patchCtrl.CurrentPatch.category2
            } else if (CategoryNR == 3) {
                selectedRow = patchCtrl.CurrentPatch.category3
            }
        }
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (CategoryNR == 1 ){
            patchCtrl.setCategory1(value: indexPath.row)
            
        } else if (CategoryNR == 2) {
            patchCtrl.setCategory2(value: indexPath.row)
        }
        
        NotificationCenter.default.post(name: Notification.Name( "CategorySelected" ), object: nil)
        dismiss(animated: true, completion: nil)
    }
}

/*  CATEGORY POPUP */

class CategoryFilterPopover: UITableViewController {
    
    //public var selectedRow: Int = app.getFilterCategory()
    private var selectedRow: Int = PatchLib.CurrentCategory.id
    public var CategoryNR = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Wanneer tab opnieuw wordt geopend, de tabel updaten
        super.viewWillAppear(animated)
        
        let indexPath = IndexPath(row: selectedRow, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        tableView.backgroundColor = .darkGray
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        app.setFilterCategory(cat: indexPath.row)
        PatchLib.setCategory( cat: indexPath.row)
        
        //NotificationCenter.default.post(name: Notification.Name( "FilterCategorySelected" ), object: nil)
        
        dismiss(animated: true, completion: nil)
    }
}
