//
//  HorizontalSlider.swift
//  Rage
//
//  Created by M. Lierop on 15/08/2018.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import UIKit

@IBDesignable open class HorizontalSlider: UISlider {
    
    let tap = UITapGestureRecognizer()
    let valueIndicatorView = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
  
    func viewDidLoad() {
        self.showValue()
    }
    
    @objc func ResetValue (){
        self.setValue(Float(64), animated: true)
        self.showValue()
        self.sendActions(for: .valueChanged)
    }
    
    
    public func EnableResetGesture() {
        // DOUBLE TAP
        self.tap.numberOfTapsRequired = 2
        self.tap.addTarget(self, action: #selector(self.ResetValue))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(self.tap)
    }
    
    public func showValue (){
        if(valueIndicatorView.tag==15){
            valueIndicatorView.removeFromSuperview()
        }
        
        var displayValue: String = ""
        displayValue = String( Int(self.value) - 64 )
        
        if (self.isEnabled == true ){
            valueIndicatorView.font = UIFont.preferredFont(forTextStyle: .footnote)
            valueIndicatorView.font.withSize(CGFloat(12))
            valueIndicatorView.textColor = UIColor(red:0.46,green:0.46,blue:0.46,alpha:1)
            valueIndicatorView.center = CGPoint(x: 160, y: 15)
            valueIndicatorView.textAlignment = .center
            valueIndicatorView.text = displayValue
            valueIndicatorView.tag = 15

            self.addSubview(valueIndicatorView)
        }
    }
}
