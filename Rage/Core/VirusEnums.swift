//
//  VirusEnums.swift
//  Rage
//
//  Created by M. Lierop on 23-11-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation

enum OscModel: Int {
    case VirtualAnalog = 0
    case HyperSaw = 1
    case Wavetable = 2
    case WavePWM = 3
    case GranularSimple = 4
    case GranularComplex = 5
    case FormantSimple = 6
    case FormantComplex = 7
}
enum OscModelLabel: String {
    case VirtualAnalog = "Virtual Analog"
    case HyperSaw = "Hyper Saw"
    case Wavetable = "Wavetable"
    case WavePWM = "Wave PWM"
    case GranularSimple = "Granular Simple"
    case GranularComplex = "Granular Complex"
    case FormantSimple = "Formant Simple"
    case FormantComplex = "Formant Complex"
}



enum FMMode: Int {
    case PosTriangle = 0
    case Triangle = 1
    case Wave = 2
    case Noise = 3
    case InL = 4
    case InLR = 5
    case InR = 6
}
enum FMModeLabel: String {
    case PosTriangle = "Pos Triangle"
    case Triangle = "Triangle"
    case Wave = "Wave"
    case Noise = "Noise"
    case InL = "In L"
    case InLR = "In L+R"
    case InR = "In R"
}




enum AnalogWaves: Int {
    case Sine = 0
    case Triangle = 1
    case Wave3 = 2
    case Wave4 = 3
    case Wave5 = 4
    case Wave6 = 5
    case Wave7 = 6
    case Wave8 = 7
    case Wave9 = 8
    case Wave10 = 9
    case Wave11 = 10
    case Wave12 = 11
    case Wave13 = 12
    case Wave14 = 13
    case Wave15 = 14
    case Wave16 = 15
    case Wave17 = 16
    case Wave18 = 17
    case Wave19 = 18
    case Wave20 = 19
    case Wave21 = 20
    case Wave22 = 21
    case Wave23 = 22
    case Wave24 = 23
    case Wave25 = 24
    case Wave26 = 25
    case Wave27 = 26
    case Wave28 = 27
    case Wave29 = 28
    case Wave30 = 29
    case Wave31 = 30
    case Wave32 = 31
    case Wave33 = 32
    case Wave34 = 33
    case Wave35 = 34
    case Wave36 = 35
    case Wave37 = 36
    case Wave38 = 37
    case Wave39 = 38
    case Wave40 = 39
    case Wave41 = 40
    case Wave42 = 41
    case Wave43 = 42
    case Wave44 = 43
    case Wave45 = 44
    case Wave46 = 45
    case Wave47 = 46
    case Wave48 = 47
    case Wave49 = 48
    case Wave50 = 49
    case Wave51 = 50
    case Wave52 = 51
    case Wave53 = 52
    case Wave54 = 53
    case Wave55 = 54
    case Wave56 = 55
    case Wave57 = 56
    case Wave58 = 57
    case Wave59 = 58
    case Wave60 = 59
    case Wave61 = 60
    case Wave62 = 61
    case Wave63 = 62
    case Wave64 = 63
}



enum Wavetables: Int {
    case Sine = 0
    case HarmncSweep = 1
    case GlassSweep = 2
    case DrawBars = 3
    case Clusters = 4
    case InsideOut = 5
    case Landing = 6
    case LiquidMetal = 7
    case Opposition = 8
    case Overtunes1 = 9
    case Overtunes2 = 10
    case ScaleTrix = 11
    case SineRider = 12
    case SqrSeries = 13
    case UpsineDown = 14
    case ThumbsUp = 15
    case Waterphone = 16
    case EChime = 17
    case ThinkAbel = 18
    case BellFizz = 19
    case Bellentine = 20
    case RobotWars = 21
    case Alternator = 22
    case FingerBass = 23
    case FizzyBar = 24
    case Flutes = 25
    case HPLove = 26
    case Majestix = 27
    case HotchPotch = 28
    case Resinator = 29
    case SmoothRough = 30
    case Sawsalito = 31
    case Bells1 = 32
    case Bells2 = 33
    case SportReport = 34
    case MetalGuru = 35
    case BatCave = 36
    case Acetate = 37
    case Buzzbizz = 38
    case Buzzpartout = 39
    case Vanish = 40
    case Overbones = 41
    case Pulsechecker = 42
    case Stratosfear = 43
    case SootySweep = 44
    case Throat = 45
    case Didgitalis = 46
    case Chords = 47
    case FMGrid = 48
    case Bellsarnie = 49
    case Octavius = 50
    case EatPulse = 51
    case SinZine = 52
    case SineSystem = 53
    case ClifSweep = 54
    case RoughHages = 55
    case Waving = 56
    case PlingSaw = 57
    case EPeas = 58
    case BumpSweep = 59
    case FilterSqr = 60
    case Fourmant = 61
    case Formantera = 62
    case Sundial1 = 63
    case Sundial2 = 64
    case Sundial3 = 65
    case Clipdial1 = 66
    case Clipdial2 = 67
    case Voxonix = 68
    case Solenoid = 69
    case KlinKlang = 70
    case Violator = 71
    case Potassium = 72
    case PileUp = 73
    case Tincanali = 74
    case Sniper = 75
    case Squeezy = 76
    case Decomposer = 77
    case Morfants = 78
    case Pingfox = 79
    case Adenoids = 80
    case Nasal = 81
    case Partialism = 82
    case TableDance = 83
    case Cascade = 84
    case Prismism = 85
    case Friction = 86
    case Robotix = 87
    case Whizzfizz = 88
    case Spangly = 89
    case Fluxbin = 90
    case Fiboglide = 91
    case Fibonice = 92
    case Fibonasty = 93
    case Penetrator = 94
    case Blinder = 95
    case Element5 = 96
    case BadSigns = 97
    case Domina7Rix = 98
}

enum Oscillator3Mode: Int {
    case Off = 0
    case Slave = 1
    case Saw = 2
    case Pulse = 3
    case Sine = 4
    case Triangle = 5
    case Wave3 = 6
    case Wave4 = 7
    case Wave5 = 8
    case Wave6 = 9
    case Wave7 = 10
    case Wave8 = 11
    case Wave9 = 12
    case Wave10 = 13
    case Wave11 = 14
    case Wave12 = 15
    case Wave13 = 16
    case Wave14 = 17
    case Wave15 = 18
    case Wave16 = 19
    case Wave17 = 20
    case Wave18 = 21
    case Wave19 = 22
    case Wave20 = 23
    case Wave21 = 24
    case Wave22 = 25
    case Wave23 = 26
    case Wave24 = 27
    case Wave25 = 28
    case Wave26 = 29
    case Wave27 = 30
    case Wave28 = 31
    case Wave29 = 32
    case Wave30 = 33
    case Wave31 = 34
    case Wave32 = 35
    case Wave33 = 36
    case Wave34 = 37
    case Wave35 = 38
    case Wave36 = 39
    case Wave37 = 40
    case Wave38 = 41
    case Wave39 = 42
    case Wave40 = 43
    case Wave41 = 44
    case Wave42 = 45
    case Wave43 = 46
    case Wave44 = 47
    case Wave45 = 48
    case Wave46 = 49
    case Wave47 = 50
    case Wave48 = 51
    case Wave49 = 52
    case Wave50 = 53
    case Wave51 = 54
    case Wave52 = 55
    case Wave53 = 56
    case Wave54 = 57
    case Wave55 = 58
    case Wave56 = 59
    case Wave57 = 60
    case Wave58 = 61
    case Wave59 = 62
    case Wave60 = 63
    case Wave61 = 64
    case Wave62 = 65
    case Wave63 = 66
    case Wave64 = 67
}


let MatrixSourceLabels: [String] = [
    "Off",
    "Pitch Bend",
    "Chan Pressure",
    "Mod wheel",
    "Breath",
    "Controller 3",
    "Foot Pedal",
    "Data entry",
    "Balance",
    "Controller 9",
    "Expression",
    "Controller 12",
    "Controller 13",
    "Controller 14",
    "Controller 15",
    "Controller 16",
    "Hold Pedal",
    "Portamento Sw",
    "Sost Pedal",
    "Amp Envelope",
    "Filter Envelope",
    "LFO 1 Bipolar",
    "LFO 2 Bipolar",
    "LFO 3 Bipolar",
    "Velocity On",
    "Velocity Off",
    "Key Follow",
    "Random",
    "Arpeggiator Input",
    "LFO 1 Unipolar",
    "LFO 2 Unipolar",
    "LFO 3 Unipolar",
    "1% Constant",
    "10% Constant",
    "Analog Key 1, Fine",
    "Analog Key 2, Fine",
    "Analog Key 1, Coarse",
    "Analog Key 1, Coarse",
    "Envelope 3",
    "Envelope 4"
]



let MatrixDestinationLabels: [String] = [
    "Off",
    "Amp Envelope Attack",
    "Amp Envelope Decay",
    "Amp Envelope Slope",
    "Amp Envelope Sustain",
    "Amp Envelope Release",
    "Arpeggiator Note Length",
    "Arpeggiator Pattern",
    "Arpeggiator Swing Factor",
    "Chorus Delay",
    "Chorus Feedback",
    "Chorus Mix",
    "Chorus Modulation Depth",
    "Chorus Modulation Rate",
    "Delay Coloration",
    "Delay Feedback",
    "Delay Modulation Depth",
    "Delay Modulation Rate",
    "Delay Send",
    "Delay Time",
    "Distortion Intensity",
    "Distortion Mix",
    "EQ Mid Frequency",
    "EQ Mid Gain",
    "Filter Envelope > FM/Sync",
    "Filter Envelope > Osc 2 Pitch",
    "Filter Balance",
    "Filter Envelope Attack",
    "Filter Envelope Decay",
    "Filter Envelope Slope",
    "Filter Envelope Sustain",
    "Filter Envelope Release",
    "Filter 1 Cutoff",
    "Filter 1 Envelope Amount",
    "Filter 1 Resonance",
    "Filter 2 Cutoff",
    "Filter 2 Envelope Amount",
    "Filter 2 Resonance",
    "Filterbank Poles",
    "Filterbank Resonance",
    "Filterbank Slope",
    "Filterbank Frequency",
    "LFO 1 Assign Amount",
    "LFO 1 Contour",
    "LFO 1 Rate",
    "LFO 1 > Filter Gain",
    "LFO 1 > Osc 1 Pitch",
    "LFO 1 > Osc 2 Pitch",
    "LFO 1 > Pulse width",
    "LFO 1 > Resonance",
    "LFO 2 Assign Amount",
    "LFO 2 Contour",
    "LFO 2 Rate",
    "LFO 2 > Cutoff 1",
    "LFO 2 > Cutoff 2",
    "LFO 2 > FM Amount",
    "LFO 2 > Panorama",
    "LFO 2 > Shape",
    "LFO 3 > Assign Amount",
    "LFO 3 > Rate",
    "Noise Color",
    "Noise Volume",
    "Osc Balance",
    "Osc Volume",
    "Osc 1 F-Shift",
    "Osc 1 F-Spread",
    "Osc 1 Interpolation",
    "Osc 1 Pitch",
    "Osc 1 Pulse Width",
    "Osc 1 Shape/Index",
    "Osc 1 Wave Select",
    "Osc 2 F-Shift",
    "Osc 2 F-Spread",
    "Osc 2 Interpolation",
    "Osc 2 Pitch",
    "Osc 2 Pulse Width",
    "Osc 2 Shape/Index",
    "Osc 2 Wave Select",
    "Osc 3 Detune",
    "Osc 3 Pitch",
    "Osc 3 Volume",
    "Sub Osc Volume",
    "Pan spread",
    "Panorama",
    "Patch Volume",
    "Phaser Feedback",
    "Phaser Frequency",
    "Phaser Mix",
    "Phaser Modulation Depth",
    "Phaser Modulation Rate",
    "Portamento",
    "Punch Intensity",
    "Reverb Send",
    "Reverb Color",
    "Reverb Damping",
    "Reverb Time",
    "Reverb PreDelay",
    "Ring Modulation",
    "Slot 1 Amount 1",
    "Slot 1 Amount 2",
    "Slot 1 Amount 3",
    "Slot 2 Amount 1",
    "Slot 2 Amount 2",
    "Slot 2 Amount 3",
    "Slot 3 Amount 1",
    "Slot 3 Amount 2",
    "Slot 3 Amount 3",
    "Slot 4 Amount 1",
    "Slot 4 Amount 2",
    "Slot 4 Amount 3",
    "Slot 5 Amount 1",
    "Slot 5 Amount 2",
    "Slot 5 Amount 3",
    "Slot 6 Amount 1",
    "Slot 6 Amount 2",
    "Slot 6 Amount 3",
    "Surround Balance",
    "Transpose",
    "Unison Detune",
    "Unison LFO Phase"
]

var presetCategoriesUI = ["Category filter: Off","Acid","Arpeggiator","Atomizer","Bass","Classic","Decay","Digital","Drums","EFX","FM","Input","Lead","Organ","Pad","Percussion","Piano","Pluck","String","Vocoder","Favourites 1","Favourites 2","Favourites 3"]

var presetCategoriesArray: Array = ["Category filter: Off","Lead","Bass","Pad","Decay","Pluck","Acid","Classic","Arpeggiator","EFX","Drums","Percussion","Input","Vocoder","Favourites 1","Favourites 2","Favourites 3","Organ","Piano","String","FM","Digital","Atomizer"]
