//
//  subPatch-Oscillators.swift
//  Rage-2
//
//  Created by M. Lierop on 21-09-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class subPatchOsc: Object {
    
    // General
    @objc dynamic var name: String = "RAGE INT"
    @objc dynamic var id = 0
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: String = ""
    @objc dynamic var category2: String = ""
    @objc dynamic var category3: String = ""
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    
    // Oscilator 1
    @objc dynamic var osc1_Model: Int  = 0
    @objc dynamic var osc1_WaveformShape: Int = 0
    @objc dynamic var osc1_Pulsewidth: Int = 0
    @objc dynamic var osc1_WaveformSelect: Int = 0
    @objc dynamic var osc1_DetuneInSemitones: Int = 64
    @objc dynamic var osc1_Keyfollow: Int = 96
    @objc dynamic var osc1_FormantSpread: Int = 0
    @objc dynamic var osc1_FormantShift: Int = 64
    @objc dynamic var osc1_LocalDetune: Int = 0
    @objc dynamic var osc1_Interpolation: Int = 0
    
    // Oscilator 2
    @objc dynamic var osc2_Model: Int = 0
    @objc dynamic var osc2_WaveformShape: Int = 0
    @objc dynamic var osc2_Pulsewidth: Int = 0
    @objc dynamic var osc2_WaveformSelect: Int = 0
    @objc dynamic var osc2_DetuneInSemitones: Int = 64
    @objc dynamic var osc2_Keyfollow: Int = 96
    @objc dynamic var osc2_FineDetune: Int = 0
    @objc dynamic var osc2_FormantSpread: Int = 0
    @objc dynamic var osc2_FormantShift: Int = 64
    @objc dynamic var osc2_LocalDetune: Int = 0
    @objc dynamic var osc2_Interpolation: Int = 0
   
    @objc dynamic var osc1_osc2_Sync: Int = 0
    @objc dynamic var osc1_osc2_Balance: Int = 64
    
    // Oscilator 3
    @objc dynamic var osc3_Volume: Int = 127
    @objc dynamic var osc3_Model: Int = 0
    @objc dynamic var osc3_DetuneInSemitones: Int = 64
    @objc dynamic var osc3_FineDetune: Int = 0
    
    // Oscilator Sub
    @objc dynamic var oscS_Volume: Int = 0
    @objc dynamic var oscS_WaveformShape: Int = 0
    
    // Oscilator Noise
    @objc dynamic var oscN_Volume: Int = 0
    @objc dynamic var oscN_Color: Int = 64
    
    // Oscillator section
    @objc dynamic var osc_InitialPhase: Int = 0
    @objc dynamic var osc_Portamento: Int = 0
    @objc dynamic var osc_FMMode: Int = 0
    @objc dynamic var osc_FMAmount: Int = 0
    @objc dynamic var osc_PunchIntensity: Int = 0
    @objc dynamic var osc_RingModulationVolume: Int = 0
    @objc dynamic var osc_RingModulationMix: Int = 0
    @objc dynamic var osc_SectionVolume: Int = 64
    @objc dynamic var osc_SectionKeyboardMode: Int = 0 // niet in gebruik, veplaatst naar controls
    @objc dynamic var osc_SectionPatchTransposition = 64
    @objc dynamic var osc_SectionSurroundSoundBalance: Int = 64
    
    @objc dynamic var filterEnvelope_ToPitch:Int = 64
    @objc dynamic var filterEnvelope_ToFM:Int = 64
    
    @objc dynamic var osc_Unison_Mode: Int = 0
    @objc dynamic var osc_Unison_Detune: Int = 0
    @objc dynamic var osc_Unison_PanoramaSpread: Int = 0
    @objc dynamic var osc_Unison_LFOPhaseOffset: Int = 0
}
