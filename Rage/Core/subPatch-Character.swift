//
//  subPatch-Character.swift
//  Rage-2
//
//  Created by M. Lierop on 22-09-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class subPatchEffects: Object {
    
    // General
    @objc dynamic var name: String = "RAGE INT"
    @objc dynamic var id = 0
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: String = ""
    @objc dynamic var category2: String = ""
    @objc dynamic var category3: String = ""
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    
    @objc dynamic var character_Type: Int = 0
    @objc dynamic var character_Intensity: Int  = 0
    @objc dynamic var character_Tune: Int  = 0
    
    @objc dynamic var eq_LowFrequency: Int  = 0
    @objc dynamic var eq_LowGain: Int  = 64
    @objc dynamic var eq_MidGain: Int  = 64
    @objc dynamic var eq_MidFrequency: Int  = 0
    @objc dynamic var eq_MidQFactor: Int  = 0
    @objc dynamic var eq_HighFrequency: Int  = 0
    @objc dynamic var eq_HighGain: Int  = 64
    
    @objc dynamic var frequencyShifter_Type: Int  = 0
    @objc dynamic var frequencyShifter_Mix: Int  = 0
    @objc dynamic var frequencyShifter_Frequency: Int  = 64
    @objc dynamic var frequencyShifter_StereoPhase: Int  = 64
    @objc dynamic var frequencyShifter_LeftShape: Int  = 64
    @objc dynamic var frequencyShifter_RightShape: Int  = 64
    @objc dynamic var frequencyShifter_Resonance: Int  = 64
    @objc dynamic var frequencyShifter_FilterType: Int  = 64
    @objc dynamic var frequencyShifter_Poles: Int  = 0
    @objc dynamic var frequencyShifter_Slope: Int  = 0
    
    @objc dynamic var phaser_Stages: Int = 0      // pp 84
    @objc dynamic var phaser_Mix: Int = 0         // pp 85
    @objc dynamic var phaser_LFORate: Int = 0     // pp 86
    @objc dynamic var phaser_Depth: Int = 0       // pp 87
    @objc dynamic var phaser_Frequency: Int = 0   // pp 88
    @objc dynamic var phaser_Feedback: Int = 64   // pp 89
    @objc dynamic var phaser_Spread: Int = 0      // pp 90
    
    @objc dynamic var distortion_Type: Int = 0         // pp 100
    @objc dynamic var distortion_Intensity: Int = 0    // pp 101 ook drive
    @objc dynamic var distortion_TrebleBooster: Int = 0 //se 70
    @objc dynamic var distortion_HighCut: Int = 0      // se 71
    @objc dynamic var distortion_Mix: Int = 0          // se 72
    @objc dynamic var distortion_Quality: Int = 0      // se 73
    @objc dynamic var distortion_ToneW: Int = 0        // se 74
    
    @objc dynamic var chorus_Type: Int = 0         // cc 103
    @objc dynamic var chorus_Mix: Int = 0          // cc 104
    @objc dynamic var chorus_MixClassic: Int = 0   // cc 105
    @objc dynamic var chorus_LFORate: Int = 0      // cc 106
    @objc dynamic var chorus_LFODepth: Int = 0     // cc 107
    @objc dynamic var chorus_Distance: Int = 0     // cc 107
    @objc dynamic var chorus_Delay: Int = 0        // cc 108
    @objc dynamic var chorus_Amount: Int = 0       // cc 108
    @objc dynamic var chorus_Feedback: Int = 64    // cc 109
    @objc dynamic var chorus_LowHigh: Int = 0      // cc 109
    @objc dynamic var chorus_LFOShape: Int = 0     // cc 110
    @objc dynamic var chorus_XOver: Int = 0        // cc 111
    
    @objc dynamic var input_Mode: Int = 0
    @objc dynamic var input_Select: Int = 0
    @objc dynamic var Atomizer: Int = 0
    @objc dynamic var VocoderMode: Int = 0
}

/*
 
 sysex 25 Character Type
 pp 97 Character Intensity
 pp 98 Character Tune
 
 pp
 45 EQ/Low Frequency (Hz)
 46 EQ/High Frequency (kHz)
 92 EQ/Mid Gain (dB)
 93 EQ/Mid Frequency (Hz)
 94 EQ/Mid Q-Factor
 95 EQ/Low Gain (dB)
 96 EQ/High Gain (dB)
 
 
 pp 18 Frequency Shifter Type
 pp 19 Frequency Shifter Mix
 pp 20 Frequency Shifter Frequency
 pp 21 Frequency Shifter Stereo Phase
 pp 22 Frequency Shifter Left Shape
 pp 23 Frequency Shifter Right Shape
 pp 24 Frequency Shifter Resonance
 */
