//
//  MIDIHandler.swift
//  Rage
//
//  Created by M. Lierop on 21/12/2018.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class MIDIHandler {
    
    func Init(){
        print("INIT midihandler")
    }
    
    
    func parseMIDIdump (data: Array<MIDIByte>, triggerSaveAction: Bool = false){
        
        //0 - 3
        self.SetPage1Parameter(Parameter: 0, Value: Int(data[9]))
        self.SetPage1Parameter(Parameter: 1, Value: Int(data[10]))
        self.SetPage1Parameter(Parameter: 2, Value: Int(data[11]))
        self.SetPage1Parameter(Parameter: 3, Value: Int(data[12]))
        self.SetPage1Parameter(Parameter: 4, Value: Int(data[13]))
        self.SetPage1Parameter(Parameter: 5, Value: Int(data[14]))
        self.SetPage1Parameter(Parameter: 6, Value: Int(data[15]))
        self.SetPage1Parameter(Parameter: 7, Value: Int(data[16]))
        self.SetPage1Parameter(Parameter: 8, Value: Int(data[17]))
        self.SetPage1Parameter(Parameter: 9, Value: Int(data[18]))
        self.SetPage1Parameter(Parameter: 10, Value: Int(data[19]))
        self.SetPage1Parameter(Parameter: 11, Value: Int(data[20]))
        self.SetPage1Parameter(Parameter: 12, Value: Int(data[21]))
        self.SetPage1Parameter(Parameter: 13, Value: Int(data[22]))
        self.SetPage1Parameter(Parameter: 14, Value: Int(data[23]))
        self.SetPage1Parameter(Parameter: 15, Value: Int(data[24]))
        self.SetPage1Parameter(Parameter: 16, Value: Int(data[25]))
        self.SetPage1Parameter(Parameter: 17, Value: Int(data[26]))
        self.SetPage1Parameter(Parameter: 18, Value: Int(data[27]))
        self.SetPage1Parameter(Parameter: 19, Value: Int(data[28]))
        self.SetPage1Parameter(Parameter: 20, Value: Int(data[29]))
        self.SetPage1Parameter(Parameter: 21, Value: Int(data[30]))
        self.SetPage1Parameter(Parameter: 22, Value: Int(data[31]))
        self.SetPage1Parameter(Parameter: 23, Value: Int(data[32]))
        self.SetPage1Parameter(Parameter: 24, Value: Int(data[33]))
        self.SetPage1Parameter(Parameter: 25, Value: Int(data[34]))
        self.SetPage1Parameter(Parameter: 26, Value: Int(data[35]))
        self.SetPage1Parameter(Parameter: 27, Value: Int(data[36]))
        self.SetPage1Parameter(Parameter: 28, Value: Int(data[37]))
        self.SetPage1Parameter(Parameter: 29, Value: Int(data[38]))
        self.SetPage1Parameter(Parameter: 30, Value: Int(data[39]))
        self.SetPage1Parameter(Parameter: 31, Value: Int(data[40]))
        //41
        self.SetPage1Parameter(Parameter: 33, Value: Int(data[42]))
        self.SetPage1Parameter(Parameter: 34, Value: Int(data[43]))
        self.SetPage1Parameter(Parameter: 35, Value: Int(data[44]))
        self.SetPage1Parameter(Parameter: 36, Value: Int(data[45]))
        self.SetPage1Parameter(Parameter: 37, Value: Int(data[46]))
        //47
        self.SetPage1Parameter(Parameter: 39, Value: Int(data[48]))
        self.SetPage1Parameter(Parameter: 40, Value: Int(data[49]))
        self.SetPage1Parameter(Parameter: 41, Value: Int(data[50]))
        self.SetPage1Parameter(Parameter: 42, Value: Int(data[51]))
        self.SetPage1Parameter(Parameter: 43, Value: Int(data[52]))
        self.SetPage1Parameter(Parameter: 44, Value: Int(data[53]))
        self.SetPage1Parameter(Parameter: 45, Value: Int(data[54]))
        self.SetPage1Parameter(Parameter: 46, Value: Int(data[55]))
        self.SetPage1Parameter(Parameter: 47, Value: Int(data[56]))
        self.SetPage1Parameter(Parameter: 48, Value: Int(data[57]))
        self.SetPage1Parameter(Parameter: 49, Value: Int(data[58]))
        self.SetPage1Parameter(Parameter: 50, Value: Int(data[59]))
        self.SetPage1Parameter(Parameter: 51, Value: Int(data[60]))
        self.SetPage1Parameter(Parameter: 52, Value: Int(data[61]))
        self.SetPage1Parameter(Parameter: 53, Value: Int(data[62]))
        self.SetPage1Parameter(Parameter: 54, Value: Int(data[63]))
        self.SetPage1Parameter(Parameter: 55, Value: Int(data[64]))
        self.SetPage1Parameter(Parameter: 56, Value: Int(data[65]))
        self.SetPage1Parameter(Parameter: 57, Value: Int(data[66]))
        self.SetPage1Parameter(Parameter: 58, Value: Int(data[67]))
        self.SetPage1Parameter(Parameter: 59, Value: Int(data[68]))
        self.SetPage1Parameter(Parameter: 60, Value: Int(data[69]))
        self.SetPage1Parameter(Parameter: 61, Value: Int(data[70]))
        self.SetPage1Parameter(Parameter: 62, Value: Int(data[71]))
        self.SetPage1Parameter(Parameter: 63, Value: Int(data[72]))
        self.SetPage1Parameter(Parameter: 64, Value: Int(data[73]))
        self.SetPage1Parameter(Parameter: 65, Value: Int(data[74]))
        self.SetPage1Parameter(Parameter: 66, Value: Int(data[75]))
        self.SetPage1Parameter(Parameter: 67, Value: Int(data[76]))
        self.SetPage1Parameter(Parameter: 68, Value: Int(data[77]))
        self.SetPage1Parameter(Parameter: 69, Value: Int(data[78]))
        self.SetPage1Parameter(Parameter: 70, Value: Int(data[79]))
        self.SetPage1Parameter(Parameter: 71, Value: Int(data[80]))
        self.SetPage1Parameter(Parameter: 72, Value: Int(data[81]))
        self.SetPage1Parameter(Parameter: 73, Value: Int(data[82]))
        self.SetPage1Parameter(Parameter: 74, Value: Int(data[83]))
        self.SetPage1Parameter(Parameter: 75, Value: Int(data[84]))
        self.SetPage1Parameter(Parameter: 76, Value: Int(data[85]))
        self.SetPage1Parameter(Parameter: 77, Value: Int(data[86]))
        self.SetPage1Parameter(Parameter: 78, Value: Int(data[87]))
        self.SetPage1Parameter(Parameter: 79, Value: Int(data[88]))
        self.SetPage1Parameter(Parameter: 80, Value: Int(data[89]))
        self.SetPage1Parameter(Parameter: 81, Value: Int(data[90]))
        self.SetPage1Parameter(Parameter: 82, Value: Int(data[91]))
        self.SetPage1Parameter(Parameter: 83, Value: Int(data[92]))
        self.SetPage1Parameter(Parameter: 84, Value: Int(data[93]))
        self.SetPage1Parameter(Parameter: 85, Value: Int(data[94]))
        self.SetPage1Parameter(Parameter: 86, Value: Int(data[95]))
        self.SetPage1Parameter(Parameter: 87, Value: Int(data[96]))
        self.SetPage1Parameter(Parameter: 88, Value: Int(data[97]))
        self.SetPage1Parameter(Parameter: 89, Value: Int(data[98]))
        self.SetPage1Parameter(Parameter: 90, Value: Int(data[99]))
        self.SetPage1Parameter(Parameter: 91, Value: Int(data[100]))
        //101
        self.SetPage1Parameter(Parameter: 93, Value: Int(data[102]))
        self.SetPage1Parameter(Parameter: 94, Value: Int(data[103]))
        // 104 - 111
        self.SetPage1Parameter(Parameter: 103, Value: Int(data[112]))
        self.SetPage1Parameter(Parameter: 104, Value: Int(data[113]))
        self.SetPage1Parameter(Parameter: 105, Value: Int(data[114]))
        self.SetPage1Parameter(Parameter: 106, Value: Int(data[115]))
        self.SetPage1Parameter(Parameter: 107, Value: Int(data[116]))
        self.SetPage1Parameter(Parameter: 108, Value: Int(data[117]))
        self.SetPage1Parameter(Parameter: 109, Value: Int(data[118]))
        self.SetPage1Parameter(Parameter: 110, Value: Int(data[119]))
        self.SetPage1Parameter(Parameter: 111, Value: Int(data[120]))
        self.SetPage1Parameter(Parameter: 112, Value: Int(data[121]))
        self.SetPage1Parameter(Parameter: 113, Value: Int(data[122]))
        self.SetPage1Parameter(Parameter: 114, Value: Int(data[123]))
        self.SetPage1Parameter(Parameter: 115, Value: Int(data[124]))
        self.SetPage1Parameter(Parameter: 116, Value: Int(data[125]))
        self.SetPage1Parameter(Parameter: 117, Value: Int(data[126]))
        self.SetPage1Parameter(Parameter: 118, Value: Int(data[127]))
        self.SetPage1Parameter(Parameter: 119, Value: Int(data[128]))
        
        
        // 0 1   (137
        self.SetPage2Parameter(Parameter: 2, Value: Int(data[139]))
        self.SetPage2Parameter(Parameter: 3, Value: Int(data[140]))
        self.SetPage2Parameter(Parameter: 4, Value: Int(data[141]))
        self.SetPage2Parameter(Parameter: 5, Value: Int(data[142]))
        self.SetPage2Parameter(Parameter: 6, Value: Int(data[143]))
        self.SetPage2Parameter(Parameter: 7, Value: Int(data[144]))
        self.SetPage2Parameter(Parameter: 8, Value: Int(data[145]))
        self.SetPage2Parameter(Parameter: 9, Value: Int(data[146]))
        self.SetPage2Parameter(Parameter: 10, Value: Int(data[147]))
        self.SetPage2Parameter(Parameter: 11, Value: Int(data[148]))
        self.SetPage2Parameter(Parameter: 12, Value: Int(data[149]))
        self.SetPage2Parameter(Parameter: 13, Value: Int(data[150]))
        // 14
        self.SetPage2Parameter(Parameter: 15, Value: Int(data[152]))
        self.SetPage2Parameter(Parameter: 16, Value: Int(data[153]))
        self.SetPage2Parameter(Parameter: 17, Value: Int(data[154]))
        self.SetPage2Parameter(Parameter: 18, Value: Int(data[155]))
        self.SetPage2Parameter(Parameter: 19, Value: Int(data[156]))
        self.SetPage2Parameter(Parameter: 20, Value: Int(data[157]))
        self.SetPage2Parameter(Parameter: 21, Value: Int(data[158]))
        // 22 - 24
        self.SetPage2Parameter(Parameter: 25, Value: Int(data[162]))
        self.SetPage2Parameter(Parameter: 26, Value: Int(data[163]))
        self.SetPage2Parameter(Parameter: 27, Value: Int(data[164]))
        self.SetPage2Parameter(Parameter: 28, Value: Int(data[165]))
        //29
        self.SetPage2Parameter(Parameter: 30, Value: Int(data[167]))
        self.SetPage2Parameter(Parameter: 31, Value: Int(data[168]))
        self.SetPage2Parameter(Parameter: 32, Value: Int(data[169]))
        self.SetPage2Parameter(Parameter: 33, Value: Int(data[170]))
        self.SetPage2Parameter(Parameter: 34, Value: Int(data[171]))
        self.SetPage2Parameter(Parameter: 35, Value: Int(data[172]))
        self.SetPage2Parameter(Parameter: 36, Value: Int(data[173]))
        //37
        self.SetPage2Parameter(Parameter: 38, Value: Int(data[175]))
        self.SetPage2Parameter(Parameter: 39, Value: Int(data[176]))
        // 40
        self.SetPage2Parameter(Parameter: 41, Value: Int(data[178]))
        self.SetPage2Parameter(Parameter: 42, Value: Int(data[179]))
        self.SetPage2Parameter(Parameter: 43, Value: Int(data[180]))
        self.SetPage2Parameter(Parameter: 44, Value: Int(data[181]))
        self.SetPage2Parameter(Parameter: 45, Value: Int(data[182]))
        self.SetPage2Parameter(Parameter: 46, Value: Int(data[183]))
        self.SetPage2Parameter(Parameter: 47, Value: Int(data[184]))
        self.SetPage2Parameter(Parameter: 48, Value: Int(data[185]))
        self.SetPage2Parameter(Parameter: 49, Value: Int(data[186]))
        self.SetPage2Parameter(Parameter: 50, Value: Int(data[187]))
        self.SetPage2Parameter(Parameter: 51, Value: Int(data[188]))
        self.SetPage2Parameter(Parameter: 52, Value: Int(data[189]))
        self.SetPage2Parameter(Parameter: 53, Value: Int(data[190]))
        self.SetPage2Parameter(Parameter: 54, Value: Int(data[191]))
        self.SetPage2Parameter(Parameter: 55, Value: Int(data[192]))
        self.SetPage2Parameter(Parameter: 56, Value: Int(data[193]))
        self.SetPage2Parameter(Parameter: 57, Value: Int(data[194]))
        self.SetPage2Parameter(Parameter: 58, Value: Int(data[195]))
        // 59
        self.SetPage2Parameter(Parameter: 60, Value: Int(data[197]))
        self.SetPage2Parameter(Parameter: 61, Value: Int(data[198]))
        self.SetPage2Parameter(Parameter: 62, Value: Int(data[199]))
        self.SetPage2Parameter(Parameter: 63, Value: Int(data[200]))
        self.SetPage2Parameter(Parameter: 64, Value: Int(data[201]))
        self.SetPage2Parameter(Parameter: 65, Value: Int(data[202]))
        self.SetPage2Parameter(Parameter: 66, Value: Int(data[203]))
        self.SetPage2Parameter(Parameter: 67, Value: Int(data[204]))
        self.SetPage2Parameter(Parameter: 68, Value: Int(data[205]))
        self.SetPage2Parameter(Parameter: 69, Value: Int(data[206]))
        self.SetPage2Parameter(Parameter: 70, Value: Int(data[207]))
        self.SetPage2Parameter(Parameter: 71, Value: Int(data[208]))
        self.SetPage2Parameter(Parameter: 72, Value: Int(data[209]))
        self.SetPage2Parameter(Parameter: 73, Value: Int(data[210]))
        self.SetPage2Parameter(Parameter: 74, Value: Int(data[211]))
        self.SetPage2Parameter(Parameter: 75, Value: Int(data[212]))
        self.SetPage2Parameter(Parameter: 76, Value: Int(data[213]))
        self.SetPage2Parameter(Parameter: 77, Value: Int(data[214]))
        self.SetPage2Parameter(Parameter: 78, Value: Int(data[215]))
        self.SetPage2Parameter(Parameter: 79, Value: Int(data[216]))
        self.SetPage2Parameter(Parameter: 80, Value: Int(data[217]))
        self.SetPage2Parameter(Parameter: 81, Value: Int(data[218]))
        self.SetPage2Parameter(Parameter: 82, Value: Int(data[219]))
        // 83
        self.SetPage2Parameter(Parameter: 84, Value: Int(data[221]))
        self.SetPage2Parameter(Parameter: 85, Value: Int(data[222]))
        self.SetPage2Parameter(Parameter: 86, Value: Int(data[223]))
        self.SetPage2Parameter(Parameter: 87, Value: Int(data[224]))
        self.SetPage2Parameter(Parameter: 88, Value: Int(data[225]))
        self.SetPage2Parameter(Parameter: 89, Value: Int(data[226]))
        self.SetPage2Parameter(Parameter: 90, Value: Int(data[227]))
        // 91
        self.SetPage2Parameter(Parameter: 92, Value: Int(data[229]))
        self.SetPage2Parameter(Parameter: 93, Value: Int(data[230]))
        self.SetPage2Parameter(Parameter: 94, Value: Int(data[231]))
        self.SetPage2Parameter(Parameter: 95, Value: Int(data[232]))
        self.SetPage2Parameter(Parameter: 96, Value: Int(data[233]))
        self.SetPage2Parameter(Parameter: 97, Value: Int(data[234]))
        self.SetPage2Parameter(Parameter: 98, Value: Int(data[235]))
        self.SetPage2Parameter(Parameter: 99, Value: Int(data[236]))
        self.SetPage2Parameter(Parameter: 100, Value: Int(data[237]))
        self.SetPage2Parameter(Parameter: 101, Value: Int(data[238]))
        // 102
        self.SetPage2Parameter(Parameter: 103, Value: Int(data[240]))
        self.SetPage2Parameter(Parameter: 104, Value: Int(data[241]))
        self.SetPage2Parameter(Parameter: 105, Value: Int(data[242]))
        self.SetPage2Parameter(Parameter: 106, Value: Int(data[243]))
        self.SetPage2Parameter(Parameter: 107, Value: Int(data[244]))
        self.SetPage2Parameter(Parameter: 108, Value: Int(data[245]))
        self.SetPage2Parameter(Parameter: 109, Value: Int(data[246]))
        self.SetPage2Parameter(Parameter: 110, Value: Int(data[247]))
        self.SetPage2Parameter(Parameter: 111, Value: Int(data[248]))
        self.SetPage2Parameter(Parameter: 112, Value: Int(data[249]))
        self.SetPage2Parameter(Parameter: 113, Value: Int(data[250]))
        self.SetPage2Parameter(Parameter: 114, Value: Int(data[251]))
        self.SetPage2Parameter(Parameter: 115, Value: Int(data[252]))
        self.SetPage2Parameter(Parameter: 116, Value: Int(data[253]))
        self.SetPage2Parameter(Parameter: 117, Value: Int(data[254]))
        self.SetPage2Parameter(Parameter: 118, Value: Int(data[255]))
        self.SetPage2Parameter(Parameter: 119, Value: Int(data[256]))
        self.SetPage2Parameter(Parameter: 120, Value: Int(data[257]))
        self.SetPage2Parameter(Parameter: 121, Value: Int(data[258]))
        self.SetPage2Parameter(Parameter: 122, Value: Int(data[259]))
        self.SetPage2Parameter(Parameter: 123, Value: Int(data[260]))
        self.SetPage2Parameter(Parameter: 124, Value: Int(data[261]))
        // 125 - 127
        
        //self.SetPage3Parameter(Parameter: 0, Value: Msg[267])
        self.SetPage3Parameter(Parameter: 1, Value: Int(data[267]))
        self.SetPage3Parameter(Parameter: 2, Value: Int(data[268]))
        self.SetPage3Parameter(Parameter: 3, Value: Int(data[269]))
        self.SetPage3Parameter(Parameter: 4, Value: Int(data[270]))
        self.SetPage3Parameter(Parameter: 5, Value: Int(data[271]))
        self.SetPage3Parameter(Parameter: 6, Value: Int(data[272]))
        self.SetPage3Parameter(Parameter: 7, Value: Int(data[273]))
        self.SetPage3Parameter(Parameter: 8, Value: Int(data[274]))
        self.SetPage3Parameter(Parameter: 9, Value: Int(data[275]))
        self.SetPage3Parameter(Parameter: 10, Value: Int(data[276]))
        // 11
        self.SetPage3Parameter(Parameter: 12, Value: Int(data[278]))
        self.SetPage3Parameter(Parameter: 13, Value: Int(data[279]))
        self.SetPage3Parameter(Parameter: 14, Value: Int(data[280]))
        //15
        //16
        self.SetPage3Parameter(Parameter: 17, Value: Int(data[283]))
        //18
        self.SetPage3Parameter(Parameter: 19, Value: Int(data[285]))
        self.SetPage3Parameter(Parameter: 20, Value: Int(data[286]))
        self.SetPage3Parameter(Parameter: 21, Value: Int(data[287]))
        self.SetPage3Parameter(Parameter: 22, Value: Int(data[288]))
        self.SetPage3Parameter(Parameter: 23, Value: Int(data[289]))
        self.SetPage3Parameter(Parameter: 24, Value: Int(data[290]))
        self.SetPage3Parameter(Parameter: 25, Value: Int(data[291]))
        self.SetPage3Parameter(Parameter: 26, Value: Int(data[292]))
        //27
        self.SetPage3Parameter(Parameter: 28, Value: Int(data[294]))
        //29
        self.SetPage3Parameter(Parameter: 30, Value: Int(data[296]))
        // 31 - 34
        self.SetPage3Parameter(Parameter: 35, Value: Int(data[301]))
        // 36
        self.SetPage3Parameter(Parameter: 37, Value: Int(data[303]))
        // 38 - 41
        self.SetPage3Parameter(Parameter: 42, Value: Int(data[308]))
        self.SetPage3Parameter(Parameter: 43, Value: Int(data[309]))
        self.SetPage3Parameter(Parameter: 44, Value: Int(data[310]))
        // 45 - 56
        self.SetPage3Parameter(Parameter: 57, Value: Int(data[323]))
        // 58 - 61
        self.SetPage3Parameter(Parameter: 62, Value: Int(data[328]))
        self.SetPage3Parameter(Parameter: 63, Value: Int(data[329]))
        self.SetPage3Parameter(Parameter: 64, Value: Int(data[330]))
        // 65 - 69
        self.SetPage3Parameter(Parameter: 70, Value: Int(data[336]))
        self.SetPage3Parameter(Parameter: 71, Value: Int(data[337]))
        self.SetPage3Parameter(Parameter: 72, Value: Int(data[338]))
        self.SetPage3Parameter(Parameter: 73, Value: Int(data[339]))
        self.SetPage3Parameter(Parameter: 74, Value: Int(data[340]))
        // 75 - 79
        self.SetPage3Parameter(Parameter: 80, Value: Int(data[346]))
        self.SetPage3Parameter(Parameter: 81, Value: Int(data[347]))
        self.SetPage3Parameter(Parameter: 82, Value: Int(data[348]))
        self.SetPage3Parameter(Parameter: 83, Value: Int(data[349]))
        self.SetPage3Parameter(Parameter: 84, Value: Int(data[350]))
        self.SetPage3Parameter(Parameter: 85, Value: Int(data[351]))
        self.SetPage3Parameter(Parameter: 86, Value: Int(data[352]))
        self.SetPage3Parameter(Parameter: 87, Value: Int(data[353]))
        self.SetPage3Parameter(Parameter: 88, Value: Int(data[354]))
        self.SetPage3Parameter(Parameter: 89, Value: Int(data[355]))
        self.SetPage3Parameter(Parameter: 90, Value: Int(data[356]))
        self.SetPage3Parameter(Parameter: 91, Value: Int(data[357]))
        self.SetPage3Parameter(Parameter: 92, Value: Int(data[358]))
        self.SetPage3Parameter(Parameter: 93, Value: Int(data[359]))
        self.SetPage3Parameter(Parameter: 94, Value: Int(data[360]))
        self.SetPage3Parameter(Parameter: 95, Value: Int(data[361]))
        self.SetPage3Parameter(Parameter: 96, Value: Int(data[362]))
        self.SetPage3Parameter(Parameter: 97, Value: Int(data[363]))
        self.SetPage3Parameter(Parameter: 98, Value: Int(data[364]))
        self.SetPage3Parameter(Parameter: 99, Value: Int(data[365]))
        self.SetPage3Parameter(Parameter: 100, Value: Int(data[366]))
        self.SetPage3Parameter(Parameter: 101, Value: Int(data[367]))
        self.SetPage3Parameter(Parameter: 102, Value: Int(data[368]))
        self.SetPage3Parameter(Parameter: 103, Value: Int(data[369]))
        self.SetPage3Parameter(Parameter: 104, Value: Int(data[370]))
        self.SetPage3Parameter(Parameter: 105, Value: Int(data[371]))
        self.SetPage3Parameter(Parameter: 106, Value: Int(data[372]))
        self.SetPage3Parameter(Parameter: 107, Value: Int(data[373]))
        // 108 - 126
        self.SetPage3Parameter(Parameter: 127, Value: Int(data[393]))
        
        
        self.SetPage4Parameter(Parameter: 1, Value: Int(data[395]))
        self.SetPage4Parameter(Parameter: 2, Value: Int(data[396]))
        //
        self.SetPage4Parameter(Parameter: 4, Value: Int(data[398]))
        self.SetPage4Parameter(Parameter: 5, Value: Int(data[399]))
        //
        self.SetPage4Parameter(Parameter: 7, Value: Int(data[401]))
        self.SetPage4Parameter(Parameter: 8, Value: Int(data[402]))
        //
        self.SetPage4Parameter(Parameter: 10, Value: Int(data[404]))
        self.SetPage4Parameter(Parameter: 11, Value: Int(data[405]))
        //
        self.SetPage4Parameter(Parameter: 13, Value: Int(data[407]))
        self.SetPage4Parameter(Parameter: 14, Value: Int(data[408]))
        //
        self.SetPage4Parameter(Parameter: 16, Value: Int(data[410]))
        self.SetPage4Parameter(Parameter: 17, Value: Int(data[411]))
        //
        self.SetPage4Parameter(Parameter: 19, Value: Int(data[413]))
        self.SetPage4Parameter(Parameter: 20, Value: Int(data[414]))
        //
        self.SetPage4Parameter(Parameter: 22, Value: Int(data[416]))
        self.SetPage4Parameter(Parameter: 23, Value: Int(data[417]))
        //
        self.SetPage4Parameter(Parameter: 25, Value: Int(data[419]))
        self.SetPage4Parameter(Parameter: 26, Value: Int(data[420]))
        //
        self.SetPage4Parameter(Parameter: 28, Value: Int(data[422]))
        self.SetPage4Parameter(Parameter: 29, Value: Int(data[422]))
        //
        self.SetPage4Parameter(Parameter: 31, Value: Int(data[425]))
        self.SetPage4Parameter(Parameter: 32, Value: Int(data[426]))
        //
        self.SetPage4Parameter(Parameter: 34, Value: Int(data[428]))
        self.SetPage4Parameter(Parameter: 35, Value: Int(data[429]))
        //
        self.SetPage4Parameter(Parameter: 37, Value: Int(data[431]))
        self.SetPage4Parameter(Parameter: 38, Value: Int(data[432]))
        //
        self.SetPage4Parameter(Parameter: 40, Value: Int(data[434]))
        self.SetPage4Parameter(Parameter: 41, Value: Int(data[435]))
        //
        self.SetPage4Parameter(Parameter: 43, Value: Int(data[437]))
        self.SetPage4Parameter(Parameter: 44, Value: Int(data[438]))
        //
        self.SetPage4Parameter(Parameter: 46, Value: Int(data[440]))
        self.SetPage4Parameter(Parameter: 47, Value: Int(data[441]))
        //
        self.SetPage4Parameter(Parameter: 49, Value: Int(data[443]))
        self.SetPage4Parameter(Parameter: 50, Value: Int(data[444]))
        //
        self.SetPage4Parameter(Parameter: 52, Value: Int(data[446]))
        self.SetPage4Parameter(Parameter: 53, Value: Int(data[447]))
        //
        self.SetPage4Parameter(Parameter: 55, Value: Int(data[449]))
        self.SetPage4Parameter(Parameter: 56, Value: Int(data[450]))
        //
        self.SetPage4Parameter(Parameter: 58, Value: Int(data[452]))
        self.SetPage4Parameter(Parameter: 59, Value: Int(data[453]))
        //
        self.SetPage4Parameter(Parameter: 61, Value: Int(data[455]))
        self.SetPage4Parameter(Parameter: 62, Value: Int(data[456]))
        //
        self.SetPage4Parameter(Parameter: 64, Value: Int(data[458]))
        self.SetPage4Parameter(Parameter: 65, Value: Int(data[459]))
        //
        self.SetPage4Parameter(Parameter: 67, Value: Int(data[461]))
        self.SetPage4Parameter(Parameter: 68, Value: Int(data[462]))
        //
        self.SetPage4Parameter(Parameter: 70, Value: Int(data[464]))
        self.SetPage4Parameter(Parameter: 71, Value: Int(data[465]))
        //
        self.SetPage4Parameter(Parameter: 73, Value: Int(data[467]))
        self.SetPage4Parameter(Parameter: 74, Value: Int(data[468]))
        //
        self.SetPage4Parameter(Parameter: 76, Value: Int(data[470]))
        self.SetPage4Parameter(Parameter: 77, Value: Int(data[471]))
        //
        self.SetPage4Parameter(Parameter: 79, Value: Int(data[473]))
        self.SetPage4Parameter(Parameter: 80, Value: Int(data[474]))
        //
        self.SetPage4Parameter(Parameter: 82, Value: Int(data[475]))
        self.SetPage4Parameter(Parameter: 83, Value: Int(data[476]))
        //7
        self.SetPage4Parameter(Parameter: 85, Value: Int(data[479]))
        self.SetPage4Parameter(Parameter: 86, Value: Int(data[480]))
        //
        self.SetPage4Parameter(Parameter: 88, Value: Int(data[482]))
        self.SetPage4Parameter(Parameter: 89, Value: Int(data[483]))
        //
        self.SetPage4Parameter(Parameter: 91, Value: Int(data[485]))
        self.SetPage4Parameter(Parameter: 92, Value: Int(data[486]))
        //
        self.SetPage4Parameter(Parameter: 94, Value: Int(data[488]))
        self.SetPage4Parameter(Parameter: 95, Value: Int(data[489]))
        
        self.SetPage4Parameter(Parameter: 120, Value: Int(data[514]))
        self.SetPage4Parameter(Parameter: 121, Value: Int(data[515]))
        self.SetPage4Parameter(Parameter: 122, Value: Int(data[516]))
        self.SetPage4Parameter(Parameter: 123, Value: Int(data[517]))
        self.SetPage4Parameter(Parameter: 124, Value: Int(data[518]))
        self.SetPage4Parameter(Parameter: 125, Value: Int(data[519]))
        self.SetPage4Parameter(Parameter: 126, Value: Int(data[520]))
        
        self.translatePatchName()
        if (triggerSaveAction){
            
            let realm = try! Realm(configuration: config)
                
            try! realm.write {
                realm.create(Patch.self, value: patchCtrl.CurrentPatch)
                realm.refresh()
            }
                
        }
    }
    
    public func ParsePatchName (data: Array<MIDIByte>) -> String {
        
        var patchName: String = ""
        
        patchName += getCharacter( Int( data[249] ) )
        patchName += getCharacter( Int( data[250] ) )
        patchName += getCharacter( Int( data[251] ) )
        patchName += getCharacter( Int( data[252] ) )
        patchName += getCharacter( Int( data[253] ) )
        patchName += getCharacter( Int( data[254] ) )
        patchName += getCharacter( Int( data[255] ) )
        patchName += getCharacter( Int( data[256] ) )
        patchName += getCharacter( Int( data[257] ) )
        patchName += getCharacter( Int( data[258] ) )
        
        return patchName
    }
    
    public func ParseLibName (data: Array<MIDIByte>) -> String {
        
        var libName: String = ""
        
        libName += getCharacter( Int( data[0] ) )
        libName += getCharacter( Int( data[1] ) )
        libName += getCharacter( Int( data[2] ) )
        libName += getCharacter( Int( data[3] ) )
        libName += getCharacter( Int( data[4] ) )
        libName += getCharacter( Int( data[5] ) )
        libName += getCharacter( Int( data[6] ) )
        libName += getCharacter( Int( data[7] ) )
        libName += getCharacter( Int( data[8] ) )
        libName += getCharacter( Int( data[9] ) )
        libName += getCharacter( Int( data[10] ) )
        libName += getCharacter( Int( data[11] ) )
        libName += getCharacter( Int( data[12] ) )
        libName += getCharacter( Int( data[13] ) )
        libName += getCharacter( Int( data[14] ) )
        libName += getCharacter( Int( data[15] ) )
        libName += getCharacter( Int( data[16] ) )
        libName += getCharacter( Int( data[17] ) )
        libName += getCharacter( Int( data[18] ) )
        libName += getCharacter( Int( data[19] ) )
        
        return libName
    }
    
    
    func SetPage1Parameter (Parameter: Int, Value: Int) {
        //DispatchQueue.main.async {
        switch Parameter {
        case 5:
            //5  Portamento
            patchCtrl.CurrentPatch.osc_Portamento = Int(Value)
            patchCtrl.OscPatch.osc_Portamento = Int(Value)
        //case 10:
            //10  Patch Panorama
            // deze heb ik niet ;( aangemaakt
        case 17:
            // Oscillator 1 Waveform Shape
            patchCtrl.CurrentPatch.osc1_WaveformShape = Int(Value)
            patchCtrl.OscPatch.osc1_WaveformShape = Int(Value)
        case 18:
            // Oscillator 1 Pulsewidth
            patchCtrl.CurrentPatch.osc1_Pulsewidth = Int(Value)
            patchCtrl.OscPatch.osc1_Pulsewidth = Int(Value)
        case 19:
            // Oscillator 1 Wave Select
            patchCtrl.CurrentPatch.osc1_WaveformSelect = Int(Value)
            patchCtrl.OscPatch.osc1_WaveformSelect = Int(Value)
        case 20:
            // Oscillator 1 Detune In Semitones
            patchCtrl.CurrentPatch.osc1_DetuneInSemitones = Int(Value)
            patchCtrl.OscPatch.osc1_DetuneInSemitones = Int(Value)
        case 21:
            //21  Oscillator 1 Keyfollow
            patchCtrl.CurrentPatch.osc1_Keyfollow = Int(Value)
            patchCtrl.OscPatch.osc1_Keyfollow = Int(Value)
        case 22:
            //22  Oscillator 2 Shape
            patchCtrl.CurrentPatch.osc2_WaveformShape = Int(Value)
            patchCtrl.OscPatch.osc2_WaveformShape = Int(Value)
        case 23:
            //23  Oscillator 2 Pulsewidth
            patchCtrl.CurrentPatch.osc2_Pulsewidth = Int(Value)
            patchCtrl.OscPatch.osc2_Pulsewidth = Int(Value)
        case 24:
            //24  Oscillator 2 Wave Select
            patchCtrl.CurrentPatch.osc2_WaveformSelect = Int(Value)
            patchCtrl.OscPatch.osc2_WaveformSelect = Int(Value)
        case 25:
            //25  Oscillator 2 Detune In Semitones
            patchCtrl.CurrentPatch.osc2_DetuneInSemitones = Int(Value)
            patchCtrl.OscPatch.osc2_DetuneInSemitones = Int(Value)
        case 26:
            //26  Oscillator 2 Fine Detune
            patchCtrl.CurrentPatch.osc2_FineDetune = Int(Value)
            patchCtrl.OscPatch.osc2_FineDetune = Int(Value)
        case 27:
            //27  FM Amount
            patchCtrl.CurrentPatch.osc_FMAmount = Int(Value)
            patchCtrl.OscPatch.osc_FMAmount = Int(Value)
        case 28:
            //28  Oscillator 1 Sync
            patchCtrl.CurrentPatch.osc1_osc2_Sync = Int(Value)
            patchCtrl.OscPatch.osc1_osc2_Sync = Int(Value)
        case 29:
            //29  Filter Envelope --> Pitch
            patchCtrl.CurrentPatch.filterEnvelope_ToPitch = Int(Value)
            patchCtrl.OscPatch.filterEnvelope_ToPitch = Int(Value)
        case 30:
            //30  Filter Envelope --> FM
            patchCtrl.CurrentPatch.filterEnvelope_ToFM = Int(Value)
            patchCtrl.OscPatch.filterEnvelope_ToFM = Int(Value)
        case 31:
            //31  Oscillator 2 Keyfollow
            patchCtrl.CurrentPatch.osc2_Keyfollow = Int(Value)
            patchCtrl.OscPatch.osc2_Keyfollow = Int(Value)
        case 33:
            patchCtrl.CurrentPatch.osc1_osc2_Balance = Int(Value)
            patchCtrl.OscPatch.osc1_osc2_Balance = Int(Value)
        case 34:
            patchCtrl.CurrentPatch.oscS_Volume =  Int(Value)
            patchCtrl.OscPatch.oscS_Volume =  Int(Value)
        case 35:
            patchCtrl.CurrentPatch.oscS_WaveformShape = Int(Value)
            patchCtrl.OscPatch.oscS_WaveformShape = Int(Value)
        case 36:
            patchCtrl.CurrentPatch.osc_SectionVolume = Int(Value)
            patchCtrl.OscPatch.osc_SectionVolume = Int(Value)
        case 37:
            patchCtrl.CurrentPatch.oscN_Volume = Int(Value)
            patchCtrl.OscPatch.oscN_Volume = Int(Value)
        case 39:
            patchCtrl.CurrentPatch.oscN_Color = Int(Value)
            patchCtrl.OscPatch.oscN_Color = Int(Value)
        case 40:
            patchCtrl.CurrentPatch.filter1_Cutoff = Int(Value)
            patchCtrl.FilterPatch.filter1_Cutoff = Int(Value)
        case 41:
            patchCtrl.CurrentPatch.filter2_Cutoff = Int(Value)
            patchCtrl.FilterPatch.filter2_Cutoff = Int(Value)
        case 42:
            patchCtrl.CurrentPatch.filter1_Resonance = Int(Value)
            patchCtrl.FilterPatch.filter1_Resonance = Int(Value)
        case 43:
            patchCtrl.CurrentPatch.filter2_Resonance = Int(Value)
            patchCtrl.FilterPatch.filter2_Resonance = Int(Value)
        case 44:
            patchCtrl.CurrentPatch.filter1_EnvelopeAmount = Int(Value)
            patchCtrl.FilterPatch.filter1_EnvelopeAmount = Int(Value)
        case 45:
            patchCtrl.CurrentPatch.filter2_EnvelopeAmount = Int(Value)
            patchCtrl.FilterPatch.filter2_EnvelopeAmount = Int(Value)
        case 46:
            patchCtrl.CurrentPatch.filter1_KeyFollow = Int(Value)
            patchCtrl.FilterPatch.filter1_KeyFollow = Int(Value)
        case 47:
            patchCtrl.CurrentPatch.filter2_KeyFollow = Int(Value)
            patchCtrl.FilterPatch.filter2_KeyFollow = Int(Value)
        case 48:
            patchCtrl.CurrentPatch.filter_Balance = Int(Value)
            patchCtrl.FilterPatch.filter_Balance = Int(Value)
        case 49:
            patchCtrl.CurrentPatch.filter_saturationType = Int(Value)
            patchCtrl.FilterPatch.filter_saturationType = Int(Value)
        case 50:
            patchCtrl.CurrentPatch.osc_RingModulationVolume = Int(Value)
            patchCtrl.OscPatch.osc_RingModulationVolume = Int(Value)
        case 51:
            patchCtrl.CurrentPatch.filter1_Mode = Int(Value)
            patchCtrl.FilterPatch.filter1_Mode = Int(Value)
        case 52:
            patchCtrl.CurrentPatch.filter2_Mode = Int(Value)
            patchCtrl.FilterPatch.filter2_Mode = Int(Value)
        case 53:
            patchCtrl.CurrentPatch.filter_routing = Int(Value)
            patchCtrl.FilterPatch.filter_routing = Int(Value)
        case 54:
            patchCtrl.CurrentPatch.filterEnvelope_Attack = Int(Value)
            patchCtrl.FilterPatch.filterEnvelope_Attack = Int(Value)
        case 55:
            patchCtrl.CurrentPatch.filterEnvelope_Decay = Int(Value)
            patchCtrl.FilterPatch.filterEnvelope_Decay = Int(Value)
        case 56:
            patchCtrl.CurrentPatch.filterEnvelope_Sustain = Int(Value)
            patchCtrl.FilterPatch.filterEnvelope_Sustain = Int(Value)
        case 57:
            patchCtrl.CurrentPatch.filterEnvelope_SustainSlope = Int(Value)
            patchCtrl.FilterPatch.filterEnvelope_SustainSlope = Int(Value)
        case 58:
            patchCtrl.CurrentPatch.filterEnvelope_Release = Int(Value)
            patchCtrl.FilterPatch.filterEnvelope_Release = Int(Value)
        case 59:
            patchCtrl.CurrentPatch.amplifierEnvelope_Attack = Int(Value)
            patchCtrl.FilterPatch.amplifierEnvelope_Attack = Int(Value)
        case 60:
            patchCtrl.CurrentPatch.amplifierEnvelope_Decay = Int(Value)
            patchCtrl.FilterPatch.amplifierEnvelope_Decay = Int(Value)
        case 61:
            patchCtrl.CurrentPatch.amplifierEnvelope_Sustain = Int(Value)
            patchCtrl.FilterPatch.amplifierEnvelope_Sustain = Int(Value)
        case 62:
            patchCtrl.CurrentPatch.amplifierEnvelope_SustainSlope = Int(Value)
            patchCtrl.FilterPatch.amplifierEnvelope_SustainSlope = Int(Value)
        case 63:
            patchCtrl.CurrentPatch.amplifierEnvelope_Release = Int(Value)
            patchCtrl.FilterPatch.amplifierEnvelope_Release = Int(Value)
        case 67:
            patchCtrl.CurrentPatch.lfo1_Rate = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_Rate = Int(Value)
        case 68:
            patchCtrl.CurrentPatch.lfo1_WaveformShape = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_WaveformShape = Int(Value)
        case 69:
            patchCtrl.CurrentPatch.lfo1_EnvelopeMode = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_EnvelopeMode = Int(Value)
        case 70:
            patchCtrl.CurrentPatch.lfo1_Mode = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_Mode = Int(Value)
        case 71:
            patchCtrl.CurrentPatch.lfo1_WaveformContour = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_WaveformContour = Int(Value)
        case 72:
            patchCtrl.CurrentPatch.lfo1_Keyfollow = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_Keyfollow = Int(Value)
        case 73:
            patchCtrl.CurrentPatch.lfo1_TriggerPhase = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_TriggerPhase = Int(Value)
        case 74:
            patchCtrl.CurrentPatch.lfo1_Osc1 = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_Osc1 = Int(Value)
        case 75:
            patchCtrl.CurrentPatch.lfo1_Osc2 = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_Osc2 = Int(Value)
        case 76:
            patchCtrl.CurrentPatch.lfo1_Pulsewidth = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_Pulsewidth = Int(Value)
        case 77:
            patchCtrl.CurrentPatch.lfo1_FilterResonance12 = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_FilterResonance12 = Int(Value)
        case 78:
            patchCtrl.CurrentPatch.lfo1_FilterEnvelopeGain = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_FilterEnvelopeGain = Int(Value)
        case 79:
            patchCtrl.CurrentPatch.lfo2_Rate = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Rate = Int(Value)
        case 80:
            patchCtrl.CurrentPatch.lfo2_WaveformShape = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_WaveformShape = Int(Value)
        case 81:
            patchCtrl.CurrentPatch.lfo2_EnvelopeMode = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_EnvelopeMode = Int(Value)
        case 82:
            patchCtrl.CurrentPatch.lfo2_Mode = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Mode = Int(Value)
        case 83:
            patchCtrl.CurrentPatch.lfo2_WaveformContour = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_WaveformContour = Int(Value)
        case 84:
            patchCtrl.CurrentPatch.lfo2_Keyfollow = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Keyfollow = Int(Value)
        case 85:
            patchCtrl.CurrentPatch.lfo2_TriggerPhase = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_TriggerPhase = Int(Value)
        case 86:
            patchCtrl.CurrentPatch.lfo2_Shape12 = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Shape12 = Int(Value)
        case 87:
            patchCtrl.CurrentPatch.lfo2_FMAmount = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_FMAmount = Int(Value)
        case 88:
            patchCtrl.CurrentPatch.lfo2_Cutoff12 = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Cutoff12 = Int(Value)
        case 89:
            patchCtrl.CurrentPatch.lfo2_Cutoff2 = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Cutoff2 = Int(Value)
        case 90:
            patchCtrl.CurrentPatch.lfo2_Panorama = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Panorama = Int(Value)
        case 91:
            patchCtrl.CurrentPatch.amp_PatchVolume = Int(Value)
            patchCtrl.FilterPatch.amp_PatchVolume = Int(Value)
        case 93:
            patchCtrl.CurrentPatch.osc_SectionPatchTransposition = Int(Value)
            patchCtrl.OscPatch.osc_SectionPatchTransposition = Int(Value)
        case 94:
            patchCtrl.CurrentPatch.control_KeyboardMode = Int(Value)
            patchCtrl.ControlPatch.control_KeyboardMode = Int(Value)
        case 103:
            patchCtrl.CurrentPatch.chorus_Type = Int(Value)
            patchCtrl.CharacterPatch.chorus_Type = Int(Value)
        case 104:
            patchCtrl.CurrentPatch.chorus_Mix = Int(Value)
            patchCtrl.CharacterPatch.chorus_Mix = Int(Value)
        case 105:
            patchCtrl.CurrentPatch.chorus_MixClassic = Int(Value)
            patchCtrl.CharacterPatch.chorus_MixClassic = Int(Value)
        case 106:
            patchCtrl.CurrentPatch.chorus_LFORate = Int(Value)
            patchCtrl.CharacterPatch.chorus_LFORate = Int(Value)
        case 107:
            patchCtrl.CurrentPatch.chorus_LFODepth = Int(Value)
            patchCtrl.CharacterPatch.chorus_LFODepth = Int(Value)
        case 108:
            patchCtrl.CurrentPatch.chorus_Delay = Int(Value)
            patchCtrl.CharacterPatch.chorus_Delay = Int(Value)
        case 109:
            patchCtrl.CurrentPatch.chorus_Feedback = Int(Value)
            patchCtrl.CharacterPatch.chorus_Feedback = Int(Value)
        case 110:
            patchCtrl.CurrentPatch.chorus_LFOShape = Int(Value)
            patchCtrl.CharacterPatch.chorus_LFOShape = Int(Value)
        case 111:
            patchCtrl.CurrentPatch.chorus_XOver = Int(Value)
            patchCtrl.CharacterPatch.chorus_XOver = Int(Value)
        case 112:
            patchCtrl.CurrentPatch.delay_Mode = Int(Value)
            patchCtrl.EffectsPatch.delay_Mode = Int(Value)
        case 113:
            patchCtrl.CurrentPatch.delay_Send = Int(Value)
            patchCtrl.EffectsPatch.delay_Send = Int(Value)
        case 114:
            patchCtrl.CurrentPatch.delay_Time = Int(Value)
            patchCtrl.EffectsPatch.delay_Time = Int(Value)
        case 115:
            patchCtrl.CurrentPatch.delay_Feedback = Int(Value)
            patchCtrl.EffectsPatch.delay_Feedback = Int(Value)
        case 116:
            patchCtrl.CurrentPatch.delay_LFORate = Int(Value)
            patchCtrl.EffectsPatch.delay_LFORate = Int(Value)
        case 117:
            patchCtrl.CurrentPatch.delay_LFODepth = Int(Value)
            patchCtrl.EffectsPatch.delay_LFODepth = Int(Value)
        case 118:
            patchCtrl.CurrentPatch.delay_LFOShape = Int(Value)
            patchCtrl.EffectsPatch.delay_LFOShape = Int(Value)
        case 119:
            patchCtrl.CurrentPatch.delay_Color = Int(Value)
            patchCtrl.EffectsPatch.delay_Color = Int(Value)
        //case 120:
            // ?????
        //case 123:
            // All Notes Off
        default:
            print ("MIDIHANDLER ,unknown PAGE 1 parameter: ",Parameter," : ",Value)
        }
    }
    
    func SetPage2Parameter (Parameter: Int, Value: Int) {
        //print("Page 2 parameter set")

        switch Parameter {
        case  2 :
            patchCtrl.CurrentPatch.arp_pattern = Int(Value)
            patchCtrl.ArpPatch.arp_pattern = Int(Value)
        case  3 :
            patchCtrl.CurrentPatch.arp_range = Int(Value)
            patchCtrl.ArpPatch.arp_range = Int(Value)
        case  4 :
            patchCtrl.CurrentPatch.arp_holdMode = Int(Value)
            patchCtrl.ArpPatch.arp_holdMode = Int(Value)
        case  5 :
            patchCtrl.CurrentPatch.arp_noteLength = Int(Value)
            patchCtrl.ArpPatch.arp_noteLength = Int(Value)
        case  6 :
            patchCtrl.CurrentPatch.arp_swingFactor = Int(Value)
            patchCtrl.ArpPatch.arp_swingFactor = Int(Value)
        case  7 :
            patchCtrl.CurrentPatch.lfo3_Rate = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_Rate = Int(Value)
        case  8 :
            patchCtrl.CurrentPatch.lfo3_WaveformShape = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_WaveformShape = Int(Value)
        case  9 :
            patchCtrl.CurrentPatch.lfo3_Mode = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_Mode = Int(Value)
        case  10 :
            patchCtrl.CurrentPatch.lfo3_Keyfollow = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_Keyfollow = Int(Value)
        case  11 :
            patchCtrl.CurrentPatch.lfo3_UserDestination = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_UserDestination = Int(Value)
        case  12 :
            patchCtrl.CurrentPatch.lfo3_UserDestinationAmount = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_UserDestinationAmount = Int(Value)
        case  13 :
            patchCtrl.CurrentPatch.lfo3_FadeInTime = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_FadeInTime = Int(Value)
        case  15 :
            patchCtrl.CurrentPatch.arp_mode = Int(Value)
            patchCtrl.ArpPatch.arp_mode = Int(Value)
        case 16:
            patchCtrl.CurrentPatch.arp_tempo = Int(Value)
            patchCtrl.ArpPatch.arp_tempo = Int(Value)
        case  17 :
            patchCtrl.CurrentPatch.arp_clock = Int(Value)
            patchCtrl.ArpPatch.arp_clock = Int(Value)
        case  18 :
            patchCtrl.CurrentPatch.lfo1_Clock = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_Clock = Int(Value)
        case  19 :
            patchCtrl.CurrentPatch.lfo2_Clock = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_Clock = Int(Value)
        case  20 :
            patchCtrl.CurrentPatch.delay_Clock = Int(Value)
            patchCtrl.EffectsPatch.delay_Clock = Int(Value)
        case  21 :
            patchCtrl.CurrentPatch.lfo3_Clock = Int(Value)
            patchCtrl.ModulatorPatch.lfo3_Clock = Int(Value)
        case  25 :
            patchCtrl.CurrentPatch.control_parameterSmoothMode = Int(Value)
            patchCtrl.ControlPatch.control_parameterSmoothMode = Int(Value)
        case  26 :
            patchCtrl.CurrentPatch.control_BenderUp = Int(Value)
            patchCtrl.ControlPatch.control_BenderUp = Int(Value)
        case  27 :
            patchCtrl.CurrentPatch.control_BenderDown = Int(Value)
            patchCtrl.ControlPatch.control_BenderDown = Int(Value)
        case  28 :
            patchCtrl.CurrentPatch.control_BenderScale = Int(Value)
            patchCtrl.ControlPatch.control_BenderScale = Int(Value)
        case  30 :
            patchCtrl.CurrentPatch.filter1_EnvelopePolarity = Int(Value)
            patchCtrl.FilterPatch.filter1_EnvelopePolarity = Int(Value)
        case  31 :
            patchCtrl.CurrentPatch.filter2_EnvelopePolarity = Int(Value)
            patchCtrl.FilterPatch.filter2_EnvelopePolarity = Int(Value)
        case  32 :
            patchCtrl.CurrentPatch.filter_cutoffLink = Int(Value)
            patchCtrl.FilterPatch.filter_cutoffLink = Int(Value)
        case  33 :
            patchCtrl.CurrentPatch.filter_keyfollowBase = Int(Value)
            patchCtrl.FilterPatch.filter_keyfollowBase = Int(Value)
        case  34 :
            patchCtrl.CurrentPatch.osc_FMMode = Int(Value)
            patchCtrl.OscPatch.osc_FMMode = Int(Value)
        case  35 :
            patchCtrl.CurrentPatch.osc_InitialPhase = Int(Value)
            patchCtrl.OscPatch.osc_InitialPhase = Int(Value)
        case  36 :
            patchCtrl.CurrentPatch.osc_PunchIntensity = Int(Value)
            patchCtrl.OscPatch.osc_PunchIntensity = Int(Value)
        case  38 :
            patchCtrl.CurrentPatch.filterInputFollower_Input = Int(Value)
            patchCtrl.CharacterPatch.input_Select = Int(Value)
        case  39 :
            patchCtrl.CurrentPatch.VocoderMode = Int(Value)
            patchCtrl.FilterPatch.VocoderMode = Int(Value)
            //patchCtrl.CharacterPatch.VocoderMode = Int(Value)
        case  41 :
            guard Int(Value) < Oscillator3ModeLabels.count else {
                patchCtrl.CurrentPatch.osc3_Model = 0
                patchCtrl.OscPatch.osc3_Model = 0
                break
            }
            patchCtrl.CurrentPatch.osc3_Model = Int(Value)
            patchCtrl.OscPatch.osc3_Model = Int(Value)
        case  42 :
            patchCtrl.CurrentPatch.osc3_Volume = Int(Value)
            patchCtrl.OscPatch.osc3_Volume = Int(Value)
        case  43 :
            patchCtrl.CurrentPatch.osc3_DetuneInSemitones = Int(Value)
            patchCtrl.OscPatch.osc3_DetuneInSemitones = Int(Value)
        case  44 :
            patchCtrl.CurrentPatch.osc3_FineDetune = Int(Value)
            patchCtrl.OscPatch.osc3_FineDetune = Int(Value)
        case  45 :
            patchCtrl.CurrentPatch.eq_LowFrequency = Int(Value)
            patchCtrl.CharacterPatch.eq_LowFrequency = Int(Value)
        case  46 :
            patchCtrl.CurrentPatch.eq_HighFrequency = Int(Value)
            patchCtrl.CharacterPatch.eq_HighFrequency = Int(Value)
        case  47 :
            patchCtrl.CurrentPatch.velocity_Osc1WaveformShape = Int(Value)
            patchCtrl.ControlPatch.velocity_Osc1WaveformShape = Int(Value)
        case  48 :
            patchCtrl.CurrentPatch.velocity_Osc2WaveformShape = Int(Value)
            patchCtrl.ControlPatch.velocity_Osc2WaveformShape = Int(Value)
        case  49 :
            patchCtrl.CurrentPatch.velocity_OscPulsewidth = Int(Value)
            patchCtrl.ControlPatch.velocity_OscPulsewidth = Int(Value)
        case  50 :
            patchCtrl.CurrentPatch.velocity_OscFMAmount = Int(Value)
            patchCtrl.ControlPatch.velocity_OscFMAmount = Int(Value)
        case 51:
            patchCtrl.CurrentPatch.control_Softknob1Name = Int(Value)
            patchCtrl.ControlPatch.control_Softknob1Name = Int(Value)
        case 52:
            patchCtrl.CurrentPatch.control_Softknob2Name = Int(Value)
            patchCtrl.ControlPatch.control_Softknob2Name = Int(Value)
        case 53:
            patchCtrl.CurrentPatch.control_Softknob3Name = Int(Value)
            patchCtrl.ControlPatch.control_Softknob3Name = Int(Value)
        case  54 :
            patchCtrl.CurrentPatch.velocity_Filter1EnvAmount = Int(Value)
            patchCtrl.ControlPatch.velocity_Filter1EnvAmount = Int(Value)
        case  55 :
            patchCtrl.CurrentPatch.velocity_Filter2EnvAmount = Int(Value)
            patchCtrl.ControlPatch.velocity_Filter2EnvAmount = Int(Value)
        case  56 :
            patchCtrl.CurrentPatch.velocity_Filter1Resonance = Int(Value)
            patchCtrl.ControlPatch.velocity_Filter1Resonance = Int(Value)
        case  57 :
            patchCtrl.CurrentPatch.velocity_Filter2Resonance = Int(Value)
            patchCtrl.ControlPatch.velocity_Filter2Resonance = Int(Value)
        case  58 :
            patchCtrl.CurrentPatch.osc_SectionSurroundSoundBalance = Int(Value)
            patchCtrl.OscPatch.osc_SectionSurroundSoundBalance = Int(Value)
        case  60 :
            patchCtrl.CurrentPatch.velocity_Volume = Int(Value)
            patchCtrl.ControlPatch.velocity_Volume = Int(Value)
        case  61 :
            patchCtrl.CurrentPatch.velocity_Panorama = Int(Value)
            patchCtrl.ControlPatch.velocity_Panorama = Int(Value)
        case  62 :
            patchCtrl.CurrentPatch.control_Softknob1Destination = Int(Value)
            patchCtrl.ControlPatch.control_Softknob1Destination = Int(Value)
        case  63 :
            patchCtrl.CurrentPatch.control_Softknob2Destination = Int(Value)
            patchCtrl.ControlPatch.control_Softknob2Destination = Int(Value)
        case  64 :
            patchCtrl.CurrentPatch.matrixSlot1_Source = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot1_Source = Int(Value)
        case  65 :
            patchCtrl.CurrentPatch.matrixSlot1_Destination1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot1_Destination1 = Int(Value)
        case  66 :
            patchCtrl.CurrentPatch.matrixSlot1_Amount1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot1_Amount1 = Int(Value)
        case  67 :
            patchCtrl.CurrentPatch.matrixSlot2_Source = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot2_Source = Int(Value)
        case  68 :
            patchCtrl.CurrentPatch.matrixSlot2_Destination1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot2_Destination1 = Int(Value)
        case  69 :
            patchCtrl.CurrentPatch.matrixSlot2_Amount1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot2_Amount1 = Int(Value)
        case  70 :
            patchCtrl.CurrentPatch.matrixSlot2_Destination2 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot2_Destination2 = Int(Value)
        case  71 :
            patchCtrl.CurrentPatch.matrixSlot2_Amount2 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot2_Amount2 = Int(Value)
        case  72 :
            patchCtrl.CurrentPatch.matrixSlot3_Source = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot3_Source = Int(Value)
        case  73 :
            patchCtrl.CurrentPatch.matrixSlot3_Destination1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot3_Destination1 = Int(Value)
        case  74 :
            patchCtrl.CurrentPatch.matrixSlot3_Amount1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot3_Amount1 = Int(Value)
        case  75 :
            patchCtrl.CurrentPatch.matrixSlot3_Destination2 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot3_Destination2 = Int(Value)
        case  76 :
            patchCtrl.CurrentPatch.matrixSlot3_Amount2 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot3_Amount2 = Int(Value)
        case  77 :
            patchCtrl.CurrentPatch.matrixSlot3_Destination3 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot3_Destination3 = Int(Value)
        case  78 :
            patchCtrl.CurrentPatch.matrixSlot3_Amount3 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot3_Amount3 = Int(Value)
        case  79 :
            patchCtrl.CurrentPatch.lfo1_UserDestination = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_UserDestination = Int(Value)
        case  80 :
            patchCtrl.CurrentPatch.lfo1_UserDestinationAmount = Int(Value)
            patchCtrl.ModulatorPatch.lfo1_UserDestinationAmount = Int(Value)
        case  81 :
            patchCtrl.CurrentPatch.lfo2_UserDestination = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_UserDestination = Int(Value)
        case  82 :
            patchCtrl.CurrentPatch.lfo2_UserDestinationAmount = Int(Value)
            patchCtrl.ModulatorPatch.lfo2_UserDestinationAmount = Int(Value)
        case  84 :
            patchCtrl.CurrentPatch.phaser_Stages = Int(Value)
            patchCtrl.CharacterPatch.phaser_Stages = Int(Value)
        case  85 :
            patchCtrl.CurrentPatch.phaser_Mix = Int(Value)
            patchCtrl.CharacterPatch.phaser_Mix = Int(Value)
        case  86 :
            patchCtrl.CurrentPatch.phaser_LFORate = Int(Value)
            patchCtrl.CharacterPatch.phaser_LFORate = Int(Value)
        case  87 :
            patchCtrl.CurrentPatch.phaser_Depth = Int(Value)
            patchCtrl.CharacterPatch.phaser_Depth = Int(Value)
        case  88 :
            patchCtrl.CurrentPatch.phaser_Frequency = Int(Value)
            patchCtrl.CharacterPatch.phaser_Frequency = Int(Value)
        case  89 :
            patchCtrl.CurrentPatch.phaser_Feedback = Int(Value)
            patchCtrl.CharacterPatch.phaser_Feedback = Int(Value)
        case  90 :
            patchCtrl.CurrentPatch.phaser_Spread = Int(Value)
            patchCtrl.CharacterPatch.phaser_Spread = Int(Value)

        case  92 :
            patchCtrl.CurrentPatch.eq_MidGain = Int(Value)
            patchCtrl.CharacterPatch.eq_MidGain = Int(Value)
        case  93 :
            patchCtrl.CurrentPatch.eq_MidFrequency = Int(Value)
            patchCtrl.CharacterPatch.eq_MidFrequency = Int(Value)
        case  94 :
            patchCtrl.CurrentPatch.eq_MidQFactor = Int(Value)
            patchCtrl.CharacterPatch.eq_MidQFactor = Int(Value)
        case  95 :
            patchCtrl.CurrentPatch.eq_LowGain = Int(Value)
            patchCtrl.CharacterPatch.eq_LowGain = Int(Value)
        case  96 :
            patchCtrl.CurrentPatch.eq_HighGain = Int(Value)
            patchCtrl.CharacterPatch.eq_HighGain = Int(Value)
        case  97 :
            patchCtrl.CurrentPatch.character_Intensity = Int(Value)
            patchCtrl.CharacterPatch.character_Intensity = Int(Value)
        case  98 :
            patchCtrl.CurrentPatch.character_Tune = Int(Value)
            patchCtrl.CharacterPatch.character_Tune = Int(Value)
        case  99 :
            patchCtrl.CurrentPatch.osc_RingModulationMix = Int(Value)
            patchCtrl.OscPatch.osc_RingModulationMix = Int(Value)
        case  100 :
            patchCtrl.CurrentPatch.distortion_Type = Int(Value)
            patchCtrl.CharacterPatch.distortion_Type = Int(Value)
        case  101 :
            patchCtrl.CurrentPatch.distortion_Intensity = Int(Value)
            patchCtrl.CharacterPatch.distortion_Intensity = Int(Value)
        case  103 :
            patchCtrl.CurrentPatch.matrixSlot4_Source = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot4_Source = Int(Value)
        case  104 :
            patchCtrl.CurrentPatch.matrixSlot4_Destination1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot4_Destination1 = Int(Value)
        case  105 :
            patchCtrl.CurrentPatch.matrixSlot4_Amount1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot4_Amount1 = Int(Value)
        case  106 :
            patchCtrl.CurrentPatch.matrixSlot5_Source = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot5_Source = Int(Value)
        case  107 :
            patchCtrl.CurrentPatch.matrixSlot5_Destination1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot5_Destination1 = Int(Value)
        case  108 :
            patchCtrl.CurrentPatch.matrixSlot5_Amount1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot5_Amount1 = Int(Value)
        case  109 :
            patchCtrl.CurrentPatch.matrixSlot6_Source = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot6_Source = Int(Value)
        case  110 :
            patchCtrl.CurrentPatch.matrixSlot6_Destination1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot6_Destination1 = Int(Value)
        case  111 :
            patchCtrl.CurrentPatch.matrixSlot6_Amount1 = Int(Value)
            patchCtrl.ModulatorPatch.matrixSlot6_Amount1 = Int(Value)
        case 112:
            patchCtrl.CurrentPatch.patchname_Character1 = Int(Value)
        case 113:
            patchCtrl.CurrentPatch.patchname_Character2 = Int(Value)
        case 114:
            patchCtrl.CurrentPatch.patchname_Character3 = Int(Value)
        case 115:
            patchCtrl.CurrentPatch.patchname_Character4 = Int(Value)
        case 116:
            patchCtrl.CurrentPatch.patchname_Character5 = Int(Value)
        case 117:
            patchCtrl.CurrentPatch.patchname_Character6 = Int(Value)
        case 118:
            patchCtrl.CurrentPatch.patchname_Character7 = Int(Value)
        case 119:
            patchCtrl.CurrentPatch.patchname_Character8 = Int(Value)
        case 120:
            patchCtrl.CurrentPatch.patchname_Character9 = Int(Value)
        case 121:
            patchCtrl.CurrentPatch.patchname_Character10 = Int(Value)
        case 122:
            patchCtrl.CurrentPatch.filter_Link = Int(Value)
            patchCtrl.FilterPatch.filter_Link = Int(Value)
        case  123 :
            // recalculate value to alfabetically ordered list
            var VirusValue = 0
            if (Int(Value) <= 16){
                let TempCat = String(presetCategoriesArray[safe: Int(Value)]!)
                VirusValue = presetCategoriesUI.lastIndex(of: TempCat)!
            } else {
                VirusValue = 16
            }

            patchCtrl.CurrentPatch.category1 = VirusValue
        case  124 :
            // recalculate value to alfabetically ordered list
            var VirusValue = 0
            if (Int(Value) <= 16){
                let TempCat = String(presetCategoriesArray[safe: Int(Value)]!)
                VirusValue = presetCategoriesUI.lastIndex(of: TempCat)!
            } else {
                VirusValue = 16
            }
            patchCtrl.CurrentPatch.category2 = VirusValue
            
        default:
            print ("MIDIHANDLER ,unknown PAGE 2 parameter: ",Parameter," : ",Value)
        }
    }
    
    
    
    func SetPage3Parameter (Parameter: Int, Value: Int) {
        //print ("MIDIHANDLER ,PAGE 3 ,",Parameter," ,",Value)
            switch Parameter {
            case 1:
                patchCtrl.CurrentPatch.reverb_Mode = Int(Value)
                patchCtrl.EffectsPatch.reverb_Mode = Int(Value)
            case 2:
                patchCtrl.CurrentPatch.reverb_Send = Int(Value)
                patchCtrl.EffectsPatch.reverb_Send = Int(Value)
            case 3:
                patchCtrl.CurrentPatch.reverb_Type = Int(Value)
                patchCtrl.EffectsPatch.reverb_Type = Int(Value)
            case 4:
                patchCtrl.CurrentPatch.reverb_Time = Int(Value)
                patchCtrl.EffectsPatch.reverb_Time = Int(Value)
            case 5:
                patchCtrl.CurrentPatch.reverb_Damping = Int(Value)
                patchCtrl.EffectsPatch.reverb_Damping = Int(Value)
            case 6:
                patchCtrl.CurrentPatch.reverb_Color = Int(Value)
                patchCtrl.EffectsPatch.reverb_Color = Int(Value)
            case 7:
                patchCtrl.CurrentPatch.reverb_Predelay = Int(Value)
                patchCtrl.EffectsPatch.reverb_Predelay = Int(Value)
            case 8:
                patchCtrl.CurrentPatch.reverb_Clock = Int(Value)
                patchCtrl.EffectsPatch.reverb_Clock = Int(Value)
            case 9:
                patchCtrl.CurrentPatch.reverb_Feedback = Int(Value)
                patchCtrl.EffectsPatch.reverb_Feedback = Int(Value)
            case 10:
                patchCtrl.CurrentPatch.delay_Type = Int(Value)
                patchCtrl.EffectsPatch.delay_Type = Int(Value)
            case 12:
                patchCtrl.CurrentPatch.delay_TapeDelayRatio = Int(Value)
                patchCtrl.EffectsPatch.delay_TapeDelayRatio = Int(Value)
            case 13:
                patchCtrl.CurrentPatch.delay_TapeDelayClockLeft = Int(Value)
                patchCtrl.EffectsPatch.delay_TapeDelayClockLeft = Int(Value)
            case 14:
                patchCtrl.CurrentPatch.delay_TapeDelayClockRight = Int(Value)
                patchCtrl.EffectsPatch.delay_TapeDelayClockRight = Int(Value)
            case 17:
                patchCtrl.CurrentPatch.delay_TapeDelayBandwidth = Int(Value)
                patchCtrl.EffectsPatch.delay_TapeDelayBandwidth = Int(Value)
            case 19:
                patchCtrl.CurrentPatch.frequencyShifter_Type = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_Type = Int(Value)
            case 20:
                patchCtrl.CurrentPatch.frequencyShifter_Mix = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_Mix = Int(Value)
            case 21:
                patchCtrl.CurrentPatch.frequencyShifter_Frequency = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_Frequency = Int(Value)
            case 22:
                patchCtrl.CurrentPatch.frequencyShifter_StereoPhase = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_StereoPhase = Int(Value)
            case 23:
                patchCtrl.CurrentPatch.frequencyShifter_LeftShape = Int(Value)
                patchCtrl.CurrentPatch.frequencyShifter_FilterType = Int(Value)
                patchCtrl.CurrentPatch.frequencyShifter_Poles = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_LeftShape = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_FilterType = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_Poles = Int(Value)
            case 24:
                patchCtrl.CurrentPatch.frequencyShifter_RightShape = Int(Value)
                patchCtrl.CurrentPatch.frequencyShifter_Slope = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_RightShape = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_Slope = Int(Value)
            case 25:
                patchCtrl.CurrentPatch.frequencyShifter_Resonance = Int(Value)
                patchCtrl.CharacterPatch.frequencyShifter_Resonance = Int(Value)
            case 26:
                patchCtrl.CurrentPatch.character_Type = Int(Value)
                patchCtrl.CharacterPatch.character_Type = Int(Value)
            case 28:
                patchCtrl.CurrentPatch.control_Softknob3Destination = Int(Value)
                patchCtrl.ControlPatch.control_Softknob3Destination = Int(Value)
            case 30:
                if (Int(Value) <= 7){
                    patchCtrl.CurrentPatch.osc1_Model = Int(Value)
                    patchCtrl.OscPatch.osc1_Model = Int(Value)
                } else {
                    patchCtrl.CurrentPatch.osc1_Model = Int(0)
                    patchCtrl.OscPatch.osc1_Model = Int(0)
                }
            case 35:
                if (Int(Value) <= 7){
                    patchCtrl.CurrentPatch.osc2_Model = Int(Value)
                    patchCtrl.OscPatch.osc2_Model = Int(Value)
                } else {
                    patchCtrl.CurrentPatch.osc2_Model = Int(0)
                    patchCtrl.OscPatch.osc2_Model = Int(0)
                }

            case 37:
                patchCtrl.CurrentPatch.osc1_FormantSpread = Int(Value)
                patchCtrl.OscPatch.osc1_FormantSpread = Int(Value)
            case 42:
                patchCtrl.CurrentPatch.osc1_FormantShift = Int(Value)
                patchCtrl.OscPatch.osc1_FormantShift = Int(Value)
            case 43:
                patchCtrl.CurrentPatch.osc1_LocalDetune = Int(Value)
                patchCtrl.OscPatch.osc1_LocalDetune = Int(Value)
            case 44:
                patchCtrl.CurrentPatch.osc1_Interpolation = Int(Value)
                patchCtrl.OscPatch.osc1_Interpolation = Int(Value)
            case 57:
                patchCtrl.CurrentPatch.osc2_FormantSpread = Int(Value)
                patchCtrl.OscPatch.osc2_FormantSpread = Int(Value)
            case 62:
                patchCtrl.CurrentPatch.osc2_FormantShift = Int(Value)
                patchCtrl.OscPatch.osc2_FormantShift = Int(Value)
            case 63:
                patchCtrl.CurrentPatch.osc2_LocalDetune = Int(Value)
                patchCtrl.OscPatch.osc2_LocalDetune = Int(Value)
            case 64:
                patchCtrl.CurrentPatch.osc2_Interpolation = Int(Value)
                patchCtrl.OscPatch.osc2_Interpolation = Int(Value)
            case 70:
                patchCtrl.CurrentPatch.distortion_TrebleBooster = Int(Value)
                patchCtrl.CharacterPatch.distortion_TrebleBooster = Int(Value)
            case 71:
                patchCtrl.CurrentPatch.distortion_HighCut = Int(Value)
                patchCtrl.CharacterPatch.distortion_HighCut = Int(Value)
            case 72:
                patchCtrl.CurrentPatch.distortion_Mix = Int(Value)
                patchCtrl.CharacterPatch.distortion_Mix = Int(Value)
            case 73:
                patchCtrl.CurrentPatch.distortion_Quality = Int(Value)
                patchCtrl.CharacterPatch.distortion_Quality = Int(Value)
            case 74:
                patchCtrl.CurrentPatch.distortion_ToneW = Int(Value)
                patchCtrl.CharacterPatch.distortion_ToneW = Int(Value)
            case 80:
                patchCtrl.CurrentPatch.envelope3_Attack = Int(Value)
                patchCtrl.ModulatorPatch.envelope3_Attack = Int(Value)
            case 81:
                patchCtrl.CurrentPatch.envelope3_Decay = Int(Value)
                patchCtrl.ModulatorPatch.envelope3_Decay = Int(Value)
            case 82:
                patchCtrl.CurrentPatch.envelope3_Sustain = Int(Value)
                patchCtrl.ModulatorPatch.envelope3_Sustain = Int(Value)
            case 83:
                patchCtrl.CurrentPatch.envelope3_SustainSlope = Int(Value)
                patchCtrl.ModulatorPatch.envelope3_SustainSlope = Int(Value)
            case 84:
                patchCtrl.CurrentPatch.envelope3_Release = Int(Value)
                patchCtrl.ModulatorPatch.envelope3_Release = Int(Value)
            case 85:
                patchCtrl.CurrentPatch.envelope4_Attack = Int(Value)
                patchCtrl.ModulatorPatch.envelope4_Attack = Int(Value)
            case 86:
                patchCtrl.CurrentPatch.envelope4_Decay = Int(Value)
                patchCtrl.ModulatorPatch.envelope4_Decay = Int(Value)
            case 87:
                patchCtrl.CurrentPatch.envelope4_Sustain = Int(Value)
                patchCtrl.ModulatorPatch.envelope4_Sustain = Int(Value)
            case 88:
                patchCtrl.CurrentPatch.envelope4_SustainSlope = Int(Value)
                patchCtrl.ModulatorPatch.envelope4_SustainSlope = Int(Value)
            case 89:
                patchCtrl.CurrentPatch.envelope4_Release = Int(Value)
                patchCtrl.ModulatorPatch.envelope4_Release = Int(Value)
            case 90:
                patchCtrl.CurrentPatch.matrixSlot1_Destination2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot1_Destination2 = Int(Value)
            case 91:
                patchCtrl.CurrentPatch.matrixSlot1_Amount2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot1_Amount2 = Int(Value)
            case 92:
                patchCtrl.CurrentPatch.matrixSlot1_Destination3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot1_Destination3 = Int(Value)
            case 93:
                patchCtrl.CurrentPatch.matrixSlot1_Amount3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot1_Amount3 = Int(Value)
            case 94:
                patchCtrl.CurrentPatch.matrixSlot2_Destination3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot2_Destination3 = Int(Value)
            case 95:
                patchCtrl.CurrentPatch.matrixSlot2_Amount3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot2_Amount3 = Int(Value)
            case 96:
                patchCtrl.CurrentPatch.matrixSlot4_Destination2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot4_Destination2 = Int(Value)
            case 97:
                patchCtrl.CurrentPatch.matrixSlot4_Amount2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot4_Amount2 = Int(Value)
            case 98:
                patchCtrl.CurrentPatch.matrixSlot4_Destination3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot4_Destination3 = Int(Value)
            case 99:
                patchCtrl.CurrentPatch.matrixSlot4_Amount3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot4_Amount3 = Int(Value)
            case 100:
                patchCtrl.CurrentPatch.matrixSlot5_Destination2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot5_Destination2 = Int(Value)
            case 101:
                patchCtrl.CurrentPatch.matrixSlot5_Amount2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot5_Amount2 = Int(Value)
            case 102:
                patchCtrl.CurrentPatch.matrixSlot5_Destination3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot5_Destination3 = Int(Value)
            case 103:
                patchCtrl.CurrentPatch.matrixSlot5_Amount3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot5_Amount3 = Int(Value)
            case 104:
                patchCtrl.CurrentPatch.matrixSlot6_Destination2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot6_Destination2 = Int(Value)
            case 105:
                patchCtrl.CurrentPatch.matrixSlot6_Amount2 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot6_Amount2 = Int(Value)
            case 106:
                patchCtrl.CurrentPatch.matrixSlot6_Destination3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot6_Destination3 = Int(Value)
            case 107:
                patchCtrl.CurrentPatch.matrixSlot6_Amount3 = Int(Value)
                patchCtrl.ModulatorPatch.matrixSlot6_Amount3 = Int(Value)
            //case 108: break
            //case 109: break
            //case 110: break   $_LFO 1/BackupShape
            //case 111: break   $_LFO 2/BackupShape
            //case 112: break   $_LFO 3/BackupShape
            //case 113: break
            //case 114: break
            //case 115: break   $_Assigns/Slot Select
            //case 116: break   $_LFOs/Select
            //case 117: break   $_FX Upper/Select
            //case 118: break   $_FX Lower/Select
            //case 119: break
            //case 120: break
            //case 121: break
            //case 122: break   $_Oscillators/BackupKeyMode
            //case 123: break   $_Arpeggiator/BackupMode
            //case 124: break   $_Oscillator 3/BackupMode
            //case 125: break
            //case 126: break
            case 127:
                patchCtrl.CurrentPatch.arp_patternLength = Int(Value)
                patchCtrl.ArpPatch.arp_patternLength = Int(Value)
            default:
                print ("MIDIHANDLER ,unknown PAGE 3 parameter: ",Parameter," : ",Value)
            }
    }
    
    func SetPage4Parameter (Parameter: Int, Value: Int) {
        //print ("MIDIHANDLER ,PAGE 4 ,",Parameter," ,",Value)

            switch Parameter {
            case 1:
                patchCtrl.CurrentPatch.arp_stepLength_1 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_1 = Int(Value)
            case 2:
                patchCtrl.CurrentPatch.arp_stepVelocity_1 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_1 = Int(Value)
            case 3:
                patchCtrl.CurrentPatch.arp_step_1 = Int(Value)
                patchCtrl.ArpPatch.arp_step_1 = Int(Value)
                
            case 4:
                patchCtrl.CurrentPatch.arp_stepLength_2 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_2 = Int(Value)
            case 5:
                patchCtrl.CurrentPatch.arp_stepVelocity_2 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_2 = Int(Value)
            case 6:
                patchCtrl.CurrentPatch.arp_step_2 = Int(Value)
                patchCtrl.ArpPatch.arp_step_2 = Int(Value)
                
            case 7:
                patchCtrl.CurrentPatch.arp_stepLength_3 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_3 = Int(Value)
            case 8:
                patchCtrl.CurrentPatch.arp_stepVelocity_3 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_3 = Int(Value)
            case 9:
                patchCtrl.CurrentPatch.arp_step_3 = Int(Value)
                patchCtrl.ArpPatch.arp_step_3 = Int(Value)
                
            case 10:
                patchCtrl.CurrentPatch.arp_stepLength_4 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_4 = Int(Value)
            case 11:
                patchCtrl.CurrentPatch.arp_stepVelocity_4 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_4 = Int(Value)
            case 12:
                patchCtrl.CurrentPatch.arp_step_4 = Int(Value)
                patchCtrl.ArpPatch.arp_step_4 = Int(Value)
                
            case 13:
                patchCtrl.CurrentPatch.arp_stepLength_5 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_5 = Int(Value)
            case 14:
                patchCtrl.CurrentPatch.arp_stepVelocity_5 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_5 = Int(Value)
            case 15:
                patchCtrl.CurrentPatch.arp_step_5 = Int(Value)
                patchCtrl.ArpPatch.arp_step_5 = Int(Value)
                
            case 16:
                patchCtrl.CurrentPatch.arp_stepLength_6 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_6 = Int(Value)
            case 17:
                patchCtrl.CurrentPatch.arp_stepVelocity_6 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_6 = Int(Value)
            case 18:
                patchCtrl.CurrentPatch.arp_step_6 = Int(Value)
                patchCtrl.ArpPatch.arp_step_6 = Int(Value)
                
            case 19:
                patchCtrl.CurrentPatch.arp_stepLength_7 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_7 = Int(Value)
            case 20:
                patchCtrl.CurrentPatch.arp_stepVelocity_7 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_7 = Int(Value)
            case 21:
                patchCtrl.CurrentPatch.arp_step_7 = Int(Value)
                patchCtrl.ArpPatch.arp_step_7 = Int(Value)
                
            case 22:
                patchCtrl.CurrentPatch.arp_stepLength_8 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_8 = Int(Value)
            case 23:
                patchCtrl.CurrentPatch.arp_stepVelocity_8 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_8 = Int(Value)
            case 24:
                patchCtrl.CurrentPatch.arp_step_8 = Int(Value)
                patchCtrl.ArpPatch.arp_step_8 = Int(Value)
                
            case 25:
                patchCtrl.CurrentPatch.arp_stepLength_9 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_9 = Int(Value)
            case 26:
                patchCtrl.CurrentPatch.arp_stepVelocity_9 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_9 = Int(Value)
            case 27:
                patchCtrl.CurrentPatch.arp_step_9 = Int(Value)
                patchCtrl.ArpPatch.arp_step_9 = Int(Value)
                
            case 28:
                patchCtrl.CurrentPatch.arp_stepLength_10 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_10 = Int(Value)
            case 29:
                patchCtrl.CurrentPatch.arp_stepVelocity_10 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_10 = Int(Value)
            case 30:
                patchCtrl.CurrentPatch.arp_step_10 = Int(Value)
                patchCtrl.ArpPatch.arp_step_10 = Int(Value)
                
            case 31:
                patchCtrl.CurrentPatch.arp_stepLength_11 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_11 = Int(Value)
            case 32:
                patchCtrl.CurrentPatch.arp_stepVelocity_11 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_11 = Int(Value)
            case 33:
                patchCtrl.CurrentPatch.arp_step_11 = Int(Value)
                patchCtrl.ArpPatch.arp_step_11 = Int(Value)
                
            case 34:
                patchCtrl.CurrentPatch.arp_stepLength_12 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_12 = Int(Value)
            case 35:
                patchCtrl.CurrentPatch.arp_stepVelocity_12 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_12 = Int(Value)
            case 36:
                patchCtrl.CurrentPatch.arp_step_12 = Int(Value)
                patchCtrl.ArpPatch.arp_step_12 = Int(Value)
                
            case 37:
                patchCtrl.CurrentPatch.arp_stepLength_13 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_13 = Int(Value)
            case 38:
                patchCtrl.CurrentPatch.arp_stepVelocity_13 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_13 = Int(Value)
            case 39:
                patchCtrl.CurrentPatch.arp_step_13 = Int(Value)
                patchCtrl.ArpPatch.arp_step_13 = Int(Value)
                
            case 40:
                patchCtrl.CurrentPatch.arp_stepLength_14 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_14 = Int(Value)
            case 41:
                patchCtrl.CurrentPatch.arp_stepVelocity_14 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_14 = Int(Value)
            case 42:
                patchCtrl.CurrentPatch.arp_step_14 = Int(Value)
                patchCtrl.ArpPatch.arp_step_14 = Int(Value)
                
            case 43:
                patchCtrl.CurrentPatch.arp_stepLength_15 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_15 = Int(Value)
            case 44:
                patchCtrl.CurrentPatch.arp_stepVelocity_15 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_15 = Int(Value)
            case 45:
                patchCtrl.CurrentPatch.arp_step_15 = Int(Value)
                patchCtrl.ArpPatch.arp_step_15 = Int(Value)
                
            case 46:
                patchCtrl.CurrentPatch.arp_stepLength_16 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_16 = Int(Value)
            case 47:
                patchCtrl.CurrentPatch.arp_stepVelocity_16 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_16 = Int(Value)
            case 48:
                patchCtrl.CurrentPatch.arp_step_16 = Int(Value)
                patchCtrl.ArpPatch.arp_step_16 = Int(Value)
                
            case 49:
                patchCtrl.CurrentPatch.arp_stepLength_17 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_17 = Int(Value)
            case 50:
                patchCtrl.CurrentPatch.arp_stepVelocity_17 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_17 = Int(Value)
            case 51:
                patchCtrl.CurrentPatch.arp_step_17 = Int(Value)
                patchCtrl.ArpPatch.arp_step_17 = Int(Value)
                
            case 52:
                patchCtrl.CurrentPatch.arp_stepLength_18 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_18 = Int(Value)
            case 53:
                patchCtrl.CurrentPatch.arp_stepVelocity_18 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_18 = Int(Value)
            case 54:
                patchCtrl.CurrentPatch.arp_step_18 = Int(Value)
                patchCtrl.ArpPatch.arp_step_18 = Int(Value)
                
            case 55:
                patchCtrl.CurrentPatch.arp_stepLength_19 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_19 = Int(Value)
            case 56:
                patchCtrl.CurrentPatch.arp_stepVelocity_19 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_19 = Int(Value)
            case 57:
                patchCtrl.CurrentPatch.arp_step_19 = Int(Value)
                patchCtrl.ArpPatch.arp_step_19 = Int(Value)
                
            case 58:
                patchCtrl.CurrentPatch.arp_stepLength_20 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_20 = Int(Value)
            case 59:
                patchCtrl.CurrentPatch.arp_stepVelocity_20 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_20 = Int(Value)
            case 60:
                patchCtrl.CurrentPatch.arp_step_20 = Int(Value)
                patchCtrl.ArpPatch.arp_step_20 = Int(Value)
                
            case 61:
                patchCtrl.CurrentPatch.arp_stepLength_21 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_21 = Int(Value)
            case 62:
                patchCtrl.CurrentPatch.arp_stepVelocity_21 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_21 = Int(Value)
            case 63:
                patchCtrl.CurrentPatch.arp_step_21 = Int(Value)
                patchCtrl.ArpPatch.arp_step_21 = Int(Value)
                
            case 64:
                patchCtrl.CurrentPatch.arp_stepLength_22 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_22 = Int(Value)
            case 65:
                patchCtrl.CurrentPatch.arp_stepVelocity_22 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_22 = Int(Value)
            case 66:
                patchCtrl.CurrentPatch.arp_step_22 = Int(Value)
                patchCtrl.ArpPatch.arp_step_22 = Int(Value)
                
            case 67:
                patchCtrl.CurrentPatch.arp_stepLength_23 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_23 = Int(Value)
            case 68:
                patchCtrl.CurrentPatch.arp_stepVelocity_23 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_23 = Int(Value)
            case 69:
                patchCtrl.CurrentPatch.arp_step_23 = Int(Value)
                patchCtrl.ArpPatch.arp_step_23 = Int(Value)
                
            case 70:
                patchCtrl.CurrentPatch.arp_stepLength_24 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_24 = Int(Value)
            case 71:
                patchCtrl.CurrentPatch.arp_stepVelocity_24 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_24 = Int(Value)
            case 72:
                patchCtrl.CurrentPatch.arp_step_24 = Int(Value)
                patchCtrl.ArpPatch.arp_step_24 = Int(Value)
                
            case 73:
                patchCtrl.CurrentPatch.arp_stepLength_25 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_25 = Int(Value)
            case 74:
                patchCtrl.CurrentPatch.arp_stepVelocity_25 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_25 = Int(Value)
            case 75:
                patchCtrl.CurrentPatch.arp_step_25 = Int(Value)
                patchCtrl.ArpPatch.arp_step_25 = Int(Value)
                
            case 76:
                patchCtrl.CurrentPatch.arp_stepLength_26 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_26 = Int(Value)
            case 77:
                patchCtrl.CurrentPatch.arp_stepVelocity_26 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_26 = Int(Value)
            case 78:
                patchCtrl.CurrentPatch.arp_step_26 = Int(Value)
                patchCtrl.ArpPatch.arp_step_26 = Int(Value)
                
            case 79:
                patchCtrl.CurrentPatch.arp_stepLength_27 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_27 = Int(Value)
            case 80:
                patchCtrl.CurrentPatch.arp_stepVelocity_27 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_27 = Int(Value)
            case 81:
                patchCtrl.CurrentPatch.arp_step_27 = Int(Value)
                patchCtrl.ArpPatch.arp_step_27 = Int(Value)
                
            case 82:
                patchCtrl.CurrentPatch.arp_stepLength_28 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_28 = Int(Value)
            case 83:
                patchCtrl.CurrentPatch.arp_stepVelocity_28 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_28 = Int(Value)
            case 84:
                patchCtrl.CurrentPatch.arp_step_28 = Int(Value)
                patchCtrl.ArpPatch.arp_step_28 = Int(Value)
                
            case 85:
                patchCtrl.CurrentPatch.arp_stepLength_29 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_29 = Int(Value)
            case 86:
                patchCtrl.CurrentPatch.arp_stepVelocity_29 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_29 = Int(Value)
            case 87:
                patchCtrl.CurrentPatch.arp_step_29 = Int(Value)
                patchCtrl.ArpPatch.arp_step_29 = Int(Value)
                
            case 88:
                patchCtrl.CurrentPatch.arp_stepLength_30 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_30 = Int(Value)
            case 89:
                patchCtrl.CurrentPatch.arp_stepVelocity_30 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_30 = Int(Value)
            case 90:
                patchCtrl.CurrentPatch.arp_step_30 = Int(Value)
                patchCtrl.ArpPatch.arp_step_30 = Int(Value)
                
            case 91:
                patchCtrl.CurrentPatch.arp_stepLength_31 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_31 = Int(Value)
            case 92:
                patchCtrl.CurrentPatch.arp_stepVelocity_31 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_31 = Int(Value)
            case 93:
                patchCtrl.CurrentPatch.arp_step_31 = Int(Value)
                patchCtrl.ArpPatch.arp_step_31 = Int(Value)
                
            case 94:
                patchCtrl.CurrentPatch.arp_stepLength_32 = Int(Value)
                patchCtrl.ArpPatch.arp_stepLength_32 = Int(Value)
            case 95:
                patchCtrl.CurrentPatch.arp_stepVelocity_32 = Int(Value)
                patchCtrl.ArpPatch.arp_stepVelocity_32 = Int(Value)
            case 96:
                patchCtrl.CurrentPatch.arp_step_32 = Int(Value)
                patchCtrl.ArpPatch.arp_step_32 = Int(Value)
            //case 97: break
            //case 98: break
            //case 99: break
            //case 100: break
            //case 101: break
            //case 102: break
            //case 103: break
            //case 104: break
            //case 105: break
            //case 106: break
            //case 107: break
            //case 108: break
            //case 109: break
            //case 110: break
            //case 111: break
            //case 112: break
            //case 113: break
            //case 114: break
            //case 115: break
            //case 116: break
            //case 117: break
            //case 118: break
            //case 119: break
            case 120:
                
                guard Int(Value) <= 7 else {
                    patchCtrl.CurrentPatch.osc_Unison_Mode =  0
                    patchCtrl.OscPatch.osc_Unison_Mode = 0
                    break
                }
                patchCtrl.CurrentPatch.osc_Unison_Mode = Int(Value)
                patchCtrl.OscPatch.osc_Unison_Mode = Int(Value)
            case 121:
                patchCtrl.CurrentPatch.osc_Unison_Detune = Int(Value)
                patchCtrl.OscPatch.osc_Unison_Detune = Int(Value)
            case 122:
                patchCtrl.CurrentPatch.osc_Unison_PanoramaSpread = Int(Value)
                patchCtrl.OscPatch.osc_Unison_PanoramaSpread = Int(Value)
            case 123:
                patchCtrl.CurrentPatch.osc_Unison_LFOPhaseOffset = Int(Value)
                patchCtrl.OscPatch.osc_Unison_LFOPhaseOffset = Int(Value)
            case 124:
                patchCtrl.CurrentPatch.input_Mode = Int(Value)
                patchCtrl.CharacterPatch.input_Mode = Int(Value)
            case 125:
                patchCtrl.CurrentPatch.input_Select = Int(Value)
                patchCtrl.CharacterPatch.input_Select = Int(Value)
            case 126:
                patchCtrl.CurrentPatch.Atomizer = Int(Value)
                patchCtrl.CharacterPatch.Atomizer = Int(Value)
            //case 127: break
            default:
                print ("MIDIHANDLER ,unknown PAGE 4 parameter: ",Parameter," : ",Value)
            }
        
    }
    
    
    
    func translatePatchName(){
        var newName: String = ""
        
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character1 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character2 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character3 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character4 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character5 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character6 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character7 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character8 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character9 )
        newName += getCharacter( patchCtrl.CurrentPatch.patchname_Character10 )
        
        print("patch name: ",newName)
        
        //DispatchQueue.main.async {
            print("async name: ",newName)
            patchCtrl.CurrentPatch.name = newName
        
        
            NotificationCenter.default.post(name: Notification.Name( "UpdatePatchName" ), object: nil)
        //}
    }
    
}
