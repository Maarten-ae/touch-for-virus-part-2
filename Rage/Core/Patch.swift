//
//  Patch.swift
//  RAGE
//
//  Created by M. Lierop on 24-07-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class Patch: Object, Codable {

    // General
    @objc dynamic var name: String = "-INIT- Patch"
    @objc dynamic var id = 0
    @objc dynamic var creationDate: Date = Date(timeIntervalSince1970: 1)
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: Int = 0
    @objc dynamic var category2: Int = 0
    @objc dynamic var category3: Int = 0
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    @objc dynamic var descr: String = ""
    @objc dynamic var isFavorite: Bool = false
    @objc dynamic var lastUsage: Date = Date(timeIntervalSince1970: 1)
    @objc dynamic var usageCount: Int = 0
    
    @objc dynamic var patchname_Character1 : Int = 0
    @objc dynamic var patchname_Character2 : Int = 0
    @objc dynamic var patchname_Character3 : Int = 0
    @objc dynamic var patchname_Character4 : Int = 0
    @objc dynamic var patchname_Character5 : Int = 0
    @objc dynamic var patchname_Character6 : Int = 0
    @objc dynamic var patchname_Character7 : Int = 0
    @objc dynamic var patchname_Character8 : Int = 0
    @objc dynamic var patchname_Character9 : Int = 0
    @objc dynamic var patchname_Character10 : Int = 0
    
    // Oscilator 1
    @objc dynamic var osc1_Model: Int  = 0
    @objc dynamic var osc1_WaveformShape: Int = 0
    @objc dynamic var osc1_Pulsewidth: Int = 0
    @objc dynamic var osc1_WaveformSelect: Int = 0
    @objc dynamic var osc1_DetuneInSemitones: Int = 64
    @objc dynamic var osc1_Keyfollow: Int = 96
    @objc dynamic var osc1_FormantSpread: Int = 0
    @objc dynamic var osc1_FormantShift: Int = 64
    @objc dynamic var osc1_LocalDetune: Int = 0
    @objc dynamic var osc1_Interpolation: Int = 0
    
    // Oscilator 2
    @objc dynamic var osc2_Model: Int = 0
    @objc dynamic var osc2_WaveformShape: Int = 0
    @objc dynamic var osc2_Pulsewidth: Int = 0
    @objc dynamic var osc2_WaveformSelect: Int = 0
    @objc dynamic var osc2_DetuneInSemitones: Int = 64
    @objc dynamic var osc2_Keyfollow: Int = 96
    @objc dynamic var osc2_FineDetune: Int = 0
    @objc dynamic var osc2_FormantSpread: Int = 0
    @objc dynamic var osc2_FormantShift: Int = 64
    @objc dynamic var osc2_LocalDetune: Int = 0
    @objc dynamic var osc2_Interpolation: Int = 0
    
    @objc dynamic var osc1_osc2_Sync: Int = 0
    @objc dynamic var osc1_osc2_Balance: Int = 64
    
    // Oscilator 3
    @objc dynamic var osc3_Volume: Int = 127
    @objc dynamic var osc3_Model: Int = 0
    @objc dynamic var osc3_DetuneInSemitones: Int = 0
    @objc dynamic var osc3_FineDetune: Int = 0
    
    // Oscilator Sub
    @objc dynamic var oscS_Volume: Int = 0
    @objc dynamic var oscS_WaveformShape: Int = 0
    
    // Oscilator Noise
    @objc dynamic var oscN_Volume: Int = 0
    @objc dynamic var oscN_Color: Int = 64
    
    // Oscillator section
    @objc dynamic var osc_InitialPhase: Int = 0
    @objc dynamic var osc_Portamento: Int = 0
    @objc dynamic var osc_FMMode: Int = 0
    @objc dynamic var osc_FMAmount: Int = 0
    @objc dynamic var osc_PunchIntensity: Int = 0
    @objc dynamic var osc_RingModulationVolume: Int = 0
    @objc dynamic var osc_RingModulationMix: Int = 0
    @objc dynamic var osc_SectionVolume: Int = 64
    @objc dynamic var osc_SectionKeyboardMode: Int = 0  // niet in gebruik, veplaatst naar controls
    @objc dynamic var osc_SectionPatchTransposition = 64
    @objc dynamic var osc_SectionSurroundSoundBalance: Int = 64
    
    @objc dynamic var filterEnvelope_ToPitch:Int = 64
    @objc dynamic var filterEnvelope_ToFM:Int = 64
    
    @objc dynamic var osc_Unison_Mode: Int = 0
    @objc dynamic var osc_Unison_Detune: Int = 0
    @objc dynamic var osc_Unison_PanoramaSpread: Int = 0
    @objc dynamic var osc_Unison_LFOPhaseOffset: Int = 0

    
    //Modulators
    @objc dynamic var envelope3_Attack:Int = 0
    @objc dynamic var envelope3_Decay:Int = 127
    @objc dynamic var envelope3_Sustain:Int = 0
    @objc dynamic var envelope3_SustainSlope:Int = 0
    @objc dynamic var envelope3_Release:Int = 0
    
    @objc dynamic var envelope4_Attack:Int = 0
    @objc dynamic var envelope4_Decay:Int = 127
    @objc dynamic var envelope4_Sustain:Int = 0
    @objc dynamic var envelope4_SustainSlope:Int = 0
    @objc dynamic var envelope4_Release:Int = 0
    
    
    @objc dynamic var lfo1_Mode:Int = 0
    @objc dynamic var lfo1_Rate:Int = 0
    @objc dynamic var lfo1_WaveformShape:Int = 0
    @objc dynamic var lfo1_EnvelopeMode:Int = 0
    @objc dynamic var lfo1_WaveformContour:Int = 0
    @objc dynamic var lfo1_Keyfollow:Int = 96
    @objc dynamic var lfo1_TriggerPhase:Int = 0
    @objc dynamic var lfo1_Clock:Int = 0
    @objc dynamic var lfo1_UserDestination:Int = 0
    @objc dynamic var lfo1_UserDestinationAmount:Int = 0
    
    @objc dynamic var lfo1_Osc1:Int = 64
    @objc dynamic var lfo1_Osc2:Int = 64
    @objc dynamic var lfo1_Pulsewidth:Int = 64
    @objc dynamic var lfo1_FilterResonance12:Int = 64
    @objc dynamic var lfo1_FilterEnvelopeGain:Int = 64
    
    
    @objc dynamic var lfo2_Mode:Int = 0
    @objc dynamic var lfo2_Rate:Int = 0
    @objc dynamic var lfo2_WaveformShape:Int = 0
    @objc dynamic var lfo2_EnvelopeMode:Int = 0
    @objc dynamic var lfo2_WaveformContour:Int = 0
    @objc dynamic var lfo2_Keyfollow:Int = 96
    @objc dynamic var lfo2_TriggerPhase:Int = 0
    @objc dynamic var lfo2_Clock:Int = 0
    @objc dynamic var lfo2_UserDestination:Int = 0
    @objc dynamic var lfo2_UserDestinationAmount:Int = 0
    
    @objc dynamic var lfo2_Shape12:Int = 64
    @objc dynamic var lfo2_FMAmount:Int = 64
    @objc dynamic var lfo2_Cutoff12:Int = 64
    @objc dynamic var lfo2_Cutoff2:Int = 64
    @objc dynamic var lfo2_Panorama:Int = 64
    
    
    @objc dynamic var lfo3_Rate:Int = 0
    @objc dynamic var lfo3_WaveformShape:Int = 0
    @objc dynamic var lfo3_Mode:Int = 0
    @objc dynamic var lfo3_Keyfollow:Int = 96
    @objc dynamic var lfo3_UserDestination:Int = 0
    @objc dynamic var lfo3_UserDestinationAmount:Int = 0
    @objc dynamic var lfo3_FadeInTime:Int = 0
    @objc dynamic var lfo3_Clock:Int = 0
    
    
    @objc dynamic var matrixSlot1_Source:Int = 0
    @objc dynamic var matrixSlot1_Destination1:Int = 0
    @objc dynamic var matrixSlot1_Amount1:Int = 64
    @objc dynamic var matrixSlot1_Destination2:Int = 0
    @objc dynamic var matrixSlot1_Amount2:Int = 64
    @objc dynamic var matrixSlot1_Destination3:Int = 0
    @objc dynamic var matrixSlot1_Amount3:Int = 64
    
    @objc dynamic var matrixSlot2_Source:Int = 0
    @objc dynamic var matrixSlot2_Destination1:Int = 0
    @objc dynamic var matrixSlot2_Amount1:Int = 64
    @objc dynamic var matrixSlot2_Destination2:Int = 0
    @objc dynamic var matrixSlot2_Amount2:Int = 64
    @objc dynamic var matrixSlot2_Destination3:Int = 0
    @objc dynamic var matrixSlot2_Amount3:Int = 64
    
    @objc dynamic var matrixSlot3_Source:Int = 0
    @objc dynamic var matrixSlot3_Destination1:Int = 0
    @objc dynamic var matrixSlot3_Amount1:Int = 64
    @objc dynamic var matrixSlot3_Destination2:Int = 0
    @objc dynamic var matrixSlot3_Amount2:Int = 64
    @objc dynamic var matrixSlot3_Destination3:Int = 0
    @objc dynamic var matrixSlot3_Amount3:Int = 64
    
    @objc dynamic var matrixSlot4_Source:Int = 0
    @objc dynamic var matrixSlot4_Destination1:Int = 0
    @objc dynamic var matrixSlot4_Amount1:Int = 64
    @objc dynamic var matrixSlot4_Destination2:Int = 0
    @objc dynamic var matrixSlot4_Amount2:Int = 64
    @objc dynamic var matrixSlot4_Destination3:Int = 0
    @objc dynamic var matrixSlot4_Amount3:Int = 64
    
    @objc dynamic var matrixSlot5_Source:Int = 38
    @objc dynamic var matrixSlot5_Destination1:Int = 0
    @objc dynamic var matrixSlot5_Amount1:Int = 64
    @objc dynamic var matrixSlot5_Destination2:Int = 0
    @objc dynamic var matrixSlot5_Amount2:Int = 64
    @objc dynamic var matrixSlot5_Destination3:Int = 0
    @objc dynamic var matrixSlot5_Amount3:Int = 64
    
    @objc dynamic var matrixSlot6_Source:Int = 39
    @objc dynamic var matrixSlot6_Destination1:Int = 0
    @objc dynamic var matrixSlot6_Amount1:Int = 64
    @objc dynamic var matrixSlot6_Destination2:Int = 0
    @objc dynamic var matrixSlot6_Amount2:Int = 64
    @objc dynamic var matrixSlot6_Destination3:Int = 0
    @objc dynamic var matrixSlot6_Amount3:Int = 64
    
    
    
    // filters . gecontroleerd
    @objc dynamic var filter1_Mode: Int = 2
    @objc dynamic var filter1_Cutoff: Int = 64
    @objc dynamic var filter1_Resonance: Int = 0
    @objc dynamic var filter1_EnvelopePolarity: Int = 0
    @objc dynamic var filter1_EnvelopeAmount: Int = 0
    @objc dynamic var filter1_KeyFollow: Int = 96
    
    @objc dynamic var filter2_Mode: Int = 2
    @objc dynamic var filter2_Cutoff: Int = 64
    @objc dynamic var filter2_Resonance: Int = 0
    @objc dynamic var filter2_EnvelopePolarity: Int = 0
    @objc dynamic var filter2_EnvelopeAmount: Int = 0
    @objc dynamic var filter2_KeyFollow: Int = 96
    
    @objc dynamic var filter_Balance: Int = 64
    @objc dynamic var filter_saturationType: Int = 0
    @objc dynamic var filter_volumeToSaturation: Int = 64 // BUITEN GEBRUIK
    @objc dynamic var filter_routing: Int = 0
    @objc dynamic var filter_Link: Int = 0
    @objc dynamic var filter_cutoffLink: Int = 0
    @objc dynamic var filter_keyfollowBase: Int = 96
    
    @objc dynamic var amplifierEnvelope_Attack:Int = 0
    @objc dynamic var amplifierEnvelope_Decay:Int = 127
    @objc dynamic var amplifierEnvelope_Sustain:Int = 0
    @objc dynamic var amplifierEnvelope_SustainSlope:Int = 0
    @objc dynamic var amplifierEnvelope_Release:Int = 0
    
    @objc dynamic var filterEnvelope_Attack:Int = 0
    @objc dynamic var filterEnvelope_Decay:Int = 127
    @objc dynamic var filterEnvelope_Sustain:Int = 0
    @objc dynamic var filterEnvelope_SustainSlope:Int = 0
    @objc dynamic var filterEnvelope_Release:Int = 0

    @objc dynamic var filterInputFollower_Input:Int = 0 // off, inL in L+R, inR  page 2 par 38

    @objc dynamic var amp_PatchVolume:Int = 127
    
    //Arpeggiator
    @objc dynamic var arp_pattern: Int = 0
    @objc dynamic var arp_patternLength: Int = 1
    @objc dynamic var arp_range: Int = 0
    @objc dynamic var arp_holdMode: Int = 0
    @objc dynamic var arp_noteLength: Int = 64
    @objc dynamic var arp_swingFactor: Int = 0
    @objc dynamic var arp_mode: Int = 0
    @objc dynamic var arp_tempo: Int = 57
    @objc dynamic var arp_clock: Int = 6
    
    @objc dynamic var arp_step_1: Int = 1
    @objc dynamic var arp_stepLength_1: Int = 90
    @objc dynamic var arp_stepVelocity_1: Int = 120
    
    @objc dynamic var arp_step_2: Int = 1
    @objc dynamic var arp_stepLength_2: Int = 90
    @objc dynamic var arp_stepVelocity_2: Int = 120
    
    @objc dynamic var arp_step_3: Int = 1
    @objc dynamic var arp_stepLength_3: Int = 90
    @objc dynamic var arp_stepVelocity_3: Int = 120
    
    @objc dynamic var arp_step_4: Int = 1
    @objc dynamic var arp_stepLength_4: Int = 90
    @objc dynamic var arp_stepVelocity_4: Int = 120
    
    @objc dynamic var arp_step_5: Int = 1
    @objc dynamic var arp_stepLength_5: Int = 90
    @objc dynamic var arp_stepVelocity_5: Int = 120
    
    @objc dynamic var arp_step_6: Int = 1
    @objc dynamic var arp_stepLength_6: Int = 90
    @objc dynamic var arp_stepVelocity_6: Int = 120
    
    @objc dynamic var arp_step_7: Int = 1
    @objc dynamic var arp_stepLength_7: Int = 90
    @objc dynamic var arp_stepVelocity_7: Int = 120
    
    @objc dynamic var arp_step_8: Int = 1
    @objc dynamic var arp_stepLength_8: Int = 90
    @objc dynamic var arp_stepVelocity_8: Int = 120
    
    @objc dynamic var arp_step_9: Int = 1
    @objc dynamic var arp_stepLength_9: Int = 90
    @objc dynamic var arp_stepVelocity_9: Int = 120
    
    
    @objc dynamic var arp_step_10: Int = 1
    @objc dynamic var arp_stepLength_10: Int = 90
    @objc dynamic var arp_stepVelocity_10: Int = 120
    
    @objc dynamic var arp_step_11: Int = 1
    @objc dynamic var arp_stepLength_11: Int = 90
    @objc dynamic var arp_stepVelocity_11: Int = 120
    
    @objc dynamic var arp_step_12: Int = 1
    @objc dynamic var arp_stepLength_12: Int = 90
    @objc dynamic var arp_stepVelocity_12: Int = 120
    
    @objc dynamic var arp_step_13: Int = 1
    @objc dynamic var arp_stepLength_13: Int = 90
    @objc dynamic var arp_stepVelocity_13: Int = 120
    
    @objc dynamic var arp_step_14: Int = 1
    @objc dynamic var arp_stepLength_14: Int = 90
    @objc dynamic var arp_stepVelocity_14: Int = 120
    
    @objc dynamic var arp_step_15: Int = 1
    @objc dynamic var arp_stepLength_15: Int = 90
    @objc dynamic var arp_stepVelocity_15: Int = 120
    
    @objc dynamic var arp_step_16: Int = 1
    @objc dynamic var arp_stepLength_16: Int = 90
    @objc dynamic var arp_stepVelocity_16: Int = 120
    
    @objc dynamic var arp_step_17: Int = 1
    @objc dynamic var arp_stepLength_17: Int = 90
    @objc dynamic var arp_stepVelocity_17: Int = 120
    
    @objc dynamic var arp_step_18: Int = 1
    @objc dynamic var arp_stepLength_18: Int = 90
    @objc dynamic var arp_stepVelocity_18: Int = 120
    
    @objc dynamic var arp_step_19: Int = 1
    @objc dynamic var arp_stepLength_19: Int = 90
    @objc dynamic var arp_stepVelocity_19: Int = 120
    
    @objc dynamic var arp_step_20: Int = 1
    @objc dynamic var arp_stepLength_20: Int = 90
    @objc dynamic var arp_stepVelocity_20: Int = 120
    
    @objc dynamic var arp_step_21: Int = 1
    @objc dynamic var arp_stepLength_21: Int = 90
    @objc dynamic var arp_stepVelocity_21: Int = 120
    
    @objc dynamic var arp_step_22: Int = 1
    @objc dynamic var arp_stepLength_22: Int = 90
    @objc dynamic var arp_stepVelocity_22: Int = 120
    
    @objc dynamic var arp_step_23: Int = 1
    @objc dynamic var arp_stepLength_23: Int = 90
    @objc dynamic var arp_stepVelocity_23: Int = 120
    
    @objc dynamic var arp_step_24: Int = 1
    @objc dynamic var arp_stepLength_24: Int = 90
    @objc dynamic var arp_stepVelocity_24: Int = 120
    
    @objc dynamic var arp_step_25: Int = 1
    @objc dynamic var arp_stepLength_25: Int = 90
    @objc dynamic var arp_stepVelocity_25: Int = 120
    
    @objc dynamic var arp_step_26: Int = 1
    @objc dynamic var arp_stepLength_26: Int = 90
    @objc dynamic var arp_stepVelocity_26: Int = 120
    
    @objc dynamic var arp_step_27: Int = 1
    @objc dynamic var arp_stepLength_27: Int = 90
    @objc dynamic var arp_stepVelocity_27: Int = 120
    
    @objc dynamic var arp_step_28: Int = 1
    @objc dynamic var arp_stepLength_28: Int = 90
    @objc dynamic var arp_stepVelocity_28: Int = 120
    
    @objc dynamic var arp_step_29: Int = 1
    @objc dynamic var arp_stepLength_29: Int = 90
    @objc dynamic var arp_stepVelocity_29: Int = 120
    
    @objc dynamic var arp_step_30: Int = 1
    @objc dynamic var arp_stepLength_30: Int = 90
    @objc dynamic var arp_stepVelocity_30: Int = 120
    
    @objc dynamic var arp_step_31: Int = 1
    @objc dynamic var arp_stepLength_31: Int = 90
    @objc dynamic var arp_stepVelocity_31: Int = 120
    
    @objc dynamic var arp_step_32: Int = 1
    @objc dynamic var arp_stepLength_32: Int = 90
    @objc dynamic var arp_stepVelocity_32: Int = 120
    
    //Character
    @objc dynamic var character_Type: Int = 0
    @objc dynamic var character_Intensity: Int = 0
    @objc dynamic var character_Tune: Int = 0
    
    @objc dynamic var eq_LowFrequency: Int = 0
    @objc dynamic var eq_LowGain: Int = 64
    @objc dynamic var eq_MidGain: Int = 64
    @objc dynamic var eq_MidFrequency: Int = 0
    @objc dynamic var eq_MidQFactor: Int = 0
    @objc dynamic var eq_HighFrequency: Int = 0
    @objc dynamic var eq_HighGain: Int = 64
    
    @objc dynamic var frequencyShifter_Type: Int  = 0
    @objc dynamic var frequencyShifter_Mix: Int  = 0
    @objc dynamic var frequencyShifter_Frequency: Int  = 64
    @objc dynamic var frequencyShifter_StereoPhase: Int  = 64
    @objc dynamic var frequencyShifter_LeftShape: Int  = 64
    @objc dynamic var frequencyShifter_RightShape: Int  = 64
    @objc dynamic var frequencyShifter_Resonance: Int  = 64
    @objc dynamic var frequencyShifter_FilterType: Int  = 64
    @objc dynamic var frequencyShifter_Poles: Int  = 0
    @objc dynamic var frequencyShifter_Slope: Int  = 0
    
    // Effects
    @objc dynamic var chorus_Type: Int = 0         // cc 103
    @objc dynamic var chorus_Mix: Int = 0          // cc 104
    @objc dynamic var chorus_MixClassic: Int = 0   // cc 105
    @objc dynamic var chorus_LFORate: Int = 0      // cc 106
    @objc dynamic var chorus_LFODepth: Int = 0     // cc 107
    @objc dynamic var chorus_Distance: Int = 0     // cc 107
    @objc dynamic var chorus_Delay: Int = 0        // cc 108
    @objc dynamic var chorus_Amount: Int = 0       // cc 108
    @objc dynamic var chorus_Feedback: Int = 64    // cc 109
    @objc dynamic var chorus_LowHigh: Int = 0      // cc 109
    @objc dynamic var chorus_LFOShape: Int = 0     // cc 110
    @objc dynamic var chorus_XOver: Int = 0        // cc 111
    
    @objc dynamic var reverb_Mode: Int = 0
    @objc dynamic var reverb_Send: Int = 0
    @objc dynamic var reverb_Type: Int = 0
    @objc dynamic var reverb_Time: Int = 0
    @objc dynamic var reverb_Damping: Int = 0
    @objc dynamic var reverb_Color: Int = 0
    @objc dynamic var reverb_Predelay: Int = 0
    @objc dynamic var reverb_Clock: Int = 0
    @objc dynamic var reverb_Feedback: Int = 0
    
    @objc dynamic var delay_Mode: Int = 0          // cc 112
    @objc dynamic var delay_Send: Int = 0          // cc 113
    @objc dynamic var delay_Time: Int = 0          // cc 114
    @objc dynamic var delay_Clock: Int = 0  
    @objc dynamic var delay_Feedback: Int = 0      // cc 115
    @objc dynamic var delay_LFORate: Int = 0       // cc 116
    @objc dynamic var delay_LFODepth: Int = 0      // cc 117
    @objc dynamic var delay_LFOShape: Int = 0      // cc 118
    @objc dynamic var delay_Color: Int = 0         // cc 119
    @objc dynamic var delay_Type: Int = 0          // se 10
    @objc dynamic var delay_TapeDelayRatio: Int = 0 //se 12
    @objc dynamic var delay_TapeDelayClockLeft: Int = 0 //se13
    @objc dynamic var delay_TapeDelayClockRight: Int = 0 //se14
    @objc dynamic var delay_TapeDelayBandwidth: Int = 0  // se17
    
    @objc dynamic var phaser_Stages: Int = 0      // pp 84
    @objc dynamic var phaser_Mix: Int = 0         // pp 85
    @objc dynamic var phaser_LFORate: Int = 0     // pp 86
    @objc dynamic var phaser_Depth: Int = 0       // pp 87
    @objc dynamic var phaser_Frequency: Int = 0   // pp 88
    @objc dynamic var phaser_Feedback: Int = 64    // pp 89
    @objc dynamic var phaser_Spread: Int = 0      // pp 90
    
    @objc dynamic var distortion_Type: Int = 0         // pp 100
    @objc dynamic var distortion_Intensity: Int = 0    // pp 101 ook drive
    @objc dynamic var distortion_TrebleBooster: Int = 0 //se 70
    @objc dynamic var distortion_HighCut: Int = 0      // se 71
    @objc dynamic var distortion_Mix: Int = 0          // se 72
    @objc dynamic var distortion_Quality: Int = 0      // se 73
    @objc dynamic var distortion_ToneW: Int = 0        // se 74
    
    @objc dynamic var input_Mode: Int = 0
    @objc dynamic var input_Select: Int = 0
    @objc dynamic var Atomizer: Int = 0
    @objc dynamic var VocoderMode: Int = 0
    
    // Control
    @objc dynamic var velocity_Osc1WaveformShape: Int = 64
    @objc dynamic var velocity_Osc2WaveformShape: Int = 64
    @objc dynamic var velocity_OscPulsewidth: Int = 64
    @objc dynamic var velocity_OscFMAmount: Int = 64
    @objc dynamic var velocity_Volume: Int = 127
    @objc dynamic var velocity_Panorama: Int = 64
    @objc dynamic var velocity_Filter1EnvAmount: Int = 64
    @objc dynamic var velocity_Filter1Resonance: Int = 64
    @objc dynamic var velocity_Filter2EnvAmount: Int = 64
    @objc dynamic var velocity_Filter2Resonance: Int = 64
    @objc dynamic var control_Softknob1Destination: Int = 0
    @objc dynamic var control_Softknob1Name: Int = 0
    @objc dynamic var control_Softknob2Destination: Int = 0
    @objc dynamic var control_Softknob2Name: Int = 0
    @objc dynamic var control_Softknob3Destination: Int = 0
    @objc dynamic var control_Softknob3Name: Int = 0
    @objc dynamic var control_BenderDown: Int = 64
    @objc dynamic var control_BenderUp: Int = 64
    @objc dynamic var control_BenderScale: Int = 0
    @objc dynamic var control_parameterSmoothMode: Int = 1
    @objc dynamic var control_KeyboardMode: Int = 0
}
