////
//// Swift MIDI Playground : Matt Grippaldi 10/17/2016
////
//// MIDI Callbacks
////
//// Updated for Swift 3 / XCode 8
////
//
//import CoreMIDI
//
//class MIDITst {
//
//    func getDisplayName(_ obj: MIDIObjectRef) -> String
//    {
//        var param: Unmanaged<CFString>?
//        var name: String = "Error"
//        
//        let err: OSStatus = MIDIObjectGetStringProperty(obj, kMIDIPropertyDisplayName, &param)
//        if err == OSStatus(noErr)
//        {
//            name =  param!.takeRetainedValue() as String
//        }
//        
//        return name
//    }
//
//    @objc func MyMIDIReadProc(pktList: UnsafePointer<MIDIPacketList>,
//                        readProcRefCon: UnsafeMutableRawPointer?, srcConnRefCon: UnsafeMutableRawPointer?) -> Void
//    {
//        let packetList:MIDIPacketList = pktList.pointee
//        let srcRef:MIDIEndpointRef = srcConnRefCon!.load(as: MIDIEndpointRef.self)
//
//        print("MIDI Received From Source: \(getDisplayName(srcRef))")
//        
//        var packet:MIDIPacket = packetList.packet
//        for _ in 1...packetList.numPackets
//        {
//            let bytes = Mirror(reflecting: packet.data).children
//            var dumpStr = ""
//            
//            // bytes mirror contains all the zero values in the ridiulous packet data tuple
//            // so use the packet length to iterate.
//            var i = packet.length
//            for (_, attr) in bytes.enumerated()
//            {
//                dumpStr += String(format:"$%02X ", attr.value as! UInt8)
//                i -= 1
//                if (i <= 0)
//                {
//                    break
//                }
//            }
//            
//            print(dumpStr)
//            packet = MIDIPacketNext(&packet).pointee
//        }
//    }
//
//    var midiClient: MIDIClientRef = 0
//    var inPort:MIDIPortRef = 0
//    var src:MIDIEndpointRef = MIDIGetSource(0)
//
//    func setup (){
//        MIDIClientCreate("MidiTestClient" as CFString, nil, nil, &midiClient)
//        MIDIInputPortCreateWithProtocol(_ client: MIDIClientRef,
//                                           _ portName: "MidiTest_InPort",
//                                           _ protocol: 1,
//                                           _ outPort: UnsafeMutablePointer<MIDIPortRef>,
//                                           _ receiveBlock: @escaping MIDIReceiveBlock) -> OSStatus
//        
//        
//        MIDIInputPortCreate(midiClient, "MidiTest_InPort" as CFString, self.MyMIDIReadProc, nil, &inPort)
//
//        MIDIPortConnectSource(inPort, src, &src)
//        
//    }
//    
//
//}
