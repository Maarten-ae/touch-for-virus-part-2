//
//  Library.swift
//  Rage
//
//  Created by M. Lierop on 30/11/2020.
//  Copyright © 2020 M. Lierop. All rights reserved.
//

import Foundation
import RealmSwift

public enum CollectionTypes {
    case custom
    case all
    case favorites
    case recentlyUsed
    case mostPopular
}
public var CollectionType: CollectionTypes = .all


class library {
    
    public var PatchList:[ Patch] = []
    public var AllPatches:[ Patch] = []
    public var FavoritePatches:[ Patch] = []
    public var RecentPatches:[ Patch] = []
    public var PopularPatches:[ Patch] = []
    public var CurrentPatch:Patch = Patch()
    
    public var Collections = [ CollectionInfo ]()
    public var CustomCollections = [ CollectionInfo ]()
    public var CurrentCollection:CollectionInfo = CollectionInfo(name: "All", count: "0", type: .all)
    public var CollectionType: CollectionTypes = .all
    
    public var CurrentCategory:CategoryInfo = CategoryInfo(id:0)
    //private var Category:Int = 0
    //private var CategoryLabel = ""
    
    private var SearchString:String = ""
    
    private var realmDB: Realm?
    
    
    public func Setup(){
        // Setup dB, initialize all vars
        
        // load DB
        self.realmDB = try! Realm(configuration: config)
        
        // Get patches in current selection
        self.CurrentPatch = patchCtrl.CurrentPatch
       
        self.update()
    }
    
    public func update(){
        // Something might have changed, so recalc all values
        self.Collections.removeAll()
        self.CustomCollections.removeAll()
        
        self.AllPatches = Array( ( realmDB?.objects( Patch.self ).filter( self.query(type: .all) ).sorted( byKeyPath:"name" ) )! )
        self.Collections.append( CollectionInfo(name: "All Patches", count: String( self.AllPatches.count ), type: .all) )
        
        
        self.FavoritePatches = Array( ( realmDB?.objects( Patch.self ).filter( self.query(type: .favorites) ).sorted( byKeyPath:"name" ) )! )
        self.Collections.append( CollectionInfo(name: "Favorites", count: String( self.FavoritePatches.count ) , type: .favorites) )

        
        self.RecentPatches = Array( ( realmDB?.objects( Patch.self ).filter( self.query(type: .recentlyUsed) ).sorted( byKeyPath:"name" ) )! )
        self.RecentPatches.sort(by: { $0.lastUsage > $1.lastUsage })
        let treshold: Date = Date().addingTimeInterval(-14*24*60*60)
        self.RecentPatches.removeAll { patch in
            return patch.lastUsage <= treshold
        }
        self.Collections.append( CollectionInfo(name: "Recently Used", count: String( self.RecentPatches.count ), type: .recentlyUsed) )
        
        self.PopularPatches = Array( ( realmDB?.objects( Patch.self ).filter( self.query(type: .mostPopular) ).sorted( byKeyPath:"usageCount" ) )! )
     
        self.Collections.append( CollectionInfo(name: "Most Used", count: String( self.PopularPatches.count ) , type: .mostPopular) )
        
        
        // Add custom collections
       
        var tempCollections = realmDB?.objects(Patch.self).value(forKey: "collectionName") as! [String]
        tempCollections = tempCollections.removeDuplicates()
        tempCollections.sort()

        for Collection in tempCollections {
            let count:String = String ( (realmDB?.objects(Patch.self).filter(self.query(type: .all, collection: Collection ) ).count)! )
            self.Collections.append( CollectionInfo(name: Collection, count: count, type: .custom) )
            self.CustomCollections.append( CollectionInfo(name: Collection, count: count, type: .custom) )
        }
        
        
        self.updatePatchList()
        NotificationCenter.default.post(name: Notification.Name( "UpdatePatchInfo" ), object: nil)
    }
    
    
    
    
    private func updatePatchList(){
        
        switch (self.CurrentCollection.type){
        case .all:
            self.PatchList = self.AllPatches
        case .favorites:
            self.PatchList = self.FavoritePatches
        case .recentlyUsed:
            self.PatchList = self.RecentPatches
        case .mostPopular:
            self.PatchList = self.PopularPatches
        case .custom:
            self.PatchList = Array(( realmDB?.objects(Patch.self).filter(self.query()).sorted(byKeyPath: "name") )! )
        default:
            self.PatchList = self.AllPatches
        }
   
        if (self.CurrentCollection.type == .recentlyUsed){
            self.PatchList.sort(by: { $0.lastUsage > $1.lastUsage })
            
            let treshold: Date = Date().addingTimeInterval(-30*24*60*60)
            self.PatchList.removeAll { patch in
                return patch.lastUsage <= treshold
            }
        } else if (self.CurrentCollection.type == .mostPopular ){
            self.PatchList.sort(by: { $0.usageCount > $1.usageCount })
            
        }
              print("updated patchlist")
        NotificationCenter.default.post(name: Notification.Name( "UpdateLibraryTables" ), object: nil)
    }
    
    private func query(type: CollectionTypes? = nil, collection: String? = nil ) -> NSPredicate {
    
        var colType: CollectionTypes
        if type == nil {
            colType = self.CurrentCollection.type!
        } else {
            colType = type!
        }
        
        var cat:Int = Int(0)
        var srchIsUsed:Bool = false
        var col:String = ""
        var colIsUsed = false
        var srch:String = ""
        
       var filters:[String] = []
       var query:String = ""
       
        
        if (self.CurrentCategory.id != 0 ) {
           cat = Int(self.CurrentCategory.id)
           
           filters.append("(category1 == \(cat) OR category2 == \(cat) )")
        }

        if (self.SearchString != "") {
            srch = self.SearchString.escapeString()
            srchIsUsed = true
            filters.append("name CONTAINS %@")
        }
        
        if (colType == .favorites){
            filters.append("isFavorite == true")
        }
        
        if (colType == .mostPopular){
            filters.append("usageCount >= 1")
        }
        
        if ( collection != nil ){
            col = collection!.escapeString()
            colIsUsed = true
            filters.append("collectionName = %@")
        } else if (colType == .custom){
            col = self.CurrentCollection.name!.escapeString()
            colIsUsed = true
            filters.append("collectionName = %@")
        }

        for (index, filter) in filters.enumerated() {
            if (index == 0){
                query = filter
            } else {
                query += " AND " + filter
            }
        }
        // just to make sure app won't crash: make dummy query for selectong everything, filter cannot be empty
        if (query == "") {query = "id == 0"}
          
        var predicate:NSPredicate
        
        if (srchIsUsed && colIsUsed){
            predicate = NSPredicate(format: query, srch, col)
        } else if (srchIsUsed) {
            predicate = NSPredicate(format: query,srch)
        } else if (colIsUsed) {
            predicate = NSPredicate(format: query,col)
        } else {
            predicate = NSPredicate(format: query)
        }
        
        
        print(predicate)
        return predicate
    }
    
    
    
    public func setCollection (nr: Int){
        
        switch (nr){
        case 0:
            self.CurrentCollection = CollectionInfo(name: "All Patches", count: "", type: .all)
        case 1:
            self.CurrentCollection = CollectionInfo(name: "Favorites", count: "", type: .favorites)
        case 2:
            self.CurrentCollection = CollectionInfo(name: "Recently Used", count: "", type: .recentlyUsed)
        case 3:
            self.CurrentCollection = CollectionInfo(name: "Most Used", count: "", type: .mostPopular)
        default:
            self.CurrentCollection = CollectionInfo(name: self.Collections[nr].name ?? "", count: "", type: .custom)
        }
        self.updatePatchList()
    }
    
    public func getCollection (name:String) -> CollectionInfo {
        
        if ( self.Collections.contains(where: {$0.name == name}) ) {
            return self.Collections.first(where: {$0.name == name})!
        } else {
            return CollectionInfo(name: "unknown", count: "0", type: .custom)
        }
    }
    
    
    public func setCategory (cat: Int){
        self.CurrentCategory.setbyId(id: cat)
        print("library:: setCategory ",self.CurrentCategory.label)
        self.update()
            NotificationCenter.default.post(name: Notification.Name( "UpdateCategoryToken" ), object: nil)
    }
    
    public func setSearchString (srch: String){
        
        self.SearchString = srch
        self.update()
        
    }
    
    
   
    
    
    
    // MARK: Modify Current Patch
    
//    public func updateLastUsageDate(){
//        
//        // update patch
//        let realm = try! Realm(configuration: config)
//        realm.autorefresh = true
//        DispatchQueue.main.async {
//            try! realm.write {
//                patchCtrl.liveEditablePatch.lastUsage = Date()
//                
//                try! realm.commitWrite()
//            }
//            self.update()
//        }
//    }
    
    public func selectPatchFromPatchlist(nr:Int) -> Bool{
        self.CurrentPatch = self.PatchList[nr]
        return true
    }
    
    
    public func deletePatch (){
        let realm = try! Realm(configuration: config)
        try! realm.write {
            
            // delete patch
            realm.delete( self.CurrentPatch )
            
            self.update()
        }
    }
    
    public func deleteFromPatchList(row: Int) {
        
        let deletable:Patch = self.PatchList[row]
        
        let realm = try! Realm(configuration: config)
        try! realm.write {
            
            // delete patch
            realm.delete( deletable )
            
            self.update()
        }
    }
    
    public func deleteCollection (name:String){
        
        if ( self.Collections.contains(where: {$0.name == name}) ) {
            
            let realm = try! Realm(configuration: config)
            let Patches = (realmDB?.objects(Patch.self).filter("collectionName = '"+name+"'"))!
            
            try! realm.write {
                
                // delete patch
                realm.delete( Patches )
                
                self.update()
            }
        }
    }
    
    public func renameCollection (old: String, new: String) {
        
        if ( new.count >= 4 ) {
        
            if ( self.Collections.contains(where: {$0.name == old}) ) {
                
                let realm = try! Realm(configuration: config)
                let Patches = (realmDB?.objects(Patch.self).filter("collectionName = '"+old+"'"))!
                
                try! realm.write {
                   
                    for patch in Patches {
                        patch.collectionName = new
                    }
                    
                    self.update()
                }
            }
          
        }
    }
    
    public func moveCurrentPatchToCollection(nr:Int){
            let destination = self.CustomCollections[nr].name
            
            let realm = try! Realm(configuration: config)
            try! realm.write {
                patchCtrl.liveEditablePatch.collectionName = destination!
                self.update()
                
            }
        }
    
    public func editPatchDescription(descr:String){
       
        let realm = try! Realm(configuration: config)
        try! realm.write {
            self.CurrentPatch.descr = descr
            self.update()
        }
        
    }
    
    public func savePatchDetails(Name: String, Author: String, Descr: String) -> Bool {
        
        let realm = try! Realm(configuration: config)
        try! realm.write {
            self.CurrentPatch.name = Name
            self.CurrentPatch.authorName = Author
            self.CurrentPatch.descr = Descr
            self.CurrentPatch.category1 = patchCtrl.CurrentPatch.category1
            self.CurrentPatch.category2 = patchCtrl.CurrentPatch.category2
            
            self.update()
        }
        
        if  ( patchCtrl.updatePatch() ){
            NotificationCenter.default.post(name: Notification.Name( "UpdatePatchInfo" ), object: nil)
            return true
        } else {
            return false
        }
    }
}

extension Array where Element: Equatable {
    func removeDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }
}


class CollectionInfo{

    var name: String?
    var count: String?
    var type: CollectionTypes?

    init (name: String, count: String, type: CollectionTypes) {
        self.name = name
        self.count = count
        self.type = type
    }
}

class CategoryInfo{
    var id:Int = 0
    var label:String = ""
    
    init(id: Int){
        self.id = id
        self.label = presetCategoriesUI[id]
    }
    
    public func setbyId(id:Int){
        self.id = id
        self.label = presetCategoriesUI[id]
    }
}


extension String {
    func escapeString() -> String {
        var newString = self.replacingOccurrences(of:"\"", with: "\"\"")
        if newString.contains(",") || newString.contains("\n") {
            newString = String(format: "\"%@\"", newString)
        }

        return newString
    }
}
