//
//  subPatch-Effects.swift
//  Rage-2
//
//  Created by M. Lierop on 21-09-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class subPatchAmbiance: Object {
    
    // General
    @objc dynamic var name: String = "RAGE INT"
    @objc dynamic var id = 0
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: String = ""
    @objc dynamic var category2: String = ""
    @objc dynamic var category3: String = ""
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    
    @objc dynamic var reverb_Mode: Int = 0
    @objc dynamic var reverb_Send: Int = 0
    @objc dynamic var reverb_Type: Int = 0
    @objc dynamic var reverb_Time: Int = 0
    @objc dynamic var reverb_Damping: Int = 0
    @objc dynamic var reverb_Color: Int = 0
    @objc dynamic var reverb_Predelay: Int = 0
    @objc dynamic var reverb_Clock: Int = 0
    @objc dynamic var reverb_Feedback: Int = 0
    
    @objc dynamic var delay_Mode: Int = 0               // cc 112
    @objc dynamic var delay_Type: Int = 0               // se 10
    @objc dynamic var delay_Clock: Int = 0              // cc 114
    @objc dynamic var delay_Time: Int = 0               // cc 114
    
    @objc dynamic var delay_Send: Int = 0               // cc 113
    @objc dynamic var delay_Feedback: Int = 0           // cc 115
    @objc dynamic var delay_Color: Int = 0              // cc 119
    
    @objc dynamic var delay_LFORate: Int = 0            // cc 116
    @objc dynamic var delay_LFODepth: Int = 0           // cc 117
    @objc dynamic var delay_LFOShape: Int = 0           // cc 118
    
    @objc dynamic var delay_TapeDelayRatio: Int = 0     //se 12
    @objc dynamic var delay_TapeDelayClockLeft: Int = 0 //se13
    @objc dynamic var delay_TapeDelayClockRight: Int = 0 //se14
    @objc dynamic var delay_TapeDelayBandwidth: Int = 0  // se17
    
    

}

/*
 cc
 103 Chorus/Type
 104 Chorus/Mix
 105 Chorus/Mix
 106 Chorus/LFO Rate
 107 Chorus/LFO Depth
 108 Chorus/Delay
 109 Chorus/Feedback
 110 Chorus/LFO Shape
 111 Chorus/X Over
 
 sysex
 0 Reverb/Mode
 1 Reverb/Send
 2 Reverb/Type
 3 Reverb/Time
 4 Reverb/Damping
 5 Reverb/Color
 6 Reverb/Predelay
 7 Reverb/Clock
 8 Reverb/Feedback
 
 112 Delay Mode
 113 Delay Send
 114 Delay Time (ms)
 115 Delay Feedback
 116 Delay LFO Rate
 cc 117 Delay LFO Depth
 cc 118 Delay LFO Shape
 cc 119 Delay Color
 sysex 9 Delay Type
 sysex  11 Delay Tape Delay Ratio
 sysex  12 Delay Tape Delay Clock Left
 sysex  13 Delay Tape Delay Clock Right
 sysex  16 Delay Tape Delay Bandwidth
 pp 20 Delay Clock
 
 pp 84 Phaser/Stages
 pp 85 Phaser/Mix
 86 Phaser/LFO Rate
 87 Phaser/Depth
 88 Phaser/Frequency
 89 Phaser/Feedback
 90 Phaser/Spread
 
 pp 99 Ring Modulator Mix
 
 pp 100 Distortion Type
 pp 101 Distortion Intensity
 sysex 69 Distortion Treble Booster
 sysex 70 Distortion High Cut
 sysex 71 Distortion Mix
 sysex 72 Distortion Quality
 sysex 73 Distortion Tone W
 */
