//
//  iCloudPublic.swift
//  Rage
//
//  Created by M. Lierop on 11/08/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//  https://www.ralfebert.de/ios/tutorials/cloudkit/

import Foundation
import CloudKit

let iCloudDB = CKContainer.init(identifier: "iCloud.touchforvirus.publicpatches").publicCloudDatabase

extension CKDatabase {
    
    func fetchAll(
        recordType: String, resultsLimit: Int = 100, timeout: TimeInterval = 60,
        completion: @escaping (Result<[CKRecord], Error>) -> Void
        ) {
        DispatchQueue.global().async { [unowned self] in
            let query = CKQuery(
                recordType: recordType, predicate: NSPredicate(value: true)
            )
            let semaphore = DispatchSemaphore(value: 0)
            var records = [CKRecord]()
            var error: Error?
            
            var operation = CKQueryOperation(query: query)
            operation.resultsLimit = resultsLimit
            operation.recordFetchedBlock = { records.append($0) }
            operation.queryCompletionBlock = { (cursor, err) in
                guard err == nil, let cursor = cursor else {
                    error = err
                    semaphore.signal()
                    return
                }
                let newOperation = CKQueryOperation(cursor: cursor)
                newOperation.resultsLimit = operation.resultsLimit
                newOperation.recordFetchedBlock = operation.recordFetchedBlock
                newOperation.queryCompletionBlock = operation.queryCompletionBlock
                operation = newOperation
                self.add(newOperation)
            }
            self.add(operation)
            
            _ = semaphore.wait(timeout: .now() + 60)
            
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(records))
            }
        }
    }
    
}

/*
 https://stackoverflow.com/questions/28402846/cloudkit-fetch-all-records-with-a-certain-record-type
 let database: CKDatabase = ...
 database.fetchAll(recordType: "User") { result in
 switch result {
 case .success(let users):
 // Handle fetched users, ex. save them to the database
 case .failure(let error):
 // Handle Error
 }
 }
 }
 */
