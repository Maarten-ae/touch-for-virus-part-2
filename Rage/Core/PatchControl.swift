//
//  PatchControl.swift
//  RAGE
//
//  Created by M. Lierop on 19-07-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift
import HandySwift

// procedure realm db toevoegen in app
// https://stackoverflow.com/questions/29839740/how-to-deliver-app-with-prefilled-realm-database
let config = Realm.Configuration(
    //fileURL: Bundle.main.url(forResource: "RagePresets", withExtension: "realm"),
    //readOnly: true
    
    schemaVersion: 9,
    migrationBlock: { migration, oldSchemaVersion in
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 1) {
            // Nothing to do!
            // Realm will automatically detect new properties and removed properties
            // And will update the schema on disk automatically
        }
    }
)

var patchLibrary = try! Realm(configuration: config)


//var patchLibrary = try! Realm()
class RealmHelper{
    
    func save(type: String) -> Bool {
        
        DispatchQueue.global().async {
            let realm = try! Realm(configuration: config)
            realm.autorefresh = true
            realm.beginWrite()
            
            switch (type){
            case "patch":
                realm.create(Patch.self, value: patchCtrl.CurrentPatch)
            case "osc":
                realm.create(subPatchOsc.self, value: patchCtrl.OscPatch)
            case "filt":
                realm.create(subPatchFilters.self, value: patchCtrl.FilterPatch)
            case "eff":
                realm.create(subPatchAmbiance.self, value: patchCtrl.EffectsPatch)
            case "mod":
                realm.create(subPatchMod.self, value: patchCtrl.ModulatorPatch)
            case "arp":
                realm.create(subPatchArp.self, value: patchCtrl.ArpPatch)
            case "char":
                realm.create(subPatchEffects.self, value: patchCtrl.CharacterPatch)
            case "ctrl":
                realm.create(subPatchControl.self, value: patchCtrl.ControlPatch)
            default:
                print("Wrong save action")
            }
            
            try! realm.commitWrite()
            //realm.refresh()
            
            //NotificationCenter.default.post(name: Notification.Name( "UpdatePresets" ), object: nil) 
        }
        return true
    }
}

class PatchControler {
    //static let patchCtrl = PatchControler()
    public var CurrentPatch: Patch
    public var liveEditablePatch: Patch
    public var OscPatch: subPatchOsc
    public var FilterPatch: subPatchFilters
    public var EffectsPatch: subPatchAmbiance
    public var ModulatorPatch: subPatchMod
    public var ArpPatch: subPatchArp
    public var CharacterPatch: subPatchEffects
    public var ControlPatch: subPatchControl
    
    init (){
        
        self.CurrentPatch = Patch()
        self.liveEditablePatch = Patch()
        self.OscPatch = subPatchOsc()
        self.FilterPatch = subPatchFilters()
        self.EffectsPatch = subPatchAmbiance()
        self.ModulatorPatch = subPatchMod()
        self.ArpPatch = subPatchArp()
        self.CharacterPatch = subPatchEffects()
        self.ControlPatch = subPatchControl()
        reset()
    }
    
    func reset(){
        //ViC.resetVirus()
        //self.LoadPatch(NewPatch: self.CurrentPatch, oscSection: true, filterSection: true, modSection: true, controlSection: true, arpSection: true, effSection: true, ambSection: true)
    }
    
    func SendName(newname: String){
        let nameCharacters = Array(newname)
        
        if (nameCharacters.count >= 1){
            ViC.send(type: "pp",controler: 112,value: getCharacterID(String(nameCharacters[0])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 2){
            ViC.send(type: "pp",controler: 113,value: getCharacterID(String(nameCharacters[1])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 3){
            ViC.send(type: "pp",controler: 114,value: getCharacterID(String(nameCharacters[2])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 4){
            ViC.send(type: "pp",controler: 115,value: getCharacterID(String(nameCharacters[3])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 5){
            ViC.send(type: "pp",controler: 116,value: getCharacterID(String(nameCharacters[4])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 6){
            ViC.send(type: "pp",controler: 117,value: getCharacterID(String(nameCharacters[5])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 7){
            ViC.send(type: "pp",controler: 118,value: getCharacterID(String(nameCharacters[6])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 8){
            ViC.send(type: "pp",controler: 119,value: getCharacterID(String(nameCharacters[7])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 9){
            ViC.send(type: "pp",controler: 120,value: getCharacterID(String(nameCharacters[8])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
        if (nameCharacters.count >= 10){
            ViC.send(type: "pp",controler: 121,value: getCharacterID(String(nameCharacters[9])))
        } else {
            ViC.send(type: "pp",controler: 121,value: getCharacterID(" "))
        }
    }
    
    
    public func updatePatch() -> Bool {
        
        
        if (self.LoadPatch(NewPatch: self.liveEditablePatch, oscSection: true, filterSection: true, modSection: true, controlSection: true, arpSection: true, effSection: true, ambSection: true)) {
            return true
        } else {
            return false
            
        }
    }
    
    public func LoadPatch( NewPatch: Patch, oscSection: Bool, filterSection: Bool, modSection: Bool, controlSection: Bool, arpSection: Bool, effSection: Bool, ambSection: Bool) -> Bool {
        self.liveEditablePatch = NewPatch
        CurrentPatch.name = NewPatch.name
        
        // to be written
        print("start met LoadPAtch functie")
        self.SendName(newname: NewPatch.name)
        CurrentPatch.category1 = NewPatch.category1
        CurrentPatch.category2 = NewPatch.category2
        CurrentPatch.collectionName = NewPatch.collectionName
        CurrentPatch.authorName = NewPatch.authorName
        CurrentPatch.descr = NewPatch.descr
        CurrentPatch.creationDate = NewPatch.creationDate
        CurrentPatch.isFavorite = NewPatch.isFavorite
        CurrentPatch.usageCount = NewPatch.usageCount + 1
        CurrentPatch.lastUsage = Date()
        
        let realm = try! Realm(configuration: config)
        realm.autorefresh = true
        DispatchQueue.main.async {
            try! realm.write {
                self.liveEditablePatch.lastUsage = Date()
                self.liveEditablePatch.usageCount = NewPatch.usageCount + 1
                try! realm.commitWrite()
            }
            NotificationCenter.default.post(name: Notification.Name( "UpdateLibraryTables" ), object: nil)
        }
        
        
        if (oscSection && filterSection && modSection && controlSection && arpSection && effSection && ambSection){
            // Volledige patch wordt verzonden
            self.SendName(newname: CurrentPatch.name)
            self.setCategory1(value: NewPatch.category1)
            self.setCategory2(value: NewPatch.category2)
        }
        
        if (oscSection) {
            self.setOsc1Model(model: NewPatch.osc1_Model)
            self.setOsc1WaveformShape(value: NewPatch.osc1_WaveformShape)
            self.setOsc1WaveformSelect(value: NewPatch.osc1_WaveformSelect)
            self.setOsc1SemiTone(value: NewPatch.osc1_DetuneInSemitones)
            self.setOsc1KeyFollow(value: NewPatch.osc1_Keyfollow)
            self.setOsc1PulseWidth (value: NewPatch.osc1_Pulsewidth)
            self.setOsc1FormantSpread(value: NewPatch.osc1_FormantSpread)
            self.setOsc1FormantShift(value: NewPatch.osc1_FormantShift)
            self.setOsc1LocalDetune(value: NewPatch.osc1_LocalDetune)
            self.setOsc1Interpolation(value: NewPatch.osc1_Interpolation)
            
            self.setOsc2Model(model: NewPatch.osc2_Model)
            self.setOsc2WaveformShape(value: NewPatch.osc2_WaveformShape)
            self.setOsc2WaveformSelect(value: NewPatch.osc2_WaveformSelect)
            self.setOsc2SemiTone(value: NewPatch.osc2_DetuneInSemitones)
            self.setOsc2KeyFollow(value: NewPatch.osc2_Keyfollow)
            self.setOsc2PulseWidth(value: NewPatch.osc2_Pulsewidth)
            self.setOsc2FineDetune(value: NewPatch.osc2_FineDetune)
            self.setOsc2FormantSpread(value: NewPatch.osc2_FormantSpread)
            self.setOsc2FormantShift(value: NewPatch.osc2_FormantShift)
            self.setOsc2LocalDetune(value: NewPatch.osc2_LocalDetune)
            self.setOsc2Interpolation(value: NewPatch.osc2_Interpolation)
            
            self.setOsc1Osc2Sync(value: NewPatch.osc1_osc2_Sync)
            self.setOsc1Osc2Balance(value: NewPatch.osc1_osc2_Balance)
            
            self.setOsc3Model(model: NewPatch.osc3_Model)
            self.setOsc3Volume(value: NewPatch.osc3_Volume)
            self.setOsc3SemiTone(value: NewPatch.osc3_DetuneInSemitones)
            self.setOsc3FineDetune(value: NewPatch.osc3_FineDetune)
            
            self.setOscSubVolume(value: NewPatch.oscS_Volume)
            self.setOscSubWaveformShapee(value: NewPatch.oscS_WaveformShape)
            
            self.setOscNoiseVolume (value: NewPatch.oscN_Volume)
            self.setOscNoiseColor (value: NewPatch.oscN_Color)
            
            self.setInitialPhase (value: NewPatch.osc_InitialPhase)
            self.setPortamento(value: NewPatch.osc_Portamento)
            self.setPunchIntensity (value: NewPatch.osc_PunchIntensity)
            self.setFMMode (value: NewPatch.osc_FMMode)
            self.setFMAmount (value: NewPatch.osc_FMAmount)
            self.setRingModulation (value: NewPatch.osc_RingModulationVolume )
            self.setRingModulationMix (value: NewPatch.osc_RingModulationMix )
            
            self.setOscSectionVolume(value: NewPatch.osc_SectionVolume)
            self.setOscSectionPatchTransposition(value: NewPatch.osc_SectionPatchTransposition)
            self.setOscSectionSurroundSoundBalance(value: NewPatch.osc_SectionSurroundSoundBalance)
        
            self.setFilterEnvelopeToPitch(value: NewPatch.filterEnvelope_ToPitch)
            self.setFilterEnvelopeToFM(value: NewPatch.filterEnvelope_ToFM)
            
            self.setOsc_Unison_Mode(value: NewPatch.osc_Unison_Mode)
            self.setOsc_Unison_Detune(value: NewPatch.osc_Unison_Detune)
            self.setOsc_Unison_PanoramaSpread(value: NewPatch.osc_Unison_PanoramaSpread)
            self.setOsc_Unison_LFOPhaseOffset(value: NewPatch.osc_Unison_LFOPhaseOffset)
        }
        
        // Filters
        delay(by: .seconds(0.1)) { // Runs in Main thread by default
            if (filterSection) {
                self.setFilter1Mode(value: NewPatch.filter1_Mode)
                self.setFilter1Cutoff(value: NewPatch.filter1_Cutoff)
                self.setFilter1Resonance(value: NewPatch.filter1_Resonance)
                self.setFilter1EnvelopePolarity(value: NewPatch.filter1_EnvelopePolarity)
                self.setFilter1EnvelopeAmount(value: NewPatch.filter1_EnvelopeAmount)
                self.setFilter1KeyFollow(value: NewPatch.filter1_KeyFollow)
                
                self.setFilter2Mode(value: NewPatch.filter2_Mode)
                self.setFilter2Cutoff(value: NewPatch.filter2_Cutoff)
                self.setFilter2Resonance(value: NewPatch.filter2_Resonance)
                self.setFilter2EnvelopePolarity(value: NewPatch.filter2_EnvelopePolarity)
                self.setFilter2EnvelopeAmount(value: NewPatch.filter2_EnvelopeAmount)
                self.setFilter2KeyFollow(value: NewPatch.filter2_KeyFollow)
                
                self.setFilterBalance(value: NewPatch.filter_Balance)
                self.setFilterSaturationType(value: NewPatch.filter_saturationType)
               // self.setFilterVolumeToSaturation(value: NewPatch.filter_volumeToSaturation)
                self.setFilterRouting(value: NewPatch.filter_routing)
                self.setFilterLink(value: NewPatch.filter_Link)
                self.setFilterCutoffLink(value: NewPatch.filter_cutoffLink)
                self.setFilterKeyfollowBase(value: NewPatch.filter_keyfollowBase)
                
                self.setAmplifierEnvelopeAttack(value: NewPatch.amplifierEnvelope_Attack)
                self.setAmplifierEnvelopeDecay(value: NewPatch.amplifierEnvelope_Decay)
                self.setAmplifierEnvelopeSustain(value: NewPatch.amplifierEnvelope_Sustain)
                self.setAmplifierEnvelopeSustainSlope(value: NewPatch.amplifierEnvelope_SustainSlope)
                self.setAmplifierEnvelopeRelease(value: NewPatch.amplifierEnvelope_Release)
                
                self.setFilterEnvelopeAttack(value: NewPatch.filterEnvelope_Attack)
                self.setFilterEnvelopeDecay(value: NewPatch.filterEnvelope_Decay)
                self.setFilterEnvelopeSustain(value: NewPatch.filterEnvelope_Sustain)
                self.setFilterEnvelopeSustainSlope(value: NewPatch.filterEnvelope_SustainSlope)
                self.setFilterEnvelopeRelease(value: NewPatch.filterEnvelope_Release)
                self.setFilterEnvelopeFollowerInput(value: NewPatch.filterInputFollower_Input)
                
                self.setPatchVolume(value: NewPatch.amp_PatchVolume)
            }
        }
        
        
        // modulation
        delay(by: .seconds(0.2)) { // Runs in Main thread by default
            if (modSection){
                self.setLFO1Mode(value: NewPatch.lfo1_Mode)
                self.setLFO1Rate(value: NewPatch.lfo1_Rate)
                self.setLFO1WaveformShape(value: NewPatch.lfo1_WaveformShape)
                self.setLFO1EnvelopeMode(value: NewPatch.lfo1_EnvelopeMode)
                self.setLFO1WaveformContour(value: NewPatch.lfo1_WaveformContour)
                self.setLFO1Keyfollow(value: NewPatch.lfo1_Keyfollow)
                self.setLFO1TriggerPhase(value: NewPatch.lfo1_TriggerPhase)
                self.setLFO1Clock(value: NewPatch.lfo1_Clock)
                self.setLFO1UserDestination(value: NewPatch.lfo1_UserDestination)
                self.setLFO1UserDestinationAmount(value: NewPatch.lfo1_UserDestinationAmount)
                
                self.setLFO1Osc1(value: NewPatch.lfo1_Osc1)
                self.setLFO1Osc2(value: NewPatch.lfo1_Osc2)
                self.setLFO1Pulsewidth(value: NewPatch.lfo1_Pulsewidth)
                self.setLFO1FilterResonance12(value: NewPatch.lfo1_Pulsewidth)
                self.setLFO1FilterEnvelopeGain(value: NewPatch.lfo1_FilterEnvelopeGain)
                
                self.setLFO2Mode(value: NewPatch.lfo2_Mode)
                self.setLFO2Rate(value: NewPatch.lfo2_Rate)
                self.setLFO2WaveformShape(value: NewPatch.lfo2_WaveformShape)
                self.setLFO2EnvelopeMode(value: NewPatch.lfo2_EnvelopeMode)
                self.setLFO2WaveformContour(value: NewPatch.lfo2_WaveformContour)
                self.setLFO2Keyfollow(value: NewPatch.lfo2_Keyfollow)
                self.setLFO2TriggerPhase(value: NewPatch.lfo2_TriggerPhase)
                self.setLFO2Clock(value: NewPatch.lfo2_Clock)
                self.setLFO2UserDestination(value: NewPatch.lfo2_UserDestination)
                self.setLFO2UserDestinationAmount(value: NewPatch.lfo2_UserDestinationAmount)
                
                self.setLFO2Shape12(value: NewPatch.lfo2_Shape12)
                self.setLFO2FMAmount(value: NewPatch.lfo2_FMAmount)
                self.setLFO2Cutoff12(value: NewPatch.lfo2_Cutoff12)
                self.setLFO2Cutoff2(value: NewPatch.lfo2_Cutoff2)
                self.setLFO2Panorama(value: NewPatch.lfo2_Panorama)
                
                self.setLFO3Rate(value: NewPatch.lfo3_Rate)
                self.setLFO3WaveformShape(value: NewPatch.lfo3_WaveformShape)
                self.setLFO3Mode(value: NewPatch.lfo3_Mode)
                self.setLFO3KeyFollow(value: NewPatch.lfo3_Keyfollow)
                self.setLFO3UserDestination(value: NewPatch.lfo3_UserDestination)
                self.setLFO3UserDestinationAmount(value: NewPatch.lfo3_UserDestinationAmount)
                self.setLFO3FadeInTime(value: NewPatch.lfo3_FadeInTime)
                self.setLFO3Clock(value: NewPatch.lfo3_Clock)
                
                self.setEnvelope3Attack(value: NewPatch.envelope3_Attack)
                self.setEnvelope3Decay(value: NewPatch.envelope3_Decay)
                self.setEnvelope3Sustain(value: NewPatch.envelope3_Sustain)
                self.setEnvelope3SustainSlope(value: NewPatch.envelope3_SustainSlope)
                self.setEnvelope3Release(value: NewPatch.envelope3_Release)
                
                self.setEnvelope4Attack(value: NewPatch.envelope4_Attack)
                self.setEnvelope4Decay(value: NewPatch.envelope4_Decay)
                self.setEnvelope4Sustain(value: NewPatch.envelope4_Sustain)
                self.setEnvelope4SustainSlope(value: NewPatch.envelope4_SustainSlope)
                self.setEnvelope4Release(value: NewPatch.envelope4_Release)
                
                //self.set (value: NewPatch.filterEnvelope_){}
                self.setMatrixSlot1Source(value: NewPatch.matrixSlot1_Source)
                self.setMatrixSlot1Destination1(value: NewPatch.matrixSlot1_Destination1)
                self.setMatrixSlot1Amount1(value: NewPatch.matrixSlot1_Amount1)
                self.setMatrixSlot1Destination2(value: NewPatch.matrixSlot1_Destination2)
                self.setMatrixSlot1Amount2(value: NewPatch.matrixSlot1_Amount2)
                self.setMatrixSlot1Destination3(value: NewPatch.matrixSlot1_Destination3)
                self.setMatrixSlot1Amount3(value: NewPatch.matrixSlot1_Amount3)
                
                self.setMatrixSlot2Source(value: NewPatch.matrixSlot2_Source)
                self.setMatrixSlot2Destination1(value: NewPatch.matrixSlot2_Destination1)
                self.setMatrixSlot2Amount1(value: NewPatch.matrixSlot2_Amount1)
                self.setMatrixSlot2Destination2(value: NewPatch.matrixSlot2_Destination2)
                self.setMatrixSlot2Amount2(value: NewPatch.matrixSlot2_Amount2)
                self.setMatrixSlot2Destination3(value: NewPatch.matrixSlot2_Destination3)
                self.setMatrixSlot2Amount3(value: NewPatch.matrixSlot2_Amount3)
            
                self.setMatrixSlot3Source(value: NewPatch.matrixSlot3_Source)
                self.setMatrixSlot3Destination1(value: NewPatch.matrixSlot3_Destination1)
                self.setMatrixSlot3Amount1(value: NewPatch.matrixSlot3_Amount1)
                self.setMatrixSlot3Destination2(value: NewPatch.matrixSlot3_Destination2)
                self.setMatrixSlot3Amount2(value: NewPatch.matrixSlot3_Amount2)
                self.setMatrixSlot3Destination3(value: NewPatch.matrixSlot3_Destination3)
                self.setMatrixSlot3Amount3(value: NewPatch.matrixSlot3_Amount3)
                
                self.setMatrixSlot4Source(value: NewPatch.matrixSlot4_Source)
                self.setMatrixSlot4Destination1(value: NewPatch.matrixSlot4_Destination1)
                self.setMatrixSlot4Amount1(value: NewPatch.matrixSlot4_Amount1)
                self.setMatrixSlot4Destination2(value: NewPatch.matrixSlot4_Destination2)
                self.setMatrixSlot4Amount2(value: NewPatch.matrixSlot4_Amount2)
                self.setMatrixSlot4Destination3(value: NewPatch.matrixSlot4_Destination3)
                self.setMatrixSlot4Amount3(value: NewPatch.matrixSlot4_Amount3)
                
                self.setMatrixSlot5Source(value: NewPatch.matrixSlot5_Source)
                self.setMatrixSlot5Destination1(value: NewPatch.matrixSlot5_Destination1)
                self.setMatrixSlot5Amount1(value: NewPatch.matrixSlot5_Amount1)
                self.setMatrixSlot5Destination2(value: NewPatch.matrixSlot5_Destination2)
                self.setMatrixSlot5Amount2(value: NewPatch.matrixSlot5_Amount2)
                self.setMatrixSlot5Destination3(value: NewPatch.matrixSlot5_Destination3)
                self.setMatrixSlot5Amount3(value: NewPatch.matrixSlot5_Amount3)
                
                self.setMatrixSlot6Source(value: NewPatch.matrixSlot6_Source)
                self.setMatrixSlot6Destination1(value: NewPatch.matrixSlot6_Destination1)
                self.setMatrixSlot6Amount1(value: NewPatch.matrixSlot6_Amount1)
                self.setMatrixSlot6Destination2(value: NewPatch.matrixSlot6_Destination2)
                self.setMatrixSlot6Amount2(value: NewPatch.matrixSlot6_Amount2)
                self.setMatrixSlot6Destination3(value: NewPatch.matrixSlot6_Destination3)
                self.setMatrixSlot6Amount3(value: NewPatch.matrixSlot6_Amount3)
            }
        }
        
        // controls
        delay(by: .seconds(0.4)) { // Runs in Main thread by default
            if (controlSection) {
                self.setVelocityOsc1WaveformShape(value: NewPatch.velocity_Osc1WaveformShape)
                self.setVelocityOsc2WaveformShape(value: NewPatch.velocity_Osc2WaveformShape)
                self.setVelocityOscPulsewidth(value: NewPatch.velocity_OscPulsewidth)
                self.setVelocityOscFMAmount(value: NewPatch.velocity_OscFMAmount)
                self.setVelocityVolume(value: NewPatch.velocity_Volume)
                self.setVelocityPanorama(value: NewPatch.velocity_Panorama)
                self.setVelocityFilter1EnvAmount(value: NewPatch.velocity_Filter1EnvAmount)
                self.setVelocityFilter1Resonance(value: NewPatch.velocity_Filter1Resonance)
                self.setVelocityFilter2EnvAmount(value: NewPatch.velocity_Filter2EnvAmount)
                self.setVelocityFilter2Resonance(value: NewPatch.velocity_Filter2Resonance)
                self.setSoftknob1Destination(value: NewPatch.control_Softknob1Destination)
                self.setSoftknob1Name(value: NewPatch.control_Softknob1Name)
                self.setSoftknob2Destination(value: NewPatch.control_Softknob2Destination)
                self.setSoftknob2Name(value: NewPatch.control_Softknob2Name)
                self.setSoftknob3Destination(value: NewPatch.control_Softknob3Destination)
                self.setSoftknob3Name(value: NewPatch.control_Softknob3Name)
                self.setBenderDown(value: NewPatch.control_BenderDown)
                self.setBenderUp(value: NewPatch.control_BenderUp)
                self.setBenderScale(value: NewPatch.control_BenderScale)
                self.setParameterSmoothMode(value: NewPatch.control_parameterSmoothMode)
                self.setKeyboardMode (value: NewPatch.control_KeyboardMode)
            }
        }
        
        // Arp patch
        delay(by: .seconds(0.5)) { // Runs in Main thread by default
            if (arpSection) {
                self.setArpMode(value: NewPatch.arp_mode)
                self.setArpPattern(value: NewPatch.arp_pattern)
                self.setArpPatternLength(value: NewPatch.arp_patternLength)
                self.setArpRange(value: NewPatch.arp_range)
                self.setArpHoldMode(value: NewPatch.arp_holdMode)
                self.setArpNoteLength(value: NewPatch.arp_noteLength)
                self.setArpSwingFactor(value: NewPatch.arp_swingFactor)
                self.setArpTempo(value: NewPatch.arp_tempo)
                self.setArpClock(value: NewPatch.arp_clock)
                
                self.setArpStepLength_1(value: NewPatch.arp_stepLength_1)
                self.setArpStepVelocity_1(value: NewPatch.arp_stepVelocity_1)
                self.setArpStep_1(value: NewPatch.arp_step_1)
                
                self.setArpStepLength_2(value: NewPatch.arp_stepLength_2)
                self.setArpStepVelocity_2(value: NewPatch.arp_stepVelocity_2)
                self.setArpStep_2(value: NewPatch.arp_step_2)
                
                self.setArpStepLength_3(value: NewPatch.arp_stepLength_3)
                self.setArpStepVelocity_3(value: NewPatch.arp_stepVelocity_3)
                self.setArpStep_3(value: NewPatch.arp_step_3)
                
                self.setArpStepLength_4(value: NewPatch.arp_stepLength_4)
                self.setArpStepVelocity_4(value: NewPatch.arp_stepVelocity_4)
                self.setArpStep_4(value: NewPatch.arp_step_4)
                
                self.setArpStepLength_5(value: NewPatch.arp_stepLength_5)
                self.setArpStepVelocity_5(value: NewPatch.arp_stepVelocity_5)
                self.setArpStep_5(value: NewPatch.arp_step_5)
                
                self.setArpStepLength_6(value: NewPatch.arp_stepLength_6)
                self.setArpStepVelocity_6(value: NewPatch.arp_stepVelocity_6)
                self.setArpStep_6(value: NewPatch.arp_step_6)
                
                self.setArpStepLength_7(value: NewPatch.arp_stepLength_7)
                self.setArpStepVelocity_7(value: NewPatch.arp_stepVelocity_7)
                self.setArpStep_7(value: NewPatch.arp_step_7)
                
                self.setArpStepLength_8(value: NewPatch.arp_stepLength_8)
                self.setArpStepVelocity_8(value: NewPatch.arp_stepVelocity_8)
                self.setArpStep_8(value: NewPatch.arp_step_8)
                
                self.setArpStepLength_9(value: NewPatch.arp_stepLength_9)
                self.setArpStepVelocity_9(value: NewPatch.arp_stepVelocity_9)
                self.setArpStep_9(value: NewPatch.arp_step_9)
                
                self.setArpStepLength_10(value: NewPatch.arp_stepLength_10)
                self.setArpStepVelocity_10(value: NewPatch.arp_stepVelocity_10)
                self.setArpStep_10(value: NewPatch.arp_step_10)
                
                self.setArpStepLength_11(value: NewPatch.arp_stepLength_11)
                self.setArpStepVelocity_11(value: NewPatch.arp_stepVelocity_11)
                self.setArpStep_11(value: NewPatch.arp_step_11)
                
                self.setArpStepLength_12(value: NewPatch.arp_stepLength_12)
                self.setArpStepVelocity_12(value: NewPatch.arp_stepVelocity_12)
                self.setArpStep_12(value: NewPatch.arp_step_12)
                
                self.setArpStepLength_13(value: NewPatch.arp_stepLength_13)
                self.setArpStepVelocity_13(value: NewPatch.arp_stepVelocity_13)
                self.setArpStep_13(value: NewPatch.arp_step_13)
                
                self.setArpStepLength_14(value: NewPatch.arp_stepLength_14)
                self.setArpStepVelocity_14(value: NewPatch.arp_stepVelocity_14)
                self.setArpStep_14(value: NewPatch.arp_step_14)
                
                self.setArpStepLength_15(value: NewPatch.arp_stepLength_15)
                self.setArpStepVelocity_15(value: NewPatch.arp_stepVelocity_15)
                self.setArpStep_15(value: NewPatch.arp_step_15)
                
                self.setArpStepLength_16(value: NewPatch.arp_stepLength_16)
                self.setArpStepVelocity_16(value: NewPatch.arp_stepVelocity_16)
                self.setArpStep_16(value: NewPatch.arp_step_16)
                
                self.setArpStepLength_17(value: NewPatch.arp_stepLength_17)
                self.setArpStepVelocity_17(value: NewPatch.arp_stepVelocity_17)
                self.setArpStep_17(value: NewPatch.arp_step_17)
                
                self.setArpStepLength_18(value: NewPatch.arp_stepLength_18)
                self.setArpStepVelocity_18(value: NewPatch.arp_stepVelocity_18)
                self.setArpStep_18(value: NewPatch.arp_step_18)
                
                self.setArpStepLength_19(value: NewPatch.arp_stepLength_19)
                self.setArpStepVelocity_19(value: NewPatch.arp_stepVelocity_19)
                self.setArpStep_19(value: NewPatch.arp_step_19)
                
                self.setArpStepLength_20(value: NewPatch.arp_stepLength_20)
                self.setArpStepVelocity_20(value: NewPatch.arp_stepVelocity_20)
                self.setArpStep_20(value: NewPatch.arp_step_20)
                
                self.setArpStepLength_21(value: NewPatch.arp_stepLength_21)
                self.setArpStepVelocity_21(value: NewPatch.arp_stepVelocity_21)
                self.setArpStep_21(value: NewPatch.arp_step_21)
                
                self.setArpStepLength_22(value: NewPatch.arp_stepLength_22)
                self.setArpStepVelocity_22(value: NewPatch.arp_stepVelocity_22)
                self.setArpStep_22(value: NewPatch.arp_step_22)
                
                self.setArpStepLength_23(value: NewPatch.arp_stepLength_23)
                self.setArpStepVelocity_23(value: NewPatch.arp_stepVelocity_23)
                self.setArpStep_23(value: NewPatch.arp_step_23)
                
                self.setArpStepLength_24(value: NewPatch.arp_stepLength_24)
                self.setArpStepVelocity_24(value: NewPatch.arp_stepVelocity_24)
                self.setArpStep_24(value: NewPatch.arp_step_24)
                
                self.setArpStepLength_25(value: NewPatch.arp_stepLength_25)
                self.setArpStepVelocity_25(value: NewPatch.arp_stepVelocity_25)
                self.setArpStep_25(value: NewPatch.arp_step_25)
                
                self.setArpStepLength_26(value: NewPatch.arp_stepLength_26)
                self.setArpStepVelocity_26(value: NewPatch.arp_stepVelocity_26)
                self.setArpStep_26(value: NewPatch.arp_step_26)
                
                self.setArpStepLength_27(value: NewPatch.arp_stepLength_27)
                self.setArpStepVelocity_27(value: NewPatch.arp_stepVelocity_27)
                self.setArpStep_27(value: NewPatch.arp_step_27)
                
                self.setArpStepLength_28(value: NewPatch.arp_stepLength_28)
                self.setArpStepVelocity_28(value: NewPatch.arp_stepVelocity_28)
                self.setArpStep_28(value: NewPatch.arp_step_28)
                
                self.setArpStepLength_29(value: NewPatch.arp_stepLength_29)
                self.setArpStepVelocity_29(value: NewPatch.arp_stepVelocity_29)
                self.setArpStep_29(value: NewPatch.arp_step_29)
                
                self.setArpStepLength_30(value: NewPatch.arp_stepLength_30)
                self.setArpStepVelocity_30(value: NewPatch.arp_stepVelocity_30)
                self.setArpStep_30(value: NewPatch.arp_step_30)
                
                self.setArpStepLength_31(value: NewPatch.arp_stepLength_31)
                self.setArpStepVelocity_31(value: NewPatch.arp_stepVelocity_31)
                self.setArpStep_31(value: NewPatch.arp_step_31)
                
                self.setArpStepLength_32(value: NewPatch.arp_stepLength_32)
                self.setArpStepVelocity_32(value: NewPatch.arp_stepVelocity_32)
                self.setArpStep_32(value: NewPatch.arp_step_32)
            }
        }
        
        // effects
        delay(by: .seconds(0.6)) { // Runs in Main thread by default
            if (effSection){
                self.setCharacterType(value: NewPatch.character_Type)
                self.setCharacterIntensity(value: NewPatch.character_Intensity)
                self.setCharacterTune(value: NewPatch.character_Tune)
            
                self.setChorusType(value: NewPatch.chorus_Type)
                self.setChorusMix(value: NewPatch.chorus_Mix)
                self.setChorusMixClassic(value: NewPatch.chorus_MixClassic)
                self.setChorusLFORate(value: NewPatch.chorus_LFORate)
                self.setChorusLFODepth(value: NewPatch.chorus_LFODepth)
                self.setChorusDistance(value: NewPatch.chorus_Distance)
                self.setChorusDelay(value: NewPatch.chorus_Delay)
                self.setChorusAmount(value: NewPatch.chorus_Amount)
                self.setChorusFeedback(value: NewPatch.chorus_Feedback)
                self.setChorusLowHigh(value: NewPatch.chorus_LowHigh)
                self.setChorusLFOShape(value: NewPatch.chorus_LFOShape)
                self.setChorusXOver(value: NewPatch.chorus_XOver)
            
                self.setPhaserStages(value: NewPatch.phaser_Stages)
                self.setPhaserMix(value: NewPatch.phaser_Mix)
                self.setPhaserLFORate(value: NewPatch.phaser_LFORate)
                self.setPhaserDepth(value: NewPatch.phaser_Depth)
                self.setPhaserFrequency(value: NewPatch.phaser_Frequency)
                self.setPhaserFeedback(value: NewPatch.phaser_Feedback)
                self.setPhaserSpread(value: NewPatch.phaser_Spread)
            
                self.setDistortionType(value: NewPatch.distortion_Type)
                self.setDistortionIntensity(value: NewPatch.distortion_Intensity)
                self.setDistortionTrebleBooster(value: NewPatch.distortion_TrebleBooster)
                self.setDistortionHighCut(value: NewPatch.distortion_HighCut)
                self.setDistortionMix(value: NewPatch.distortion_Mix)
                self.setDistortionQuality(value: NewPatch.distortion_Quality)
                self.setDistortionToneW(value: NewPatch.distortion_ToneW)
            
                self.setInputMode(value: NewPatch.input_Mode)
                self.setInputSelect(value: NewPatch.input_Select)
                self.setAtomizer(value: NewPatch.Atomizer)
                self.setVocoderMode(value: NewPatch.VocoderMode)
            
                self.setEQLowFrequency(value: NewPatch.eq_LowFrequency)
                self.setEQLowGain(value: NewPatch.eq_LowGain)
                self.setEQMidGain(value: NewPatch.eq_LowGain)
                self.setEQMidFrequency(value: NewPatch.eq_MidFrequency)
                self.setEQMidQFactor(value: NewPatch.eq_MidQFactor)
                self.setEQHighFrequency(value: NewPatch.eq_HighFrequency)
                self.setEQHighGain(value: NewPatch.eq_HighGain)
            
                self.setFrequencyShifterType(value: NewPatch.frequencyShifter_Type)
                self.setFrequencyShifterMix(value: NewPatch.frequencyShifter_Mix)
                self.setFrequencyShifterFrequency(value: NewPatch.frequencyShifter_Frequency)
                self.setFrequencyShifterStereoPhase(value: NewPatch.frequencyShifter_StereoPhase)
                self.setFrequencyShifterLeftShape(value: NewPatch.frequencyShifter_LeftShape)
                self.setFrequencyShifterRightShape(value: NewPatch.frequencyShifter_RightShape)
                self.setFrequencyShifterResonance(value: NewPatch.frequencyShifter_Resonance)
                self.setFrequencyShifterFilterType(value: NewPatch.frequencyShifter_FilterType)
                self.setFrequencyShifterPoles(value: NewPatch.frequencyShifter_Poles)
                self.setFrequencyShifterSlope(value: NewPatch.frequencyShifter_Slope)
            }
        }
        
        // ambiance
        delay(by: .seconds(0.7)) { // Runs in Main thread by default
            if (ambSection){
                self.setReverbMode(value: NewPatch.reverb_Mode)
                self.setReverbSend(value: NewPatch.reverb_Send)
                self.setReverbType(value: NewPatch.reverb_Type)
                self.setReverbTime(value: NewPatch.reverb_Time)
                self.setReverbDamping(value: NewPatch.reverb_Damping)
                self.setReverbColor(value: NewPatch.reverb_Color)
                self.setReverbPredelay(value: NewPatch.reverb_Predelay)
                self.setReverbClock(value: NewPatch.reverb_Clock)
                self.setReverbFeedback(value: NewPatch.reverb_Feedback)
                
                self.setDelayMode(value: NewPatch.delay_Mode)
                self.setDelaySend(value: NewPatch.delay_Send)
                self.setDelayTime(value: NewPatch.delay_Time)
                self.setDelayClock(value: NewPatch.delay_Clock)
                self.setDelayFeedback(value: NewPatch.delay_Feedback)
                self.setDelayLFORate(value: NewPatch.delay_LFORate)
                self.setDelayLFODepth(value: NewPatch.delay_LFODepth)
                self.setDelayLFOShape(value: NewPatch.delay_LFOShape)
                self.setDelayColor(value: NewPatch.delay_Color)
                self.setDelayType(value: NewPatch.delay_Type)
                self.setDelayTapeDelayRatio(value: NewPatch.delay_TapeDelayRatio)
                self.setDelayTapeDelayClockLeft(value: NewPatch.delay_TapeDelayClockLeft)
                self.setDelayTapeDelayClockRight(value: NewPatch.delay_TapeDelayClockRight)
                self.setDelayTapeDelayBandwidth(value: NewPatch.delay_TapeDelayBandwidth)
            }
        }
        NotificationCenter.default.post(name: Notification.Name( "UpdateView" ), object: nil)

        return true
    }
    
    func LoadOscPatch( NewPatch: subPatchOsc) -> Bool {
        //CurrentPatch.name = NewPatch.name
        self.setOsc1Model(model: NewPatch.osc1_Model)
        self.setOsc1WaveformShape(value: NewPatch.osc1_WaveformShape)
        self.setOsc1WaveformSelect(value: NewPatch.osc1_WaveformSelect)
        self.setOsc1SemiTone(value: NewPatch.osc1_DetuneInSemitones)
        self.setOsc1KeyFollow(value: NewPatch.osc1_Keyfollow)
        self.setOsc1PulseWidth (value: NewPatch.osc1_Pulsewidth)
        self.setOsc1FormantSpread(value: NewPatch.osc1_FormantSpread)
        self.setOsc1FormantShift(value: NewPatch.osc1_FormantShift)
        self.setOsc1LocalDetune(value: NewPatch.osc1_LocalDetune)
        self.setOsc1Interpolation(value: NewPatch.osc1_Interpolation)
        
        self.setOsc2Model(model: NewPatch.osc2_Model)
        self.setOsc2WaveformShape(value: NewPatch.osc2_WaveformShape)
        self.setOsc2WaveformSelect(value: NewPatch.osc2_WaveformSelect)
        self.setOsc2SemiTone(value: NewPatch.osc2_DetuneInSemitones)
        self.setOsc2KeyFollow(value: NewPatch.osc2_Keyfollow)
        self.setOsc2PulseWidth(value: NewPatch.osc2_Pulsewidth)
        self.setOsc2FineDetune(value: NewPatch.osc2_FineDetune)
        self.setOsc2FormantSpread(value: NewPatch.osc2_FormantSpread)
        self.setOsc2FormantShift(value: NewPatch.osc2_FormantShift)
        self.setOsc2LocalDetune(value: NewPatch.osc2_LocalDetune)
        self.setOsc2Interpolation(value: NewPatch.osc2_Interpolation)
        
        self.setOsc1Osc2Sync(value: NewPatch.osc1_osc2_Sync)
        self.setOsc1Osc2Balance(value: NewPatch.osc1_osc2_Balance)
        
        self.setOsc3Model(model: NewPatch.osc3_Model)
        self.setOsc3Volume(value: NewPatch.osc3_Volume)
        self.setOsc3SemiTone(value: NewPatch.osc3_DetuneInSemitones)
        self.setOsc3FineDetune(value: NewPatch.osc3_FineDetune)
        
        self.setOscSubVolume(value: NewPatch.oscS_Volume)
        self.setOscSubWaveformShapee(value: NewPatch.oscS_WaveformShape)
        
        self.setOscNoiseVolume (value: NewPatch.oscN_Volume)
        self.setOscNoiseColor (value: NewPatch.oscN_Color)
        
        self.setInitialPhase (value: NewPatch.osc_InitialPhase)
        self.setPortamento(value: NewPatch.osc_Portamento)
        self.setPunchIntensity (value: NewPatch.osc_PunchIntensity)
        self.setFMMode (value: NewPatch.osc_FMMode)
        self.setFMAmount (value: NewPatch.osc_FMAmount)
        self.setRingModulation (value: NewPatch.osc_RingModulationVolume )
        self.setRingModulationMix (value: NewPatch.osc_RingModulationMix )
        
        self.setOscSectionVolume(value: NewPatch.osc_SectionVolume)
        self.setOscSectionPatchTransposition(value: NewPatch.osc_SectionPatchTransposition)
        self.setOscSectionSurroundSoundBalance(value: NewPatch.osc_SectionSurroundSoundBalance)
        
        self.setFilterEnvelopeToPitch(value: NewPatch.filterEnvelope_ToPitch)
        self.setFilterEnvelopeToFM(value: NewPatch.filterEnvelope_ToFM)
        
        self.setOsc_Unison_Mode(value: NewPatch.osc_Unison_Mode)
        self.setOsc_Unison_Detune(value: NewPatch.osc_Unison_Detune)
        self.setOsc_Unison_PanoramaSpread(value: NewPatch.osc_Unison_PanoramaSpread)
        self.setOsc_Unison_LFOPhaseOffset(value: NewPatch.osc_Unison_LFOPhaseOffset)
        return true
    }
    
    
    func LoadFilterPatch( NewPatch: subPatchFilters) -> Bool {
        //CurrentPatch.name = NewPatch.name
        
        self.setFilter1Mode(value: NewPatch.filter1_Mode)
        self.setFilter1Cutoff(value: NewPatch.filter1_Cutoff)
        self.setFilter1Resonance(value: NewPatch.filter1_Resonance)
        self.setFilter1EnvelopePolarity(value: NewPatch.filter1_EnvelopePolarity)
        self.setFilter1EnvelopeAmount(value: NewPatch.filter1_EnvelopeAmount)
        self.setFilter1KeyFollow(value: NewPatch.filter1_KeyFollow)
        
        self.setFilter2Mode(value: NewPatch.filter2_Mode)
        self.setFilter2Cutoff(value: NewPatch.filter2_Cutoff)
        self.setFilter2Resonance(value: NewPatch.filter2_Resonance)
        self.setFilter2EnvelopePolarity(value: NewPatch.filter2_EnvelopePolarity)
        self.setFilter2EnvelopeAmount(value: NewPatch.filter2_EnvelopeAmount)
        self.setFilter2KeyFollow(value: NewPatch.filter2_KeyFollow)
        
        self.setFilterBalance(value: NewPatch.filter_Balance)
        self.setFilterSaturationType(value: NewPatch.filter_saturationType)
        //self.setFilterVolumeToSaturation(value: NewPatch.filter_volumeToSaturation)
        self.setFilterRouting(value: NewPatch.filter_routing)
        self.setFilterLink(value: NewPatch.filter_Link)
        self.setFilterCutoffLink(value: NewPatch.filter_cutoffLink)
        self.setFilterKeyfollowBase(value: NewPatch.filter_keyfollowBase)
        
        self.setAmplifierEnvelopeAttack(value: NewPatch.amplifierEnvelope_Attack)
        self.setAmplifierEnvelopeDecay(value: NewPatch.amplifierEnvelope_Decay)
        self.setAmplifierEnvelopeSustain(value: NewPatch.amplifierEnvelope_Sustain)
        self.setAmplifierEnvelopeSustainSlope(value: NewPatch.amplifierEnvelope_SustainSlope)
        self.setAmplifierEnvelopeRelease(value: NewPatch.amplifierEnvelope_Release)
        
        self.setFilterEnvelopeAttack(value: NewPatch.filterEnvelope_Attack)
        self.setFilterEnvelopeDecay(value: NewPatch.filterEnvelope_Decay)
        self.setFilterEnvelopeSustain(value: NewPatch.filterEnvelope_Sustain)
        self.setFilterEnvelopeSustainSlope(value: NewPatch.filterEnvelope_SustainSlope)
        self.setFilterEnvelopeRelease(value: NewPatch.filterEnvelope_Release)
        
        self.setVocoderMode(value: NewPatch.VocoderMode)
        
        self.setPatchVolume(value: NewPatch.amp_PatchVolume)
        return true
    }
    
    func LoadModPatch( NewPatch: subPatchMod) -> Bool {
        //CurrentPatch.name = NewPatch.name
        
        self.setLFO1Mode(value: NewPatch.lfo1_Mode)
        self.setLFO1Rate(value: NewPatch.lfo1_Rate)
        self.setLFO1WaveformShape(value: NewPatch.lfo1_WaveformShape)
        self.setLFO1EnvelopeMode(value: NewPatch.lfo1_EnvelopeMode)
        self.setLFO1WaveformContour(value: NewPatch.lfo1_WaveformContour)
        self.setLFO1Keyfollow(value: NewPatch.lfo1_Keyfollow)
        self.setLFO1TriggerPhase(value: NewPatch.lfo1_TriggerPhase)
        self.setLFO1Clock(value: NewPatch.lfo1_Clock)
        self.setLFO1UserDestination(value: NewPatch.lfo1_UserDestination)
        self.setLFO1UserDestinationAmount(value: NewPatch.lfo1_UserDestinationAmount)
        
        self.setLFO1Osc1(value: NewPatch.lfo1_Osc1)
        self.setLFO1Osc2(value: NewPatch.lfo1_Osc2)
        self.setLFO1Pulsewidth(value: NewPatch.lfo1_Pulsewidth)
        self.setLFO1FilterResonance12(value: NewPatch.lfo1_Pulsewidth)
        self.setLFO1FilterEnvelopeGain(value: NewPatch.lfo1_FilterEnvelopeGain)
        
        self.setLFO2Mode(value: NewPatch.lfo2_Mode)
        self.setLFO2Rate(value: NewPatch.lfo2_Rate)
        self.setLFO2WaveformShape(value: NewPatch.lfo2_WaveformShape)
        self.setLFO2EnvelopeMode(value: NewPatch.lfo2_EnvelopeMode)
        self.setLFO2WaveformContour(value: NewPatch.lfo2_WaveformContour)
        self.setLFO2Keyfollow(value: NewPatch.lfo2_Keyfollow)
        self.setLFO2TriggerPhase(value: NewPatch.lfo2_TriggerPhase)
        self.setLFO2Clock(value: NewPatch.lfo2_Clock)
        self.setLFO2UserDestination(value: NewPatch.lfo2_UserDestination)
        self.setLFO2UserDestinationAmount(value: NewPatch.lfo2_UserDestinationAmount)

        self.setLFO2Shape12(value: NewPatch.lfo2_Shape12)
        self.setLFO2FMAmount(value: NewPatch.lfo2_FMAmount)
        self.setLFO2Cutoff12(value: NewPatch.lfo2_Cutoff12)
        self.setLFO2Cutoff2(value: NewPatch.lfo2_Cutoff2)
        self.setLFO2Panorama(value: NewPatch.lfo2_Panorama)

        self.setLFO3Rate(value: NewPatch.lfo3_Rate)
        self.setLFO3WaveformShape(value: NewPatch.lfo3_WaveformShape)
        self.setLFO3Mode(value: NewPatch.lfo3_Mode)
        self.setLFO3KeyFollow(value: NewPatch.lfo3_Keyfollow)
        self.setLFO3UserDestination(value: NewPatch.lfo3_UserDestination)
        self.setLFO3UserDestinationAmount(value: NewPatch.lfo3_UserDestinationAmount)
        self.setLFO3FadeInTime(value: NewPatch.lfo3_FadeInTime)
        self.setLFO3Clock(value: NewPatch.lfo3_Clock)
        
        self.setEnvelope3Attack(value: NewPatch.envelope3_Attack)
        self.setEnvelope3Decay(value: NewPatch.envelope3_Decay)
        self.setEnvelope3Sustain(value: NewPatch.envelope3_Sustain)
        self.setEnvelope3SustainSlope(value: NewPatch.envelope3_SustainSlope)
        self.setEnvelope3Release(value: NewPatch.envelope3_Release)
        
        self.setEnvelope4Attack(value: NewPatch.envelope4_Attack)
        self.setEnvelope4Decay(value: NewPatch.envelope4_Decay)
        self.setEnvelope4Sustain(value: NewPatch.envelope4_Sustain)
        self.setEnvelope4SustainSlope(value: NewPatch.envelope4_SustainSlope)
        self.setEnvelope4Release(value: NewPatch.envelope4_Release)
        
        //self.set (value: NewPatch.filterEnvelope_){}
        self.setMatrixSlot1Source(value: NewPatch.matrixSlot1_Source)
        self.setMatrixSlot1Destination1(value: NewPatch.matrixSlot1_Destination1)
        self.setMatrixSlot1Amount1(value: NewPatch.matrixSlot1_Amount1)
        self.setMatrixSlot1Destination2(value: NewPatch.matrixSlot1_Destination2)
        self.setMatrixSlot1Amount2(value: NewPatch.matrixSlot1_Amount2)
        self.setMatrixSlot1Destination3(value: NewPatch.matrixSlot1_Destination3)
        self.setMatrixSlot1Amount3(value: NewPatch.matrixSlot1_Amount3)
        
        self.setMatrixSlot2Source(value: NewPatch.matrixSlot2_Source)
        self.setMatrixSlot2Destination1(value: NewPatch.matrixSlot2_Destination1)
        self.setMatrixSlot2Amount1(value: NewPatch.matrixSlot2_Amount1)
        self.setMatrixSlot2Destination2(value: NewPatch.matrixSlot2_Destination2)
        self.setMatrixSlot2Amount2(value: NewPatch.matrixSlot2_Amount2)
        self.setMatrixSlot2Destination3(value: NewPatch.matrixSlot2_Destination3)
        self.setMatrixSlot2Amount3(value: NewPatch.matrixSlot2_Amount3)
        
        self.setMatrixSlot3Source(value: NewPatch.matrixSlot3_Source)
        self.setMatrixSlot3Destination1(value: NewPatch.matrixSlot3_Destination1)
        self.setMatrixSlot3Amount1(value: NewPatch.matrixSlot3_Amount1)
        self.setMatrixSlot3Destination2(value: NewPatch.matrixSlot3_Destination2)
        self.setMatrixSlot3Amount2(value: NewPatch.matrixSlot3_Amount2)
        self.setMatrixSlot3Destination3(value: NewPatch.matrixSlot3_Destination3)
        self.setMatrixSlot3Amount3(value: NewPatch.matrixSlot3_Amount3)
        
        self.setMatrixSlot4Source(value: NewPatch.matrixSlot4_Source)
        self.setMatrixSlot4Destination1(value: NewPatch.matrixSlot4_Destination1)
        self.setMatrixSlot4Amount1(value: NewPatch.matrixSlot4_Amount1)
        self.setMatrixSlot4Destination2(value: NewPatch.matrixSlot4_Destination2)
        self.setMatrixSlot4Amount2(value: NewPatch.matrixSlot4_Amount2)
        self.setMatrixSlot4Destination3(value: NewPatch.matrixSlot4_Destination3)
        self.setMatrixSlot4Amount3(value: NewPatch.matrixSlot4_Amount3)
        
        self.setMatrixSlot5Source(value: NewPatch.matrixSlot5_Source)
        self.setMatrixSlot5Destination1(value: NewPatch.matrixSlot5_Destination1)
        self.setMatrixSlot5Amount1(value: NewPatch.matrixSlot5_Amount1)
        self.setMatrixSlot5Destination2(value: NewPatch.matrixSlot5_Destination2)
        self.setMatrixSlot5Amount2(value: NewPatch.matrixSlot5_Amount2)
        self.setMatrixSlot5Destination3(value: NewPatch.matrixSlot5_Destination3)
        self.setMatrixSlot5Amount3(value: NewPatch.matrixSlot5_Amount3)
        
        self.setMatrixSlot6Source(value: NewPatch.matrixSlot6_Source)
        self.setMatrixSlot6Destination1(value: NewPatch.matrixSlot6_Destination1)
        self.setMatrixSlot6Amount1(value: NewPatch.matrixSlot6_Amount1)
        self.setMatrixSlot6Destination2(value: NewPatch.matrixSlot6_Destination2)
        self.setMatrixSlot6Amount2(value: NewPatch.matrixSlot6_Amount2)
        self.setMatrixSlot6Destination3(value: NewPatch.matrixSlot6_Destination3)
        self.setMatrixSlot6Amount3(value: NewPatch.matrixSlot6_Amount3)
        
        return true
    }
    
    func LoadArpPatch( NewPatch: subPatchArp) -> Bool {
        CurrentPatch.name = NewPatch.name
        
        self.setArpMode(value: NewPatch.arp_mode)
        self.setArpPattern(value: NewPatch.arp_pattern)
        self.setArpPatternLength(value: NewPatch.arp_patternLength)
        self.setArpRange(value: NewPatch.arp_range)
        self.setArpHoldMode(value: NewPatch.arp_holdMode)
        self.setArpNoteLength(value: NewPatch.arp_noteLength)
        self.setArpSwingFactor(value: NewPatch.arp_swingFactor)
        self.setArpTempo(value: NewPatch.arp_tempo)
        self.setArpClock(value: NewPatch.arp_clock)

        self.setArpStepLength_1(value: NewPatch.arp_stepLength_1)
        self.setArpStepVelocity_1(value: NewPatch.arp_stepVelocity_1)
        self.setArpStep_1(value: NewPatch.arp_step_1)
        
        self.setArpStepLength_2(value: NewPatch.arp_stepLength_2)
        self.setArpStepVelocity_2(value: NewPatch.arp_stepVelocity_2)
        self.setArpStep_2(value: NewPatch.arp_step_2)
        
        self.setArpStepLength_3(value: NewPatch.arp_stepLength_3)
        self.setArpStepVelocity_3(value: NewPatch.arp_stepVelocity_3)
        self.setArpStep_3(value: NewPatch.arp_step_3)
        
        self.setArpStepLength_4(value: NewPatch.arp_stepLength_4)
        self.setArpStepVelocity_4(value: NewPatch.arp_stepVelocity_4)
        self.setArpStep_4(value: NewPatch.arp_step_4)
        
        self.setArpStepLength_5(value: NewPatch.arp_stepLength_5)
        self.setArpStepVelocity_5(value: NewPatch.arp_stepVelocity_5)
        self.setArpStep_5(value: NewPatch.arp_step_5)
        
        self.setArpStepLength_6(value: NewPatch.arp_stepLength_6)
        self.setArpStepVelocity_6(value: NewPatch.arp_stepVelocity_6)
        self.setArpStep_6(value: NewPatch.arp_step_6)
        
        self.setArpStepLength_7(value: NewPatch.arp_stepLength_7)
        self.setArpStepVelocity_7(value: NewPatch.arp_stepVelocity_7)
        self.setArpStep_7(value: NewPatch.arp_step_7)
        
        self.setArpStepLength_8(value: NewPatch.arp_stepLength_8)
        self.setArpStepVelocity_8(value: NewPatch.arp_stepVelocity_8)
        self.setArpStep_8(value: NewPatch.arp_step_8)
        
        self.setArpStepLength_9(value: NewPatch.arp_stepLength_9)
        self.setArpStepVelocity_9(value: NewPatch.arp_stepVelocity_9)
        self.setArpStep_9(value: NewPatch.arp_step_9)
        
        self.setArpStepLength_10(value: NewPatch.arp_stepLength_10)
        self.setArpStepVelocity_10(value: NewPatch.arp_stepVelocity_10)
        self.setArpStep_10(value: NewPatch.arp_step_10)
        
        self.setArpStepLength_11(value: NewPatch.arp_stepLength_11)
        self.setArpStepVelocity_11(value: NewPatch.arp_stepVelocity_11)
        self.setArpStep_11(value: NewPatch.arp_step_11)
        
        self.setArpStepLength_12(value: NewPatch.arp_stepLength_12)
        self.setArpStepVelocity_12(value: NewPatch.arp_stepVelocity_12)
        self.setArpStep_12(value: NewPatch.arp_step_12)
        
        self.setArpStepLength_13(value: NewPatch.arp_stepLength_13)
        self.setArpStepVelocity_13(value: NewPatch.arp_stepVelocity_13)
        self.setArpStep_13(value: NewPatch.arp_step_13)
        
        self.setArpStepLength_14(value: NewPatch.arp_stepLength_14)
        self.setArpStepVelocity_14(value: NewPatch.arp_stepVelocity_14)
        self.setArpStep_14(value: NewPatch.arp_step_14)
        
        self.setArpStepLength_15(value: NewPatch.arp_stepLength_15)
        self.setArpStepVelocity_15(value: NewPatch.arp_stepVelocity_15)
        self.setArpStep_15(value: NewPatch.arp_step_15)
        
        self.setArpStepLength_16(value: NewPatch.arp_stepLength_16)
        self.setArpStepVelocity_16(value: NewPatch.arp_stepVelocity_16)
        self.setArpStep_16(value: NewPatch.arp_step_16)
        
        self.setArpStepLength_17(value: NewPatch.arp_stepLength_17)
        self.setArpStepVelocity_17(value: NewPatch.arp_stepVelocity_17)
        self.setArpStep_17(value: NewPatch.arp_step_17)
        
        self.setArpStepLength_18(value: NewPatch.arp_stepLength_18)
        self.setArpStepVelocity_18(value: NewPatch.arp_stepVelocity_18)
        self.setArpStep_18(value: NewPatch.arp_step_18)
        
        self.setArpStepLength_19(value: NewPatch.arp_stepLength_19)
        self.setArpStepVelocity_19(value: NewPatch.arp_stepVelocity_19)
        self.setArpStep_19(value: NewPatch.arp_step_19)
        
        self.setArpStepLength_20(value: NewPatch.arp_stepLength_20)
        self.setArpStepVelocity_20(value: NewPatch.arp_stepVelocity_20)
        self.setArpStep_20(value: NewPatch.arp_step_20)
        
        self.setArpStepLength_21(value: NewPatch.arp_stepLength_21)
        self.setArpStepVelocity_21(value: NewPatch.arp_stepVelocity_21)
        self.setArpStep_21(value: NewPatch.arp_step_21)
        
        self.setArpStepLength_22(value: NewPatch.arp_stepLength_22)
        self.setArpStepVelocity_22(value: NewPatch.arp_stepVelocity_22)
        self.setArpStep_22(value: NewPatch.arp_step_22)
        
        self.setArpStepLength_23(value: NewPatch.arp_stepLength_23)
        self.setArpStepVelocity_23(value: NewPatch.arp_stepVelocity_23)
        self.setArpStep_23(value: NewPatch.arp_step_23)
        
        self.setArpStepLength_24(value: NewPatch.arp_stepLength_24)
        self.setArpStepVelocity_24(value: NewPatch.arp_stepVelocity_24)
        self.setArpStep_24(value: NewPatch.arp_step_24)
        
        self.setArpStepLength_25(value: NewPatch.arp_stepLength_25)
        self.setArpStepVelocity_25(value: NewPatch.arp_stepVelocity_25)
        self.setArpStep_25(value: NewPatch.arp_step_25)
        
        self.setArpStepLength_26(value: NewPatch.arp_stepLength_26)
        self.setArpStepVelocity_26(value: NewPatch.arp_stepVelocity_26)
        self.setArpStep_26(value: NewPatch.arp_step_26)
        
        self.setArpStepLength_27(value: NewPatch.arp_stepLength_27)
        self.setArpStepVelocity_27(value: NewPatch.arp_stepVelocity_27)
        self.setArpStep_27(value: NewPatch.arp_step_27)
        
        self.setArpStepLength_28(value: NewPatch.arp_stepLength_28)
        self.setArpStepVelocity_28(value: NewPatch.arp_stepVelocity_28)
        self.setArpStep_28(value: NewPatch.arp_step_28)
        
        self.setArpStepLength_29(value: NewPatch.arp_stepLength_29)
        self.setArpStepVelocity_29(value: NewPatch.arp_stepVelocity_29)
        self.setArpStep_29(value: NewPatch.arp_step_29)
        
        self.setArpStepLength_30(value: NewPatch.arp_stepLength_30)
        self.setArpStepVelocity_30(value: NewPatch.arp_stepVelocity_30)
        self.setArpStep_30(value: NewPatch.arp_step_30)
        
        self.setArpStepLength_31(value: NewPatch.arp_stepLength_31)
        self.setArpStepVelocity_31(value: NewPatch.arp_stepVelocity_31)
        self.setArpStep_31(value: NewPatch.arp_step_31)
        
        self.setArpStepLength_32(value: NewPatch.arp_stepLength_32)
        self.setArpStepVelocity_32(value: NewPatch.arp_stepVelocity_32)
        self.setArpStep_32(value: NewPatch.arp_step_32)
        
        return true
    }
    
    
    func LoadCharacterPatch( NewPatch: subPatchEffects) -> Bool {
        
        self.setCharacterType(value: NewPatch.character_Type)
        self.setCharacterIntensity(value: NewPatch.character_Intensity)
        self.setCharacterTune(value: NewPatch.character_Tune)
        
        self.setChorusType(value: NewPatch.chorus_Type)
        self.setChorusMix(value: NewPatch.chorus_Mix)
        self.setChorusMixClassic(value: NewPatch.chorus_MixClassic)
        self.setChorusLFORate(value: NewPatch.chorus_LFORate)
        self.setChorusLFODepth(value: NewPatch.chorus_LFODepth)
        self.setChorusDistance(value: NewPatch.chorus_Distance)
        self.setChorusDelay(value: NewPatch.chorus_Delay)
        self.setChorusAmount(value: NewPatch.chorus_Amount)
        self.setChorusFeedback(value: NewPatch.chorus_Feedback)
        self.setChorusLowHigh(value: NewPatch.chorus_LowHigh)
        self.setChorusLFOShape(value: NewPatch.chorus_LFOShape)
        self.setChorusXOver(value: NewPatch.chorus_XOver)
        
        self.setPhaserStages(value: NewPatch.phaser_Stages)
        self.setPhaserMix(value: NewPatch.phaser_Mix)
        self.setPhaserLFORate(value: NewPatch.phaser_LFORate)
        self.setPhaserDepth(value: NewPatch.phaser_Depth)
        self.setPhaserFrequency(value: NewPatch.phaser_Frequency)
        self.setPhaserFeedback(value: NewPatch.phaser_Feedback)
        self.setPhaserSpread(value: NewPatch.phaser_Spread)
        
        self.setDistortionType(value: NewPatch.distortion_Type)
        self.setDistortionIntensity(value: NewPatch.distortion_Intensity)
        self.setDistortionTrebleBooster(value: NewPatch.distortion_TrebleBooster)
        self.setDistortionHighCut(value: NewPatch.distortion_HighCut)
        self.setDistortionMix(value: NewPatch.distortion_Mix)
        self.setDistortionQuality(value: NewPatch.distortion_Quality)
        self.setDistortionToneW(value: NewPatch.distortion_ToneW)
        
        self.setInputMode(value: NewPatch.input_Mode)
        self.setInputSelect(value: NewPatch.input_Select)
        self.setAtomizer(value: NewPatch.Atomizer)
       // self.setVocoderMode(value: NewPatch.VocoderMode)
        
        self.setEQLowFrequency(value: NewPatch.eq_LowFrequency)
        self.setEQLowGain(value: NewPatch.eq_LowGain)
        self.setEQMidGain(value: NewPatch.eq_LowGain)
        self.setEQMidFrequency(value: NewPatch.eq_MidFrequency)
        self.setEQMidQFactor(value: NewPatch.eq_MidQFactor)
        self.setEQHighFrequency(value: NewPatch.eq_HighFrequency)
        self.setEQHighGain(value: NewPatch.eq_HighGain)
        
        self.setFrequencyShifterType(value: NewPatch.frequencyShifter_Type)
        self.setFrequencyShifterMix(value: NewPatch.frequencyShifter_Mix)
        self.setFrequencyShifterFrequency(value: NewPatch.frequencyShifter_Frequency)
        self.setFrequencyShifterStereoPhase(value: NewPatch.frequencyShifter_StereoPhase)
        self.setFrequencyShifterLeftShape(value: NewPatch.frequencyShifter_LeftShape)
        self.setFrequencyShifterRightShape(value: NewPatch.frequencyShifter_RightShape)
        self.setFrequencyShifterResonance(value: NewPatch.frequencyShifter_Resonance)
        self.setFrequencyShifterFilterType(value: NewPatch.frequencyShifter_FilterType)
        self.setFrequencyShifterPoles(value: NewPatch.frequencyShifter_Poles)
        self.setFrequencyShifterSlope(value: NewPatch.frequencyShifter_Slope)
        
        return true
    }
    
    func LoadEffPatch( NewPatch: subPatchAmbiance) -> Bool {
        //CurrentPatch.name = NewPatch.name
        
        
        self.setReverbMode(value: NewPatch.reverb_Mode)
        self.setReverbSend(value: NewPatch.reverb_Send)
        self.setReverbType(value: NewPatch.reverb_Type)
        self.setReverbTime(value: NewPatch.reverb_Time)
        self.setReverbDamping(value: NewPatch.reverb_Damping)
        self.setReverbColor(value: NewPatch.reverb_Color)
        self.setReverbPredelay(value: NewPatch.reverb_Predelay)
        self.setReverbClock(value: NewPatch.reverb_Clock)
        self.setReverbFeedback(value: NewPatch.reverb_Feedback)
        
        self.setDelayMode(value: NewPatch.delay_Mode)
        self.setDelaySend(value: NewPatch.delay_Send)
        self.setDelayTime(value: NewPatch.delay_Time)
        self.setDelayClock(value: NewPatch.delay_Clock)
        self.setDelayFeedback(value: NewPatch.delay_Feedback)
        self.setDelayLFORate(value: NewPatch.delay_LFORate)
        self.setDelayLFODepth(value: NewPatch.delay_LFODepth)
        self.setDelayLFOShape(value: NewPatch.delay_LFOShape)
        self.setDelayColor(value: NewPatch.delay_Color)
        self.setDelayType(value: NewPatch.delay_Type)
        self.setDelayTapeDelayRatio(value: NewPatch.delay_TapeDelayRatio)
        self.setDelayTapeDelayClockLeft(value: NewPatch.delay_TapeDelayClockLeft)
        self.setDelayTapeDelayClockRight(value: NewPatch.delay_TapeDelayClockRight)
        self.setDelayTapeDelayBandwidth(value: NewPatch.delay_TapeDelayBandwidth)
        
        return true
    }
    
    func LoadControlPatch(NewPatch: subPatchControl) -> Bool {
        self.setVelocityOsc1WaveformShape(value: NewPatch.velocity_Osc1WaveformShape)
        self.setVelocityOsc2WaveformShape(value: NewPatch.velocity_Osc2WaveformShape)
        self.setVelocityOscPulsewidth(value: NewPatch.velocity_OscPulsewidth)
        self.setVelocityOscFMAmount(value: NewPatch.velocity_OscFMAmount)
        self.setVelocityVolume(value: NewPatch.velocity_Volume)
        self.setVelocityPanorama(value: NewPatch.velocity_Panorama)
        self.setVelocityFilter1EnvAmount(value: NewPatch.velocity_Filter1EnvAmount)
        self.setVelocityFilter1Resonance(value: NewPatch.velocity_Filter1Resonance)
        self.setVelocityFilter2EnvAmount(value: NewPatch.velocity_Filter2EnvAmount)
        self.setVelocityFilter2Resonance(value: NewPatch.velocity_Filter2Resonance)
        self.setSoftknob1Destination(value: NewPatch.control_Softknob1Destination)
        self.setSoftknob1Name(value: NewPatch.control_Softknob1Name)
        self.setSoftknob2Destination(value: NewPatch.control_Softknob2Destination)
        self.setSoftknob2Name(value: NewPatch.control_Softknob2Name)
        self.setSoftknob3Destination(value: NewPatch.control_Softknob3Destination)
        self.setSoftknob3Name(value: NewPatch.control_Softknob3Name)
        self.setBenderDown(value: NewPatch.control_BenderDown)
        self.setBenderUp(value: NewPatch.control_BenderUp)
        self.setBenderScale(value: NewPatch.control_BenderScale)
        self.setParameterSmoothMode(value: NewPatch.control_parameterSmoothMode)
        self.setKeyboardMode (value: NewPatch.control_KeyboardMode)
        return true
    }
    
    
    // ***********************  Patch name
    
    func getPatchName() -> String {
        return CurrentPatch.name
    }
    
    func setPatchName(newName: String){
        CurrentPatch.name = newName
    }
    
    
    // ------------------------------------------------------------------- OSC 1
    func setOsc1Model (model: Int){
        CurrentPatch.osc1_Model = model
        OscPatch.osc1_Model = model
        ViC.send(type: "sysex", controler: 30, value: model)
    }
    func setOsc1WaveformShape (value: Int){
        CurrentPatch.osc1_WaveformShape = value
        OscPatch.osc1_WaveformShape = value
        ViC.send(type: "cc", controler: 17, value:  value)
    }
    func setOsc1WaveformSelect (value: Int){
        CurrentPatch.osc1_WaveformSelect = value
        OscPatch.osc1_WaveformSelect = value
        ViC.send(type: "cc", controler: 19, value:  value)
    }
    func setOsc1SemiTone (value: Int){
        CurrentPatch.osc1_DetuneInSemitones = value
        OscPatch.osc1_DetuneInSemitones = value
        ViC.send(type: "cc", controler: 20, value: value)
    }
    func setOsc1KeyFollow (value: Int){
        CurrentPatch.osc1_Keyfollow = value
        OscPatch.osc1_Keyfollow = value
        ViC.send(type: "cc", controler: 21, value:  value)
    }
    func setOsc1PulseWidth (value: Int){
        CurrentPatch.osc1_Pulsewidth = value
        OscPatch.osc1_Pulsewidth = value
        ViC.send(type: "cc", controler: 18, value:  value)
    }
    func setOsc1FormantSpread(value: Int){
        CurrentPatch.osc1_FormantSpread = value
        OscPatch.osc1_FormantSpread = value
        ViC.send(type: "sysex", controler: 37, value:  value)
    }
    func setOsc1FormantShift(value: Int){
        CurrentPatch.osc1_FormantShift = value
        OscPatch.osc1_FormantShift = value
        ViC.send(type: "sysex", controler: 42, value:  value)
    }
    func setOsc1LocalDetune(value: Int){
        CurrentPatch.osc1_LocalDetune = value
        OscPatch.osc1_LocalDetune = value
        ViC.send(type: "sysex", controler: 43, value:  value)
    }
    func setOsc1Interpolation(value: Int){
        CurrentPatch.osc1_Interpolation = value
        OscPatch.osc1_Interpolation = value
        ViC.send(type: "sysex", controler: 44, value:  value)
    }
    // ------------------------------------------------------------------- OSC 2
    func setOsc2Model (model: Int){
        CurrentPatch.osc2_Model = model
        OscPatch.osc2_Model = model
        ViC.send(type: "sysex", controler: 35, value: model)
    }
    func setOsc2WaveformShape (value: Int){
        CurrentPatch.osc2_WaveformShape = value
        OscPatch.osc2_WaveformShape = value
        ViC.send(type: "cc", controler: 22, value:  value)
    }
    func setOsc2WaveformSelect (value: Int){
        CurrentPatch.osc2_WaveformSelect = value
        OscPatch.osc2_WaveformSelect = value
        ViC.send(type: "cc", controler: 24, value:  value)
    }
    func setOsc2SemiTone (value: Int){
        CurrentPatch.osc2_DetuneInSemitones = value
        OscPatch.osc2_DetuneInSemitones = value
        ViC.send(type: "cc", controler: 25, value:  value)
    }
    func setOsc2FineDetune (value: Int){
        CurrentPatch.osc2_FineDetune = value
        OscPatch.osc2_FineDetune = value
        ViC.send(type: "cc", controler: 26, value:  value)
    }
    func setOsc2KeyFollow (value: Int){
        CurrentPatch.osc2_Keyfollow = value
        OscPatch.osc2_Keyfollow = value
        ViC.send(type: "cc", controler: 31, value:  value)
    }
    func setOsc2PulseWidth (value: Int){
        CurrentPatch.osc2_Pulsewidth = value
        OscPatch.osc2_Pulsewidth = value
        ViC.send(type: "cc", controler: 23, value:  value)
    }
    func setOsc2FormantSpread(value: Int){
        CurrentPatch.osc2_FormantSpread = value
        OscPatch.osc2_FormantSpread = value
        ViC.send(type: "sysex", controler: 57, value:  value)
    }
    func setOsc2FormantShift(value: Int){
        CurrentPatch.osc2_FormantShift = value
        OscPatch.osc2_FormantShift = value
        ViC.send(type: "sysex", controler: 62, value:  value)
    }
    func setOsc2LocalDetune(value: Int){
        CurrentPatch.osc2_LocalDetune = value
        OscPatch.osc2_LocalDetune = value
        ViC.send(type: "sysex", controler: 63, value:  value)
    }
    func setOsc2Interpolation(value: Int){
        CurrentPatch.osc2_Interpolation = value
        OscPatch.osc2_Interpolation = value
        ViC.send(type: "sysex", controler: 64, value:  value)
    }
    func setOsc1Osc2Sync(value: Int){
        CurrentPatch.osc1_osc2_Sync = value
        OscPatch.osc1_osc2_Sync = value
        ViC.send(type: "cc", controler: 28, value:  value)
    }
    func setOsc1Osc2Balance (value: Int){
        CurrentPatch.osc1_osc2_Balance = value
        OscPatch.osc1_osc2_Balance = value
        ViC.send(type: "cc", controler: 33, value: value)
    }
    // ------------------------------------------------------------------- OSC 3
    func setOsc3Model (model: Int){
        CurrentPatch.osc3_Model = model
        OscPatch.osc3_Model = model
        ViC.send(type: "pp", controler: 41, value: model)
    }
    func setOsc3Volume (value: Int){
        CurrentPatch.osc3_Volume = value
        OscPatch.osc3_Volume = value
        ViC.send(type: "pp", controler: 42, value: value)
    }
    func setOsc3SemiTone (value: Int){
        if (value >= 0 && value <= 127){
            CurrentPatch.osc3_DetuneInSemitones = value
            OscPatch.osc3_DetuneInSemitones = value
            ViC.send(type: "pp", controler: 43, value: value)
        }
    }
    func setOsc3FineDetune (value: Int){
        CurrentPatch.osc3_FineDetune = value
        OscPatch.osc3_FineDetune = value
        ViC.send(type: "pp", controler: 44, value: value)
    }
    // ------------------------------------------------------------------- OSC SUB
    func setOscSubVolume (value: Int){
        CurrentPatch.oscS_Volume = value
        OscPatch.oscS_Volume = value
        ViC.send(type: "cc", controler: 34, value: value)
    }
    func setOscSubWaveformShapee (value: Int){
        CurrentPatch.oscS_WaveformShape = value
        OscPatch.oscS_WaveformShape = value
        ViC.send(type: "cc", controler: 35, value: value)
    }
    // ------------------------------------------------------------------- OSC Noise
    func setOscNoiseVolume (value: Int){
        CurrentPatch.oscN_Volume = value
        OscPatch.oscN_Volume = value
        ViC.send(type: "cc", controler: 37, value: value)
    }
    func setOscNoiseColor (value: Int){
        CurrentPatch.oscN_Color = value
        OscPatch.oscN_Color = value
        ViC.send(type: "cc", controler: 39, value: value)
    }
    // ------------------------------------------------------------------- OSC Section
    
    func setInitialPhase (value: Int){
        CurrentPatch.osc_InitialPhase = value
        OscPatch.osc_InitialPhase = value
        ViC.send(type: "pp", controler: 35, value: value)
    }
    func setPortamento (value: Int){
        CurrentPatch.osc_Portamento = value
        OscPatch.osc_Portamento = value
        ViC.send(type: "cc", controler: 5, value: value)
    }
    func setFilterEnvelopeToPitch(value: Int){
        CurrentPatch.filterEnvelope_ToPitch = value
        OscPatch.filterEnvelope_ToPitch = value
        ViC.send(type: "cc", controler: 29, value: value)
    }
    func setFilterEnvelopeToFM(value: Int){
        CurrentPatch.filterEnvelope_ToFM = value
        OscPatch.filterEnvelope_ToFM = value
        ViC.send(type: "cc", controler: 30, value: value)
    }
    func setPunchIntensity (value: Int){
        CurrentPatch.osc_PunchIntensity = value
        OscPatch.osc_PunchIntensity = value
        ViC.send(type: "pp", controler: 36, value: value)
    }
    func setFMMode (value: Int){
        CurrentPatch.osc_FMMode = value
        OscPatch.osc_FMMode = value
        ViC.send(type: "pp", controler: 34, value: value)
    }
    func setFMAmount (value: Int){
        CurrentPatch.osc_FMAmount = value
        OscPatch.osc_FMAmount = value
        ViC.send(type: "cc", controler: 27, value: value)
    }
    func setRingModulation (value: Int){
        CurrentPatch.osc_RingModulationVolume = value
        OscPatch.osc_RingModulationVolume = value
        ViC.send(type: "cc", controler: 50, value: value)
    }
    func setRingModulationMix (value: Int){
        CurrentPatch.osc_RingModulationMix = value
        OscPatch.osc_RingModulationMix = value
        ViC.send(type: "pp", controler: 99, value: value)
    }
    func setOscSectionPatchTransposition (value: Int){
        CurrentPatch.osc_SectionPatchTransposition = value
        OscPatch.osc_SectionPatchTransposition = value
        ViC.send(type: "cc", controler: 93, value: value)
    }
    func setPatchVolume (value: Int){
        // used in filter section (amp volume)
        CurrentPatch.amp_PatchVolume = value
        FilterPatch.amp_PatchVolume = value
        ViC.send(type: "cc", controler: 91, value: value) // --> Amp volume
        //ViC.send(type: "cc", controler: 7, value: value) // --> Part volume
    }
    func setOscSectionVolume(value: Int){
        //general volume in osc section
        CurrentPatch.osc_SectionVolume = value
        OscPatch.osc_SectionVolume = value
        ViC.send(type: "cc", controler: 36, value: value) // --> Patch volume
    }
    func setOscSectionSurroundSoundBalance(value: Int){
        CurrentPatch.osc_SectionSurroundSoundBalance = value
        OscPatch.osc_SectionSurroundSoundBalance = value
        ViC.send(type: "pp", controler: 58, value: value)
    }
    
    func setOsc_Unison_Mode(value: Int){
        CurrentPatch.osc_Unison_Mode = value
        OscPatch.osc_Unison_Mode = value
        ViC.send(type: "sysex2", controler: 120, value: value)
    }
    func setOsc_Unison_Detune(value: Int){
        CurrentPatch.osc_Unison_Detune = value
        OscPatch.osc_Unison_Detune = value
        ViC.send(type: "sysex2", controler: 121, value: value)
    }
    func setOsc_Unison_PanoramaSpread(value: Int){
        CurrentPatch.osc_Unison_PanoramaSpread = value
        OscPatch.osc_Unison_PanoramaSpread = value
        ViC.send(type: "sysex2", controler: 122, value: value)
    }
    func setOsc_Unison_LFOPhaseOffset(value: Int){
        CurrentPatch.osc_Unison_LFOPhaseOffset = value
        OscPatch.osc_Unison_LFOPhaseOffset = value
        ViC.send(type: "sysex2", controler: 123, value: value)
    }
    
    // MODULATORS
    
    
    // ------------------------------------------------------------------- ENVELOPE 3
    func setEnvelope3Attack(value: Int){
        CurrentPatch.envelope3_Attack = value
        ModulatorPatch.envelope3_Attack = value
        ViC.send(type: "sysex", controler: 80, value: value)
    }
    func setEnvelope3Decay(value: Int){
        CurrentPatch.envelope3_Decay = value
        ModulatorPatch.envelope3_Decay = value
        ViC.send(type: "sysex", controler: 81, value: value)
    }
    func setEnvelope3Sustain(value: Int){
        CurrentPatch.envelope3_Sustain = value
        ModulatorPatch.envelope3_Sustain = value
        ViC.send(type: "sysex", controler: 82, value: value)
    }
    func setEnvelope3SustainSlope(value: Int){
        CurrentPatch.envelope3_SustainSlope = value
        ModulatorPatch.envelope3_SustainSlope = value
        ViC.send(type: "sysex", controler: 83, value: value)
    }
    func setEnvelope3Release(value: Int){
        CurrentPatch.envelope3_Release = value
        ModulatorPatch.envelope3_Release = value
        ViC.send(type: "sysex", controler: 84, value: value)
    }
    
    // ------------------------------------------------------------------- ENVELOPE 4
    func setEnvelope4Attack(value: Int){
        CurrentPatch.envelope4_Attack = value
        ModulatorPatch.envelope4_Attack = value
        ViC.send(type: "sysex", controler: 85, value: value)
    }
    func setEnvelope4Decay(value: Int){
        CurrentPatch.envelope4_Decay = value
        ModulatorPatch.envelope4_Decay = value
        ViC.send(type: "sysex", controler: 86, value: value)
    }
    func setEnvelope4Sustain(value: Int){
        CurrentPatch.envelope4_Sustain = value
        ModulatorPatch.envelope4_Sustain = value
        ViC.send(type: "sysex", controler: 87, value: value)
    }
    func setEnvelope4SustainSlope(value: Int){
        CurrentPatch.envelope4_SustainSlope = value
        ModulatorPatch.envelope4_SustainSlope = value
        ViC.send(type: "sysex", controler: 88, value: value)
    }
    func setEnvelope4Release(value: Int){
        CurrentPatch.envelope4_Release = value
        ModulatorPatch.envelope4_Release = value
        ViC.send(type: "sysex", controler: 89, value: value)
    }
    
    // ------------------------------------------------------------------- LFO 1
    func setLFO1Mode(value: Int){
        CurrentPatch.lfo1_Mode = value
        ModulatorPatch.lfo1_Mode = value
        ViC.send(type: "cc", controler: 70, value: value)
    }
    func setLFO1Rate(value: Int){
        CurrentPatch.lfo1_Rate = value
        ModulatorPatch.lfo1_Rate = value
        ViC.send(type: "cc", controler: 67, value: value)
    }
    func setLFO1WaveformShape(value: Int){
        CurrentPatch.lfo1_WaveformShape = value
        ModulatorPatch.lfo1_WaveformShape = value
        ViC.send(type: "cc", controler: 68, value: value)
    }
    func setLFO1EnvelopeMode(value: Int){
        CurrentPatch.lfo1_EnvelopeMode = value
        ModulatorPatch.lfo1_EnvelopeMode = value
        ViC.send(type: "cc", controler: 69, value: value)
    }
    func setLFO1WaveformContour(value: Int){
        CurrentPatch.lfo1_WaveformContour = value
        ModulatorPatch.lfo1_WaveformContour = value
        ViC.send(type: "cc", controler: 71, value: value)
    }
    func setLFO1Keyfollow(value: Int){
        CurrentPatch.lfo1_Keyfollow = value
        ModulatorPatch.lfo1_Keyfollow = value
        ViC.send(type: "cc", controler: 72, value: value)
    }
    func setLFO1TriggerPhase(value: Int){
        CurrentPatch.lfo1_TriggerPhase = value
        ModulatorPatch.lfo1_TriggerPhase = value
        ViC.send(type: "cc", controler: 73, value: value)
    }
    func setLFO1Clock(value: Int){
        CurrentPatch.lfo1_Clock = value
        ModulatorPatch.lfo1_Clock = value
        ViC.send(type: "pp", controler: 18, value: value)
    }
    func setLFO1UserDestination(value: Int){
        CurrentPatch.lfo1_UserDestination = value
        ModulatorPatch.lfo1_UserDestination = value
        ViC.send(type: "pp", controler: 79, value: value)
    }
    func setLFO1UserDestinationAmount(value: Int){
        CurrentPatch.lfo1_UserDestinationAmount = value
        ModulatorPatch.lfo1_UserDestinationAmount = value
        ViC.send(type: "pp", controler: 80, value: value)
    }
    func setLFO1Osc1(value: Int){
        CurrentPatch.lfo1_Osc1 = value
        ModulatorPatch.lfo1_Osc1 = value
        ViC.send(type: "cc", controler: 74, value: value)
    }
    func setLFO1Osc2(value: Int){
        CurrentPatch.lfo1_Osc2 = value
        ModulatorPatch.lfo1_Osc2 = value
        ViC.send(type: "cc", controler: 75, value: value)
    }
    func setLFO1Pulsewidth(value: Int){
        CurrentPatch.lfo1_Pulsewidth = value
        ModulatorPatch.lfo1_Pulsewidth = value
        ViC.send(type: "cc", controler: 76, value: value)
    }
    func setLFO1FilterResonance12(value: Int){
        CurrentPatch.lfo1_FilterResonance12 = value
        ModulatorPatch.lfo1_FilterResonance12 = value
        ViC.send(type: "cc", controler: 77, value: value)
    }
    func setLFO1FilterEnvelopeGain(value: Int){
        CurrentPatch.lfo1_FilterEnvelopeGain = value
        ModulatorPatch.lfo1_FilterEnvelopeGain = value
        ViC.send(type: "cc", controler: 78, value: value)
    }
    // ------------------------------------------------------------------- LFO 2
    func setLFO2Mode(value: Int){
        CurrentPatch.lfo2_Mode = value
        ModulatorPatch.lfo2_Mode = value
        ViC.send(type: "cc", controler: 82, value: value)
    }
    func setLFO2Rate(value: Int){
        CurrentPatch.lfo2_Rate = value
        ModulatorPatch.lfo2_Rate = value
        ViC.send(type: "cc", controler: 79, value: value)
    }
    func setLFO2WaveformShape(value: Int){
        CurrentPatch.lfo2_WaveformShape = value
        ModulatorPatch.lfo2_WaveformShape = value
        ViC.send(type: "cc", controler: 80, value: value)
    }
    func setLFO2EnvelopeMode(value: Int){
        CurrentPatch.lfo2_EnvelopeMode = value
        ModulatorPatch.lfo2_EnvelopeMode = value
        ViC.send(type: "cc", controler: 81, value: value)
    }
    func setLFO2WaveformContour(value: Int){
        CurrentPatch.lfo2_WaveformContour = value
        ModulatorPatch.lfo2_WaveformContour = value
        ViC.send(type: "cc", controler: 83, value: value)
    }
    func setLFO2Keyfollow(value: Int){
        CurrentPatch.lfo2_Keyfollow = value
        ModulatorPatch.lfo2_Keyfollow = value
        ViC.send(type: "cc", controler: 84, value: value)
    }
    func setLFO2TriggerPhase(value: Int){
        CurrentPatch.lfo2_TriggerPhase = value
        ModulatorPatch.lfo2_TriggerPhase = value
        ViC.send(type: "cc", controler: 85, value: value)
    }
    func setLFO2Clock(value: Int){
        CurrentPatch.lfo2_Clock = value
        ModulatorPatch.lfo2_Clock = value
        ViC.send(type: "pp", controler: 19, value: value)
    }
    func setLFO2UserDestination(value: Int){
        CurrentPatch.lfo2_UserDestination = value
        ModulatorPatch.lfo2_UserDestination = value
        ViC.send(type: "pp", controler: 81, value: value)
    }
    func setLFO2UserDestinationAmount(value: Int){
        CurrentPatch.lfo2_UserDestinationAmount = value
        ModulatorPatch.lfo2_UserDestinationAmount = value
        ViC.send(type: "pp", controler: 82, value: value)
    }
    func setLFO2Shape12(value: Int){
        CurrentPatch.lfo2_Shape12 = value
        ModulatorPatch.lfo2_Shape12 = value
        ViC.send(type: "cc", controler: 86, value: value)
    }
    func setLFO2FMAmount(value: Int){
        CurrentPatch.lfo2_FMAmount = value
        ModulatorPatch.lfo2_FMAmount = value
        ViC.send(type: "cc", controler: 87, value: value)
    }
    func setLFO2Cutoff12(value: Int){
        CurrentPatch.lfo2_Cutoff12 = value
        ModulatorPatch.lfo2_Cutoff12 = value
        ViC.send(type: "cc", controler: 88, value: value)
    }
    func setLFO2Cutoff2(value: Int){
        CurrentPatch.lfo2_Cutoff2 = value
        ModulatorPatch.lfo2_Cutoff2 = value
        ViC.send(type: "cc", controler: 89, value: value)
    }
    func setLFO2Panorama(value: Int){
        CurrentPatch.lfo2_Panorama = value
        ModulatorPatch.lfo2_Panorama = value
        ViC.send(type: "cc", controler: 90, value: value)
    }
    // ------------------------------------------------------------------- LFO 3
    func setLFO3Rate(value: Int){
        CurrentPatch.lfo3_Rate = value
        ModulatorPatch.lfo3_Rate = value
        ViC.send(type: "pp", controler: 7, value: value)
    }
    func setLFO3WaveformShape(value: Int){
        CurrentPatch.lfo3_WaveformShape = value
        ModulatorPatch.lfo3_WaveformShape = value
        ViC.send(type: "pp", controler: 8, value: value)
    }
    func setLFO3Mode(value: Int){
        CurrentPatch.lfo3_Mode = value
        ModulatorPatch.lfo3_Mode = value
        ViC.send(type: "pp", controler: 9, value: value)
    }
    func setLFO3KeyFollow(value: Int){
        CurrentPatch.lfo3_Keyfollow = value
        ModulatorPatch.lfo3_Keyfollow = value
        ViC.send(type: "pp", controler: 10, value: value)
    }
    func setLFO3UserDestination(value: Int){
        CurrentPatch.lfo3_UserDestination = value
        ModulatorPatch.lfo3_UserDestination = value
        ViC.send(type: "pp", controler: 11, value: value)
    }
    func setLFO3UserDestinationAmount(value: Int){
        CurrentPatch.lfo3_UserDestinationAmount = value
        ModulatorPatch.lfo3_UserDestinationAmount = value
        ViC.send(type: "pp", controler: 12, value: value)
    }
    func setLFO3FadeInTime(value: Int){
        CurrentPatch.lfo3_FadeInTime = value
        ModulatorPatch.lfo3_FadeInTime = value
        ViC.send(type: "pp", controler: 13, value: value)
    }
    func setLFO3Clock(value: Int){
        CurrentPatch.lfo3_Clock = value
        ModulatorPatch.lfo3_Clock = value
        ViC.send(type: "pp", controler: 21, value: value)
    }
    // ------------------------------------------------------------------- MATRIX SLOT 1
    func setMatrixSlot1Source(value: Int){
        CurrentPatch.matrixSlot1_Source = value
        ModulatorPatch.matrixSlot1_Source = value
        ViC.send(type: "pp", controler: 64, value: value)
    }
    func setMatrixSlot1Destination1(value: Int){
        CurrentPatch.matrixSlot1_Destination1 = value
        ModulatorPatch.matrixSlot1_Destination1 = value
        ViC.send(type: "pp", controler: 65, value: value)
    }
    func setMatrixSlot1Amount1(value: Int){
        CurrentPatch.matrixSlot1_Amount1 = value
        ModulatorPatch.matrixSlot1_Amount1 = value
        ViC.send(type: "pp", controler: 66, value: value)
    }
    func setMatrixSlot1Destination2(value: Int){
        CurrentPatch.matrixSlot1_Destination2 = value
        ModulatorPatch.matrixSlot1_Destination2 = value
        ViC.send(type: "sysex", controler: 90, value: value)
    }
    func setMatrixSlot1Amount2(value: Int){
        CurrentPatch.matrixSlot1_Amount2 = value
        ModulatorPatch.matrixSlot1_Amount2 = value
        ViC.send(type: "sysex", controler: 91, value: value)
    }
    func setMatrixSlot1Destination3(value: Int){
        CurrentPatch.matrixSlot1_Destination3 = value
        ModulatorPatch.matrixSlot1_Destination3 = value
        ViC.send(type: "sysex", controler: 92, value: value)
    }
    func setMatrixSlot1Amount3(value: Int){
        CurrentPatch.matrixSlot1_Amount3 = value
        ModulatorPatch.matrixSlot1_Amount3 = value
        ViC.send(type: "sysex", controler: 93, value: value)
    }
    // ------------------------------------------------------------------- MATRIX SLOT 2
    func setMatrixSlot2Source(value: Int){
        CurrentPatch.matrixSlot2_Source = value
        ModulatorPatch.matrixSlot2_Source = value
        ViC.send(type: "pp", controler: 67, value: value)
    }
    func setMatrixSlot2Destination1(value: Int){
        CurrentPatch.matrixSlot2_Destination1 = value
        ModulatorPatch.matrixSlot2_Destination1 = value
        ViC.send(type: "pp", controler: 68, value: value)
    }
    func setMatrixSlot2Amount1(value: Int){
        CurrentPatch.matrixSlot2_Amount1 = value
        ModulatorPatch.matrixSlot2_Amount1 = value
        ViC.send(type: "pp", controler: 69, value: value)
    }
    func setMatrixSlot2Destination2(value: Int){
        CurrentPatch.matrixSlot2_Destination2 = value
        ModulatorPatch.matrixSlot2_Destination2 = value
        ViC.send(type: "pp", controler: 70, value: value)
    }
    func setMatrixSlot2Amount2(value: Int){
        CurrentPatch.matrixSlot2_Amount2 = value
        ModulatorPatch.matrixSlot2_Amount2 = value
        ViC.send(type: "pp", controler: 71, value: value)
    }
    func setMatrixSlot2Destination3(value: Int){
        CurrentPatch.matrixSlot2_Destination3 = value
        ModulatorPatch.matrixSlot2_Destination3 = value
        ViC.send(type: "sysex", controler: 94, value: value)
    }
    func setMatrixSlot2Amount3(value: Int){
        CurrentPatch.matrixSlot2_Amount3 = value
        ModulatorPatch.matrixSlot2_Amount3 = value
        ViC.send(type: "sysex", controler: 95, value: value)
    }
    // ------------------------------------------------------------------- MATRIX SLOT 3
    func setMatrixSlot3Source(value: Int){
        CurrentPatch.matrixSlot3_Source = value
        ModulatorPatch.matrixSlot3_Source = value
        ViC.send(type: "pp", controler: 72, value: value)
    }
    func setMatrixSlot3Destination1(value: Int){
        CurrentPatch.matrixSlot3_Destination1 = value
        ModulatorPatch.matrixSlot3_Destination1 = value
        ViC.send(type: "pp", controler: 73, value: value)
    }
    func setMatrixSlot3Amount1(value: Int){
        CurrentPatch.matrixSlot3_Amount1 = value
        ModulatorPatch.matrixSlot3_Amount1 = value
        ViC.send(type: "pp", controler: 74, value: value)
    }
    func setMatrixSlot3Destination2(value: Int){
        CurrentPatch.matrixSlot3_Destination2 = value
        ModulatorPatch.matrixSlot3_Destination2 = value
        ViC.send(type: "pp", controler: 75, value: value)
    }
    func setMatrixSlot3Amount2(value: Int){
        CurrentPatch.matrixSlot3_Amount2 = value
        ModulatorPatch.matrixSlot3_Amount2 = value
        ViC.send(type: "pp", controler: 76, value: value)
    }
    func setMatrixSlot3Destination3(value: Int){
        CurrentPatch.matrixSlot3_Destination3 = value
        ModulatorPatch.matrixSlot3_Destination3 = value
        ViC.send(type: "pp", controler: 77, value: value)
    }
    func setMatrixSlot3Amount3(value: Int){
        CurrentPatch.matrixSlot3_Amount3 = value
        ModulatorPatch.matrixSlot3_Amount3 = value
        ViC.send(type: "pp", controler: 78, value: value)
    }
    // ------------------------------------------------------------------- MATRIX SLOT 4
    func setMatrixSlot4Source(value: Int){
        CurrentPatch.matrixSlot4_Source = value
        ModulatorPatch.matrixSlot4_Source = value
        ViC.send(type: "pp", controler: 103, value: value)
    }
    func setMatrixSlot4Destination1(value: Int){
        CurrentPatch.matrixSlot4_Destination1 = value
        ModulatorPatch.matrixSlot4_Destination1 = value
        ViC.send(type: "pp", controler: 104, value: value)
    }
    func setMatrixSlot4Amount1(value: Int){
        CurrentPatch.matrixSlot4_Amount1 = value
        ModulatorPatch.matrixSlot4_Amount1 = value
        ViC.send(type: "pp", controler: 105, value: value)
    }
    func setMatrixSlot4Destination2(value: Int){
        CurrentPatch.matrixSlot4_Destination2 = value
        ModulatorPatch.matrixSlot4_Destination2 = value
        ViC.send(type: "sysex", controler: 96, value: value)
    }
    func setMatrixSlot4Amount2(value: Int){
        CurrentPatch.matrixSlot4_Amount2 = value
        ModulatorPatch.matrixSlot4_Amount2 = value
        ViC.send(type: "sysex", controler: 97, value: value)
    }
    func setMatrixSlot4Destination3(value: Int){
        CurrentPatch.matrixSlot4_Destination3 = value
        ModulatorPatch.matrixSlot4_Destination3 = value
        ViC.send(type: "sysex", controler: 98, value: value)
    }
    func setMatrixSlot4Amount3(value: Int){
        CurrentPatch.matrixSlot4_Amount3 = value
        ModulatorPatch.matrixSlot4_Amount3 = value
        ViC.send(type: "sysex", controler: 99, value: value)
    }
    // ------------------------------------------------------------------- MATRIX SLOT 5
    func setMatrixSlot5Source(value: Int){
        CurrentPatch.matrixSlot5_Source = value
        ModulatorPatch.matrixSlot5_Source = value
        ViC.send(type: "pp", controler: 106, value: value)
    }
    func setMatrixSlot5Destination1(value: Int){
        CurrentPatch.matrixSlot5_Destination1 = value
        ModulatorPatch.matrixSlot5_Destination1 = value
        ViC.send(type: "pp", controler: 107, value: value)
    }
    func setMatrixSlot5Amount1(value: Int){
        CurrentPatch.matrixSlot5_Amount1 = value
        ModulatorPatch.matrixSlot5_Amount1 = value
        ViC.send(type: "pp", controler: 108, value: value)
    }
    func setMatrixSlot5Destination2(value: Int){
        CurrentPatch.matrixSlot5_Destination2 = value
        ModulatorPatch.matrixSlot5_Destination2 = value
        ViC.send(type: "sysex", controler: 100, value: value)
    }
    func setMatrixSlot5Amount2(value: Int){
        CurrentPatch.matrixSlot5_Amount2 = value
        ModulatorPatch.matrixSlot5_Amount2 = value
        ViC.send(type: "sysex", controler: 101, value: value)
    }
    func setMatrixSlot5Destination3(value: Int){
        CurrentPatch.matrixSlot5_Destination3 = value
        ModulatorPatch.matrixSlot5_Destination3 = value
        ViC.send(type: "sysex", controler: 102, value: value)
    }
    func setMatrixSlot5Amount3(value: Int){
        CurrentPatch.matrixSlot5_Amount3 = value
        ModulatorPatch.matrixSlot5_Amount3 = value
        ViC.send(type: "sysex", controler: 103, value: value)
    }
    // ------------------------------------------------------------------- MATRIX SLOT 6
    func setMatrixSlot6Source(value: Int){
        CurrentPatch.matrixSlot6_Source = value
        ModulatorPatch.matrixSlot6_Source = value
        ViC.send(type: "pp", controler: 109, value: value)
    }
    func setMatrixSlot6Destination1(value: Int){
        CurrentPatch.matrixSlot6_Destination1 = value
        ModulatorPatch.matrixSlot6_Destination1 = value
        ViC.send(type: "pp", controler: 110, value: value)
    }
    func setMatrixSlot6Amount1(value: Int){
        CurrentPatch.matrixSlot6_Amount1 = value
        ModulatorPatch.matrixSlot6_Amount1 = value
        ViC.send(type: "pp", controler: 111, value: value)
    }
    func setMatrixSlot6Destination2(value: Int){
        CurrentPatch.matrixSlot6_Destination2 = value
        ModulatorPatch.matrixSlot6_Destination2 = value
        ViC.send(type: "sysex", controler: 104, value: value)
    }
    func setMatrixSlot6Amount2(value: Int){
        CurrentPatch.matrixSlot6_Amount2 = value
        ModulatorPatch.matrixSlot6_Amount2 = value
        ViC.send(type: "sysex", controler: 105, value: value)
    }
    func setMatrixSlot6Destination3(value: Int){
        CurrentPatch.matrixSlot6_Destination3 = value
        ModulatorPatch.matrixSlot6_Destination3 = value
        ViC.send(type: "sysex", controler: 106, value: value)
    }
    func setMatrixSlot6Amount3(value: Int){
        CurrentPatch.matrixSlot6_Amount3 = value
        ModulatorPatch.matrixSlot6_Amount3 = value
        ViC.send(type: "sysex", controler: 107, value: value)
    }
    // ------------------------------------------------------------------- Filter 1
    // FILTER SECTION gecontroleerd
    func setFilter1Mode(value: Int){
        CurrentPatch.filter1_Mode = value
        FilterPatch.filter1_Mode = value
        ViC.send(type: "cc", controler: 51, value: value)
    }
    func setFilter1Cutoff(value: Int){
        CurrentPatch.filter1_Cutoff = value
        FilterPatch.filter1_Cutoff = value
        ViC.send(type: "cc", controler: 40, value: value)
    }
    func setFilter1Resonance(value: Int){
        CurrentPatch.filter1_Resonance = value
        FilterPatch.filter1_Resonance = value
        ViC.send(type: "cc", controler: 42, value: value)
    }
    func setFilter1EnvelopePolarity(value: Int){
        CurrentPatch.filter1_EnvelopePolarity = value
        FilterPatch.filter1_EnvelopePolarity = value
        ViC.send(type: "pp", controler: 30, value: value)
    }
    func setFilter1EnvelopeAmount(value: Int){
        CurrentPatch.filter1_EnvelopeAmount = value
        FilterPatch.filter1_EnvelopeAmount = value
        ViC.send(type: "cc", controler: 44, value: value)
    }
    func setFilter1KeyFollow(value: Int){
        CurrentPatch.filter1_KeyFollow = value
        FilterPatch.filter1_KeyFollow = value
        ViC.send(type: "cc", controler: 46, value: value)
    }
    // ------------------------------------------------------------------- FILTER 2
    func setFilter2Mode(value: Int){
        CurrentPatch.filter2_Mode = value
        FilterPatch.filter2_Mode = value
        ViC.send(type: "cc", controler: 52, value: value)
    }
    func setFilter2Cutoff(value: Int){
        CurrentPatch.filter2_Cutoff = value
        FilterPatch.filter2_Cutoff = value
        ViC.send(type: "cc", controler: 41, value: value)
    }
    func setFilter2Resonance(value: Int){
        CurrentPatch.filter2_Resonance = value
        FilterPatch.filter2_Resonance = value
        ViC.send(type: "cc", controler: 43, value: value)
    }
    func setFilter2EnvelopePolarity(value: Int){
        CurrentPatch.filter2_EnvelopePolarity = value
        FilterPatch.filter2_EnvelopePolarity = value
        ViC.send(type: "pp", controler: 31, value: value)
    }
    func setFilter2EnvelopeAmount(value: Int){
        CurrentPatch.filter2_EnvelopeAmount = value
        FilterPatch.filter2_EnvelopeAmount = value
        ViC.send(type: "cc", controler: 45, value: value)
    }
    func setFilter2KeyFollow(value: Int){
        CurrentPatch.filter2_KeyFollow = value
        FilterPatch.filter2_KeyFollow = value
        ViC.send(type: "cc", controler: 47, value: value)
    }
    // ------------------------------------------------------------------- FILTER SECTION
    func setFilterBalance(value: Int){
        CurrentPatch.filter_Balance = value
        FilterPatch.filter_Balance = value
        ViC.send(type: "cc", controler: 48, value: value)
    }
    func setFilterSaturationType(value: Int){
        CurrentPatch.filter_saturationType = value
        FilterPatch.filter_saturationType = value
        ViC.send(type: "cc", controler: 49, value: value)
    }
   // func setFilterVolumeToSaturation(value: Int){
   //     CurrentPatch.filter_volumeToSaturation = value
   //     FilterPatch.filter_volumeToSaturation = value
   //     ViC.send(type: "cc", controler: 36, value: value)
   // }
    func setFilterRouting(value: Int){
        CurrentPatch.filter_routing = value
        FilterPatch.filter_routing = value
        ViC.send(type: "cc", controler: 53, value: value)
    }
    func setFilterLink(value: Int){
        CurrentPatch.filter_Link = value
        FilterPatch.filter_Link = value
        ViC.send(type: "pp", controler: 122, value: value)
    }
    func setFilterCutoffLink(value: Int){
        CurrentPatch.filter_cutoffLink = value
        FilterPatch.filter_cutoffLink = value
        ViC.send(type: "pp", controler: 32, value: value)
    }
    func setFilterKeyfollowBase(value: Int){
        CurrentPatch.filter_keyfollowBase = value
        FilterPatch.filter_keyfollowBase = value
        ViC.send(type: "pp", controler: 33, value: value)
    }
    
    // ------------------------------------------------------------------- AMPLIFIER ENVELOPE
    func setAmplifierEnvelopeAttack(value: Int){
        CurrentPatch.amplifierEnvelope_Attack = value
        FilterPatch.amplifierEnvelope_Attack = value
        ViC.send(type: "cc", controler: 59, value: value)
    }
    func setAmplifierEnvelopeDecay(value: Int){
        CurrentPatch.amplifierEnvelope_Decay = value
        FilterPatch.amplifierEnvelope_Decay = value
        ViC.send(type: "cc", controler: 60, value: value)
    }
    func setAmplifierEnvelopeSustain(value: Int){
        CurrentPatch.amplifierEnvelope_Sustain = value
        FilterPatch.amplifierEnvelope_Sustain = value
        ViC.send(type: "cc", controler: 61, value: value)
    }
    func setAmplifierEnvelopeSustainSlope(value: Int){
        CurrentPatch.amplifierEnvelope_SustainSlope = value
        FilterPatch.amplifierEnvelope_SustainSlope = value
        ViC.send(type: "cc", controler: 62, value: value)
    }
    func setAmplifierEnvelopeRelease(value: Int){
        CurrentPatch.amplifierEnvelope_Release = value
        FilterPatch.amplifierEnvelope_Release = value
        ViC.send(type: "cc", controler: 63, value: value)
    }
    
    // ------------------------------------------------------------------- FILTER ENVELOPE
    func setFilterEnvelopeAttack(value: Int){
        CurrentPatch.filterEnvelope_Attack = value
        FilterPatch.filterEnvelope_Attack = value
        ViC.send(type: "cc", controler: 54, value: value)
    }
    func setFilterEnvelopeDecay(value: Int){
        CurrentPatch.filterEnvelope_Decay = value
        FilterPatch.filterEnvelope_Decay = value
        ViC.send(type: "cc", controler: 55, value: value)
    }
    func setFilterEnvelopeSustain(value: Int){
        CurrentPatch.filterEnvelope_Sustain = value
        FilterPatch.filterEnvelope_Sustain = value
        ViC.send(type: "cc", controler: 56, value: value)
    }
    func setFilterEnvelopeSustainSlope(value: Int){
        CurrentPatch.filterEnvelope_SustainSlope = value
        FilterPatch.filterEnvelope_SustainSlope = value
        ViC.send(type: "cc", controler: 57, value: value)
    }
    func setFilterEnvelopeRelease(value: Int){
        CurrentPatch.filterEnvelope_Release = value
        FilterPatch.filterEnvelope_Release = value
        ViC.send(type: "cc", controler: 58, value: value)
    }
    
    func setFilterEnvelopeFollowerInput(value: Int){
        CurrentPatch.filterInputFollower_Input = value
        FilterPatch.filterInputFollower_Input = value
        ViC.send(type: "cc", controler: 38, value: value)
    }
    
    
    // ------------------------------------------------------------------- ARPEGGIATOR
    // ***********************  ARPEGGIATOR SUBPATCH SECTION
    func setArpPattern(value: Int){
        CurrentPatch.arp_pattern = value
        ArpPatch.arp_pattern = value
        ViC.send(type: "pp", controler: 2, value: value)
    }
    
    func setArpPatternLength(value: Int){
        CurrentPatch.arp_patternLength = value
        ArpPatch.arp_patternLength = value
        ViC.send(type: "sysex", controler: 127, value: value)
    }
    func setArpRange(value: Int){
        CurrentPatch.arp_range = value
        ArpPatch.arp_range = value
        ViC.send(type: "pp", controler: 3, value: value)
    }
    func setArpHoldMode(value: Int){
        CurrentPatch.arp_holdMode = value
        ArpPatch.arp_holdMode = value
        ViC.send(type: "pp", controler: 4, value: value)
    }
    func setArpNoteLength(value: Int){
        CurrentPatch.arp_noteLength = value
        ArpPatch.arp_noteLength = value
        ViC.send(type: "pp", controler: 5, value: value)
    }
    func setArpSwingFactor(value: Int){
        CurrentPatch.arp_swingFactor = value
        ArpPatch.arp_swingFactor = value
        ViC.send(type: "pp", controler: 6, value: value)
    }
    func setArpMode(value: Int){
        CurrentPatch.arp_mode = value
        ArpPatch.arp_mode = value
        ViC.send(type: "pp", controler: 15, value: value)
    }
    func setArpTempo(value: Int){
        CurrentPatch.arp_tempo = value
        ArpPatch.arp_tempo = value
        ViC.send(type: "pp", controler: 16, value: value)
    }
    func setArpClock(value: Int){
        CurrentPatch.arp_clock = value
        ArpPatch.arp_clock = value
        ViC.send(type: "pp", controler: 17, value: value)
    }
    

    
    func setArpStepLength_1(value: Int){
        CurrentPatch.arp_stepLength_1 = value
        ArpPatch.arp_stepLength_1 = value
        ViC.send(type: "sysex2", controler: 0, value: value)
    }
    func setArpStepVelocity_1(value: Int){
        CurrentPatch.arp_stepVelocity_1 = value
        ArpPatch.arp_stepVelocity_1 = value
        ViC.send(type: "sysex2", controler: 1, value: value)
    }
    func setArpStep_1(value: Int){
        CurrentPatch.arp_step_1 = value
        ArpPatch.arp_step_1 = value
        ViC.send(type: "sysex2", controler: 2, value: value)
    }
    
    func setArpStepLength_2(value: Int){
        CurrentPatch.arp_stepLength_2 = value
        ArpPatch.arp_stepLength_2 = value
        ViC.send(type: "sysex2", controler: 3, value: value)
    }
    func setArpStepVelocity_2(value: Int){
        CurrentPatch.arp_stepVelocity_2 = value
        ArpPatch.arp_stepVelocity_2 = value
        ViC.send(type: "sysex2", controler: 4, value: value)
    }
    func setArpStep_2(value: Int){
        CurrentPatch.arp_step_2 = value
        ArpPatch.arp_step_2 = value
        ViC.send(type: "sysex2", controler: 5, value: value)
    }
    
    func setArpStepLength_3(value: Int){
        CurrentPatch.arp_stepLength_3 = value
        ArpPatch.arp_stepLength_3 = value
        ViC.send(type: "sysex2", controler: 6, value: value)
    }
    func setArpStepVelocity_3(value: Int){
        CurrentPatch.arp_stepVelocity_3 = value
        ArpPatch.arp_stepVelocity_3 = value
        ViC.send(type: "sysex2", controler: 7, value: value)
    }
    func setArpStep_3(value: Int){
        CurrentPatch.arp_step_3 = value
        ArpPatch.arp_step_3 = value
        ViC.send(type: "sysex2", controler: 8, value: value)
    }
    
    func setArpStepLength_4(value: Int){
        CurrentPatch.arp_stepLength_4 = value
        ArpPatch.arp_stepLength_4 = value
        ViC.send(type: "sysex2", controler: 9, value: value)
    }
    func setArpStepVelocity_4(value: Int){
        CurrentPatch.arp_stepVelocity_4 = value
        ArpPatch.arp_stepVelocity_4 = value
        ViC.send(type: "sysex2", controler: 10, value: value)
    }
    func setArpStep_4(value: Int){
        CurrentPatch.arp_step_4 = value
        ArpPatch.arp_step_4 = value
        ViC.send(type: "sysex2", controler: 11, value: value)
    }
    
    func setArpStepLength_5(value: Int){
        CurrentPatch.arp_stepLength_5 = value
        ArpPatch.arp_stepLength_5 = value
        ViC.send(type: "sysex2", controler: 12, value: value)
    }
    func setArpStepVelocity_5(value: Int){
        CurrentPatch.arp_stepVelocity_5 = value
        ArpPatch.arp_stepVelocity_5 = value
        ViC.send(type: "sysex2", controler: 13, value: value)
    }
    func setArpStep_5(value: Int){
        CurrentPatch.arp_step_5 = value
        ArpPatch.arp_step_5 = value
        ViC.send(type: "sysex2", controler: 14, value: value)
    }
    
    func setArpStepLength_6(value: Int){
        CurrentPatch.arp_stepLength_6 = value
        ArpPatch.arp_stepLength_6 = value
        ViC.send(type: "sysex2", controler: 15, value: value)
    }
    func setArpStepVelocity_6(value: Int){
        CurrentPatch.arp_stepVelocity_6 = value
        ArpPatch.arp_stepVelocity_6 = value
        ViC.send(type: "sysex2", controler: 16, value: value)
    }
    func setArpStep_6(value: Int){
        CurrentPatch.arp_step_6 = value
        ArpPatch.arp_step_6 = value
        ViC.send(type: "sysex2", controler: 17, value: value)
    }
    
    func setArpStepLength_7(value: Int){
        CurrentPatch.arp_stepLength_7 = value
        ArpPatch.arp_stepLength_7 = value
        ViC.send(type: "sysex2", controler: 18, value: value)
    }
    func setArpStepVelocity_7(value: Int){
        CurrentPatch.arp_stepVelocity_7 = value
        ArpPatch.arp_stepVelocity_7 = value
        ViC.send(type: "sysex2", controler: 19, value: value)
    }
    func setArpStep_7(value: Int){
        CurrentPatch.arp_step_7 = value
        ArpPatch.arp_step_7 = value
        ViC.send(type: "sysex2", controler: 20, value: value)
    }
    
    func setArpStepLength_8(value: Int){
        CurrentPatch.arp_stepLength_8 = value
        ArpPatch.arp_stepLength_8 = value
        ViC.send(type: "sysex2", controler: 21, value: value)
    }
    func setArpStepVelocity_8(value: Int){
        CurrentPatch.arp_stepVelocity_8 = value
        ArpPatch.arp_stepVelocity_8 = value
        ViC.send(type: "sysex2", controler: 22, value: value)
    }
    func setArpStep_8(value: Int){
        CurrentPatch.arp_step_8 = value
        ArpPatch.arp_step_8 = value
        ViC.send(type: "sysex2", controler: 23, value: value)
    }
    
    func setArpStepLength_9(value: Int){
        CurrentPatch.arp_stepLength_9 = value
        ArpPatch.arp_stepLength_9 = value
        ViC.send(type: "sysex2", controler: 24, value: value)
    }
    func setArpStepVelocity_9(value: Int){
        CurrentPatch.arp_stepVelocity_9 = value
        ArpPatch.arp_stepVelocity_9 = value
        ViC.send(type: "sysex2", controler: 25, value: value)
    }
    func setArpStep_9(value: Int){
        CurrentPatch.arp_step_9 = value
        ArpPatch.arp_step_9 = value
        ViC.send(type: "sysex2", controler: 26, value: value)
    }
    
    func setArpStepLength_10(value: Int){
        CurrentPatch.arp_stepLength_10 = value
        ArpPatch.arp_stepLength_10 = value
        ViC.send(type: "sysex2", controler: 27, value: value)
    }
    func setArpStepVelocity_10(value: Int){
        CurrentPatch.arp_stepVelocity_10 = value
        ArpPatch.arp_stepVelocity_10 = value
        ViC.send(type: "sysex2", controler: 28, value: value)
    }
    func setArpStep_10(value: Int){
        CurrentPatch.arp_step_10 = value
        ArpPatch.arp_step_10 = value
        ViC.send(type: "sysex2", controler: 29, value: value)
    }
    
    func setArpStepLength_11(value: Int){
        CurrentPatch.arp_stepLength_11 = value
        ArpPatch.arp_stepLength_11 = value
        ViC.send(type: "sysex2", controler: 30, value: value)
    }
    func setArpStepVelocity_11(value: Int){
        CurrentPatch.arp_stepVelocity_11 = value
        ArpPatch.arp_stepVelocity_11 = value
        ViC.send(type: "sysex2", controler: 31, value: value)
    }
    func setArpStep_11(value: Int){
        CurrentPatch.arp_step_11 = value
        ArpPatch.arp_step_11 = value
        ViC.send(type: "sysex2", controler: 32, value: value)
    }
    
    func setArpStepLength_12(value: Int){
        CurrentPatch.arp_stepLength_12 = value
        ArpPatch.arp_stepLength_12 = value
        ViC.send(type: "sysex2", controler: 33, value: value)
    }
    func setArpStepVelocity_12(value: Int){
        CurrentPatch.arp_stepVelocity_12 = value
        ArpPatch.arp_stepVelocity_12 = value
        ViC.send(type: "sysex2", controler: 34, value: value)
    }
    func setArpStep_12(value: Int){
        CurrentPatch.arp_step_12 = value
        ArpPatch.arp_step_12 = value
        ViC.send(type: "sysex2", controler: 35, value: value)
    }
    
    func setArpStepLength_13(value: Int){
        CurrentPatch.arp_stepLength_13 = value
        ArpPatch.arp_stepLength_13 = value
        ViC.send(type: "sysex2", controler: 36, value: value)
    }
    func setArpStepVelocity_13(value: Int){
        CurrentPatch.arp_stepVelocity_13 = value
        ArpPatch.arp_stepVelocity_13 = value
        ViC.send(type: "sysex2", controler: 37, value: value)
    }
    func setArpStep_13(value: Int){
        CurrentPatch.arp_step_13 = value
        ArpPatch.arp_step_13 = value
        ViC.send(type: "sysex2", controler: 38, value: value)
    }
    
    func setArpStepLength_14(value: Int){
        CurrentPatch.arp_stepLength_14 = value
        ArpPatch.arp_stepLength_14 = value
        ViC.send(type: "sysex2", controler: 39, value: value)
    }
    func setArpStepVelocity_14(value: Int){
        CurrentPatch.arp_stepVelocity_14 = value
        ArpPatch.arp_stepVelocity_14 = value
        ViC.send(type: "sysex2", controler: 40, value: value)
    }
    func setArpStep_14(value: Int){
        CurrentPatch.arp_step_14 = value
        ArpPatch.arp_step_14 = value
        ViC.send(type: "sysex2", controler: 41, value: value)
    }
    
    func setArpStepLength_15(value: Int){
        CurrentPatch.arp_stepLength_15 = value
        ArpPatch.arp_stepLength_15 = value
        ViC.send(type: "sysex2", controler: 42, value: value)
    }
    func setArpStepVelocity_15(value: Int){
        CurrentPatch.arp_stepVelocity_15 = value
        ArpPatch.arp_stepVelocity_15 = value
        ViC.send(type: "sysex2", controler: 43, value: value)
    }
    func setArpStep_15(value: Int){
        CurrentPatch.arp_step_15 = value
        ArpPatch.arp_step_15 = value
        ViC.send(type: "sysex2", controler: 44, value: value)
    }
    
    func setArpStepLength_16(value: Int){
        CurrentPatch.arp_stepLength_16 = value
        ArpPatch.arp_stepLength_16 = value
        ViC.send(type: "sysex2", controler: 45, value: value)
    }
    func setArpStepVelocity_16(value: Int){
        CurrentPatch.arp_stepVelocity_16 = value
        ArpPatch.arp_stepVelocity_16 = value
        ViC.send(type: "sysex2", controler: 46, value: value)
    }
    func setArpStep_16(value: Int){
        CurrentPatch.arp_step_16 = value
        ArpPatch.arp_step_16 = value
        ViC.send(type: "sysex2", controler: 47, value: value)
    }
    
    func setArpStepLength_17(value: Int){
        CurrentPatch.arp_stepLength_17 = value
        ArpPatch.arp_stepLength_17 = value
        ViC.send(type: "sysex2", controler: 48, value: value)
    }
    func setArpStepVelocity_17(value: Int){
        CurrentPatch.arp_stepVelocity_17 = value
        ArpPatch.arp_stepVelocity_17 = value
        ViC.send(type: "sysex2", controler: 49, value: value)
    }
    func setArpStep_17(value: Int){
        CurrentPatch.arp_step_17 = value
        ArpPatch.arp_step_17 = value
        ViC.send(type: "sysex2", controler: 50, value: value)
    }
    
    func setArpStepLength_18(value: Int){
        CurrentPatch.arp_stepLength_18 = value
        ArpPatch.arp_stepLength_18 = value
        ViC.send(type: "sysex2", controler: 51, value: value)
    }
    func setArpStepVelocity_18(value: Int){
        CurrentPatch.arp_stepVelocity_18 = value
        ArpPatch.arp_stepVelocity_18 = value
        ViC.send(type: "sysex2", controler: 52, value: value)
    }
    func setArpStep_18(value: Int){
        CurrentPatch.arp_step_18 = value
        ArpPatch.arp_step_18 = value
        ViC.send(type: "sysex2", controler: 53, value: value)
    }
    
    func setArpStepLength_19(value: Int){
        CurrentPatch.arp_stepLength_19 = value
        ArpPatch.arp_stepLength_19 = value
        ViC.send(type: "sysex2", controler: 54, value: value)
    }
    func setArpStepVelocity_19(value: Int){
        CurrentPatch.arp_stepVelocity_19 = value
        ArpPatch.arp_stepVelocity_19 = value
        ViC.send(type: "sysex2", controler: 55, value: value)
    }
    func setArpStep_19(value: Int){
        CurrentPatch.arp_step_19 = value
        ArpPatch.arp_step_19 = value
        ViC.send(type: "sysex2", controler: 56, value: value)
    }
    
    func setArpStepLength_20(value: Int){
        CurrentPatch.arp_stepLength_20 = value
        ArpPatch.arp_stepLength_20 = value
        ViC.send(type: "sysex2", controler: 57, value: value)
    }
    func setArpStepVelocity_20(value: Int){
        CurrentPatch.arp_stepVelocity_20 = value
        ArpPatch.arp_stepVelocity_20 = value
        ViC.send(type: "sysex2", controler: 58, value: value)
    }
    func setArpStep_20(value: Int){
        CurrentPatch.arp_step_20 = value
        ArpPatch.arp_step_20 = value
        ViC.send(type: "sysex2", controler: 59, value: value)
    }
    
    func setArpStepLength_21(value: Int){
        CurrentPatch.arp_stepLength_21 = value
        ArpPatch.arp_stepLength_21 = value
        ViC.send(type: "sysex2", controler: 60, value: value)
    }
    func setArpStepVelocity_21(value: Int){
        CurrentPatch.arp_stepVelocity_21 = value
        ArpPatch.arp_stepVelocity_21 = value
        ViC.send(type: "sysex2", controler: 61, value: value)
    }
    func setArpStep_21(value: Int){
        CurrentPatch.arp_step_21 = value
        ArpPatch.arp_step_21 = value
        ViC.send(type: "sysex2", controler: 62, value: value)
    }
    
    func setArpStepLength_22(value: Int){
        CurrentPatch.arp_stepLength_22 = value
        ArpPatch.arp_stepLength_22 = value
        ViC.send(type: "sysex2", controler: 63, value: value)
    }
    func setArpStepVelocity_22(value: Int){
        CurrentPatch.arp_stepVelocity_22 = value
        ArpPatch.arp_stepVelocity_22 = value
        ViC.send(type: "sysex2", controler: 64, value: value)
    }
    func setArpStep_22(value: Int){
        CurrentPatch.arp_step_22 = value
        ArpPatch.arp_step_22 = value
        ViC.send(type: "sysex2", controler: 65, value: value)
    }
    
    func setArpStepLength_23(value: Int){
        CurrentPatch.arp_stepLength_23 = value
        ArpPatch.arp_stepLength_23 = value
        ViC.send(type: "sysex2", controler: 66, value: value)
    }
    func setArpStepVelocity_23(value: Int){
        CurrentPatch.arp_stepVelocity_23 = value
        ArpPatch.arp_stepVelocity_23 = value
        ViC.send(type: "sysex2", controler: 67, value: value)
    }
    func setArpStep_23(value: Int){
        CurrentPatch.arp_step_23 = value
        ArpPatch.arp_step_23 = value
        ViC.send(type: "sysex2", controler: 68, value: value)
    }
    
    func setArpStepLength_24(value: Int){
        CurrentPatch.arp_stepLength_24 = value
        ArpPatch.arp_stepLength_24 = value
        ViC.send(type: "sysex2", controler: 69, value: value)
    }
    func setArpStepVelocity_24(value: Int){
        CurrentPatch.arp_stepVelocity_24 = value
        ArpPatch.arp_stepVelocity_24 = value
        ViC.send(type: "sysex2", controler: 70, value: value)
    }
    func setArpStep_24(value: Int){
        CurrentPatch.arp_step_24 = value
        ArpPatch.arp_step_24 = value
        ViC.send(type: "sysex2", controler: 71, value: value)
    }
    
    func setArpStepLength_25(value: Int){
        CurrentPatch.arp_stepLength_25 = value
        ArpPatch.arp_stepLength_25 = value
        ViC.send(type: "sysex2", controler: 72, value: value)
    }
    func setArpStepVelocity_25(value: Int){
        CurrentPatch.arp_stepVelocity_25 = value
        ArpPatch.arp_stepVelocity_25 = value
        ViC.send(type: "sysex2", controler: 73, value: value)
    }
    func setArpStep_25(value: Int){
        CurrentPatch.arp_step_25 = value
        ArpPatch.arp_step_25 = value
        ViC.send(type: "sysex2", controler: 74, value: value)
    }
    
    func setArpStepLength_26(value: Int){
        CurrentPatch.arp_stepLength_26 = value
        ArpPatch.arp_stepLength_26 = value
        ViC.send(type: "sysex2", controler: 75, value: value)
    }
    func setArpStepVelocity_26(value: Int){
        CurrentPatch.arp_stepVelocity_26 = value
        ArpPatch.arp_stepVelocity_26 = value
        ViC.send(type: "sysex2", controler: 76, value: value)
    }
    func setArpStep_26(value: Int){
        CurrentPatch.arp_step_26 = value
        ArpPatch.arp_step_26 = value
        ViC.send(type: "sysex2", controler: 77, value: value)
    }
    
    func setArpStepLength_27(value: Int){
        CurrentPatch.arp_stepLength_27 = value
        ArpPatch.arp_stepLength_27 = value
        ViC.send(type: "sysex2", controler: 78, value: value)
    }
    func setArpStepVelocity_27(value: Int){
        CurrentPatch.arp_stepVelocity_27 = value
        ArpPatch.arp_stepVelocity_27 = value
        ViC.send(type: "sysex2", controler: 79, value: value)
    }
    func setArpStep_27(value: Int){
        CurrentPatch.arp_step_27 = value
        ArpPatch.arp_step_27 = value
        ViC.send(type: "sysex2", controler: 80, value: value)
    }
    
    func setArpStepLength_28(value: Int){
        CurrentPatch.arp_stepLength_28 = value
        ArpPatch.arp_stepLength_28 = value
        ViC.send(type: "sysex2", controler: 81, value: value)
    }
    func setArpStepVelocity_28(value: Int){
        CurrentPatch.arp_stepVelocity_28 = value
        ArpPatch.arp_stepVelocity_28 = value
        ViC.send(type: "sysex2", controler: 82, value: value)
    }
    func setArpStep_28(value: Int){
        CurrentPatch.arp_step_28 = value
        ArpPatch.arp_step_28 = value
        ViC.send(type: "sysex2", controler: 83, value: value)
    }
    
    func setArpStepLength_29(value: Int){
        CurrentPatch.arp_stepLength_29 = value
        ArpPatch.arp_stepLength_29 = value
        ViC.send(type: "sysex2", controler: 84, value: value)
    }
    func setArpStepVelocity_29(value: Int){
        CurrentPatch.arp_stepVelocity_29 = value
        ArpPatch.arp_stepVelocity_29 = value
        ViC.send(type: "sysex2", controler: 85, value: value)
    }
    func setArpStep_29(value: Int){
        CurrentPatch.arp_step_29 = value
        ArpPatch.arp_step_29 = value
        ViC.send(type: "sysex2", controler: 86, value: value)
    }
    
    func setArpStepLength_30(value: Int){
        CurrentPatch.arp_stepLength_30 = value
        ArpPatch.arp_stepLength_30 = value
        ViC.send(type: "sysex2", controler: 87, value: value)
    }
    func setArpStepVelocity_30(value: Int){
        CurrentPatch.arp_stepVelocity_30 = value
        ArpPatch.arp_stepVelocity_30 = value
        ViC.send(type: "sysex2", controler: 88, value: value)
    }
    func setArpStep_30(value: Int){
        CurrentPatch.arp_step_30 = value
        ArpPatch.arp_step_30 = value
        ViC.send(type: "sysex2", controler: 89, value: value)
    }
    
    func setArpStepLength_31(value: Int){
        CurrentPatch.arp_stepLength_31 = value
        ArpPatch.arp_stepLength_31 = value
        ViC.send(type: "sysex2", controler: 90, value: value)
    }
    func setArpStepVelocity_31(value: Int){
        CurrentPatch.arp_stepVelocity_31 = value
        ArpPatch.arp_stepVelocity_31 = value
        ViC.send(type: "sysex2", controler: 91, value: value)
    }
    func setArpStep_31(value: Int){
        CurrentPatch.arp_step_31 = value
        ArpPatch.arp_step_31 = value
        ViC.send(type: "sysex2", controler: 92, value: value)
    }
    
    func setArpStepLength_32(value: Int){
        CurrentPatch.arp_stepLength_32 = value
        ArpPatch.arp_stepLength_32 = value
        ViC.send(type: "sysex2", controler: 93, value: value)
    }
    func setArpStepVelocity_32(value: Int){
        CurrentPatch.arp_stepVelocity_32 = value
        ArpPatch.arp_stepVelocity_32 = value
        ViC.send(type: "sysex2", controler: 94, value: value)
    }
    func setArpStep_32(value: Int){
        CurrentPatch.arp_step_32 = value
        ArpPatch.arp_step_32 = value
        ViC.send(type: "sysex2", controler: 95, value: value)
    }
    
    
    // ***********************  EFFECTS SECTION
    // ------------------------------------------------------------------- CHORUS
    func setChorusType(value: Int){
        CurrentPatch.chorus_Type = value
        CharacterPatch.chorus_Type = value
        ViC.send(type: "cc", controler: 103, value: value)
    }
    func setChorusMix(value: Int){
        CurrentPatch.chorus_Mix = value
        CharacterPatch.chorus_Mix = value
        ViC.send(type: "cc", controler: 104, value: value)
    }
    func setChorusMixClassic(value: Int){
        CurrentPatch.chorus_MixClassic = value
        CharacterPatch.chorus_MixClassic = value
        ViC.send(type: "cc", controler: 105, value: value)
    }
    func setChorusLFORate(value: Int){
        CurrentPatch.chorus_LFORate = value
        CharacterPatch.chorus_LFORate = value
        ViC.send(type: "cc", controler: 106, value: value)
    }
    func setChorusLFODepth(value: Int){
        CurrentPatch.chorus_LFODepth = value
        CharacterPatch.chorus_LFODepth = value
        ViC.send(type: "cc", controler: 107, value: value)
    }
    func setChorusDistance(value: Int){
        CurrentPatch.chorus_Distance = value
        CharacterPatch.chorus_Distance = value
        // ViC.send(type: "", controler: , value: value)  ----------------------<missing
    }
    func setChorusDelay(value: Int){
        CurrentPatch.chorus_Delay = value
        CharacterPatch.chorus_Delay = value
        ViC.send(type: "cc", controler: 108, value: value)
    }
    func setChorusAmount(value: Int){
        CurrentPatch.chorus_Amount = value
        CharacterPatch.chorus_Amount = value
        //ViC.send(type: "", controler: , value: value) ----------------------<missing
    }
    func setChorusFeedback(value: Int){
        CurrentPatch.chorus_Feedback = value
        CharacterPatch.chorus_Feedback = value
        ViC.send(type: "cc", controler: 109, value: value)
    }
    func setChorusLowHigh(value: Int){
        CurrentPatch.chorus_LowHigh = value
        CharacterPatch.chorus_LowHigh = value
        // ViC.send(type: "", controler: , value: value)  ----------------------<missing
    }
    func setChorusLFOShape(value: Int){
        CurrentPatch.chorus_LFOShape = value
        CharacterPatch.chorus_LFOShape = value
        ViC.send(type: "cc", controler: 110, value: value)
    }
    func setChorusXOver(value: Int){
        CurrentPatch.chorus_XOver = value
        CharacterPatch.chorus_XOver = value
        ViC.send(type: "cc", controler: 111, value: value)
    }
    
    // ------------------------------------------------------------------- REVERB
    func setReverbMode(value: Int){
        CurrentPatch.reverb_Mode = value
        EffectsPatch.reverb_Mode = value
        ViC.send(type: "sysex", controler: 1, value: value)
    }
    func setReverbSend(value: Int){
        CurrentPatch.reverb_Send = value
        EffectsPatch.reverb_Send = value
        ViC.send(type: "sysex", controler: 2, value: value)
    }
    func setReverbType(value: Int){
        CurrentPatch.reverb_Type = value
        EffectsPatch.reverb_Type = value
        ViC.send(type: "sysex", controler: 3, value: value)
    }
    func setReverbTime(value: Int){
        CurrentPatch.reverb_Time = value
        EffectsPatch.reverb_Time = value
        ViC.send(type: "sysex", controler: 4, value: value)
    }
    func setReverbDamping(value: Int){
        CurrentPatch.reverb_Damping = value
        EffectsPatch.reverb_Damping = value
        ViC.send(type: "sysex", controler: 5, value: value)
    }
    func setReverbColor(value: Int){
        CurrentPatch.reverb_Color = value
        EffectsPatch.reverb_Color = value
        ViC.send(type: "sysex", controler: 6, value: value)
    }
    func setReverbPredelay(value: Int){
        CurrentPatch.reverb_Predelay = value
        EffectsPatch.reverb_Predelay = value
        ViC.send(type: "sysex", controler: 7, value: value)
    }
    func setReverbClock(value: Int){
        CurrentPatch.reverb_Clock = value
        EffectsPatch.reverb_Clock = value
        ViC.send(type: "sysex", controler: 8, value: value)
    }
    func setReverbFeedback(value: Int){
        CurrentPatch.reverb_Feedback = value
        EffectsPatch.reverb_Feedback = value
        ViC.send(type: "sysex", controler: 9, value: value)
    }
    
    // ------------------------------------------------------------------- DELAY
    func setDelayMode(value: Int){
        CurrentPatch.delay_Mode = value
        EffectsPatch.delay_Mode = value
        ViC.send(type: "cc", controler: 112, value: value)
    }
    func setDelaySend(value: Int){
        CurrentPatch.delay_Send = value
        EffectsPatch.delay_Send = value
        ViC.send(type: "cc", controler: 113, value: value)
    }
    func setDelayTime(value: Int){
        CurrentPatch.delay_Time = value
        EffectsPatch.delay_Time = value
        ViC.send(type: "cc", controler: 114, value: value)
    }
    func setDelayClock(value: Int){
        CurrentPatch.delay_Clock = value
        EffectsPatch.delay_Clock = value
        ViC.send(type: "pp", controler: 20, value: value)
    }
    func setDelayFeedback(value: Int){
        CurrentPatch.delay_Feedback = value
        EffectsPatch.delay_Feedback = value
        ViC.send(type: "cc", controler: 115, value: value)
    }
    func setDelayLFORate(value: Int){
        CurrentPatch.delay_LFORate = value
        EffectsPatch.delay_LFORate = value
        ViC.send(type: "cc", controler: 116, value: value)
    }
    func setDelayLFODepth(value: Int){
        CurrentPatch.delay_LFODepth = value
        EffectsPatch.delay_LFODepth = value
        ViC.send(type: "cc", controler: 117, value: value)
    }
    func setDelayLFOShape(value: Int){
        CurrentPatch.delay_LFOShape = value
        EffectsPatch.delay_LFOShape = value
        ViC.send(type: "cc", controler: 118, value: value)
    }
    func setDelayColor(value: Int){
        CurrentPatch.delay_Color = value
        EffectsPatch.delay_Color = value
        ViC.send(type: "cc", controler: 119, value: value)
    }
    func setDelayType(value: Int){
        CurrentPatch.delay_Type = value
        EffectsPatch.delay_Type = value
        ViC.send(type: "sysex", controler: 10, value: value)
    }
    func setDelayTapeDelayRatio(value: Int){
        CurrentPatch.delay_TapeDelayRatio = value
        EffectsPatch.delay_TapeDelayRatio = value
        ViC.send(type: "sysex", controler: 12, value: value)
    }
    func setDelayTapeDelayClockLeft(value: Int){
        CurrentPatch.delay_TapeDelayClockLeft = value
        EffectsPatch.delay_TapeDelayClockLeft = value
        ViC.send(type: "sysex", controler: 13, value: value)
    }
    func setDelayTapeDelayClockRight(value: Int){
        CurrentPatch.delay_TapeDelayClockRight = value
        EffectsPatch.delay_TapeDelayClockRight = value
        ViC.send(type: "sysex", controler: 14, value: value)
    }
    func setDelayTapeDelayBandwidth(value: Int){
        CurrentPatch.delay_TapeDelayBandwidth = value
        EffectsPatch.delay_TapeDelayBandwidth = value
        ViC.send(type: "sysex", controler: 17, value: value)
    }
    
    
    // ------------------------------------------------------------------- PHASER
    func setPhaserStages(value: Int){
        CurrentPatch.phaser_Stages = value
        CharacterPatch.phaser_Stages = value
        ViC.send(type: "pp", controler: 84, value: value)
    }
    func setPhaserMix(value: Int){
        CurrentPatch.phaser_Mix = value
        CharacterPatch.phaser_Mix = value
        ViC.send(type: "pp", controler: 85, value: value)
    }
    func setPhaserLFORate(value: Int){
        CurrentPatch.phaser_LFORate = value
        CharacterPatch.phaser_LFORate = value
        ViC.send(type: "pp", controler: 86, value: value)
    }
    func setPhaserDepth(value: Int){
        CurrentPatch.phaser_Depth = value
        CharacterPatch.phaser_Depth = value
        ViC.send(type: "pp", controler: 87, value: value)
    }
    func setPhaserFrequency(value: Int){
        CurrentPatch.phaser_Frequency = value
        CharacterPatch.phaser_Frequency = value
        ViC.send(type: "pp", controler: 88, value: value)
    }
    func setPhaserFeedback(value: Int){
        CurrentPatch.phaser_Feedback = value
        CharacterPatch.phaser_Feedback = value
        ViC.send(type: "pp", controler: 89, value: value)
    }
    func setPhaserSpread(value: Int){
        CurrentPatch.phaser_Spread = value
        CharacterPatch.phaser_Spread = value
        ViC.send(type: "pp", controler: 90, value: value)
    }
    
    // ------------------------------------------------------------------- DISTORTION
    func setDistortionType(value: Int){
        CurrentPatch.distortion_Type = value
        CharacterPatch.distortion_Type = value
        ViC.send(type: "pp", controler: 100, value: value)
    }
    func setDistortionIntensity(value: Int){
        CurrentPatch.distortion_Intensity = value
        CharacterPatch.distortion_Intensity = value
        ViC.send(type: "pp", controler: 101, value: value)
    }
    func setDistortionTrebleBooster(value: Int){
        CurrentPatch.distortion_TrebleBooster = value
        CharacterPatch.distortion_TrebleBooster = value
        ViC.send(type: "sysex", controler: 70, value: value)
    }
    func setDistortionHighCut(value: Int){
        CurrentPatch.distortion_HighCut = value
        CharacterPatch.distortion_HighCut = value
        ViC.send(type: "sysex", controler: 71, value: value)
    }
    func setDistortionMix(value: Int){
        CurrentPatch.distortion_Mix = value
        CharacterPatch.distortion_Mix = value
        ViC.send(type: "sysex", controler: 72, value: value)
    }
    func setDistortionQuality(value: Int){
        CurrentPatch.distortion_Quality = value
        CharacterPatch.distortion_Quality = value
        ViC.send(type: "sysex", controler: 73, value: value)
    }
    func setDistortionToneW(value: Int){
        CurrentPatch.distortion_ToneW = value
        CharacterPatch.distortion_ToneW = value
        ViC.send(type: "sysex", controler: 74, value: value)
    }
    
    func setInputMode(value: Int){
        CurrentPatch.input_Mode = value
        CharacterPatch.input_Mode = value
        ViC.send(type: "sysex2", controler: 124, value: value)
    }
    
    func setInputSelect(value: Int){
        CurrentPatch.input_Select = value
        CharacterPatch.input_Select = value
        //ViC.send(type: "pp", controler: 38, value: value)
        ViC.send(type: "sysex2", controler: 125, value: value)
    }
    
    func setAtomizer(value: Int){
         CurrentPatch.Atomizer = value
         CharacterPatch.Atomizer = value
         ViC.send(type: "sysex2", controler: 126, value: value)
    }
    
    func setVocoderMode(value: Int){
        CurrentPatch.VocoderMode = value
        FilterPatch.VocoderMode = value
        ViC.send(type: "pp", controler: 39, value: value)
    }
    
    // ------------------------------------------------------------------- CHARACTER EFFECTS
    // ***********************  CHARACTER SUBPATCH SECTION
    
    func setCharacterType (value: Int){
        CurrentPatch.character_Type = value
        CharacterPatch.character_Type = value
        ViC.send(type: "sysex", controler: 26, value: value)
    }
    func setCharacterIntensity (value: Int){
        CurrentPatch.character_Intensity = value
        CharacterPatch.character_Intensity = value
        ViC.send(type: "pp", controler: 97, value: value)
    }
    func setCharacterTune (value: Int){
        CurrentPatch.character_Tune = value
        CharacterPatch.character_Tune = value
        ViC.send(type: "pp", controler: 98, value: value)
    }
    // ------------------------------------------------------------------- EQ
    func setEQLowFrequency (value: Int){
        CurrentPatch.eq_LowFrequency = value
        CharacterPatch.eq_LowFrequency = value
        ViC.send(type: "pp", controler: 45, value: value)
    }
    func setEQLowGain (value: Int){
        CurrentPatch.eq_LowGain = value
        CharacterPatch.eq_LowGain = value
        ViC.send(type: "pp", controler: 95, value: value)
        //ViC.send(type: "pp", controler: 46, value: value)
    }
    func setEQMidGain (value: Int){
        CurrentPatch.eq_MidGain = value
        CharacterPatch.eq_MidGain = value
        ViC.send(type: "pp", controler: 92, value: value)
    }
    func setEQMidFrequency (value: Int){
        CurrentPatch.eq_MidFrequency = value
        CharacterPatch.eq_MidFrequency = value
        ViC.send(type: "pp", controler: 93, value: value)
    }
    func setEQMidQFactor (value: Int){
        CurrentPatch.eq_MidQFactor = value
        CharacterPatch.eq_MidQFactor = value
        ViC.send(type: "pp", controler: 94, value: value)
    }
    func setEQHighFrequency (value: Int){
        CurrentPatch.eq_HighFrequency = value
        CharacterPatch.eq_HighFrequency = value
        ViC.send(type: "pp", controler: 46, value: value)
        //ViC.send(type: "pp", controler: 95, value: value)
    }
    func setEQHighGain (value: Int){
        CurrentPatch.eq_HighGain = value
        CharacterPatch.eq_HighGain = value
        ViC.send(type: "pp", controler: 96, value: value)
    }
    func setFrequencyShifterType (value: Int){
        CurrentPatch.frequencyShifter_Type = value
        CharacterPatch.frequencyShifter_Type = value
        ViC.send(type: "sysex", controler: 19, value: value)
    }
    func setFrequencyShifterMix (value: Int){
        CurrentPatch.frequencyShifter_Mix = value
        CharacterPatch.frequencyShifter_Mix = value
        ViC.send(type: "sysex", controler: 20, value: value)
    }
    func setFrequencyShifterFrequency (value: Int){
        CurrentPatch.frequencyShifter_Frequency = value
        CharacterPatch.frequencyShifter_Frequency = value
        ViC.send(type: "sysex", controler: 21, value: value)
    }
    func setFrequencyShifterStereoPhase (value: Int){
        CurrentPatch.frequencyShifter_StereoPhase = value
        CharacterPatch.frequencyShifter_StereoPhase = value
        ViC.send(type: "sysex", controler: 22, value: value)
    }
    
    
    func setFrequencyShifterLeftShape (value: Int){
        CurrentPatch.frequencyShifter_LeftShape = value
        CharacterPatch.frequencyShifter_LeftShape = value
        
        self.sendSysex23()
    }
    func setFrequencyShifterFilterType (value: Int){
        CurrentPatch.frequencyShifter_FilterType = value
        CharacterPatch.frequencyShifter_FilterType = value
        
        self.sendSysex23()
    }
    func setFrequencyShifterPoles (value: Int){
        CurrentPatch.frequencyShifter_Poles = value
        CharacterPatch.frequencyShifter_Poles = value
        
        self.sendSysex23()
    }
    func sendSysex23(){
        switch (CurrentPatch.frequencyShifter_Type){
        case 0,1,2,3,4:
            ViC.send(type: "sysex", controler: 23, value: CurrentPatch.frequencyShifter_LeftShape)
        case 5,6,7,8:
            ViC.send(type: "sysex", controler: 23, value: CurrentPatch.frequencyShifter_FilterType)
        case 9,10,11:
            ViC.send(type: "sysex", controler: 23, value: CurrentPatch.frequencyShifter_Poles)
        default:
            break
        }
    }
    
    
    func setFrequencyShifterRightShape (value: Int){
        CurrentPatch.frequencyShifter_RightShape = value
        CharacterPatch.frequencyShifter_RightShape = value
        
        self.sendSysex24()
    }
    func setFrequencyShifterSlope (value: Int){
        CurrentPatch.frequencyShifter_Slope = value
        CharacterPatch.frequencyShifter_Slope = value
        
        self.sendSysex24()
    }
    func sendSysex24(){
        switch (CurrentPatch.frequencyShifter_Type){
        case 0,1,2,3,4,5,6,7,8:
            ViC.send(type: "sysex", controler: 24, value: CurrentPatch.frequencyShifter_RightShape)
        case 9,10,11:
            ViC.send(type: "sysex", controler: 24, value: CurrentPatch.frequencyShifter_Slope )
        default:
            break
        }
    }
    
    
    func setFrequencyShifterResonance (value: Int){
        CurrentPatch.frequencyShifter_Resonance = value
        CharacterPatch.frequencyShifter_Resonance = value
        ViC.send(type: "sysex", controler: 25, value: value)
    }
    
    
    
    // ------------------------------------------------------------------- CONTROLS
    // ***********************  CONTROLS SUBPATCH SECTION
    func setVelocityOsc1WaveformShape (value: Int){
        CurrentPatch.velocity_Osc1WaveformShape = value
        ControlPatch.velocity_Osc1WaveformShape = value
        ViC.send(type: "pp", controler: 47, value: value)
    }
    func setVelocityOsc2WaveformShape (value: Int){
        CurrentPatch.velocity_Osc2WaveformShape = value
        ControlPatch.velocity_Osc2WaveformShape = value
        ViC.send(type: "pp", controler: 48, value: value)
    }
    func setVelocityOscPulsewidth (value: Int){
        CurrentPatch.velocity_OscPulsewidth = value
        ControlPatch.velocity_OscPulsewidth = value
        ViC.send(type: "pp", controler: 49, value: value)
    }
    func setVelocityOscFMAmount (value: Int){
        CurrentPatch.velocity_OscFMAmount = value
        ControlPatch.velocity_OscFMAmount = value
        ViC.send(type: "pp", controler: 50, value: value)
    }
    func setVelocityVolume (value: Int){
        CurrentPatch.velocity_Volume = value
        ControlPatch.velocity_Volume = value
        ViC.send(type: "pp", controler: 60, value: value)
    }
    func setVelocityPanorama (value: Int){
        CurrentPatch.velocity_Panorama = value
        ControlPatch.velocity_Panorama = value
        ViC.send(type: "pp", controler: 61, value: value)
    }
    func setVelocityFilter1EnvAmount (value: Int){
        CurrentPatch.velocity_Filter1EnvAmount = value
        ControlPatch.velocity_Filter1EnvAmount = value
        ViC.send(type: "pp", controler: 54, value: value)
    }
    func setVelocityFilter1Resonance (value: Int){
        CurrentPatch.velocity_Filter1Resonance = value
        ControlPatch.velocity_Filter1Resonance = value
        ViC.send(type: "pp", controler: 56, value: value)
    }
    func setVelocityFilter2EnvAmount (value: Int){
        CurrentPatch.velocity_Filter2EnvAmount = value
        ControlPatch.velocity_Filter2EnvAmount = value
        ViC.send(type: "pp", controler: 55, value: value)
    }
    func setVelocityFilter2Resonance (value: Int){
        CurrentPatch.velocity_Filter2Resonance = value
        ControlPatch.velocity_Filter2Resonance = value
        ViC.send(type: "pp", controler: 57, value: value)
    }
    
    func setSoftknob1Destination (value: Int){
        CurrentPatch.control_Softknob1Destination = value
        ControlPatch.control_Softknob1Destination = value
        ViC.send(type: "pp", controler: 62, value: value)
    }
    func setSoftknob1Name (value: Int){
        CurrentPatch.control_Softknob1Name = value
        ControlPatch.control_Softknob1Name = value
        ViC.send(type: "pp", controler: 51, value: value)
    }
    func setSoftknob2Destination (value: Int){
        CurrentPatch.control_Softknob2Destination = value
        ControlPatch.control_Softknob2Destination = value
        ViC.send(type: "pp", controler: 63, value: value)
    }
    func setSoftknob2Name (value: Int){
        CurrentPatch.control_Softknob2Name = value
        ControlPatch.control_Softknob2Name = value
        ViC.send(type: "pp", controler: 52, value: value)
    }
    func setSoftknob3Destination (value: Int){
        CurrentPatch.control_Softknob3Destination = value
        ControlPatch.control_Softknob3Destination = value
        ViC.send(type: "sysex", controler: 28, value: value)
    }
    func setSoftknob3Name (value: Int){
        CurrentPatch.control_Softknob3Name = value
        ControlPatch.control_Softknob3Name = value
        ViC.send(type: "pp", controler: 53, value: value)
    }
    
    func setBenderDown (value: Int){
        CurrentPatch.control_BenderDown = value
        ControlPatch.control_BenderDown = value
        ViC.send(type: "pp", controler: 27, value: value)
    }
    func setBenderUp (value: Int){
        CurrentPatch.control_BenderUp = value
        ControlPatch.control_BenderUp = value
        ViC.send(type: "pp", controler: 26, value: value)
    }
    func setBenderScale (value: Int){
        CurrentPatch.control_BenderScale = value
        ControlPatch.control_BenderScale = value
        ViC.send(type: "pp", controler: 28, value: value)
    }
    
    func setParameterSmoothMode (value: Int){
        CurrentPatch.control_parameterSmoothMode = value
        ControlPatch.control_parameterSmoothMode = value
        ViC.send(type: "pp", controler: 25, value: value)
    }
    func setKeyboardMode (value: Int){
        CurrentPatch.control_KeyboardMode = value
        ControlPatch.control_KeyboardMode = value
        ViC.send(type: "cc", controler: 94, value: value)
    }
    
    /*   OVERIGEN */
    func setCategory1 (value: Int){
        CurrentPatch.category1 = value
        let VirusValue = presetCategoriesArray.lastIndex(of: presetCategoriesUI[value])

        ViC.send(type: "pp", controler: 123, value: VirusValue ?? 0)
    }
    
    func setCategory2 (value: Int){
        CurrentPatch.category2 = value
        let VirusValue = presetCategoriesArray.lastIndex(of: presetCategoriesUI[value])
        ViC.send(type: "pp", controler: 124, value: VirusValue ?? 0)
    }
    
    
}

func getCharacterID(_ str: String) -> Int {
    switch (str){
    case " ":
        return 32
    case "!":
        return 33
    case "\"":
        return 34
    case "#":
        return 35
    case "$":
        return 36
    case "%":
        return 37
    case "&":
        return 38
    case "'":
        return 39 // of 96
    case "(":
        return 40
    case ")":
        return 41
    case "*":
        return 42
    case "+":
        return 43
    case ",":
        return 44
    case "-":
        return 45
    case ".":
        return 46
    case "/":
        return 47
    case "0":
        return 48
    case "1":
        return 49
    case "2":
        return 50
    case "3":
        return 51
    case "4":
        return 52
    case "5":
        return 53
    case "6":
        return 54
    case "7":
        return 55
    case "8":
        return 56
    case "9":
        return 57
    case ":":
        return 58
    case ";":
        return 59
    case "<":
        return 60
    case "=":
        return 61
    case ">":
        return 62
    case "?":
         return 63
    case "@":
        return 64
    case "A":
        return 65
    case "B":
        return 66
    case "C":
        return 67
    case "D":
        return 68
    case "E":
        return 69
    case "F":
        return 70
    case "G":
        return 71
    case "H":
        return 72
    case "I":
        return 73
    case "J":
        return 74
    case "K":
        return 75
    case "L":
        return 76
    case "M":
        return 77
    case "N":
        return 78
    case "O":
        return 79
    case "P":
        return 80
    case "Q":
        return 81
    case "R":
        return 82
    case "S":
        return 83
    case "T":
        return 84
    case "U":
        return 85
    case "V":
        return 86
    case "W":
        return 87
    case "X":
        return 88
    case "Y":
        return 89
    case "Z":
        return 90
    case "[":
        return 91
    case "\\":
        return 92
    case "]":
        return 93
    case "^":
        return 94
    case "_":
        return 95
    case "a":
        return 97
    case "b":
        return 98
    case "c":
        return 99
    case "d":
        return 100
    case "e":
        return 101
    case "f":
        return 102
    case "g":
        return 103
    case "h":
        return 104
    case "i":
        return 105
    case "j":
        return 106
    case "k":
        return 107
    case "l":
        return 108
    case "m":
        return 109
    case "n":
        return 110
    case "o":
        return 111
    case "p":
        return 112
    case "q":
        return 113
    case "r":
        return 114
    case "s":
        return 115
    case "t":
        return 116
    case "u":
        return 117
    case "v":
        return 118
    case "w":
        return 119
    case "x":
        return 120
    case "y":
        return 121
    case "z":
        return 122
    case "{":
        return 123
    case "|":
        return 124
    case "}":
        return 125
    case "~":
        return 126
    default:
        return 42
    }
}

func getCharacter(_ nr: Int) -> String {
    switch (nr){
    case 32:
        return " "
    case 33:
        return "!"
    case 34:
        return "\""
    case 35:
        return "#"
    case 36:
        return "$"
    case 37:
        return "%"
    case 38:
        return "&"
    case 39:
        return "'"
    case 40:
        return "("
    case 41:
        return ")"
    case 42:
        return "*"
    case 43:
        return "+"
    case 44:
        return ","
    case 45:
        return "-"
    case 46:
        return "."
    case 47:
        return "/"
    case 48:
        return "0"
    case 49:
        return "1"
    case 50:
        return "2"
    case 51:
        return "3"
    case 52:
        return "4"
    case 53:
        return "5"
    case 54:
        return "6"
    case 55:
        return "7"
    case 56:
        return "8"
    case 57:
        return "9"
    case 58:
        return ":"
    case 59:
        return ";"
    case 60:
        return "<"
    case 61:
        return "="
    case 62:
        return ">"
    case 63:
        return "?"
    case 64:
        return "@"
    case 65:
        return "A"
    case 66:
        return "B"
    case 67:
        return "C"
    case 68:
        return "D"
    case 69:
        return "E"
    case 70:
        return "F"
    case 71:
        return "G"
    case 72:
        return "H"
    case 73:
        return "I"
    case 74:
        return "J"
    case 75:
        return "K"
    case 76:
        return "L"
    case 77:
        return "M"
    case 78:
        return "N"
    case 79:
        return "O"
    case 80:
        return "P"
    case 81:
        return "Q"
    case 82:
        return "R"
    case 83:
        return "S"
    case 84:
        return "T"
    case 85:
        return "U"
    case 86:
        return "V"
    case 87:
        return "W"
    case 88:
        return "X"
    case 89:
        return "Y"
    case 90:
        return "Z"
    case 91:
        return "["
    case 92:
        return "\\"
    case 93:
        return "]"
    case 94:
        return "^"
    case 95:
        return "_"
    case 96:
        return "'"
    case 97:
        return "a"
    case 98:
        return "b"
    case 99:
        return "c"
    case 100:
        return "d"
    case 101:
        return "e"
    case 102:
        return "f"
    case 103:
        return "g"
    case 104:
        return "h"
    case 105:
        return "i"
    case 106:
        return "j"
    case 107:
        return "k"
    case 108:
        return "l"
    case 109:
        return "m"
    case 110:
        return "n"
    case 111:
        return "o"
    case 112:
        return "p"
    case 113:
        return "q"
    case 114:
        return "r"
    case 115:
        return "s"
    case 116:
        return "t"
    case 117:
        return "u"
    case 118:
        return "v"
    case 119:
        return "w"
    case 120:
        return "x"
    case 121:
        return "y"
    case 122:
        return "z"
    case 123:
        return "{"
    case 124:
        return "|"
    case 125:
        return "}"
    case 126:
        return "~"
    case 127:
        return "<"
    default:
        return"?"
    }
}

