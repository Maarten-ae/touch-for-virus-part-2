//
//  MidiListener.swift
//  Rage MacOS
//
//  Created by M. Lierop on 08-10-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import CoreMIDI

extension MainAppViewController: MIDIListener {
    
    func receivedMIDIAftertouch(noteNumber: MIDINoteNumber, pressure: MIDIByte, channel: MIDIChannel, portID: MIDIUniqueID?, offset: MIDITimeStamp) {
        print("receivedMIDIAftertouch 1")
    }
    
    func receivedMIDIAftertouch(_ pressure: MIDIByte, channel: MIDIChannel, portID: MIDIUniqueID?, offset: MIDITimeStamp) {
        print("receivedMIDIAftertouch 2")
    }
    
    
    func receivedMIDIPitchWheel(_ pitchWheelValue: MIDIWord, channel: MIDIChannel, portID: MIDIUniqueID?, offset: MIDITimeStamp) {
        print("receivedMIDIPitchWheel")
    }
    
    func receivedMIDIProgramChange(_ program: MIDIByte, channel: MIDIChannel, portID: MIDIUniqueID?, offset: MIDITimeStamp) {
        print("receivedMIDIProgramChange")
    }
    
    func receivedMIDIPropertyChange(propertyChangeInfo: MIDIObjectPropertyChangeNotification) {
        print("receivedMIDIPropertyChange")
    }
    
    func receivedMIDINotification(notification: MIDINotification) {
        print("receivedMIDINotification")
    }
    
    
    // MIDI through
    func receivedMIDINoteOn(noteNumber: MIDINoteNumber, velocity: MIDIVelocity, channel: MIDIChannel, portID: MIDIUniqueID? = nil, offset: MIDITimeStamp = 0) {
        //midi.sendNoteOnMessage(noteNumber: noteNumber, velocity: velocity, channel: channel)
        print("receivedMIDINoteOn")
      }
    
    func receivedMIDINoteOff(noteNumber: MIDINoteNumber, velocity: MIDIVelocity, channel: MIDIChannel, portID: MIDIUniqueID? = nil, offset: MIDITimeStamp = 0) {
        //midi.sendNoteOffMessage(noteNumber: noteNumber, velocity: velocity, channel: channel)
        print("receivedMIDINoteOff")
    }
    
    public func receivedMIDIPitchWheel(_ pitchWheelValue: MIDIWord, channel: MIDIChannel) {
        //midi.sendPitchBendMessage(value: pitchWheelValue, channel: channel)
        print("receivedMIDIPitchWheel")
    }
    
    func receivedMIDIController(_ controller: MIDIByte, value: MIDIByte, channel: MIDIChannel,
                                portID: MIDIUniqueID? = nil, offset: MIDITimeStamp = 0) {
        // CC controlers
        print("Channel: \(channel + 1) controller: \(controller) value: \(value) ","  portID:",portID as Any)
        
        DispatchQueue.main.async {
            self.MIDIHandlr.SetPage1Parameter(Parameter: Int(controller), Value: Int(value))
        }

        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name( "UpdateView" ), object: nil)
        }
        
        if ( portID == 201977 ){
            // if message was delivered via MIDI IN for MIDI THRU, send it to the midi out
            midi.sendControllerMessage(controller, value: value)
        }
    }
    
//    func receivedMIDIAftertouch(noteNumber: MIDINoteNumber,
//                                pressure: MIDIByte,
//                                channel: MIDIChannel,
//                                portID: MIDIUniqueID? = nil,
//                                offset: MIDITimeStamp = 0) {
//
//        // PP controlers
//        print("Channel: \(channel + 1) midiAftertouchOnNote: \(noteNumber) pressure: \(pressure) ")
//
//        DispatchQueue.main.async {
//            self.MIDIHandlr.SetPage2Parameter(Parameter: Int(noteNumber), Value: Int(pressure))
//        }
//
//        DispatchQueue.main.async {
//            NotificationCenter.default.post(name: Notification.Name( "UpdateView" ), object: nil)
//        }
//
//    }
    
//    public func receivedMIDIProgramChange(_ program: MIDIByte, channel: MIDIChannel) {
//       print("pgrogram change")
//    }
  
  

    
    func receivedMIDISystemCommand(_ data: [MIDIByte], portID: MIDIUniqueID? = nil, offset: MIDITimeStamp = 1) {
               
       // Sysex controlers
       print("Audiokit SYSEX RECEIVED:: ",data.count)
        print("Audiokit SYSEX RECEIVED:: ",data)
        print("Audiokit SYSEX RECEIVED:: END")
        if ( portID != 201977 ) {
                    
            print("Audiokit SYSEX RECEIVED:: portID: ",portID as Any)
            var Msg:[Int] = []
        
            if (data.count == 11) {
                
                Msg.append( Int(data[0]) ) // start sysex
                Msg.append( Int(data[1]) )
                Msg.append( Int(data[2]) )
                Msg.append( Int(data[3]) )
                Msg.append( Int(data[4]) )
                Msg.append( Int(data[5]) )  // Device ID
                Msg.append( Int(data[6]) )  // parameter page
                Msg.append( Int(data[7]) )  // Part number
                Msg.append( Int(data[8]) )  // controller nr
                Msg.append( Int(data[9]) )  // controller value
                Msg.append( Int(data[10]) ) // end sysex
                
                print("CC MSG: ",Msg)
                // if mididevice id does not match, or set to global, show error
                if ( ViC.MIDIDeviceId != Msg[5] && ViC.MIDIDeviceId != 16 ){
                    print("wrong device id")
                    var styling = ToastStyle()
                    styling.messageColor = .red
                    let message:String = "Ignoring incomming SYSEX. Device ID mismatch."
                    DispatchQueue.main.async {
                        self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
                    }
                } else {
                    
                    DispatchQueue.main.async {
                        if ( Int(Msg[6]) == 110 ) {
                            self.MIDIHandlr.SetPage3Parameter(Parameter: Msg[8], Value: Msg[9])
                        } else if ( Msg[6] == 111 ) {
                            self.MIDIHandlr.SetPage4Parameter(Parameter: Msg[8], Value: Msg[9])
                        } else if ( Msg[6] == 112 ) {
                            self.MIDIHandlr.SetPage1Parameter(Parameter: Msg[8], Value: Msg[9])
                        } else if ( Msg[6] == 113 ) {
                            self.MIDIHandlr.SetPage2Parameter(Parameter: Msg[8], Value: Msg[9])
                        } else {
                            print("unknown page: ",Msg[6])
                        }
                        NotificationCenter.default.post(name: Notification.Name( "UpdateView" ), object: nil)
                    }
                }
        
            
        } else if (data.count == 770) {
            // OUDE op foutieve aanlevering gebasseerde parameter dump via SYSEX
            
            
            Msg.append( Int(data[0]) ) // start sysex
            Msg.append( Int(data[1]) )
            Msg.append( Int(data[2]) )
            Msg.append( Int(data[256]) )
            Msg.append( Int(data[257]) )
            Msg.append( Int(data[258]) )  // Device ID
            Msg.append( Int(data[512]) )  // parameter page
            Msg.append( Int(data[513]) )  // Part number
            Msg.append( Int(data[514]) )  // controller nr
            Msg.append( Int(data[768]) )  // controller value
            Msg.append( Int(data[769]) ) // end sysex
            
            print("CC MSG: ",Msg)
            
            if (Msg[5] != ViC.MIDIDeviceId ){
                print("wrong device id")
                var styling = ToastStyle()
                styling.messageColor = .red
                let message:String = "Ignoring incomming SYSEX. Device ID mismatch."
                DispatchQueue.main.async {
                    self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
                }
            } else {
                
                DispatchQueue.main.async {
                    if ( Int(Msg[6]) == 110 ) {
                        self.MIDIHandlr.SetPage3Parameter(Parameter: Msg[8], Value: Msg[9])
                    } else if ( Msg[6] == 111 ) {
                        self.MIDIHandlr.SetPage4Parameter(Parameter: Msg[8], Value: Msg[9])
                    } else if ( Msg[6] == 112 ) {
                        self.MIDIHandlr.SetPage1Parameter(Parameter: Msg[8], Value: Msg[9])
                    } else if ( Msg[6] == 113 ) {
                        self.MIDIHandlr.SetPage2Parameter(Parameter: Msg[8], Value: Msg[9])
                    } else {
                        print("unknown page: ",Msg[6])
                    }
                    NotificationCenter.default.post(name: Notification.Name( "UpdateView" ), object: nil)
                }
            }
            
            
            } else if (data.count == 524 ) {
              // Dit is de enige echte klassieke patchdump

                DispatchQueue.main.async {
                    
                    var styling = ToastStyle()
                    styling.messageColor = .green
                    self.view.makeToast("Receiving Patch", duration: 3.0, position: .top, style: styling)
                    
                    self.MIDIHandlr.parseMIDIdump(data: data)
                    
                    NotificationCenter.default.post(name: Notification.Name( "UpdateView" ), object: nil)
                }
                
            } else {
                
                print("Error: Received Message length: ",data.count)
                print(data)
                
                var styling = ToastStyle()
                styling.messageColor = .red
                let message:String = "Received SYSEX dump is incomplete (" + String(data.count) + ")"
                DispatchQueue.main.async {
                    self.view.makeToast(message, duration: 3.0, position: .top, style: styling)
                }
            }
        }
    }
    
    func receivedMIDISetupChange(){
        print("wijziging in midisetup")
        NotificationCenter.default.post(name: Notification.Name( "MIDISetupChanged" ), object: nil)

        var styling = ToastStyle()
        styling.messageColor = .green
        self.view.makeToast("MIDI Setup changed", duration: 3.0, position: .top, style: styling)
        
        //midi.createVirtualInputPort(98536, name: "Touch For Virus")
        //midi.openInput()
        ViC.resetVirus()
    }

}
