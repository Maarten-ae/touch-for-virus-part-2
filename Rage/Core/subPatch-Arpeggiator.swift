//
//  subPatch-Arpeggiator.swift
//  Rage-2
//
//  Created by M. Lierop on 21-09-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift


class subPatchArp: Object {
    
    // General
    @objc dynamic var name: String = "RAGE INT"
    @objc dynamic var id = 0
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: String = ""
    @objc dynamic var category2: String = ""
    @objc dynamic var category3: String = ""
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    
    @objc dynamic var arp_pattern: Int = 0
    @objc dynamic var arp_patternLength: Int = 1
    @objc dynamic var arp_range: Int = 0
    @objc dynamic var arp_holdMode: Int = 0
    @objc dynamic var arp_noteLength: Int = 64
    @objc dynamic var arp_swingFactor: Int = 0
    @objc dynamic var arp_mode: Int = 0
    @objc dynamic var arp_tempo: Int = 57
    @objc dynamic var arp_clock: Int = 6
    
    @objc dynamic var arp_step_1: Int = 1
    @objc dynamic var arp_stepLength_1: Int = 90
    @objc dynamic var arp_stepVelocity_1: Int = 120
    
    @objc dynamic var arp_step_2: Int = 1
    @objc dynamic var arp_stepLength_2: Int = 90
    @objc dynamic var arp_stepVelocity_2: Int = 120
    
    @objc dynamic var arp_step_3: Int = 1
    @objc dynamic var arp_stepLength_3: Int = 90
    @objc dynamic var arp_stepVelocity_3: Int = 120
    
    @objc dynamic var arp_step_4: Int = 1
    @objc dynamic var arp_stepLength_4: Int = 90
    @objc dynamic var arp_stepVelocity_4: Int = 120
    
    @objc dynamic var arp_step_5: Int = 1
    @objc dynamic var arp_stepLength_5: Int = 90
    @objc dynamic var arp_stepVelocity_5: Int = 120
    
    @objc dynamic var arp_step_6: Int = 1
    @objc dynamic var arp_stepLength_6: Int = 90
    @objc dynamic var arp_stepVelocity_6: Int = 120
    
    @objc dynamic var arp_step_7: Int = 1
    @objc dynamic var arp_stepLength_7: Int = 90
    @objc dynamic var arp_stepVelocity_7: Int = 120
    
    @objc dynamic var arp_step_8: Int = 1
    @objc dynamic var arp_stepLength_8: Int = 90
    @objc dynamic var arp_stepVelocity_8: Int = 120
    
    @objc dynamic var arp_step_9: Int = 1
    @objc dynamic var arp_stepLength_9: Int = 90
    @objc dynamic var arp_stepVelocity_9: Int = 120
    
    
    @objc dynamic var arp_step_10: Int = 1
    @objc dynamic var arp_stepLength_10: Int = 90
    @objc dynamic var arp_stepVelocity_10: Int = 120
    
    @objc dynamic var arp_step_11: Int = 1
    @objc dynamic var arp_stepLength_11: Int = 90
    @objc dynamic var arp_stepVelocity_11: Int = 120
    
    @objc dynamic var arp_step_12: Int = 1
    @objc dynamic var arp_stepLength_12: Int = 90
    @objc dynamic var arp_stepVelocity_12: Int = 120
    
    @objc dynamic var arp_step_13: Int = 1
    @objc dynamic var arp_stepLength_13: Int = 90
    @objc dynamic var arp_stepVelocity_13: Int = 120
    
    @objc dynamic var arp_step_14: Int = 1
    @objc dynamic var arp_stepLength_14: Int = 90
    @objc dynamic var arp_stepVelocity_14: Int = 120
    
    @objc dynamic var arp_step_15: Int = 1
    @objc dynamic var arp_stepLength_15: Int = 90
    @objc dynamic var arp_stepVelocity_15: Int = 120
    
    @objc dynamic var arp_step_16: Int = 1
    @objc dynamic var arp_stepLength_16: Int = 90
    @objc dynamic var arp_stepVelocity_16: Int = 120
    
    @objc dynamic var arp_step_17: Int = 1
    @objc dynamic var arp_stepLength_17: Int = 90
    @objc dynamic var arp_stepVelocity_17: Int = 120
    
    @objc dynamic var arp_step_18: Int = 1
    @objc dynamic var arp_stepLength_18: Int = 90
    @objc dynamic var arp_stepVelocity_18: Int = 120
    
    @objc dynamic var arp_step_19: Int = 1
    @objc dynamic var arp_stepLength_19: Int = 90
    @objc dynamic var arp_stepVelocity_19: Int = 120
    
    @objc dynamic var arp_step_20: Int = 1
    @objc dynamic var arp_stepLength_20: Int = 90
    @objc dynamic var arp_stepVelocity_20: Int = 120
    
    @objc dynamic var arp_step_21: Int = 1
    @objc dynamic var arp_stepLength_21: Int = 90
    @objc dynamic var arp_stepVelocity_21: Int = 120
    
    @objc dynamic var arp_step_22: Int = 1
    @objc dynamic var arp_stepLength_22: Int = 90
    @objc dynamic var arp_stepVelocity_22: Int = 120
    
    @objc dynamic var arp_step_23: Int = 1
    @objc dynamic var arp_stepLength_23: Int = 90
    @objc dynamic var arp_stepVelocity_23: Int = 120
    
    @objc dynamic var arp_step_24: Int = 1
    @objc dynamic var arp_stepLength_24: Int = 90
    @objc dynamic var arp_stepVelocity_24: Int = 120
    
    @objc dynamic var arp_step_25: Int = 1
    @objc dynamic var arp_stepLength_25: Int = 90
    @objc dynamic var arp_stepVelocity_25: Int = 120
    
    @objc dynamic var arp_step_26: Int = 1
    @objc dynamic var arp_stepLength_26: Int = 90
    @objc dynamic var arp_stepVelocity_26: Int = 120
    
    @objc dynamic var arp_step_27: Int = 1
    @objc dynamic var arp_stepLength_27: Int = 90
    @objc dynamic var arp_stepVelocity_27: Int = 120
    
    @objc dynamic var arp_step_28: Int = 1
    @objc dynamic var arp_stepLength_28: Int = 90
    @objc dynamic var arp_stepVelocity_28: Int = 120
    
    @objc dynamic var arp_step_29: Int = 1
    @objc dynamic var arp_stepLength_29: Int = 90
    @objc dynamic var arp_stepVelocity_29: Int = 120
    
    @objc dynamic var arp_step_30: Int = 1
    @objc dynamic var arp_stepLength_30: Int = 90
    @objc dynamic var arp_stepVelocity_30: Int = 120

    @objc dynamic var arp_step_31: Int = 1
    @objc dynamic var arp_stepLength_31: Int = 90
    @objc dynamic var arp_stepVelocity_31: Int = 120
    
    @objc dynamic var arp_step_32: Int = 1
    @objc dynamic var arp_stepLength_32: Int = 90
    @objc dynamic var arp_stepVelocity_32: Int = 120
    
    /*
    pp2 Arpeggiator/Pattern
    3 Arpeggiator Range In Octaves
    4 Arpeggiator Hold Mode
    5 Arpeggiator Note Length
    6 Arpeggiator Swing Factor
    15 Arpeggiator/Mode
    pp16 Tempo (Disabled When Used With Virus Control)
    pp 17 Arpeggiator Clock
    
    sysex
    127 Arpeggiator Pattern Length
 */
}
