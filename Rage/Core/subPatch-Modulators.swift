//
//  subPatch-Modulators.swift
//  Rage-2
//
//  Created by M. Lierop on 21-09-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class subPatchMod: Object {
    
    // General
    @objc dynamic var name: String = "RAGE INT"
    @objc dynamic var id = 0
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: String = ""
    @objc dynamic var category2: String = ""
    @objc dynamic var category3: String = ""
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    
    @objc dynamic var envelope3_Attack:Int = 0
    @objc dynamic var envelope3_Decay:Int = 127
    @objc dynamic var envelope3_Sustain:Int = 0
    @objc dynamic var envelope3_SustainSlope:Int = 64
    @objc dynamic var envelope3_Release:Int = 20
    
    @objc dynamic var envelope4_Attack:Int = 0
    @objc dynamic var envelope4_Decay:Int = 127
    @objc dynamic var envelope4_Sustain:Int = 0
    @objc dynamic var envelope4_SustainSlope:Int = 64
    @objc dynamic var envelope4_Release:Int = 20
    

    @objc dynamic var lfo1_Mode:Int = 0
    @objc dynamic var lfo1_Rate:Int = 0
    @objc dynamic var lfo1_WaveformShape:Int = 0
    @objc dynamic var lfo1_EnvelopeMode:Int = 0
    @objc dynamic var lfo1_WaveformContour:Int = 0
    @objc dynamic var lfo1_Keyfollow:Int = 96
    @objc dynamic var lfo1_TriggerPhase:Int = 0
    @objc dynamic var lfo1_Clock:Int = 0
    @objc dynamic var lfo1_UserDestination:Int = 0
    @objc dynamic var lfo1_UserDestinationAmount:Int = 0
    
    @objc dynamic var lfo1_Osc1:Int = 64
    @objc dynamic var lfo1_Osc2:Int = 64
    @objc dynamic var lfo1_Pulsewidth:Int = 64
    @objc dynamic var lfo1_FilterResonance12:Int = 64
    @objc dynamic var lfo1_FilterEnvelopeGain:Int = 64
    
    
    @objc dynamic var lfo2_Mode:Int = 0
    @objc dynamic var lfo2_Rate:Int = 0
    @objc dynamic var lfo2_WaveformShape:Int = 0
    @objc dynamic var lfo2_EnvelopeMode:Int = 0
    @objc dynamic var lfo2_WaveformContour:Int = 0
    @objc dynamic var lfo2_Keyfollow:Int = 96
    @objc dynamic var lfo2_TriggerPhase:Int = 0
    @objc dynamic var lfo2_Clock:Int = 0
    @objc dynamic var lfo2_UserDestination:Int = 0
    @objc dynamic var lfo2_UserDestinationAmount:Int = 0
    
    @objc dynamic var lfo2_Shape12:Int = 64
    @objc dynamic var lfo2_FMAmount:Int = 64
    @objc dynamic var lfo2_Cutoff12:Int = 64
    @objc dynamic var lfo2_Cutoff2:Int = 64
    @objc dynamic var lfo2_Panorama:Int = 64
    
    
    @objc dynamic var lfo3_Rate:Int = 0
    @objc dynamic var lfo3_WaveformShape:Int = 0
    @objc dynamic var lfo3_Mode:Int = 0
    @objc dynamic var lfo3_Keyfollow:Int = 96
    @objc dynamic var lfo3_UserDestination:Int = 0
    @objc dynamic var lfo3_UserDestinationAmount:Int = 0
    @objc dynamic var lfo3_FadeInTime:Int = 0
    @objc dynamic var lfo3_Clock:Int = 0
    
    
    
    @objc dynamic var  matrixSlot1_Source:Int = 0
    @objc dynamic var  matrixSlot1_Destination1:Int = 0
    @objc dynamic var  matrixSlot1_Amount1:Int = 64
    @objc dynamic var  matrixSlot1_Destination2:Int = 0
    @objc dynamic var  matrixSlot1_Amount2:Int = 64
    @objc dynamic var  matrixSlot1_Destination3:Int = 0
    @objc dynamic var  matrixSlot1_Amount3:Int = 64
    
    @objc dynamic var  matrixSlot2_Source:Int = 0
    @objc dynamic var  matrixSlot2_Destination1:Int = 0
    @objc dynamic var  matrixSlot2_Amount1:Int = 64
    @objc dynamic var  matrixSlot2_Destination2:Int = 0
    @objc dynamic var  matrixSlot2_Amount2:Int = 64
    @objc dynamic var  matrixSlot2_Destination3:Int = 0
    @objc dynamic var  matrixSlot2_Amount3:Int = 64
    
    @objc dynamic var  matrixSlot3_Source:Int = 0
    @objc dynamic var  matrixSlot3_Destination1:Int = 0
    @objc dynamic var  matrixSlot3_Amount1:Int = 64
    @objc dynamic var  matrixSlot3_Destination2:Int = 0
    @objc dynamic var  matrixSlot3_Amount2:Int = 64
    @objc dynamic var  matrixSlot3_Destination3:Int = 0
    @objc dynamic var  matrixSlot3_Amount3:Int = 64
    
    @objc dynamic var  matrixSlot4_Source:Int = 0
    @objc dynamic var  matrixSlot4_Destination1:Int = 0
    @objc dynamic var  matrixSlot4_Amount1:Int = 64
    @objc dynamic var  matrixSlot4_Destination2:Int = 0
    @objc dynamic var  matrixSlot4_Amount2:Int = 64
    @objc dynamic var  matrixSlot4_Destination3:Int = 0
    @objc dynamic var  matrixSlot4_Amount3:Int = 64
    
    @objc dynamic var  matrixSlot5_Source:Int = 38
    @objc dynamic var  matrixSlot5_Destination1:Int = 0
    @objc dynamic var  matrixSlot5_Amount1:Int = 64
    @objc dynamic var  matrixSlot5_Destination2:Int = 0
    @objc dynamic var  matrixSlot5_Amount2:Int = 64
    @objc dynamic var  matrixSlot5_Destination3:Int = 0
    @objc dynamic var  matrixSlot5_Amount3:Int = 64
    
    @objc dynamic var  matrixSlot6_Source:Int = 39
    @objc dynamic var  matrixSlot6_Destination1:Int = 0
    @objc dynamic var  matrixSlot6_Amount1:Int = 64
    @objc dynamic var  matrixSlot6_Destination2:Int = 0
    @objc dynamic var  matrixSlot6_Amount2:Int = 64
    @objc dynamic var  matrixSlot6_Destination3:Int = 0
    @objc dynamic var  matrixSlot6_Amount3:Int = 64
    
}



/*
 cc
 59 Amplifier Envelope/Attack
 61 Amplifier Envelope/Sustain
 62 Amplifier Envelope/Sustain Slope
 63 Amplifier Envelope/Release
 
 67 LFO 1/Rate
 68 LFO 1/Waveform Shape
 69 LFO 1 Envelope Mode
 70 LFO 1 Mode
 71 LFO 1/Waveform Contour
 72 LFO 1 Keyfollow
 73 LFO 1 Trigger Phase
 74 LFO 1 --> Osc 1+2
 75 LFO1-->Osc2
 76 LFO 1 --> Pulsewidth
 77 LFO 1 -->Filter Resonance 1+2
 78 LFO 1 --> Filter Envelope Gain
 79 LFO 2/Rate
 80 LFO 2/Waveform Shape
 81 LFO 2/Envelope Mode
 82 LFO 2 Mode
 83 LFO 2 Waveform Contour
 84 LFO 2/Keyfollow
 85 LFO 2/Trigger Phase
 86 LFO 2 --> Shape 1+2
 87 LFO 2 --> FM Amount
 88 LFO 2 --> Cutoff 1+2
 89 LFO 2 --> Cutoff 2
 90 LFO 2 --> Panorama
 
 
 pp
 7 LFO 3/Rate
 8 LFO 3/Waveform Shape
 9 LFO 3 Mode
 10 LFO 3 Keyfollow
 11 LFO 3 User Destination
 12 LFO 3 User Destination Amount
 13 LFO 3/Fade In Time
 
 18 LFO 1/Clock
 19 LFO 2/Clock
 
 21 LFO 3/Clock
 64 Mod Matrix Slot 1/Source
 65 Mod Matrix Slot 1/Destination 1
 66 Mod Matrix Slot 1/Amount 1
 67 Mod Matrix Slot 2/Source
 68 Mod Matrix Slot 2/Destination 1
 69 Mod Matrix Slot 2/Amount 1
 70 Mod Matrix Slot 2/Destination 2
 71 Mod Matrix Slot 2/Amount 2
 72 Mod Matrix Slot 3/Source
 73 Mod Matrix Slot 3/Destination 1
 74 Mod Matrix Slot 3/Amount 1
 75 Mod Matrix Slot 3/Destination 2
 76 Mod Matrix Slot 3/Amount 2
 77 Mod Matrix Slot 3/Destination 3
 78 Mod Matrix Slot 3/Amount 3
 79 LFO 1 User Destination
 80 LFO 1 User Destination Amount
 81 LFO 2 User Destination
 82 LFO 2 User Destination Amount
 
 103 Mod Matrix Slot 4/Source
 104 Mod Matrix Slot 4/Destination 1
 105 Assign Slot 4/Amount 1
 106 Mod Matrix Slot 5/Source
 107 Mod Matrix Slot 5/Destination 1
 108 Mod Matrix Slot 5/Amount 1
 109 Mod Matrix Slot 6/Source
 110 Mod Matrix Slot 6/Destination 1
 111 Mod Matrix Slot 6/Amount 1
 
 
 sysex
 79 Envelope 3/Attack
 80 Envelope 3/Decay
 81 Envelope 3/Sustain
 82 Envelope 3/Sustain Slope
 83 Envelope 3/Release
 84 Envelope 4/Attack
 85 Envelope 4/Decay
 86 Envelope 4/Sustain
 87 Envelope 4/Sustain Slope
 88 Envelope 4/Release
 89 Mod Matrix Slot 1/Destination 2
 90 Mod Matrix Slot 1/Amount 2
 91 Mod Matrix Slot 1/Destination 3
 92 Mod Matrix Slot 1/Amount 3
 93 Mod Matrix Slot 2/Destination 3
 94 Mod Matrix Slot 2/Amount 3
 95 Mod Matrix Slot 4/Destination 2
 96 Assign Slot 4/Amount 2
 97 Mod Matrix Slot 4/Destination 3
 98 Mod Matrix Slot 4/Amount 3
 99 Mod Matrix Slot 5/Destination 2
 100 Mod Matrix Slot 5/Amount 2
 101 Mod Matrix Slot 5/Destination 3
 102 Mod Matrix Slot 5/Amount 3
 103 Mod Matrix Slot 6/Destination 2
 104 Mod Matrix Slot 6/Amount 2
 105 Mod Matrix Slot 6/Destination 3
 106 Mod Matrix Slot 6/Amount 3
 */
