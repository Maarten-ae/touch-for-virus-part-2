//
//  Enums_Effects.swift
//  Rage
//
//  Created by M. Lierop on 23-02-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation

let FilterBankTypes: [String] = ["Off","Ring Modulator","Frequency shifter","Vowel Filter","Comb Filter","1 Pole XFade","2 Pole XFade","4 Pole XFade","6 Pole XFade","Low Pass VariSlope","Hi Pass VariSlope","By Pass VariSlope"]

let CharacterTypes: [String] = ["Analog Boost","Vintage 1","Vintage 2","Vintage 3","Pad Opener","Lead Enhancer","Bass Enhancer","Stereo Widener","Speaker Cabinet"]

let ReverbModes: [String] = ["Off","Reverb","Feedback 1","Feedback 2"]
let ReverbTypes: [String] = ["Ambience","Small Room","Large Room","Hall"]
let ReverbClock: [String] = ["Off","1/64","1/32","1/16","1/8","1/4","1/2","3/64","3/32","3/16","3/8","1/24","1/12","1/6","1/3","2/3","3/4"]

let DelayTypes: [String] = ["Classic Delay","Tape Clocked","Tape Free","Tape Doppler"]
let DelayModes: [String] = ["Off","Simple Delay","Ping Pong 2:1","Ping Pong 4:3","Ping Pong 4:1","Ping Pong 8:7","Pattern 1+1","Pattern 2+1","Pattern 3+1","Pattern 4+1","Pattern 5+1","Pattern 2+3","Pattern 2+5","Pattern 3+2","Pattern 3+3","Pattern 3+4","Pattern 3+5","Pattern 4+3","Pattern 4+5","Pattern 5+2","Pattern 5+3","Pattern 5+4","Pattern 5+5"]
let DelayClock: [String] = ["Off","1/64","1/32","1/16","1/8","1/4","1/2","3/64","3/32","3/16","3/8","1/24","1/12","1/6","1/3","2/3","3/4"]
let DelayLFOWave: [String] = ["Sine","Triangle","Sawtooth","Square","S&H","S&G"]

let ChorusTypes: [String] = ["Off","Classic","Vintage","Hyper Chorus","Air Chorus","Vibrato","Rotary Speaker"]

let DistortionTypes: [String] = ["Off","Light","Soft","Medium","Hard","Digital","Wave Shaper","Rectifier","Bit Reducer Old","Rate Reducer Old","Low Pass","High Pass","Wide","Soft Bounce","Hard Bounce","Sine Fold","Triangle Fold","Sawtooth Fold","Rate Reducer","Bit Reducer","Mint Overdrive","Curry Overdrive","Saffron Overdrive","Onion Overdrive","Pepper Overdrive","Chili Overdrive"]
