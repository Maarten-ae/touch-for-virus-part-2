//
//  subPatch-Control.swift
//  Rage-2
//
//  Created by M. Lierop on 22-09-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class subPatchControl: Object {
    
    // General
    @objc dynamic var name: String = "RAGE INT"
    @objc dynamic var id = 0
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: String = ""
    @objc dynamic var category2: String = ""
    @objc dynamic var category3: String = ""
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    
    @objc dynamic var velocity_Osc1WaveformShape: Int = 64
    @objc dynamic var velocity_Osc2WaveformShape: Int = 64
    @objc dynamic var velocity_OscPulsewidth: Int = 64
    @objc dynamic var velocity_OscFMAmount: Int = 64
    @objc dynamic var velocity_Volume: Int = 127
    @objc dynamic var velocity_Panorama: Int = 64
    @objc dynamic var velocity_Filter1EnvAmount: Int = 64
    @objc dynamic var velocity_Filter1Resonance: Int = 64
    @objc dynamic var velocity_Filter2EnvAmount: Int = 64
    @objc dynamic var velocity_Filter2Resonance: Int = 64
    @objc dynamic var control_Softknob1Destination: Int = 0
    @objc dynamic var control_Softknob1Name: Int = 0
    @objc dynamic var control_Softknob2Destination: Int = 0
    @objc dynamic var control_Softknob2Name: Int = 0
    @objc dynamic var control_Softknob3Destination: Int = 0
    @objc dynamic var control_Softknob3Name: Int = 0
    @objc dynamic var control_BenderDown: Int = 64
    @objc dynamic var control_BenderUp: Int = 64
    @objc dynamic var control_BenderScale: Int = 0
    @objc dynamic var control_parameterSmoothMode: Int = 1
    @objc dynamic var control_KeyboardMode: Int = 0
}

/*
pp

47 Velocity --> Osc1 Waveform Shape
48 Velocity --> Osc2 Waveform Shape
49 Velocity --> Pulsewidth
50 Velocity --> FM Amount
60 Velocity --> Volume
61 Velocity --> Panorama

 ook pitchbend meuk, aftertouch etc?
 
 */
