//
//  Enums_Arpeggiator.swift
//  Rage
//
//  Created by M. Lierop on 19-02-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation


let ArpModeLabels: [String] = ["Off","Up","Down","Up&Down","As Played","Random","Chord","Arp>Matrix"]
let ArpOctaveRangeLabels: [String] = ["1 Octave","2 Octaves","3 Octaves","4 Octaves"]
let ArpPAtternLengthLabels: [String] = ["8 steps","16 steps","14 steps","32 steps"]
let ArpClockLabels: [String] = ["illegal","1/128","1/64","1/32","1/16","1/8","1/4","3/128","3/64","3/32","3/16","1/48","1/24","1/12","1/6","1/3","3/8","1/2"]
let ArpPatternLabels: [String] = ["User","Pattern 2","Pattern 3","Pattern 4","Pattern 5","Pattern 6","Pattern 7","Pattern 8","Pattern 9","Pattern 10","Pattern 11","Pattern 12","Pattern 13","Pattern 14","Pattern 15","Pattern 16","Pattern 17","Pattern 18","Pattern 19","Pattern 20","Pattern 21","Pattern 22","Pattern 23","Pattern 24","Pattern 25","Pattern 26","Pattern 27","Pattern 28","Pattern 29","Pattern 30","Pattern 31","Pattern 32","Pattern 33","Pattern 34","Pattern 35","Pattern 36","Pattern 37","Pattern 38","Pattern 39","Pattern 40","Pattern 41","Pattern 42","Pattern 43","Pattern 44","Pattern 45","Pattern 46","Pattern 47","Pattern 48","Pattern 49","Pattern 50","Pattern 51","Pattern 52","Pattern 53","Pattern 54","Pattern 55","Pattern 56","Pattern 57","Pattern 58","Pattern 59","Pattern 60","Pattern 61","Pattern 62","Pattern 63","Pattern 64"]
