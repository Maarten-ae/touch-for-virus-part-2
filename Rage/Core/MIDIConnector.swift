//
//  connector.swift
//  RAGE
//
//  Created by M. Lierop on 20-07-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit



class VirusConnect {
    var MIDIPart: MIDIByte = MIDIByte(1)
    var partNumber: MIDIByte = MIDIByte(64)
    var MIDIDeviceId: MIDIByte = MIDIByte(16)
    var MIDITargetOutput: String = ""
    
    init (ID: Int){
        setMode(mode: "single");
       
        //midi.createVirtualOutputPort(name: "RAGE Out")
        midi.openOutput()
        
        self.requestPatch()
    }
    
     func send (type: String, controler: Int, value: Int){
        let controlerByte: MIDIByte = MIDIByte(controler)
        let valueByte: MIDIByte = MIDIByte(value)
        
        //print ("VirusConnector.send for type: ",type," / control: ",controlerByte," / value",valueByte," / Channel",MIDIPart)
        //print ("VirusConnector.send ,",type," ,",controlerByte," ,",valueByte)
        switch type {
        
        case "cc":
            // continuous controler change
            //midi.sendEvent(AKMIDIEvent(controllerChange: controlerByte, value: valueByte, channel: 01))
            let sysexmessage: [MIDIByte] = [MIDIByte(240), MIDIByte(00), MIDIByte(32), MIDIByte(51), MIDIByte(01), MIDIDeviceId, MIDIByte(112), partNumber, controlerByte, valueByte, MIDIByte(247)]
            midi.sendMessage(sysexmessage)
           // print ("VirusConnector.send ,A ,",controlerByte," ,",valueByte)
        case "pp":
            // polypressure
            //midi.sendEvent(AKMIDIEvent(polyphonicAftertouch: controlerByte, value: valueByte, channel: 01))
            let sysexmessage: [MIDIByte] = [MIDIByte(240), MIDIByte(00), MIDIByte(32), MIDIByte(51), MIDIByte(01), MIDIDeviceId, MIDIByte(113), partNumber, controlerByte, valueByte, MIDIByte(247)]
            midi.sendMessage(sysexmessage)
            //print ("VirusConnector.send ,B ,",controlerByte," ,",valueByte)
        case "sysex":
            // system exclusive message
            let sysexmessage: [MIDIByte] = [MIDIByte(240), MIDIByte(00), MIDIByte(32), MIDIByte(51), MIDIByte(01), MIDIDeviceId, MIDIByte(110), partNumber, controlerByte, valueByte, MIDIByte(247)]
            midi.sendMessage(sysexmessage)
            //print ("VirusConnector.send ,C ,",controlerByte," ,",valueByte)
        case "sysex2":
            // system exclusive message
            let sysexmessage: [MIDIByte] = [MIDIByte(240), MIDIByte(00), MIDIByte(32), MIDIByte(51), MIDIByte(01), MIDIDeviceId, MIDIByte(111), partNumber, controlerByte, valueByte, MIDIByte(247)]
            midi.sendMessage(sysexmessage)
            //print ("VirusConnector.send ,D ,",controlerByte," ,",valueByte)
        default:
            print ("unknown midi message type: ", type);
        }
    
    }
    
    func setTargetDevice (ID: Int) {
        switch ID {
        case 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15:
            MIDIDeviceId = MIDIByte(ID)
        default:
            MIDIDeviceId = MIDIByte(16)
        }
        resetVirus()
    }
    
    func setPartNumber (nr: Int){
        if (nr >= 0 && nr <= 16){
            partNumber = MIDIByte(nr)
        } else {
            partNumber = MIDIByte(64)
        }
    }
    
    func setMode (mode: String) {
    
        switch mode {
            case "single":
                // switch to single mode: F0 00 20 33 01 dd 72 00 7A 00 F7
                let sysexmessage: [MIDIByte] = [MIDIByte(240), MIDIByte(00), MIDIByte(32), MIDIByte(51), MIDIByte(01), MIDIByte(16), MIDIByte(110), MIDIByte(64), MIDIByte(122), MIDIByte(00), MIDIByte(247)]
                midi.sendMessage(sysexmessage)
            case "multi":
                // switch to single mode: F0 00 20 33 01 dd 72 00 7A 02 F7
                let sysexmessage: [MIDIByte] = [MIDIByte(240), MIDIByte(00), MIDIByte(32), MIDIByte(51), MIDIByte(01), MIDIByte(16), MIDIByte(110), MIDIByte(64), MIDIByte(122), MIDIByte(02), MIDIByte(247)]
                midi.sendMessage(sysexmessage)
            default:
                // set to single mode
                let sysexmessage: [MIDIByte] = [MIDIByte(240), MIDIByte(00), MIDIByte(32), MIDIByte(51), MIDIByte(01), MIDIByte(16), MIDIByte(110), MIDIByte(64), MIDIByte(122), MIDIByte(00), MIDIByte(247)]
                midi.sendMessage(sysexmessage)
        }
    }
    
    public func requestPatch(){
        let BankNR: MIDIByte = MIDIByte(0)
       // bank nr 0 selectes the currently selected patch
        print("vic patch request")
        let sysexmessage: [MIDIByte] = [MIDIByte(240),
                                        MIDIByte(00),
                                        MIDIByte(32),
                                        MIDIByte(51),
                                        MIDIByte(01),
                                        MIDIByte(MIDIDeviceId),
                                        MIDIByte(48),
                                        MIDIByte(BankNR),
                                        MIDIByte(partNumber),
                                        MIDIByte(247)]
        midi.sendMessage(sysexmessage)
    }
    
    /// Send a pitch bend message.
    ///
    /// - Parameters:
    ///   - value: Value of pitch shifting between 0 and 16383. Send 8192 for no pitch bending.
    ///   - channel: Channel you want to send pitch bend message. Defaults 0.
    public func sendPitchBendMessage(value: UInt16, channel: MIDIChannel = 0) {
        let pitchCommand = MIDIByte(0xE0) + channel
        let mask: UInt16 = 0x007F
        let byte1 = MIDIByte(value & mask) // MSB, bit shift right 7
        let byte2 = MIDIByte((value & (mask << 7)) >> 7) // LSB, mask of 127
        let message: [MIDIByte] = [pitchCommand, byte1, byte2]
        midi.sendMessage(message)
    }
    
    
    func setMIDIChannel(channel: Int){
        switch channel {
        case 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16:
            MIDIPart = MIDIByte(channel)
        default:
            MIDIPart = MIDIByte(0)
        }
        resetVirus()
    }
    
    func reconnectOutput(OutputId: String){
        
        //midi.closeAllInputs()
        midi.clearEndpoints()
        midi.openOutput()
        
       // for inputname in midi.inputNames {
       //     print(inputname)
       //     midi.openInput(name: inputname)
       // }
        
        resetVirus()
    }
    
    func resetVirus(){
        //print("virus reset")
        setMode(mode: "single");
        patchCtrl.reset()
    }
}
