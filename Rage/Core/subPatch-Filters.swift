//
//  subPatch-Filters.swift
//  Rage-2
//
//  Created by M. Lierop on 21-09-17.
//  Copyright © 2017 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import RealmSwift

class subPatchFilters: Object {
    
    // General
    @objc dynamic var name: String = "RAGE INT"
    @objc dynamic var id = 0
    @objc dynamic var authorName: String = ""
    @objc dynamic var type: String = "" // preset / custom
    @objc dynamic var category1: String = ""
    @objc dynamic var category2: String = ""
    @objc dynamic var category3: String = ""
    @objc dynamic var collectionID: Int = 0
    @objc dynamic var collectionName : String = ""
    
    @objc dynamic var filter1_Mode: Int = 2
    @objc dynamic var filter1_Cutoff: Int = 64
    @objc dynamic var filter1_Resonance: Int = 0
    @objc dynamic var filter1_EnvelopePolarity: Int = 0
    @objc dynamic var filter1_EnvelopeAmount: Int = 0
    @objc dynamic var filter1_KeyFollow: Int = 96
    
    @objc dynamic var filter2_Mode: Int = 2
    @objc dynamic var filter2_Cutoff: Int = 64
    @objc dynamic var filter2_Resonance: Int = 0
    @objc dynamic var filter2_EnvelopePolarity: Int = 0
    @objc dynamic var filter2_EnvelopeAmount: Int = 0
    @objc dynamic var filter2_KeyFollow: Int = 96
    
    @objc dynamic var filter_Balance: Int = 64
    @objc dynamic var filter_saturationType: Int = 0
    @objc dynamic var filter_volumeToSaturation: Int = 64  // BUITEN GEBRUIK
    @objc dynamic var filter_routing: Int = 0
    @objc dynamic var filter_Link: Int = 0
    @objc dynamic var filter_cutoffLink: Int = 0
    @objc dynamic var filter_keyfollowBase: Int = 96
    
    @objc dynamic var amplifierEnvelope_Attack:Int = 0
    @objc dynamic var amplifierEnvelope_Decay:Int = 127
    @objc dynamic var amplifierEnvelope_Sustain:Int = 0
    @objc dynamic var amplifierEnvelope_SustainSlope:Int = 64
    @objc dynamic var amplifierEnvelope_Release:Int = 20
    
    @objc dynamic var filterEnvelope_Attack:Int = 0
    @objc dynamic var filterEnvelope_Decay:Int = 127
    @objc dynamic var filterEnvelope_Sustain:Int = 0
    @objc dynamic var filterEnvelope_SustainSlope:Int = 64
    @objc dynamic var filterEnvelope_Release:Int = 20
    
    @objc dynamic var filterInputFollower_Input: Int = 0
    @objc dynamic var VocoderMode: Int = 0
    
    @objc dynamic var amp_PatchVolume: Int = 127
}
