//
//  Enums_Filters.swift
//  Rage
//
//  Created by M. Lierop on 09-02-18.
//  Copyright © 2018 M. Lierop. All rights reserved.
//

import Foundation

let FilterSaturationModes: [String] = [
    "Off",
    "Light",
    "Soft",
    "Middle",
    "Hard",
    "Digital",
    "Wave shaper",
    "Rectifier",
    "Bit Reducer",
    "Rate Reducer",
    "Rate+Follow",
    "Low Pass",
    "Low+Follow",
    "High Pass",
    "High+Follow"]

