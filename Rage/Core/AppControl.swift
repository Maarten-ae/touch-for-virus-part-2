//
//  AppControl.swift
//  Rage
//
//  Created by M. Lierop on 21/08/2019.
//  Copyright © 2019 M. Lierop. All rights reserved.
//

import Foundation
import AudioKit
import CloudKit

class AppSettings {
    
    let UserDefs = UserDefaults.standard
    
    /* ------------- Author (audit) */
    dynamic var AuthorName: String = "Über Patcher"
    
    public func setAuthorName(name: String = "Über Patcher"){
        if (name != ""){
            self.UserDefs.set(name, forKey: "AuthorName")
        } else {
            self.UserDefs.set("anonymous", forKey: "AuthorName")
        }
    }
    
    public func getAuthorName() -> String {
        let name = String( self.UserDefs.string(forKey: "AuthorName") ?? "" )
        print("get author:",name)
        if (name != ""){
            return String(name)
        } else {
            return String("anonymous")
        }
    }
    
    
     /* ------------- MIDI CHANNEL (audit) */
    dynamic var AuditMidiChannel:Int = 0
    
    public func setAuditMIDIChannel(channel: Int){
        print("set channel to ",channel)
        if (channel >= 0 && channel <= 16){
            self.UserDefs.set(channel, forKey: "AuditMidiChannel")
        } else {
            self.UserDefs.set(0, forKey: "AuditMidiChannel")
        }
    }
    
    public func getAuditMIDIChannel() -> MIDIChannel {
        let channel = self.UserDefs.integer(forKey: "AuditMidiChannel")
        print("GET channel ",channel)
        if (channel >= 0 && channel <= 16){
            return MIDIChannel(channel)
        } else {
            return MIDIChannel(0)
        }
    }
    
    
    /* ------------- MIDI Output (audit) */
    dynamic var MidiOutput:String = ""
    
    public func setMIDIOutput(Output: String){
        self.UserDefs.set(Output, forKey: "MidiOutput")
    }
    
    public func getMIDIOutput() -> String {
        guard let output = self.UserDefs.string(forKey: "MidiOutput") else { return "" }
        return output
    }
    
    var cloudPatches: Array<CKRecord> = []
    var FilteredCloudPatches: Array<CKRecord> = []
    var distinctCloudCollections = [String]()
    var reloadCloudPatches: Bool = true
    
    
    /* ------------- CATEGORY most recently used in filter */
    private var SelectedFilterCategory: Int = 0
    
    public func setFilterCategory(cat: Int){
        SelectedFilterCategory = cat
    }
    
    public func getFilterCategory() -> Int {
        return SelectedFilterCategory
    }
    
    
    /* ------------- COLLECTION last used in filter for local patches */
    private var SelectedFilterCollection: String = ""
    
    public func setFilterCollection(cat: String){
        SelectedFilterCollection = cat
        UserDefs.set(cat, forKey: "MostRecentPatchCollectionName")
    }
    
    public func getFilterCollection() -> String {
        return SelectedFilterCollection
    }
    
    
    /* ------------- COLLECTION last used in filter for cloud patches*/
    private var SelectedFilterCloudCollection: String = ""
    
    public func setFilterCloudCollection(cat: String){
        SelectedFilterCloudCollection = cat
        UserDefs.set(cat, forKey: "MostRecentPatchCloudCollectionName")
    }
    
    public func getFilterCloudCollection() -> String {
        return SelectedFilterCloudCollection
    }
    
    
    /* ------------- SEARCH String last used in filter for patches*/
    private var SearchFilterString: String = ""
    
    public func setSearchFilterString(srchString: String){
        SearchFilterString = srchString
        UserDefs.set(srchString, forKey: "MostRecentSearchFilterString")
    }
    
    public func getSearchFilterString() -> String {
        return SearchFilterString
    }
    
    
    /* ------------- Disable or Enable MIDI through behavior */
    public func SetMIDIThru (Enable: Bool){
        if (Enable){
            UserDefs.set(true, forKey: "EnableMIDIThru")
        } else {
            UserDefs.set(false, forKey: "EnableMIDIThru")
        }
    }
    
    public func GetMIDIThru() -> Bool {
        return self.UserDefs.bool(forKey: "EnableMIDIThru")
    }
    
    
    /* ------------- Disable or Enable MIDI through behavior */
    
    public enum virusModes {
        case single
        case multi
    }
    public func SetVirusMode (mode: virusModes){
        UserDefs.set(mode, forKey: "VirusMode")
    }
    
    public func GetVirusMode() -> Bool {
        return self.UserDefs.bool(forKey: "VirusMode")
    }
}
